package com.fangia.savy.esy.pro.nativeModules.checkGpsState;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;

import android.content.Intent;
import android.location.LocationManager;

public class CheckGpsStateModule extends ReactContextBaseJavaModule {
    LocationManager locationManager;

    public CheckGpsStateModule(ReactApplicationContext reactContext) {
        super(reactContext);
        locationManager = (LocationManager) reactContext.getSystemService(reactContext.LOCATION_SERVICE);
    }

    @Override
    public String getName() {
        return "CheckGpsState";
    }

    /**
     * Go to GPS setting
     */
    @ReactMethod
    public void openGpsSetting() {
        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        getCurrentActivity().startActivityForResult(callGPSSettingIntent, 0);
    }

    /**
     * Check if Gps is on or not. Return boolean
     * 
     * @param promise
     */
    @ReactMethod
    public void isGpsOn(Promise promise) {
        boolean GPS_PROVIDER = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean NETWORK_PROVIDER = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        promise.resolve(GPS_PROVIDER || NETWORK_PROVIDER);
    }
}