package com.fangia.savy.esy.pro;

import android.app.Application;
import android.content.Context;
import com.facebook.react.PackageList;

import com.facebook.react.ReactApplication;
import com.rnfs.RNFSPackage;
import com.pilloxa.dfu.RNNordicDfuPackage;
import org.wonday.orientation.OrientationPackage;
import com.masteratul.RNAppstoreVersionCheckerPackage;
import cl.json.RNSharePackage;
import cl.json.ShareApplication;
import com.christopherdro.htmltopdf.RNHTMLtoPDFPackage;
import com.microsoft.codepush.react.CodePush;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.airbnb.android.react.lottie.LottiePackage;

// Firebase
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.auth.RNFirebaseAuthPackage;
import io.invertase.firebase.analytics.RNFirebaseAnalyticsPackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import io.invertase.firebase.fabric.crashlytics.RNFirebaseCrashlyticsPackage;

import com.learnium.RNDeviceInfo.RNDeviceInfo;
import it.innove.BleManagerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

// My native module
import com.fangia.savy.esy.pro.nativeModules.checkGpsState.CheckGpsStatePackage; // Check GPS state module

public class MainApplication extends Application implements ReactApplication, ShareApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

    @Override
    protected String getJSBundleFile() {
      return CodePush.getJSBundleFile();
    }

    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      @SuppressWarnings("UnnecessaryLocalVariable")
      List<ReactPackage> packages = new PackageList(this).getPackages();
      // Packages that cannot be autolinked yet can be added manually here, for
      // example:
      // packages.add(new MyReactNativePackage());
      packages.add(new CheckGpsStatePackage());
      packages.add(new CodePush(getResources().getString(R.string.reactNativeCodePush_androidDeploymentKey),
          getApplicationContext(), BuildConfig.DEBUG, R.string.CodePushPublicKey));
      // Firebase
      packages.add(new RNFirebaseAuthPackage());
      packages.add(new RNFirebaseAnalyticsPackage());
      packages.add(new RNFirebaseMessagingPackage());
      packages.add(new RNFirebaseNotificationsPackage());
      packages.add(new RNFirebaseCrashlyticsPackage());

      return packages;
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
    initializeFlipper(this); // Remove this line if you don't want Flipper enabled
  }

  // React native Share
  @Override
  public String getFileProviderAuthority() {
    return "com.fangia.savy.esy.pro.fileprovider";
  }

  /**
   * Loads Flipper in React Native templates.
   *
   * @param context
   */
  private static void initializeFlipper(Context context) {
    if (BuildConfig.DEBUG) {
      try {
        /*
         * We use reflection here to pick up the class that initializes Flipper, since
         * Flipper library is not available in release mode
         */
        Class<?> aClass = Class.forName("com.facebook.flipper.ReactNativeFlipper");
        aClass.getMethod("initializeFlipper", Context.class).invoke(null, context);
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        e.printStackTrace();
      }
    }
  }
}
