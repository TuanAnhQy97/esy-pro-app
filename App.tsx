/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import { View, Text } from "react-native";
import { Provider } from "react-redux";
import SplashScreen from "react-native-splash-screen";
import firebase from "react-native-firebase";

import Orientation from "react-native-orientation-locker";
import codePush from "react-native-code-push";
import Store from "./src/Store";
import RootRouter, { SCREEN_KEY_NAME } from "./src/Routers";

import OverLayWaitingKline, {
  tools as OverLayWaitingKline_tools
} from "./src/Components/Popup/OverLayWaitingKline";
import MyActivityIndicator from "./src/Components/MyActivityIndicator";
import { setInternetChangeHandler } from "./src/Fetchers/InternetChangeHandler";

import {
  initNotification,
  cancelListenerForFCMmessages
} from "./src/services/notification/FCM";
import ble from "./src/services/ble";
import navigationService from "./src/services/navigationService";
import { REGULAR_FONT } from "./src/Components/Text";

let currentRouteName: SCREEN_KEY_NAME = "AuthLoading";

export { currentRouteName };

const codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_START,
  installMode: codePush.InstallMode.ON_NEXT_RESTART
};

// Disable auto scaling Font size.
// Changing of font size in Setting not affect App font size
if (!Text.defaultProps) {
  Text.defaultProps = {};
}
Text.defaultProps.allowFontScaling = false;
Text.defaultProps.fontSize = 14;
Text.defaultProps.color = "#000000";
Text.defaultProps.fontFamily = REGULAR_FONT;

class App extends Component<{}, { isLoading: boolean }> {
  constructor(props: any) {
    super(props);
    this.state = { isLoading: true };
  }

  componentDidMount() {
    // INIT BLE MODULE, Do only once when open app
    ble.init();

    // Add Internet change handler
    setInternetChangeHandler();
    // Orientation listener
    Orientation.addOrientationListener(this._onOrientationDidChange);

    // Notification
    initNotification();

    this.setState({ isLoading: false });
    SplashScreen.hide();
  }

  render() {
    if (this.state.isLoading) {
      return <MyActivityIndicator />;
    }
    return (
      <Provider store={Store}>
        <View style={{ flex: 1 }}>
          <RootRouter
            ref={navigatorRef => {
              navigationService.setTopLevelNavigator(navigatorRef);
            }}
            onNavigationStateChange={(prevState, currentState) => {
              const currentScreen = getRouteName(currentState);
              const prevScreen = getRouteName(prevState);
              if (prevScreen !== currentScreen) {
                // the line below uses the Google Analytics tracker
                // change the tracker here to use other Mobile analytics SDK.
                if (currentScreen !== null) {
                  currentRouteName = currentScreen;
                  firebase
                    .analytics()
                    .setCurrentScreen(currentScreen, currentScreen);
                }
              }
            }}
          />
          <OverLayWaitingKline
            ref={_ref => {
              OverLayWaitingKline_tools.setRef(_ref);
            }}
          />
        </View>
      </Provider>
    );
  }

  componentWillUnmount() {
    // Orientation listener
    Orientation.removeOrientationListener(this._onOrientationDidChange);
    cancelListenerForFCMmessages();
    ble.destroyBle();
  }

  _onOrientationDidChange = orientation => {
    console.log("_onOrientationDidChange", orientation);
    switch (orientation) {
      // Chỉ quan tâm những trường hợp sau
      case "PORTRAIT":
      case "LANDSCAPE-LEFT":
      case "LANDSCAPE-RIGHT":
        Store.dispatch({ type: "UPDATE_ORIENTATION", values: orientation });
        break;
      default:
        break;
    }
  };
}

const AppWrapper = __DEV__ ? App : codePush(codePushOptions)(App);
export default AppWrapper;

// gets the current screen from navigation state
function getRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getRouteName(route);
  }
  return route.routeName;
}
export function getCurrentRouteName(): SCREEN_KEY_NAME {
  return currentRouteName;
}
