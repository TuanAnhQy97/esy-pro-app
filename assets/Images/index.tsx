export default {
  NOT_FOUND: require("./data/notFound.png"),
  smartKey: require("./data/smartKey.png"),
  abs: require("./data/abs.png")
};
