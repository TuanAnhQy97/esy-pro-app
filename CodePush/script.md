# Release to Staging

code-push release-react ESY-PRO-iOS ios -m -k ./CodePush/private.pem --description "Red color"
code-push release-react ESY-PRO-Android android -m -k ./CodePush/private.pem --description "Red color"

# Promote

code-push promote ESY-PRO-Android Staging Production -t "_"
code-push promote ESY-PRO-iOS Staging Production -t "_"

# Listing

code-push deployment ls ESY-PRO-iOS -k
code-push deployment ls ESY-PRO-Android -k

code-push deployment history -a ESY-PRO-Android Staging
code-push deployment history -a ESY-PRO-iOS Staging

# Clear release

code-push deployment clear ESY-PRO-iOS Staging
