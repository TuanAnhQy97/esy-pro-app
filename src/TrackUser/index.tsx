/**
 * @flow
 */
import { analytics as Tracker } from "react-native-firebase";

type REPAIR_TOOLS = "tool_reset_ecu" | "tool_erase_error" | "tool_update_ecu";

type DIAGNOSTIC_TOOLS =
  | "open_auto_check_one"
  | "open_auto_check_two"
  | "open_read_fi_error"
  | "open_dashboard"
  | "open_report";

type ACCOUNT = "new_account_created";

type EVENT_NAMES = REPAIR_TOOLS | DIAGNOSTIC_TOOLS | ACCOUNT;

/**
 * Log User event
 * @param {*} eventName
 * @param {*} params
 */
const logEvent = (eventName: EVENT_NAMES, params: Object = {}) => {
  Tracker().logEvent(eventName, params);
};

const MyTracker = { logEvent };

export default MyTracker;
