/**
 * @flow
 */
import { ESY_ERROR_CODE } from "../Library/ExplainError";
import bikeLibHelper from "../services/bikeLibHelper";

const BATTERY_VERY_LOW_WHEN_IDLE = 10.5;
const BATTERY_SLIGHTLY_LOW_WHEN_IDLE = 11.5;

export type DATA1234 = {
  data1: number[];
  data2: number[];
  data3: number[];
  data4: number[];
};

class MyInstance {
  analyzeAutoCheckOneData(dataList: DATA1234): number[] {
    const notGoodList = [];
    analyzeData1(dataList.data1, notGoodList);
    analyzeData2(dataList.data2, notGoodList);
    return notGoodList;
  }
}

class DiagnosticHelper {
  static instance: MyInstance;

  static getInstance() {
    if (!DiagnosticHelper.instance) {
      DiagnosticHelper.instance = new MyInstance();
    }
    return DiagnosticHelper.instance;
  }
}
export default DiagnosticHelper.getInstance();

/**
 *  Analyze data1 from of realtime data to see if any error
 * @param {*} data12
 */
function analyzeData1(data1: number[], notGoodList: number[]) {
  const converted = bikeLibHelper.honda.convert.convertData1(data1);
  const vBatt = converted[4];
  const temp = converted[2];
  const map = converted[7];
  const tps = converted[6];
  const rpm = converted[1];

  /**
   *  Trong cả hai trường hợp đã hoặc chưa khởi động
   */
  // TEMPERATURE
  if (temp !== null) {
    if (temp > bikeLibHelper.honda._hondaLib.standardQD[1][1] + 10) {
      if (!notGoodList.includes(ESY_ERROR_CODE.TEMPERATURE_VERY_HIGH)) {
        notGoodList.push(ESY_ERROR_CODE.TEMPERATURE_VERY_HIGH);
      }
    } else if (temp > bikeLibHelper.honda._hondaLib.standardQD[1][1] + 5) {
      if (!notGoodList.includes(ESY_ERROR_CODE.TEMPERATURE_HIGH)) {
        notGoodList.push(ESY_ERROR_CODE.TEMPERATURE_HIGH);
      }
    }
  }
  /**
   * BATTERY
   */
  if (vBatt !== null) {
    if (vBatt <= BATTERY_VERY_LOW_WHEN_IDLE) {
      if (!notGoodList.includes(ESY_ERROR_CODE.BATTERY_VERY_LOW)) {
        notGoodList.push(ESY_ERROR_CODE.BATTERY_VERY_LOW);
      }
    } else if (vBatt <= BATTERY_SLIGHTLY_LOW_WHEN_IDLE) {
      if (!notGoodList.includes(ESY_ERROR_CODE.BATTERY_LOW)) {
        notGoodList.push(ESY_ERROR_CODE.BATTERY_LOW);
      }
    }
  }

  if (tps !== null) {
    /**
     * TPS
     */
    if (tps >= 60) {
      if (!notGoodList.includes(ESY_ERROR_CODE.TPS_VERY_HIGH)) {
        notGoodList.push(ESY_ERROR_CODE.TPS_VERY_HIGH);
      }
    } else if (tps > 5) {
      if (!notGoodList.includes(ESY_ERROR_CODE.TPS_HIGH)) {
        notGoodList.push(ESY_ERROR_CODE.TPS_HIGH);
      }
    } else if (tps >= 1) {
      if (!notGoodList.includes(ESY_ERROR_CODE.TPS_SLIGHTLY_HIGH)) {
        notGoodList.push(ESY_ERROR_CODE.TPS_SLIGHTLY_HIGH);
      }
    }
  }
  /**
   * MAP
   */
  if (map !== null) {
    if (map < 50) {
      if (!notGoodList.includes(ESY_ERROR_CODE.MAP_LOW)) {
        notGoodList.push(ESY_ERROR_CODE.MAP_LOW);
      }
    }
  }
}
/**
 *  Analyze data2 from of realtime data to see if any error
 * @param {*} data12
 */
function analyzeData2(data2: number[], notGoodList: number[]) {
  const converted = bikeLibHelper.honda.convert.convertData2(data2);
  const oxy = converted[0];
  if (oxy === 0) {
    if (!notGoodList.includes(ESY_ERROR_CODE.OXY_0_mV)) {
      notGoodList.push(ESY_ERROR_CODE.OXY_0_mV);
    }
  }
}
