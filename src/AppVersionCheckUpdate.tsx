/**
 * @flow
 */
import { Platform, Alert, Linking } from "react-native";

import DeviceInfo from "react-native-device-info";

const storeSpecificId =
  Platform.OS === "ios" ? "1437777474" : "com.fangia.savy.esy.pro";

import { getAppstoreAppVersion } from "react-native-appstore-version-checker";
import { getInternetState } from "./Fetchers/InternetChangeHandler";
import { wait, to } from "./SharedFunctions";

class MyInstance {
  didCheck = false;
  async getLastestVersion() {
    let lastest = "",
      error;
    [error, lastest] = await to(getAppstoreAppVersion(storeSpecificId)); // put any apps packageId here
    if (error) {
      console.log("getLastestVersion", "error", error);
      return 0;
    } else {
      return lastest;
    }
  }
  async isNeededToUpdate() {
    let internetState = getInternetState(); // check Internet state
    console.log("isNeededToUpdate", "internetState", internetState);
    if (!internetState) return;
    // Set timeout
    if (this.didCheck) {
      return false;
    } else {
      this.didCheck = true;
    }

    let lastest = await this.getLastestVersion(); // Get lastest version on Store
    console.log("isNeededToUpdate", "lastest", lastest);
    if (lastest === 0) {
      return false;
    } else {
      let current = await DeviceInfo.getVersion();
      if (this.compareAppVersion(current, lastest)) {
        //  Has newer Update
        console.log("App Has Update");
        Alert.alert(
          "Cập nhật ứng dụng",
          "Ứng dụng có bản cập nhật cấp mới cải thiện hiệu suất",
          [
            { text: "Để sau" },
            { text: "Cập nhật ngay", onPress: this.goToStore }
          ]
        );
      }
    }
  }
  goToStore() {
    let link =
      Platform.OS === "ios"
        ? "https://itunes.apple.com/US/app/id1437777474?mt=8"
        : "https://play.google.com/store/apps/details?id=com.fangia.savy.esy.pro";

    Linking.openURL(link).catch(e => {});
  }
  /**
   * Compare App Version. Current vs Lastest
   * @return true if lastest is higher
   * @note Appversion must contain 3 parts xx.xx.xx
   * @param {*} _current
   * @param {*} _lastest
   */
  compareAppVersion(_current: string, _lastest: string) {
    console.log("compareAppVersion", _current, _lastest);
    // Split into 3 parts
    let c = _current.split(".");
    c = c.map(value => {
      return parseInt(value);
    });
    let l = _lastest.split(".");
    l = l.map(value => {
      return parseInt(value);
    });

    if (c.length !== l.length) {
      return false;
    }
    // So sánh từ to tới bé
    for (let i = 0; i < c.length; i++) {
      if (l[i] == c[i]) {
        // Bằng nhau, so sánh phần bé hơn
        continue;
      } else if (l[i] > c[i]) {
        // Lớn hơn -> có phiên bản mới hơn
        return true;
      } else if (l[i] < c[i]) {
        // Nhỏ hơn -> Phiên bản hiện tại đang mới nhất
        return false;
      }
    }

    return false;
  }
}

class AppVersionCheckUpdate {
  static instance: MyInstance;
  static getInstance() {
    if (!AppVersionCheckUpdate.instance) {
      AppVersionCheckUpdate.instance = new MyInstance();
    }
    return AppVersionCheckUpdate.instance;
  }
}
export default AppVersionCheckUpdate.getInstance();
