/**
 * @flow
 */
import { PermissionsAndroid, Platform, Alert } from "react-native";

class MyInstance {
  /**
   * [Android Only]
   * Check Bluetooth Permission
   * Request if need
   */
  async checkBlePermission() {
    if (Platform.OS === "android") {
      // Check GPS state
      let ChecGpsState = require("../NativeModules/CheckGpsState");
      let isGpsOn = await ChecGpsState.isGpsOn();
      if (!isGpsOn) {
        Alert.alert(
          "GPS không được bật",
          "Nếu bạn ứng dụng không thể tìm thấy thiết bị. Vui lòng bật GPS và thử lại",
          [
            { text: "Bỏ qua" },
            { text: "Bật GPS", onPress: ChecGpsState.openGpsSetting }
          ]
        );
        // Warning + Alert users
      }
      // Check Permission
      try {
        const multiGranted = await PermissionsAndroid.requestMultiple([
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        ]);
        switch (
          multiGranted[PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION]
        ) {
          case PermissionsAndroid.RESULTS.GRANTED:
            console.log("You can use BLE");
            return true;
          case PermissionsAndroid.RESULTS.DENIED:
            console.log("BLE denied");
            Alert.alert(
              "Không thể sử dụng Bluetooth",
              "Vui cấp quyền truy cập để có ứng dụng có thể hoạt động",
              [
                { text: "Huỷ" },
                {
                  text: "Thử lại",
                  onPress: this.checkBlePermission
                }
              ]
            );
            return false;
          case PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN:
            console.log("BLE denied forever");
            Alert.alert(
              "Không thể sử dụng Bluetooth",
              'Vui cấp quyền truy cập để có ứng dụng có thể hoạt động. Vào "Cài đặt" của điện thoại để thực hiện',
              [{ text: "Huỷ" }]
            );
            return false;
          default:
            return false;
        }
      } catch (err) {
        console.warn("REQUEST ERROR", err);
      }
    } else {
      // iOS
      return true;
    }
  }
}

class CheckPermission {
  static instance: MyInstance;
  static getInstance() {
    if (!CheckPermission.instance) {
      CheckPermission.instance = new MyInstance();
      return CheckPermission.instance;
    } else {
      return CheckPermission.instance;
    }
  }
}

export default CheckPermission.getInstance();
