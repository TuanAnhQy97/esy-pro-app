/**
 * @flow
 */

const MAIN_SCREEN_EMPTYCASE = 0,
  MAIN_SCREEN_NORMALCASE = 1;
export { MAIN_SCREEN_EMPTYCASE, MAIN_SCREEN_NORMALCASE };

export class MainScreenUIData extends Object {
  case: number; // MAIN_SCREEN_EMPTYCASE, MAIN_SCREEN_NORMALCASE
  title: string;
  isLoading: boolean;
  constructor() {
    super();
    this.case = MAIN_SCREEN_EMPTYCASE;
    this.title = "";
    this.isLoading = true;
  }
}
