/**
 * @flow
 */
export class DeviceInfo extends Object {
  deviceId: number[];
  addr: string;
  ecuId: number[];
  version: number[];

  rssi: {
    value: number,
    sampleCount: number
  } = {
    value: 0,
    sampleCount: 0
  };
  constructor() {
    super();
    this.deviceId = [];
    this.addr = "";
    this.ecuId = [];
    this.version = [];
  }
}
