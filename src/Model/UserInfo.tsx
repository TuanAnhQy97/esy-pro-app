/**
 *  @flow
 */
export type USER_INFO = {
  phone: string,
  expiredDate: number, // Thời gian hết hạn
  // Store Info
  storeInfo: {
    name: string,
    addr: string, // Địa chỉ
    ward: string,
    district: string,
    city: string
  }
};
