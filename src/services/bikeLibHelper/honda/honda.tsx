import BasicBikeLibPro from "../../../Library/BasicBikeLibPro";
import BasicBikeLib, { ECU_TYPE } from "../../../Library/BasicBikeLib";
import { compareArrays } from "../../../SharedFunctions";
import { fetchDataForPaired } from "../../../Fetchers/DeviceFetcher";
import { BLE_ERROR } from "../../../constants";
import ble from "../../ble";
import * as convert from "./convert";
import performanceExplain from "./performanceExplain";

let _ecuId: number[];
let _hondaLib: BasicBikeLib;
let _hondaLibPro: BasicBikeLibPro;

export { convert, performanceExplain, _hondaLib, _hondaLibPro };

export const fetchHondaLib = async (ecuId: number[]) => {
  const trimEcuId = ecuId.slice(0, 4);
  // Exists in Cache
  if (_hondaLib && compareArrays(_ecuId, ecuId)) {
  } else {
    const libRes = await fetchDataForPaired.getBikeLibVer3FromServer(trimEcuId);
    if (libRes.error) {
      let error;
      // Chưa có bản quyền
      if (libRes.error === "not_purchased_yet") {
        error = BLE_ERROR.UNKNOWN;
      } else if (libRes.error === 6) {
        // Not support yet
        error = BLE_ERROR.BIKE_NOT_SUPPORT;
      } else {
        // Khác
        error = BLE_ERROR.NETWORK_ERROR;
      }
      throw new Error(error);
    } else {
      // Get lib okay
      _ecuId = [...trimEcuId];
      _hondaLib = libRes.data.normalLib;
      _hondaLibPro = libRes.data.proLib;
    }
  }
};

export const sendHondaBikeConfigToESY = async () => {
  const chipConfig = ble.generateCommand.configECUType(_hondaLib.ecuType);
  await ble.sendData({
    data: chipConfig,
    char: ble.CHAR_CONFIG_ECU_TYPE
  });
  // Là chip C
  if (_hondaLib.ecuType === ECU_TYPE.TYPE_C) {
    // Gửi lệnh config Request data 1 2 3 4 nếu có
    const dataConfigArr = [];
    if (_hondaLib.overideReq1) {
      const dataConfig = ble.generateCommand.configDataForTypeC(
        _hondaLib.overideReq1,
        1
      );
      dataConfigArr.push(dataConfig);
    }
    if (_hondaLib.overideReq2) {
      const dataConfig = ble.generateCommand.configDataForTypeC(
        _hondaLib.overideReq2,
        2
      );
      dataConfigArr.push(dataConfig);
    }
    if (_hondaLib.overideReq3) {
      const dataConfig = ble.generateCommand.configDataForTypeC(
        _hondaLib.overideReq3,
        3
      );
      dataConfigArr.push(dataConfig);
    }
    if (_hondaLib.overideReq4) {
      const dataConfig = ble.generateCommand.configDataForTypeC(
        _hondaLib.overideReq4,
        4
      );
      dataConfigArr.push(dataConfig);
    }
    for (let i = 0; i < dataConfigArr.length; i++) {
      await ble.sendData({
        data: dataConfigArr[i],
        service: ble.SERVICE_CONFIG_ESY,
        char: ble.CHAR_CONFIG_REQUEST_DATA
      });
    }
    // Kết thúc config
    await ble.sendData({
      data: ble.generateCommand.endConfigTypeC(),
      service: ble.SERVICE_CONFIG_ESY,
      char: ble.CHAR_CONFIG_REQUEST_DATA
    });
  }
};

export const getBikeName = () => {
  return (
    `${_hondaLib.basicInfo.name} ${_hondaLib.basicInfo.cylinderCap}cm³` +
    ` ${_hondaLib.basicInfo.gen}`
  );
};
