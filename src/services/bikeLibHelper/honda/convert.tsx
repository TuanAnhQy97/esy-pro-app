import { _hondaLib, _hondaLibPro } from "./honda";

export const convertData1 = (data: number[]) => {
  return [
    convertSpeed(data), // Speed 0
    convertRPM(data), // RPM 1
    convertEngineTemp(data), // Temp 2
    convertInjectionTime(data), // IT 3
    convertVBat(data), // Vbat 4
    convertGDL(data), // GDL 5
    convertTPS(data), // TPS 6
    convertMAP(data), // MAP 7
    convertIAT(data) // IAT 8,
  ];
};

export const convertData2 = (data: number[]) => {
  return [convertOxy(data)]; // Oxy 0
};

export const convertData4 = (data: number[]) => {
  return [
    convertSidekick(data), // Side kick
    convertBreakAndStartSignal(data), // Break and start Button
    convertFuelPump(data) // Bơm xăng
  ];
};

// #region Sub-convert Functions
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertRPM = (data: number[]) => {
  const pos = _hondaLib.pos1[0];
  if (pos === null) return null;
  const value = data[pos[0]] * 256 + data[pos[1]];
  return value;
};

/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertEngineTemp = (data: number[]) => {
  const pos = _hondaLib.pos1[1];
  if (!pos.length) return null;
  const value = data[pos[0]] - 40;
  return value;
};

/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertInjectionTime = (data: number[]) => {
  const pos = _hondaLib.pos1[2];
  if (!pos.length) return null;
  const value = (data[pos[0]] * 256 + data[pos[1]]) / 250;
  return value;
};

/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertVBat = (data: number[]) => {
  const pos = _hondaLib.pos1[3];
  if (!pos.length) return null;
  const value = data[pos[0]] / 10;
  return value;
};

/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertSpeed = (data: number[]) => {
  const pos = _hondaLib.pos1[4];
  if (!pos.length) return null;
  const value = data[pos[0]];
  return value;
};

/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertGDL = (data: number[]) => {
  const pos = _hondaLib.pos1[5];
  if (!pos.length) return null;
  const value = data[pos[0]] / 2 - 64;
  return value;
};

/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertTPS = (data: number[]) => {
  const pos = _hondaLib.pos1[6];
  if (!pos.length) return null;
  const value = data[pos[0]] / 2;
  return value;
};

/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertMAP = (data: number[]) => {
  const pos = _hondaLib.pos1[7];
  if (!pos.length) return null;
  const value = data[pos[0]];
  return value;
};

/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertIAT = (data: number[]) => {
  const pos = _hondaLib.pos1[8];
  if (!pos.length) return null;
  const value = data[pos[0]] - 40;
  if (value < 0) return 0;
  return value;
};

/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertOxy = (data: number[]) => {
  const pos = _hondaLib.pos2[0];
  if (!pos.length) return null;
  const value = (data[pos[0]] * 625) / 32; // * 5 * 1000 / 256
  return value;
};

/**
 *
 * @param {*} data
 */
const convertSidekick = (data: number[]) => {
  const pos = [4];
  if (!pos.length) return null;
  const value = data[pos[0]] % 16;
  return value;
};

/**
 *
 * @param {*} data
 */
const convertBreakAndStartSignal = (data: number[]) => {
  const pos = [4];
  if (!pos.length) return null;
  const value = Math.floor(data[pos[0]] / 16);
  return value;
};

/**
 *
 * @param {*} data
 */
const convertFuelPump = (data: number[]) => {
  const pos = [8];
  if (!pos.length) return null;
  const value = data[pos[0]];
  return value;
};

/**
 *  PRO
 */

export const convertData1Pro = (data: number[]) => {
  return [
    convertTPSVolt(data), // TPS
    convertTempVolt(data), // Temp
    convertIATVolt(data), // IAT
    convertMAPVolt(data), // MAP
    convertPullCNT(data), // Step
    convertIACCommand(data) // Lệnh IAC
  ];
};

export const convertData2Pro = (data: number[]) => {
  return [
    convertAF(data), // AF
    convertHeatOxy(data) // Nung Oxy
  ];
};

export const convertData3Pro = (data: number[]) => {
  return [
    convertMill(data), // Mill Status
    convertBaValue(data), // Góc nghiêng
    convertMAPAver(data) // MAP trung bình
  ];
};

export const convertData4Pro = (data: number[]) => {
  return [
    convertFuelPumpPro(data), // Bơm xăng
    convertSideStand(data), // Side kick
    convertStartSwitchInputSignal(data), // Break and start Button
    convertFan(data), // Quạt
    convertSCS(data), // SCS
    convertStarterSwitchCommand(data), // Lệnh công tắc đề
    convertStartSolenoidValve(data), // Van từ nhanh
    convertEVAPstatus(data), // Trạng thái hoạt động EVAP
    convertBaStatus(data), // Trạng thái ngắt của cảm biến nghiêng
    convertPCV(data), // Van PCV
    convertEngineStartInhibiterStatus(data)
  ];
};

// #region       INTERNAL METHOD
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertTPSVolt = (data: number[]) => {
  if (!_hondaLibPro.pos1.tps) {
    return null;
  }
  const value = (data[6] * 5) / 256;
  return value;
};
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertTempVolt = (data: number[]) => {
  // Không có cảm biến này
  if (!_hondaLibPro.pos1.tempA && !_hondaLibPro.pos1.tempB) {
    return null;
  }
  let value;
  // Nhiệt độ theo Chip A
  if (_hondaLibPro.pos1.tempA) {
    value = (data[8] * 5) / 256;
  }
  // Nhiệt độ theo Chip B
  else if (_hondaLibPro.pos1.tempB) {
    value = (data[12] * 5) / 256;
  }
  return value;
};
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertIATVolt = (data: number[]) => {
  if (!_hondaLibPro.pos1.iat) {
    return null;
  }
  const value = (data[10] * 5) / 256;
  return value;
};
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertMAPVolt = (data: number[]) => {
  if (!_hondaLibPro.pos1.map) {
    return null;
  }
  const value = (data[12] * 5) / 256;
  return value;
};
/**
 *
 * @param {*} data
 */
const convertPullCNT = (data: number[]) => {
  if (!_hondaLibPro.pos1.pulcnt) {
    return null;
  }
  const value = data[21];
  return value;
};
/**
 *
 * @param {*} data
 */
const convertIACCommand = (data: number[]) => {
  if (!_hondaLibPro.pos1.iacCommand) {
    return null;
  }
  const value = data[22] * 1.74431 + data[23] * 0.0068;
  return value;
};
// /////////////////////////////// DATA 2
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertAF = (data: number[]) => {
  if (!_hondaLibPro.pos2.af) {
    return null;
  }
  const value = data[5] * 0.0078;
  return value;
};
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
const convertHeatOxy = (data: number[]) => {
  if (!_hondaLibPro.pos2.heatOxy) {
    return null;
  }
  const value = data[6];
  return value;
};
// ////////////////////////////////// DATA 3
/**
 * @param {*} data
 */
const convertMill = (data: number[]) => {
  if (!_hondaLibPro.pos3.mill) {
    return null;
  }
  return data[4];
};
/**
 * @param {*} data
 */
const convertBaValue = (data: number[]) => {
  if (!_hondaLibPro.pos3.baValue) {
    return null;
  }
  const value = (data[17] * 5) / 256;
  return value;
};
/**
 * @param {*} data
 */
const convertMAPAver = (data: number[]) => {
  if (!_hondaLibPro.pos3.mapAverage) {
    return null;
  }
  const value = data[24];
  return value;
};
// ////////////////////////////////// DATA 4
/**
 *
 * @param {*} data
 */
const convertEngineStartInhibiterStatus = (data: number[]) => {
  if (!_hondaLibPro.pos4.engineStartInhibiterStatus) {
    return null;
  }
  return byteTobitArr(data[4])[0];
};
/**
 *
 * @param {*} data
 */
const convertSideStand = (data: number[]) => {
  if (!_hondaLibPro.pos4.sideStand) {
    return null;
  }
  return byteTobitArr(data[4])[1];
};
/**
 * @param {*} data
 */
const convertSCS = (data: number[]) => {
  if (!_hondaLibPro.pos4.scs) {
    return null;
  }
  return byteTobitArr(data[4])[2];
};

/**
 * @param {*} data
 */
const convertBaStatus = (data: number[]) => {
  if (!_hondaLibPro.pos4.ba) {
    return null;
  }
  return byteTobitArr(data[4])[4];
};

/**
 *
 * @param {*} data
 */
const convertStartSwitchInputSignal = (data: number[]) => {
  if (!_hondaLibPro.pos4.starterSwitchInputSignal) {
    return null;
  }
  return byteTobitArr(data[4])[5];
};
/**
 *
 * @param {*} data
 */
const convertFuelPumpPro = (data: number[]) => {
  if (!_hondaLibPro.pos4.pump) {
    return null;
  }
  return byteTobitArr(data[8])[0];
};
/**
 *
 * @param {*} data
 */
const convertPCV = (data: number[]) => {
  if (!_hondaLibPro.pos4.pcv) {
    return null;
  }
  return byteTobitArr(data[8])[2];
};
/**
 * @param {*} data
 */
const convertEVAPstatus = (data: number[]) => {
  if (!_hondaLibPro.pos4.evap) {
    return null;
  }
  return byteTobitArr(data[8])[3];
};
/**
 * @param {*} data
 */
const convertStarterSwitchCommand = (data: number[]) => {
  if (!_hondaLibPro.pos4.startSwitchCommand) {
    return null;
  }
  return byteTobitArr(data[8])[4];
};
/**
 * @param {*} data
 */
const convertStartSolenoidValve = (data: number[]) => {
  if (!_hondaLibPro.pos4.startSolenoidValve) {
    return null;
  }
  return byteTobitArr(data[8])[6];
};
/**
 * @param {*} data
 */
const convertFan = (data: number[]) => {
  if (!_hondaLibPro.pos4.fan) {
    return null;
  }
  return byteTobitArr(data[9])[0];
};
/**
 * Đổi byte thành mảng bit
 * @param {*} byte  0 <= số <= 255
 */
export function byteTobitArr(byte: number) {
  // let bitArr = [
  //   (byte & 0x80) >> 7,
  //   (byte & 0x40) >> 6,
  //   (byte & 0x20) >> 5,
  //   (byte & 0x10) >> 4,
  //   (byte & 0x08) >> 3,
  //   (byte & 0x04) >> 2,
  //   (byte & 0x02) >> 1,
  //   byte & 0x01
  // ];
  const bitArr = [
    byte & 0x01,
    (byte & 0x02) >> 1,
    (byte & 0x04) >> 2,
    (byte & 0x08) >> 3,
    (byte & 0x10) >> 4,
    (byte & 0x20) >> 5,
    (byte & 0x40) >> 6,
    (byte & 0x80) >> 7
  ];
  return bitArr;
}
