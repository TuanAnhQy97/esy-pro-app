import {compareArrays} from '../../../SharedFunctions';
import {byteTobitArr} from '../honda/convert';

const convertData = (key: number, value: number[]): number | string => {
  switch (key) {
    // RPM
    case 0x001:
      return value[0] * 50;
    // EOT
    case 0x101:
      return value[3] - 30;

    // IAT
    case 0x05:
      return value[3] - 30;

    // BARO
    case 0x08:
      return (value[3] - 8) / 2;

    // Speed
    case 0x80:
      return value[3];

    // BATT Volt
    case 0x81:
      return value[3] * 0.073;

    // TPS Volt
    case 0x82:
      return (value[3] / 255) * 5;

    // O2 sensor 1 Volt
    case 0x84:
      return (value[3] / 255) * 5;

    // MAP
    case 0x86:
      return 0.496 * value[3];

    // TPS Angle
    case 0x88:
      return value[3] * 0.488;

    // Number of fault deleti
    case 0x96:
      return value[3];

    // Injection duration
    case 0x98:
      return value[3] * 0.5;

    // Ignition timing
    case 0x99:
      return value[3] - 30;

    // ISC ( van ISC)
    case 0x9a:
      return value[3] / 2;

    // O2 trim fuel
    case 0x9b:
      return (value[3] - 128) * 0.384;

    // APS Volt
    case 0x0c:
      return (value[3] / 255) * 5;

    // lean angle sensor or O
    case 0x83:
      return (value[3] / 255) * 5;

    // O2 sensor 2 Voltage
    case 0x85:
      return (value[3] / 255) * 5;

    // Intake air pressure se
    case 0x87:
      return value[3] * 0.496;

    // APS Angle
    case 0x89:
      return value[3] * 0.488;

    // Mode
    case 0x90:
      return (value[3] - 1) / 2;

    // Gear position
    case 0x91:
      return (value[3] - 1) / 2;

    case 0x192:
      return byteTobitArr(value[3])[1] ? 'yes' : 'no';
    case 0x292:
      return byteTobitArr(value[3])[2] ? 'yes' : 'no';
    case 0x392:
      return byteTobitArr(value[3])[3] ? 'yes' : 'no';
    case 0x492:
      return byteTobitArr(value[4])[0] ? 'yes' : 'no';

    // primary sheave position
    case 0xa1:
      return 0.0194 * value[3];

    // I-Shift switch
    case 0x0a2:
      return byteTobitArr(value[3])[0] ? 'yes' : 'no';
    // Ycc-AT mode switch
    case 0x1a2:
      return byteTobitArr(value[3])[1] ? 'yes' : 'no';

    // YCC AT
    case 0xa0:
      return value[3] * 0.5;

    default:
      return value[3];
  }
};

const convertAdvanceData = (key: number, value: number): number | string => {
  switch (key) {
    case 0x01:
      return value * 0.488;

    case 0x03:
      return value * 0.496;

    case 0x05:
      return value - 30;

    case 0x09:
      return value * 0.073;

    case 0x0b:
      return value - 30;

    default:
      return value;
  }
};

const getBikeName = (ecuId: number[]) => {
  let name = 'Yamaha';
  const reduced = ecuId.slice(5);
  if (compareArrays(reduced, [0x32, 0x50, 0x56, 0x31, 0x80])) {
    name = 'Exciter';
  } else if (compareArrays(reduced, [0x32, 0x54, 0x44, 0x31, 0x72])) {
    name = 'Acuzor';
  } else if (compareArrays(reduced, [0x31, 0x46, 0x43, 0x39, 0x6a])) {
    name = 'Sirus 2017';
  } else if (compareArrays(reduced, [0x32, 0x54, 0x44, 0x31, 0x72])) {
    name = 'Acuzor';
  } else if (compareArrays(reduced, [0x32, 0x42, 0x4d, 0x31, 0x69])) {
    name = 'Grander';
  } else if (compareArrays(reduced, [0x32, 0x58, 0x43, 0x31, 0x75])) {
    name = 'Novou 5';
  } else if (compareArrays(reduced, [0x32, 0x4e, 0x44, 0x43, 0x7e])) {
    name = 'Exciter 2019';
  } else if (compareArrays(reduced, [0x31, 0x46, 0x43, 0x31, 0x62])) {
    name = 'SIRIUS RC FI 2017';
  } else if (compareArrays(reduced, [0x42, 0x36, 0x33, 0x33, 0x55])) {
    name = 'NVX 2018';
  } else {
    name = reduced
      .map(v => {
        let s = v.toString(16);
        if (s.length === 1) {
          s = `0${s}`;
        }
        return s;
      })
      .join('-')
      .toUpperCase();
  }
  return name;
};

const workingFunction = (ecuId: number[]) => {
  const reduced = ecuId.slice(5);
  const working = {
    dashboard: true,
    error: true,
    advance: true,
  };
  // NVX 2018
  if (compareArrays(reduced, [0x42, 0x36, 0x33, 0x33, 0x55])) {
    working.dashboard = false;
    working.advance = false;
  } else {
  }
  return working;
};

export default {convertData, convertAdvanceData, getBikeName, workingFunction};
