import convert from "./convert";
import convertObd from "./convertObd";

let _ecuId: number[];
let partList: number[];

export const fetchYamahaLib = async (ecuId: number[]) => {};

export const setPartList = (list: number[]) => {
  partList = list;
};

export const setEcuId = (id: number[]) => {
  _ecuId = id;
};

export const getEcuId = () => _ecuId;

export const getPartList = () => partList;

export const sendYamahaBikeConfigToESY = async () => {};

export { convert, convertObd };
