import {byteTobitArr} from '../honda/convert';

const convertDataObd = (data: number[]): {convertedValue: string | number; key: number}[] => {
  const result: {convertedValue: string | number; key: number}[] = [];

  data.forEach((v, i) => {
    switch (i) {
      case 5:
        result.push({convertedValue: data[i] * 255 + data[i], key: i});
        break;

      case 7:
        result.push({convertedValue: data[i], key: i});
        break;

      case 8:
        result.push({convertedValue: data[i] / 2, key: i});
        break;

      case 9:
        result.push({convertedValue: data[i] - 30, key: i});
        break;

      case 10:
        result.push({convertedValue: data[i], key: i});
        break;

      case 11:
        result.push({convertedValue: data[i] * 0.073, key: i});
        break;

      case 12:
        result.push({convertedValue: (data[i] * 5) / 256, key: i});
        break;

      case 13:
        result.push({convertedValue: data[i] * 0.488, key: i});
        break;

      case 14:
        result.push({convertedValue: (data[i] * 5) / 256, key: i});
        break;

      case 15:
        result.push({convertedValue: data[i], key: i});
        break;

      case 16:
        result.push({convertedValue: (data[i] * 5) / 256, key: i});
        break;

      case 17:
        result.push({convertedValue: (data[i] * 5) / 256, key: i});
        break;

      case 18:
        result.push({convertedValue: (data[i] * 5) / 256, key: i});
        break;

      case 19:
        result.push({convertedValue: data[i] - 30, key: i});
        break;

      case 20:
        result.push({convertedValue: data[i] - 30, key: i});
        break;

      case 21:
        result.push({convertedValue: data[i] * 0.496, key: i});
        break;

      case 22:
        result.push({convertedValue: data[i] * 0.496, key: i});
        break;

      case 23:
        result.push({convertedValue: data[i] * 0.496, key: i});
        break;

      case 24:
        result.push({convertedValue: data[i], key: i});
        break;

      case 25:
        result.push({convertedValue: data[i], key: i});
        break;

      case 26:
        result.push({convertedValue: data[i], key: i});
        break;

      case 27:
        result.push({convertedValue: -(data[i] - 0x80) / 4, key: i});
        break;

      case 28:
        result.push({convertedValue: data[i], key: i});
        break;

      case 29:
        result.push({convertedValue: data[i], key: i});
        break;

      case 31:
        result.push({convertedValue: data[i], key: i});
        break;

      case 32:
        result.push({convertedValue: (data[i] * 100) / 256, key: i});
        break;

      case 33:
        result.push({convertedValue: data[i], key: i});
        break;

      case 34:
        result.push({convertedValue: -(data[i] - 0x80) / 4, key: i});
        break;

      case 35:
        result.push({convertedValue: -(data[i] - 0x80) / 4, key: i});
        break;

      case 36:
        result.push({convertedValue: data[i], key: i});
        break;

      case 37:
        result.push({convertedValue: data[i], key: i});
        break;

      default:
        break;
    }
  });

  return result;
};

export default {convertDataObd};
