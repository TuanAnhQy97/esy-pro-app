import honda from "./honda";
import yamaha from "./yamaha";

let currentBikeBrand: string;

export const setCurrentBikeBrand = (brand: string) =>
  (currentBikeBrand = brand);
export const getCurrentBikeBrand = () => currentBikeBrand;

export { honda, yamaha };
