import { NavigationActions, StackActions } from "react-navigation";
import { Alert } from "react-native";

let navigator: any;

function setTopLevelNavigator(navigatorRef: any) {
  navigator = navigatorRef;
}

function navigate(routeName: string, params?: any) {
  navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params
    })
  );
}

function goBack() {
  navigator.dispatch(StackActions.pop());
}

function showAlertNoInternet() {
  Alert.alert("Chưa có kết nối Internet", "Vui lòng kết nối mạng đề sử dụng", [
    { text: "Đồng ý" }
  ]);
}

// add other navigation functions that you need and export them

export default {
  navigate,
  goBack,
  setTopLevelNavigator,
  showAlertNoInternet
};
