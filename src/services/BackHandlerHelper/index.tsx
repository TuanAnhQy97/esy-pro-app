/**
 * @flow
 */
import React from "react";
import { BackHandler } from "react-native";

import navigationService from "../navigationService";

type Props = {
  onBackPress: Function;
  screenName: string;
};

class BackHandlerHelper extends React.PureComponent<Props> {
  static defaultProps = {
    onBackPress: async () => navigationService.goBack(),
    screenName: ""
  };

  componentDidMount() {
    console.log("BackHandlerHelper add", this.props.screenName);
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    console.log("BackHandlerHelper remove", this.props.screenName);
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    const { onBackPress } = this.props;
    console.log("handleBackPress", this.props.screenName);
    onBackPress(); // works best when the goBack is async
    return true;
  };

  render() {
    return null;
  }
}

export default BackHandlerHelper;
