import { esyVersion } from "../ble";
import { ECU_TYPE } from "../../../Library/BasicBikeLib";

export default {
  keyS1: (key: number[]) => {
    switch (esyVersion) {
      case "ver1":
        return key;

      case "ver2":
        return [0xfb, 0xfb, ...key];
      default:
        return [];
    }
  },
  keyS3: (s3: number[]) => {
    switch (esyVersion) {
      case "ver1":
        return s3;

      case "ver2":
        return [0xfb, 0xfb, ...s3];
      default:
        return [];
    }
  },
  // Choose bike brand
  chooseBikeBrand: (brand: string) => {
    switch (brand) {
      case "honda":
        return [0xfa, 0xfa, 0x03, 0x00];

      case "yamaha":
        return [0xfa, 0xfa, 0x03, 0x01];

      default:
        return [];
    }
  },

  // Get ECU ID
  getEcuId: () => {
    switch (esyVersion) {
      case "ver1":
        return [0x04];

      case "ver2":
        return [0xfa, 0xfa, 0x06, 0x00];
      default:
        return [];
    }
  },

  // Config bike ECU
  configECUType: (ecuType: number) => {
    switch (esyVersion) {
      case "ver1":
        switch (ecuType) {
          case ECU_TYPE.TYPE_A:
            return [0x01];

          case ECU_TYPE.TYPE_B:
            return [0x02];

          case ECU_TYPE.TYPE_C:
            return [0x03];

          default:
            return [];
        }

      case "ver2":
        return [0xfd, 0xfd, ecuType];
      default:
        return [];
    }
  },
  configDataForTypeC: (requestArray: number[], dataType: 1 | 2 | 3 | 4) => {
    switch (esyVersion) {
      case "ver1":
        const length1 = requestArray.length + 3;
        return [0xf0 + dataType, length1, ...requestArray, 0x00];

      case "ver2":
        const length2 = requestArray.length + 3;
        return [0xf0 + dataType, length2, ...requestArray, 0x00];
      default:
        return [];
    }
  },

  endConfigTypeC: () => {
    switch (esyVersion) {
      case "ver1":
        return [0xff];

      case "ver2":
        return [0xff];
      default:
        return [];
    }
  },

  // Go to idle mode
  goToIdle: () => {
    switch (esyVersion) {
      case "ver1":
        return [0x08];

      case "ver2":
        return [0xfa, 0xfa, 0x08];
      default:
        return [];
    }
  },

  // Go to dashboard mode
  goToCombo1234Mode: () => {
    switch (esyVersion) {
      case "ver1":
        return [0x66];

      case "ver2":
        return [0xfa, 0xfa, 0x04];
      default:
        return [];
    }
  },

  goToReadError: () => {
    switch (esyVersion) {
      case "ver1":
        return [0x68];

      case "ver2":
        return [0xfa, 0xfa, 0x05];
      default:
        return [];
    }
  },

  goToResetECU: () => {
    switch (esyVersion) {
      case "ver1":
        return [0x67];

      case "ver2":
        return [0xfa, 0xfa, 0x06, 0x02];
      default:
        return [];
    }
  },

  goToClearErrorROM: () => {
    switch (esyVersion) {
      case "ver1":
        return [0x69];

      case "ver2":
        return [0xfa, 0xfa, 0x06, 0x01];
      default:
        return [];
    }
  },

  goToUpdateEcu: () => {
    switch (esyVersion) {
      case "ver1":
        return [];

      case "ver2":
        return [0xfa, 0xfa, 0x07];
      default:
        return [];
    }
  },

  /**
   * Yamaha
   */

  goToReadFullDataYa: () => {
    return [0xfa, 0xfa, 0x0b];
  },

  goToReadErrorYa: () => {
    return [0xfa, 0xfa, 0x05];
  },

  goToEraseErrorYa: () => {
    return [];
  },

  getPartList: () => {
    return [0xff, 0xff, 0x03];
  },

  goToAdvanceData: () => {
    return [0xfa, 0xfa, 0x04];
  },

  advanceSelectPart: (key: number) => {
    return [0xff, 0xff, 0x01, key];
  },

  advanceTrigger: () => {
    return [0xff, 0xff, 0x02];
  },

  getEcuIdYa: () => {
    return [0xfa, 0xfa, 0x06];
  },

  deleteErrorYa: (errorMem: number, errorCode: number) => {
    return [0xfa, 0xfa, 0x0d, errorMem, errorCode];
  }
};
