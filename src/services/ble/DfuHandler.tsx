/**
 * @flow
 */
import { NordicDFU, DFUEmitter } from "react-native-nordic-dfu";
import { Alert } from "react-native";
import ble from ".";
import navigationService from "../navigationService";
import { wait } from "../../SharedFunctions";
import { DEVICE_NAME } from "../../constants";

console.log("PASS IMPORT");

class DfuHandler {
  static instance: DfuHandler;

  static getInstance() {
    if (!DfuHandler.instance) {
      DfuHandler.instance = new DfuHandler();
    }
    return DfuHandler.instance;
  }

  callbackDFUProgress: Function = () => {};

  callbackDFUStateChange: Function = () => {};

  ignoreCheckUpdate = false;

  handlerDiscover: any;

  /**
   * Check if ESY HW have any Update
   * @param {*} currentVersion current version of ESY HW known through Adver. Package
   */
  async checkUpdateForHW() {
    // Update for all ver
    if (ble.esyVersion === "ver1") {
      Alert.alert(
        "Phần mềm mới hỗ trợ dòng xe Yamaha",
        "Phần cứng gắn trên xe có cập nhật mới. Bạn nên thực hiện ngay. Cập nhật miễn phí, quá trình diễn ra từ 3 đến 5 phút",
        [
          {
            text: "Để sau",
            onPress: () => {
              this.ignoreCheckUpdate = true;
            }
          },
          {
            text: "Cập nhật ngay",
            onPress: async () => {
              await wait(1000); // Delay
              navigationService.navigate("DFU");
            }
          }
        ],
        { cancelable: false }
      );
    }
  }

  /**
   * Start DFU progress
   * @param {*} deviceAddress MAC address (Android) / UUID (iOS)
   * @param {*} filePath FW location on storage
   */
  startDFU = (deviceAddress: string, filePath: string) => {
    console.log("startDFU", deviceAddress, filePath);
    NordicDFU.startDFU({
      deviceAddress,
      deviceName: DEVICE_NAME,
      filePath
    })
      .then(res => {
        console.log("Transfer done:", res);
      })
      .catch(e => {
        console.log("startDFU error ", e.code, e);
      });
  };

  /**
   * Callback for DFU progress changes
   * @param {*} callback callback for DFU progress
   * @example  ({ percent, currentPart, partsTotal, avgSpeed, speed }) => {
        console.log("DFU progress: " + percent + "%");
      }
   */
  registerDFUProgressCallback(callback: Function) {
    this.callbackDFUProgress = callback;
    DFUEmitter.addListener("DFUProgress", this.callbackDFUProgress);
  }

  /**
   * Callback when DFU state changes
   * @param {*} callback
   * @example 
   *  ({ state }) => {
        console.log("DFU State:", state);
      });
      
     @note value of state:
      - "DFU_FAILED"
   */
  registerDFUStateChangedCallback(callback: Function) {
    this.callbackDFUStateChange = callback;
    DFUEmitter.addListener("DFUStateChanged", this.callbackDFUStateChange);
  }

  unresgisterAllCallback() {
    DFUEmitter.removeListener("DFUProgress", this.callbackDFUProgress);
    DFUEmitter.removeListener("DFUStateChanged", this.callbackDFUStateChange);
  }
}
export default DfuHandler.getInstance();
