import {NativeEventEmitter, NativeModules, Platform, PermissionsAndroid} from 'react-native';
import BleManager from 'react-native-ble-manager';

import {
  BLE_ERROR,
  BLE_RESPONSE_DATA_TYPE,
  DEVICE_NAME,
  DEVICE_VERSION_2,
  EVENT_BUS,
  BIKE_BRAND_LIST,
} from '../../constants';

import {fetchDataForPaired} from '../../Fetchers/DeviceFetcher';
import {FETCH_ERROR} from '../../Fetchers/Shared';
import generateCommand from './generateCommand';
import extractDataFromResponse from './extractDataFromResponse';
import esyDataManager from './esyDataManager';
import {compareVersions} from '../../SharedFunctions';
import eventBus from '../eventBus';
import bikeLibHelper from '../bikeLibHelper';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

let didInit: boolean;

let isConnected = false;
let isScanning = false;
let bleOn = false;

let deviceAddr: string = '';
let targetDeviceId: number[] | null = null;
let deviceVersion: number[];
let ecuId: number[] = [];

const SERVICE_DATA_UUID = '6e400001-b5a3-f393-e0a9-e50e24dcca9e';
const CHAR_DATA_UUID = '6e400003-b5a3-f393-e0a9-e50e24dcca9e';
const CHAR_CONTROL_UUID = '6e400006-b5a3-f393-e0a9-e50e24dcca9e';
export const CHAR_CONFIG_ECU_TYPE = '6e400005-b5a3-f393-e0a9-e50e24dcca9e';

export const SERVICE_CONFIG_ESY = '6e400010-b5a3-f393-e0a9-e50e24dcca9e';
export const CHAR_CONFIG_REQUEST_DATA = '6e400012-b5a3-f393-e0a9-e50e24dcca9e';

export const SERVICE_ADVANCE_DATA = '6e400030-b5a3-f393-e0a9-e50e24dcca9e';
export const CHAR_DATA_ADVANCE_CONTROL = '6e400032-b5a3-f393-e0a9-e50e24dcca9e';
const CHAR_DATA_ADVANCE = '6e400031-b5a3-f393-e0a9-e50e24dcca9e';

const SERVICE_SECURITY_UUID = '6e400001-b5a3-f393-e0a9-e50e24dcca9e';
const CHAR_SECURITY_SEND = '6e401089-b5a3-f393-e0a9-e50e24dcca9e';
const CHAR_SECURITY_RECEIVE = '6e401008-b5a3-f393-e0a9-e50e24dcca9e';

type ESY_VERSION_TYPE = 'ver1' | 'ver2';
let esyVersion: ESY_VERSION_TYPE = 'ver1';

/**
 * Callback from outside
 */
let callbackConnected: Function | null = null;
let rejectConnected: Function | null = null;

let callbackUpdateChar: Function | null = null;
let fireErrorWhenDisconnect = true;

const _onBleError = (error: string | Error) => {
  // force disconnect when ble error
  if (error.toString().includes(BLE_ERROR.ALREADY_CONNECTED)) {
    disconnect();
  } else if (error === BLE_ERROR.KLINE_ERROR && bikeLibHelper.getCurrentBikeBrand() === BIKE_BRAND_LIST.YAMAHA) {
  } else {
    disconnect();
  }
  // Dismiss overlay switch key
  eventBus.fireEvent(EVENT_BUS.SWITCH_KEY_OKAY);
};

eventBus.addListener(EVENT_BUS.BLE_CHANGE_ERROR, _onBleError);

/**
 * Set verion of ESY PRO
 * @param ver
 */
export const setESYVersion = (ver: ESY_VERSION_TYPE) => {
  esyVersion = ver;
};

export const getCurrentVersion = () =>{
  return deviceVersion;
}

const setECUId = (id: number[]) => (ecuId = id);

export {
  generateCommand,
  extractDataFromResponse,
  targetDeviceId,
  ecuId,
  deviceAddr,
  esyVersion,
  esyDataManager,
  setECUId,
};

export const addListenerUpdateCharValue = (callback: (data: any) => any) => {
  bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', callback);
};

/**
 * Inside handlers
 */
const handleDiscoverPeripheral = async (data: any) => {
  console.log('handleDiscoverPeripheral', data);
  // Found
  if (deviceAddr) {
    return;
  }
  // Filtered by device name
  if (data.name !== DEVICE_NAME && data.advertising.localName !== DEVICE_NAME) {
    return;
  }

  const adverPackage = data.advertising;
  // Filter by company name
  if (!isSavyDevice(adverPackage)) {
    return;
  }
  // Filter by device ID
  const deviceId = getDeviceId(adverPackage); // array length = 8
  if (!deviceId) {
    return;
  }
  if (targetDeviceId && !compareArrays(targetDeviceId, deviceId)) {
    return;
  }

  ecuId = getEcuId(adverPackage) || [];

  deviceVersion = getDeviceVersion(adverPackage) || [1, 0, 0];

  // Found
  deviceAddr = data.id;
  await stopScan();
  BleManager.connect(deviceAddr).catch(async e => {
    if (e.includes('Connection')) {
      await new Promise(resolve => setTimeout(resolve, 1000));
    }
    rejectConnected && rejectConnected(e);
  });
};

const handleStopScan = () => {
  isScanning = false;
  if (!deviceAddr) {
    rejectConnected && rejectConnected(BLE_ERROR.NOT_FOUND);
  }
};
const handleDisconnectedPeripheral = () => {
  isConnected = false;
  rejectConnected && rejectConnected(BLE_ERROR.DISCONNECTED);
  fireErrorWhenDisconnect && eventBus.fireEvent(EVENT_BUS.BLE_CHANGE_ERROR, BLE_ERROR.DISCONNECTED);
};
const handleUpdateValueForCharacteristic = data => {
  const {value} = data;
  callbackUpdateChar && callbackUpdateChar(value);
  callbackUpdateChar = null;
};
const handleUpdateState = ({state}) => {
  bleOn = state === 'on';
};
const handleConnectedPeripheral = async () => {
  if (isConnected) {
    return;
  }
  isConnected = true;

  try {
    await BleManager.retrieveServices(deviceAddr);
    console.log('handleConnectedPeripheral', ' pass retrieveServices');
    // Security
    await BleManager.startNotification(deviceAddr, SERVICE_SECURITY_UUID, CHAR_SECURITY_RECEIVE);
    if (compareVersions(deviceVersion, DEVICE_VERSION_2) >= 0) {
      setESYVersion('ver2');
    } else {
      setESYVersion('ver1');
    }
    const keyS1 = [
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
    ];
    const sendKeyS1 = generateCommand.keyS1(keyS1);

    const responseS2 = await sendData({
      data: sendKeyS1,
      waitForResponse: true,
      service: SERVICE_SECURITY_UUID,
      char: CHAR_SECURITY_SEND,
    });
    const keyS2 = extractDataFromResponse(responseS2, BLE_RESPONSE_DATA_TYPE.SECURITY);
    const keyS3 = await fetchDataForPaired.checkSeedKeyThroughServer(keyS1, keyS2);

    if (keyS3.error) {
      if (keyS3.error === FETCH_ERROR.ERROR_NO_NETWORK) {
        rejectConnected && rejectConnected(BLE_ERROR.NETWORK_ERROR);
        disconnect();
      } else if (keyS3.error === FETCH_ERROR.ERROR_NO_INFO) {
        rejectConnected && rejectConnected(BLE_ERROR.BLE_SECURITY_FAIL);
        disconnect();
      }
      return;
    }
    await sendData({
      data: generateCommand.keyS3(keyS3.data),
      service: SERVICE_SECURITY_UUID,
      char: CHAR_SECURITY_SEND,
    });
    console.log('handleConnectedPeripheral', ' pass security');

    await BleManager.startNotification(deviceAddr, SERVICE_DATA_UUID, CHAR_DATA_UUID);

    if (esyVersion === 'ver2') {
      await BleManager.startNotification(deviceAddr, SERVICE_ADVANCE_DATA, CHAR_DATA_ADVANCE);
    }

    // Connect succeed
    callbackConnected && callbackConnected({deviceVersion, ecuId, deviceAddr});
    callbackConnected = null;
  } catch (e) {
    rejectConnected && rejectConnected(e);
    disconnect();
  }
};

export async function init() {
  // Not init yet
  if (!didInit) {
    console.log('ble init');
    // Init module
    await BleManager.start({showAlert: true});
    // Add event listeners
    didInit = true;

    addListener();
    // init esyDataManager
    esyDataManager.init();
    BleManager.checkState();
    await new Promise(resolve => setTimeout(resolve, 175));
    // Permission check
    await requestPermission();
    // Enable BLE
    await BleManager.enableBluetooth();
  } else {
    console.warn('BLE ALREADY INIT');
  }
}
let a;
let b;
let c;
let d;
let e;
let f;

export function addListener() {
  a = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', handleDiscoverPeripheral);
  b = bleManagerEmitter.addListener('BleManagerDidUpdateState', handleUpdateState);
  c = bleManagerEmitter.addListener('BleManagerStopScan', handleStopScan);
  d = bleManagerEmitter.addListener('BleManagerConnectPeripheral', handleConnectedPeripheral);
  e = bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', handleDisconnectedPeripheral);
  f = bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', handleUpdateValueForCharacteristic);
}

export function removeListener() {
  a.remove();
  b.remove();
  c.remove();
  d.remove();
  e.remove();
  f.remove();
}

export const forceCheckBleStatus = () => BleManager.checkState();

export async function requestPermission() {
  if (Platform.OS === 'android' && Platform.Version >= 23) {
    const permitted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
    if (permitted) {
      console.log('Permission is OK');
      return true;
    }
    const requestResult = await PermissionsAndroid.requestPermission(
      PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
    );
    if (requestResult) {
      console.log('User accept');
      return true;
    }
    console.log('User refuse');
    throw new Error(BLE_ERROR.NO_PERMISSION);
  }
  return true;
}

async function stopScan() {
  if (isScanning) {
    await BleManager.stopScan();
    isScanning = false;
  } else {
    console.warn('BLE IS NOT SCANNING');
  }
}

export function connect(deviceId: number[]) {
  return new Promise(async (resolve, reject) => {
    if (!(await requestPermission())) {
      return reject(BLE_ERROR.NO_PERMISSION);
    }

    // Ble is off
    if (!bleOn) {
      return reject(BLE_ERROR.BLE_OFF);
    }

    deviceAddr = '';
    if (!isConnected) {
      targetDeviceId = deviceId;
      // reset all other callbacks
      callbackUpdateChar = null;
      // connect calback
      callbackConnected = resolve;
      rejectConnected = reject;
      if (!isScanning) {
        BleManager.scan([], 5, false)
          .then(() => {
            console.log('Start scan');
            isScanning = true;
          })
          .catch(e => {
            reject(e);
          });
      } else {
        console.warn('BLE ALREADY SCANNING');
        reject(BLE_ERROR.ALREADY_SCANNING);
      }
    } else {
      console.warn('BLE IS ALREADY CONNECTED');
      reject(BLE_ERROR.ALREADY_CONNECTED);
    }
  });
}

export function sendData({
  data,
  waitForResponse = false,
  service = SERVICE_DATA_UUID,
  char = CHAR_CONTROL_UUID,
}: {
  data: number[];
  waitForResponse?: boolean;
  service?: string;
  char?: string;
}) {
  console.group && console.group('%cSendData', 'color:white;font-weight:bold;background:blue;padding:2px 6px');
  console.log('Data\t\t', data);
  console.log('Service\t\t', service);
  console.log('Char\t\t', char);
  console.log('WaitForResponse\t\t', waitForResponse);
  console.groupEnd && console.groupEnd();

  // if (data && data.length) {
  if (waitForResponse) {
    return new Promise((resolve, reject) => {
      callbackUpdateChar = resolve;
      if (data.length) {
        BleManager.write(deviceAddr, service, char, data).catch(e => {
          if (e === "You're already writing") {
            setTimeout(() => {
              BleManager.write(deviceAddr, service, char, data).catch(e => {
                console.warn('sendData error', e);
                reject(e);
                disconnect();
              });
            }, 180);
          } else {
            console.warn('sendData error', e);
            reject(e);
            disconnect();
          }
        });
      } else {
        console.log('SEND DATA ARRAY EMPTY');
      }
    });
  }
  if (data.length) {
    return new Promise((resolve, reject) => {
      BleManager.write(deviceAddr, service, char, data)
        .then(resolve)
        .catch(e => {
          if (e === "You're already writing") {
            setTimeout(() => {
              BleManager.write(deviceAddr, service, char, data)
                .then(resolve)
                .catch(e => {
                  console.warn('sendData error', e);
                  reject(e);
                  disconnect();
                });
            }, 180);
          } else {
            console.warn('sendData error', e);
            reject(e);
            disconnect();
          }
        });
    });
  }
  console.log('SEND DATA ARRAY EMPTY');
}

export function fireEventWhenDisconnect(on: boolean) {
  fireErrorWhenDisconnect = on;
}

export function disconnect() {
  isConnected = false;
  return BleManager.disconnect(deviceAddr);
}

export async function destroyBle() {
  try {
    await stopScan();
    await disconnect();
  } catch (e) {
    console.warn('destroyBle', e);
  }
}

function getDeviceId(adverPackage: any) {
  const raw = adverPackage.manufacturerData.bytes;
  try {
    if (Platform.OS === 'ios') {
      const deviceId = []; // array length = 8
      for (let i = 2; i <= 9; i++) {
        if (raw[i] === undefined) return null;
        deviceId.push(raw[i]);
      }
      return deviceId;
    }
    if (Platform.OS === 'android') {
      const deviceId = []; // array length = 8
      if (!adverPackage.manufacturerData) return false;
      for (let i = 7; i <= 14; i++) {
        if (raw[i] === undefined) return null;
        deviceId.push(raw[i]);
      }
      return deviceId;
    }
  } catch (e) {
    return null;
  }
}

const isSavyDevice = (adverPackage: any): boolean => {
  try {
    if (Platform.OS === 'ios') {
      if (!adverPackage.manufacturerData) return false; // device not have kCBAdvDataManufacturerData
      const raw = adverPackage.manufacturerData.bytes;
      if (raw[0] == 0xff && raw[1] == 0xff) {
        return true;
      }
      return false;
    }
    if (Platform.OS === 'android') {
      if (!adverPackage.manufacturerData) return false; // device not have kCBAdvDataManufacturerData
      const raw = adverPackage.manufacturerData.bytes;
      if (raw[4] == 0xff && raw[5] == 0xff) {
        return true;
      }
      return false;
    }
    return false;
  } catch (e) {
    return false;
  }
};

const getDeviceVersion = (adverPackage: any) => {
  const raw = adverPackage.manufacturerData.bytes;
  try {
    let startPos;
    if (Platform.OS === 'ios') {
      switch (raw.length) {
        case 13: // Ver 2
          startPos = 10;
          break;

        default:
          startPos = 15;
          break;
      }
    } else {
      switch (raw.length) {
        case 18: // Ver 2
          startPos = 15;
          break;

        default:
          startPos = 20;
          break;
      }
    }
    const _deviceVersion = []; // array length = 4
    for (let i = startPos; i <= startPos + 2; i++) {
      if (raw[i] === undefined) return null;
      _deviceVersion.push(raw[i]);
    }
    return _deviceVersion;
  } catch (e) {
    return null;
  }
};

const getEcuId = (adverPackage: any) => {
  const _ecuId = []; // array length = 5
  try {
    if (Platform.OS === 'ios') {
      const raw = adverPackage.manufacturerData.bytes;
      for (let i = 10; i <= 14; i++) {
        if (raw[i] === undefined) return null;
        _ecuId.push(raw[i]);
      }
    } else {
      const raw = adverPackage.manufacturerData.bytes;
      for (let i = 15; i <= 19; i++) {
        if (raw[i] === undefined) return null;
        _ecuId.push(raw[i]);
      }
    }
  } catch (e) {
    return null;
  }
  return _ecuId;
};

const compareArrays = (ar1: number[], ar2: number[]) => {
  if (ar1.length !== ar2.length) {
    return false;
  }
  for (let i = 0; i < ar1.length; i += 1) {
    if (ar1[i] !== ar2[i]) {
      return false;
    }
  }
  return true;
};
