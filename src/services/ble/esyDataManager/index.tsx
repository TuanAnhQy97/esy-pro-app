import honda from "./honda";
import yamaha from "./yamaha";
import { init } from "./esyDataManager";

export default { honda, yamaha, init };
