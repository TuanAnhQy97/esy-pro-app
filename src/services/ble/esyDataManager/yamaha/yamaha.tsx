import eventBus from "../../../eventBus";
import {
  EVENT_BUS,
  AUTO_CHECK_ONE_PROGRESS,
  BLE_ERROR
} from "../../../../constants";
import { tools as OverLayEraseError_ResetEcm_tools } from "../../../../Components/Popup/OverLayEraseError_ResetEcm";
import dashboardHandler from "./dashboardHandler";
import ble from "../..";
import errorhandler from "./errorhandler";
import { compareArrays } from "../../../../SharedFunctions";
import advanceHandler from "./advanceHandler";

const ARRAY_WAIT_FOR_ESY_REINIT = Object.freeze([0xff, 0xff, 0xff]);
const ARRAY_KLINE_ERROR = Object.freeze([0xff, 0xff, 0xff]);
export const ARRAY_SWITCH_KEY_REQUEST = Object.freeze([0xfa, 0xfa, 0xfa, 0xfa]);
export const ARRAY_SWITCH_KEY_OKAY = Object.freeze([0xfa, 0xfa, 0xfa, 0x01]);
export const ARRAY_SWITCH_KEY_FAILED = Object.freeze([0xfa, 0xfa, 0xfa, 0x00]);

type MODE = "" | "idle" | "dashboard" | "readError" | "erase" | "advance";

let currentMode: MODE = "";

export const handleUpdateValueForCharacteristic = (data: any) => {
  const { value } = data;
  // if (compareArrays(value, ARRAY_WAIT_FOR_ESY_REINIT)) {
  //   console.warn("WAIT_FOR_ESY_REINIT");
  //   return;
  // }

  if (compareArrays(value, ARRAY_KLINE_ERROR)) {
    console.warn("KLINE_ERROR");
    setTimeout(() => {
      eventBus.fireEvent(EVENT_BUS.BLE_CHANGE_ERROR, BLE_ERROR.KLINE_ERROR);
    }, 1000);
    return;
  }

  if (compareArrays(value, ARRAY_SWITCH_KEY_REQUEST)) {
    console.warn("SWITCH_KEY_REQUEST");
    eventBus.fireEvent(EVENT_BUS.SWITCH_KEY_REQUEST);
    return;
  }

  if (compareArrays(value, ARRAY_SWITCH_KEY_OKAY)) {
    console.warn("SWITCH_KEY_OKAY");
    eventBus.fireEvent(EVENT_BUS.SWITCH_KEY_OKAY);
    return;
  }

  if (compareArrays(value, ARRAY_SWITCH_KEY_FAILED)) {
    console.warn("SWITCH_KEY_FAILED");
    eventBus.fireEvent(EVENT_BUS.SWITCH_KEY_FAILED);
    return;
  }

  switch (currentMode) {
    case "dashboard":
      dashboardHandler.handleDashboardData(value);
      break;

    case "readError":
      errorhandler.handleErrorFI(value);
      break;

    case "advance":
      advanceHandler.handleAdvanceData(value);
      break;

    default:
      break;
  }
};

const jobWithTryCatch = async (job: Function) => {
  eventBus.fireEvent(EVENT_BUS.BLE_CHANGE_DATA_MODE, true);
  try {
    await job();
  } catch (e) {
    eventBus.fireEvent(EVENT_BUS.BLE_CHANGE_ERROR, e);
  }
  eventBus.fireEvent(EVENT_BUS.BLE_CHANGE_DATA_MODE, false);
};

export const toIdleMode = async () => {
  currentMode = "idle";
  await jobWithTryCatch(async () => {
    // await ble.sendData({ data: [0xfa, 0xfa, 0x08] });
  });
};

export const toDashboardMode = async () => {
  currentMode = "dashboard";
  await jobWithTryCatch(() => {
    dashboardHandler.resetObdBuffer();
    ble.sendData({ data: ble.generateCommand.goToReadFullDataYa() });
  });
};

export const toReadErrorMode = async () => {
  currentMode = "readError";
  errorhandler.resetErrorsData();
  await jobWithTryCatch(() =>
    ble.sendData({ data: ble.generateCommand.goToReadErrorYa() })
  );
};

export const toClearErrorROM = async () => {
  eventBus.fireEvent(EVENT_BUS.BIKE_ERASE_RESET_PROGRESS_CHANGE, "doing");
  OverLayEraseError_ResetEcm_tools.showPopup(true, "erase");
  currentMode = "erase";
  setTimeout(
    () =>
      jobWithTryCatch(async () => {
        await ble.sendData({ data: ble.generateCommand.goToEraseErrorYa() });
        eventBus.fireEvent(EVENT_BUS.BIKE_ERASE_RESET_PROGRESS_CHANGE, "done");

        toIdleMode();
      }),
    3000
  );
};

export const toAdvanceData = async () => {
  currentMode = "advance";
  await jobWithTryCatch(() =>
    ble.sendData({
      data: ble.generateCommand.goToAdvanceData()
      // char: ble.CHAR_DATA_ADVANCE_CONTROL,
      // service: ble.SERVICE_ADVANCE_DATA
    })
  );
};
