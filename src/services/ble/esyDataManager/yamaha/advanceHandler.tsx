import bikeLibHelper from "../../../bikeLibHelper";
import eventBus from "../../../eventBus";
import { EVENT_BUS } from "../../../../constants";
import { compareArrays } from "../../../../SharedFunctions";

const ARRAY_TURN_ON_PART = [0xff, 0xff, 0xc0];

const handleAdvanceData = (values: number[]) => {
  if (compareArrays(values, ARRAY_TURN_ON_PART)) {
    eventBus.fireEvent(EVENT_BUS.YAMAHA_ADVANCE_DATA_TRIGGER);
  } else if (values[0] === 0xff && values[1] === 0xff) {
    const value = values[4];
    const key = values[3];

    const convertedValue = bikeLibHelper.yamaha.convert.convertAdvanceData(
      key,
      value
    );

    eventBus.fireEvent(EVENT_BUS.YAMAHA_ADVANCE_DATA_CHANGE, {
      key,
      convertedValue
    });
  }
};

const advanceHandler = {
  handleAdvanceData
};
export default advanceHandler;
