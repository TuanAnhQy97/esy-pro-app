import eventBus from "../../../eventBus";
import { EVENT_BUS } from "../../../../constants";

let confirmStartA = false;
let confirmStartB = false;

let errorA: number[] | null = null;
let errorALength: number = 0;
let errorB: number[] | null = null;
let errorBLength: number = 0;

export const handleErrorFI = (values: number[]) => {
  const extractedErrors = extractErrorData(values);
  if (extractedErrors) {
    // Update UI
    eventBus.fireEvent(EVENT_BUS.YAMAHA_ERROR_LIST_CHANGE, extractedErrors);
    // Lấy lỗi tiếp
    resetErrorsData();
  }
};

const extractErrorData = (values: number[]) => {
  console.log("data", values);

  // Check header
  if (!errorA) {
    console.log("not have errorA");
    exactErrorA(values);
  } else if (errorA && errorA.length < errorALength) {
    console.log("get rest of errorA");
    exactErrorA(values);
  } else if (!errorB) {
    exactErrorB(values);
  } else if (errorB && errorB.length < errorBLength) {
    exactErrorB(values);
  } else {
    resetErrorsData();
  }
  // Get errors done
  if (errorA && errorB) {
    const realAValues = errorA.slice(8).filter((value, index) => {
      if ((index - 1) % 3 === 0) {
        return true;
      }
      return false;
    });
    const realBValues = errorB.slice(8).filter((value, index) => {
      if ((index - 1) % 3 === 0) {
        return true;
      }
      return false;
    });
    console.log("realAValues", realAValues);
    console.log("realBValues", realBValues);

    const notDuplicateList = [...realAValues];
    realBValues.forEach(v => {
      if (notDuplicateList.indexOf(v) === -1) {
        notDuplicateList.push(v);
      }
    });

    const errorA1: [number, number, 0xa1][] = [];
    const error20: [number, number, 0x20][] = [];

    const extractError = (errorList: number[]) => {
      const formatedList = errorList.slice(8); // Get list of error
      formatedList.pop(); // remove check sum

      for (let i = 0; i < formatedList.length; i += 3) {
        const type = formatedList[i + 2];
        if (type === 0xa1) {
          errorA1.push([formatedList[i], formatedList[i + 1], 0xa1]);
        } else if (type === 0x20) {
          error20.push([formatedList[i], formatedList[i + 1], 0x20]);
        }
      }
    };
    extractError(errorA);
    extractError(errorB);

    const errorGen = errorA[0] === 0xfa ? "kline" : "obd"; // FA or FF: FA - Kline, FF - OBD

    return {
      errorA: realAValues,
      errorB: realBValues,
      notDuplicateList,
      errorA1,
      error20,
      errorGen
    };
  }
  return null;
};

const exactErrorA = (values: number[]) => {
  if (!confirmStartA || !errorA) {
    if (
      (values[0] === 0xfa || values[0] === 0xff) &&
      values[1] === 0xfa &&
      values[2] === 0x05
    ) {
      errorA = [];
      // errorALength = values[7];
      // confirmStartA = true;
      // startPos = 9;
      confirmStartA = true;
      const errorCount = values[7];
      errorALength = errorCount * 3 + 1 + 3 + 5;
      console.log("exactErrorA length", errorALength);
    } else {
      resetErrorsData();
      return;
    }
  }
  for (let i = 0; i < values.length && errorA.length < errorALength; i += 1) {
    errorA.push(values[i]);
  }
};

const exactErrorB = (values: number[]) => {
  if (!confirmStartB || !errorB) {
    if (
      (values[0] === 0xfa || values[0] === 0xff) &&
      values[1] === 0xfa &&
      values[2] === 0x05
    ) {
      errorB = [];
      // errorBLength = values[7];
      // confirmStartB = true;
      // startPos = 9;
      confirmStartB = true;
      const errorCount = values[7];
      errorBLength = errorCount * 3 + 1 + 3 + 5;
    } else {
      resetErrorsData();
      return;
    }
  }
  for (let i = 0; i < values.length && errorB.length < errorBLength; i += 1) {
    errorB.push(values[i]);
  }
};

const resetErrorsData = () => {
  console.log("resetErrorsData");
  confirmStartA = false;
  confirmStartB = false;
  errorA = null;
  errorALength = 0;
  errorB = null;
  errorBLength = 0;
};

const errorhandler = { handleErrorFI, resetErrorsData, extractErrorData };
export default errorhandler;
