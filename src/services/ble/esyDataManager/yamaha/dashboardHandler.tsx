/* eslint-disable no-case-declarations */
import bikeLibHelper from "../../../bikeLibHelper";
import eventBus from "../../../eventBus";
import { EVENT_BUS } from "../../../../constants";

let odbConfirmStarted = false;
let odbDataLength = 0;
let obdData: number[] = [];

const handleDashboardData = (values: number[]) => {
  if (values.length === 8 && values[0] === 0xfa && values[1] === 0xfa) {
    const { convertData } = bikeLibHelper.yamaha.convert;
    const valueArr = values.slice(3);
    const rootKey = values[2];

    let subKeys;
    switch (rootKey) {
      case 0x01:
        subKeys = [0x001, 0x101];
        break;

      case 0x92:
        subKeys = [0x192, 0x292, 0x392, 0x492];
        break;

      case 0xa2:
        subKeys = [0x0a2, 0x1a2];
        break;

      default:
        subKeys = [rootKey];
        break;
    }

    const data: {
      key: number;
      convertedValue: number | string;
    }[] = subKeys.map(key => {
      return {
        key,
        convertedValue: convertData(key, valueArr)
      };
    });

    eventBus.fireEvent(EVENT_BUS.YAMAHA_DATA_LIST_CHANGE, {
      data,
      type: "kline"
    });
  } else {
    // eslint-disable-next-line no-lonely-if
    const HeaderLength = 3;
    if (!odbConfirmStarted) {
      if (values[0] === 0xff && values[1] === 0xfa && values[2] === 0x05) {
        odbConfirmStarted = true;
        odbDataLength = values[3] - 0x7c + HeaderLength;
      } else {
        resetObdBuffer();
      }
    }

    if (odbConfirmStarted) {
      for (
        let i = 0;
        i < values.length && obdData.length < odbDataLength;
        i += 1
      ) {
        obdData.push(values[i]);
      }
      // Enough data
      if (odbDataLength && obdData.length >= odbDataLength) {
        // Convert data
        const data: {
          key: number;
          convertedValue: number | string;
        }[] = bikeLibHelper.yamaha.convertObd.convertDataObd(
          obdData.slice(HeaderLength)
        );
        // Update UI
        eventBus.fireEvent(EVENT_BUS.YAMAHA_DATA_LIST_CHANGE, {
          data,
          type: "obd"
        });
        resetObdBuffer();
      }
    }
  }
};

const resetObdBuffer = () => {
  // Clear buffer data
  odbConfirmStarted = false;
  odbDataLength = 0;
  obdData = [];
};

const dashboardHandler = {
  handleDashboardData,
  resetObdBuffer
};
export default dashboardHandler;
