import bikeLibHelper from "../../bikeLibHelper";
import { BIKE_BRAND_LIST } from "../../../constants";
import honda from "./honda";
import yamaha from "./yamaha";
import ble from "..";

const handleUpdateValueForCharacteristic = (data: any) => {
  switch (bikeLibHelper.getCurrentBikeBrand()) {
    case BIKE_BRAND_LIST.HONDA:
      honda.handleUpdateValueForCharacteristic(data);
      break;

    case BIKE_BRAND_LIST.YAMAHA:
      yamaha.handleUpdateValueForCharacteristic(data);
      break;

    default:
      break;
  }
};

export const init = () =>
  ble.addListenerUpdateCharValue(handleUpdateValueForCharacteristic);
