import bikeLibHelper from "../../../bikeLibHelper";
import { ECU_TYPE } from "../../../../Library/BasicBikeLib";
import eventBus from "../../../eventBus";
import { EVENT_BUS } from "../../../../constants";
import ble from "../..";

let confirmStart = false;
let errors: number[][] = [];
let errorLineCount = 0;

export const handleErrorFI = (value: number[]) => {
  const extractedData = ble.extractDataFromResponse(value);
  const extractedErrors = extractErrorData(extractedData);
  if (extractedErrors) {
    // Lấy lỗi tiếp
    eventBus.fireEvent(EVENT_BUS.BIKE_ERROR_FI_LIST_CHANGE, extractedErrors);
    resetErrorsData();
  }
};

const extractErrorData = (value: number[]) => {
  // Check header
  if (value[0] === 2 && value[2] === 115) {
    errorLineCount += 1;
    if (value[1] === 7) return;

    for (let i = 5; i < value.length - 1; i += 2) {
      if (value[i] !== 0) {
        const error = [value[i], value[i + 1]];
        errors.push(error);
      }
    }
    const maxLine =
      bikeLibHelper.honda._hondaLib.ecuType === ECU_TYPE.TYPE_A ? 10 : 11;
    if (errorLineCount === maxLine) {
      return errors;
    }
    return null;
  }
  resetErrorsData();
  console.log("extractErrorData", "SKIP wrong format ");
  return null;
};

const resetErrorsData = () => {
  confirmStart = false;
  errorLineCount = 0;
  errors = [];
};

const errorhandler = { handleErrorFI, resetErrorsData, extractErrorData };
export default errorhandler;
