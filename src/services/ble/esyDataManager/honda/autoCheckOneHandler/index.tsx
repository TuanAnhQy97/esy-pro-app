import { DATA1234 } from "../../../../../DiagnosticHelper/DiagnosticHelper";
import errorhandler from "../errorhandler";
import combo1234Handler from "../combo1234Handler";
import eventBus from "../../../../eventBus";
import { EVENT_BUS, AUTO_CHECK_ONE_PROGRESS } from "../../../../../constants";
import DataForReport from "../../../../../Screens/ReportScreen/DataForReport";
import ble from "../../..";
import bikeLibHelper from "../../../../bikeLibHelper";

const MIN_REAL_TIME_SAMPLE = 10;

let dataType: "errorData" | "realTimeData" = "errorData";

export const resetDataType = () => {
  dataType = "errorData";
};

/**
 * Buffer
 */
let errorList: number[][] = [];
let realTimeSampleSink: DATA1234[] = [];
let realTimeAverage: DATA1234 = { data1: [], data2: [], data3: [], data4: [] };
/**
 * Xử lý dữ liệu nhận được
 * @param {*} value
 */
const handleData = async (value: number[]) => {
  //  Lấy dữ liệu lỗi FI
  if (dataType === "errorData") {
    const extractedData = ble.extractDataFromResponse(value);
    const _errorList = errorhandler.extractErrorData(extractedData);
    if (_errorList) {
      errorList = _errorList;
      // Đủ dữ liệu -> Chuyển sang combo1234
      combo1234Handler.resetBufferData1234();
      dataType = "realTimeData";
      ble.esyDataManager.honda.toAutoCheckOneRealTime();
    }
  } else {
    // Lấy dữ liệu data list
    const data1234 = combo1234Handler.extractData1234(value);
    if (data1234) {
      const convertedData1 = bikeLibHelper.honda.convert.convertData1(
        data1234.data1
      );
      const rpm = convertedData1[1];
      if (rpm !== 0) {
        // Đang nổ máy
        eventBus.fireEvent(
          EVENT_BUS.BIKE_AUTO_CHECK_ONE_PROGRESS_CHANGE,
          AUTO_CHECK_ONE_PROGRESS.SHOW_WARNING
        );
        // reset buffer
        if (realTimeSampleSink.length !== 0) {
          realTimeSampleSink = [];
        }
      } else {
        // Đã tắt máy
        // Xử lý xong rồi -> k cần làm gì nữa
        if (realTimeSampleSink.length === MIN_REAL_TIME_SAMPLE) {
          return;
        }
        // Thêm vào mảng
        if (realTimeSampleSink.length < MIN_REAL_TIME_SAMPLE) {
          realTimeSampleSink.push(data1234);
        }
        // Đủ dữ liệu rồi -> Xử lý -> Hiển thị
        if (realTimeSampleSink.length === MIN_REAL_TIME_SAMPLE) {
          realTimeAverage = calculateAverage(realTimeSampleSink);
          // Update UI
          eventBus.fireEvent(
            EVENT_BUS.BIKE_AUTO_CHECK_ONE_PROGRESS_CHANGE,
            AUTO_CHECK_ONE_PROGRESS.SHOW_RESULT
          );
          eventBus.fireEvent(EVENT_BUS.BIKE_AUTO_CHECK_ONE_RESULT_CHANGE, {
            errorList,
            dataList: realTimeAverage
          });
          // Lưu giá trị vào report
          DataForReport.setAutoCheckOne({
            errorList,
            dataList: realTimeAverage
          });
        } else {
          // Đang lấy dữ liệu
          eventBus.fireEvent(
            EVENT_BUS.BIKE_AUTO_CHECK_ONE_PROGRESS_CHANGE,
            AUTO_CHECK_ONE_PROGRESS.PROCESSING
          );
        }
      }
    }
  }
};
/**
 * Reset các buffer của Auto Check One
 */
const resetAutoCheckOneBuffer = () => {
  errorList = [];
  realTimeSampleSink = [];
  realTimeAverage = { data1: [], data2: [], data3: [], data4: [] };
};

const calculateAverage = (sampleSink: DATA1234[]) => {
  const sumOfData1: any = [];
  const sumOfData2: any = [];
  const sumOfData3: any = [];
  const sumOfData4: any = [];
  // Calculate sum
  for (let i = 0; i < sampleSink.length; i++) {
    const { data1 } = sampleSink[i];
    const { data2 } = sampleSink[i];
    const { data3 } = sampleSink[i];
    const { data4 } = sampleSink[i];

    mergeValue2Array(sumOfData1, data1);
    mergeValue2Array(sumOfData2, data2);
    mergeValue2Array(sumOfData3, data3);
    mergeValue2Array(sumOfData4, data4);
  }
  // Calculate aver.
  divideElementTo(sumOfData1, sampleSink.length);
  divideElementTo(sumOfData2, sampleSink.length);
  divideElementTo(sumOfData3, sampleSink.length);
  divideElementTo(sumOfData4, sampleSink.length);

  return {
    data1: sumOfData1,
    data2: sumOfData2,
    data3: sumOfData3,
    data4: sumOfData4
  };
};

/**
 * Cộng đè giá trị của 2 mảng số
 * @param {*} arr1 đích
 * @param {*} arr2 nguồn
 */
const mergeValue2Array = (arr1: number[], arr2: number[]) => {
  for (let i = 0; i < arr2.length; i++) {
    // Nếu chưa được khời tạo giá trị
    if (arr1[i] === undefined) {
      arr1[i] = 0;
    }
    arr1[i] += arr2[i];
  }
};
/**
 * Chia tất cả phần tử của mảng cho 1 số
 * @param {*} arr mảng số
 * @param {*} n số chia
 */
const divideElementTo = (arr: number[], n: number) => {
  for (let i = 0; i < arr.length; i++) {
    arr[i] = arr[i] / n;
  }
  return arr;
};

const autoCheckOneHandler = {
  resetDataType,
  handleData,
  resetAutoCheckOneBuffer
};

export default autoCheckOneHandler;
