import combo1234Handler from "./combo1234Handler";
import bikeLibHelper from "../../../bikeLibHelper";
import eventBus from "../../../eventBus";
import { EVENT_BUS } from "../../../../constants";

const handleData = (value: number[]) => {
  const data1234 = combo1234Handler.extractData1234(value);

  if (data1234) {
    const convertData1 = bikeLibHelper.honda.convert.convertData1(
      data1234.data1
    );
    const convertData2 = bikeLibHelper.honda.convert.convertData2(
      data1234.data2
    );

    const data = {
      data1: convertData1,
      data2: convertData2
    };

    eventBus.fireEvent(EVENT_BUS.BIKE_AUTO_CHECK_TWO_RESULT_CHANGE, data);
  }
};

const autoCheckTwoHandler = {
  handleData,
  resetBuffer: combo1234Handler.resetBufferData1234
};
export default autoCheckTwoHandler;
