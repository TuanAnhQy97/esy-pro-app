import combo1234Handler from "./combo1234Handler";
import bikeLibHelper from "../../../bikeLibHelper";
import eventBus from "../../../eventBus";
import { EVENT_BUS } from "../../../../constants";

const handleDashboardData = (value: number[]) => {
  const data1234 = combo1234Handler.extractData1234(value);

  if (data1234) {
    const convertData1 = bikeLibHelper.honda.convert.convertData1(
      data1234.data1
    );
    const convertData2 = bikeLibHelper.honda.convert.convertData2(
      data1234.data2
    );

    const extraData1 = bikeLibHelper.honda.convert.convertData1Pro(
      data1234.data1
    );
    const extraData2 = bikeLibHelper.honda.convert.convertData2Pro(
      data1234.data2
    );
    const extraData3 = bikeLibHelper.honda.convert.convertData3Pro(
      data1234.data3
    );
    const extraData4 = bikeLibHelper.honda.convert.convertData4Pro(
      data1234.data4
    );

    const data = {
      data1: convertData1,
      data2: convertData2,
      /**
       * Extra Data
       */
      extraData1,
      extraData2,
      extraData3,
      extraData4
    };

    eventBus.fireEvent(EVENT_BUS.BIKE_DASHBOARD_VALUE_CHANGE, data);
  }
};

const dashboardHandler = {
  handleDashboardData,
  resetBuffer: combo1234Handler.resetBufferData1234
};
export default dashboardHandler;
