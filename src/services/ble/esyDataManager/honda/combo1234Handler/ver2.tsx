/**
 *      BUFFERs
 */
let confirmStartD1 = false;
let confirmStartD2 = false;
let confirmStartD3 = false;
let confirmStartD4 = false;
let hasD1 = false;
let hasD2 = false;
let hasD3 = false;
let hasD4 = false;
let dataLength: number;
let data1: number[] = [];
let data2: number[] = [];
let data3: number[] = [];
let data4: number[] = [];

const combo1234Handler = {
  extractData1234,
  resetBufferData1234
};
export default combo1234Handler;

function resetBufferData1234(): void {
  confirmStartD1 = false;
  confirmStartD2 = false;
  confirmStartD3 = false;
  confirmStartD4 = false;
  hasD1 = false;
  hasD2 = false;
  hasD3 = false;
  hasD4 = false;
  data1 = [];
  data2 = [];
  data3 = [];
  data4 = [];
}

function extractData1234(value: number[]) {
  if (!hasD1) {
    extractData1(value, 3);
    return;
  }
  if (!hasD2) {
    // has data1, now get data2
    extractData2(value, 3);
  } else if (!hasD3) {
    // has data2, now get data3
    extractData3(value, 3);
  } else if (!hasD4) {
    // has data3, now get data4
    extractData4(value, 3);
  }

  if (hasD1 && hasD2 && hasD3 && hasD4) {
    const data1234 = {
      data1: [...data1],
      data2: [...data2],
      data3: [...data3],
      data4: [...data4]
    };
    resetBufferData1234();
    return data1234;
  }
  return undefined;
}

function extractData1(value: number[], startPos: number = 0) {
  // get data1
  if (!confirmStartD1) {
    const DATA_TYPE = 0x01;
    if (
      value[0] === 0xfa &&
      value[1] === 0xfa &&
      value[2] === DATA_TYPE &&
      value[3] === 0x02
    ) {
      confirmStartD1 = true;
      dataLength = value[startPos + 1];
      // if error, reset process
      if (!dataLength || dataLength < 4) {
        resetBufferData1234();
      } else {
        // push values to array
        for (let i = startPos; i < value.length; i++) {
          data1.push(value[i]);
          // if has enough data, skip to data2
          if (data1.length >= dataLength) {
            hasD1 = true;
            break;
          }
        }
      }
    } else {
      resetBufferData1234();
    }
  } else {
    // get the rest data1
    for (let i = 0; i < value.length; i++) {
      data1.push(value[i]);
      if (data1.length >= dataLength) {
        hasD1 = true;
        break;
      }
    }
  }
}

function extractData2(value: number[], startPos: number = 0): void {
  // get data2
  if (!confirmStartD2) {
    const DATA_TYPE = 0x02;
    if (
      value[0] === 0xfa &&
      value[1] === 0xfa &&
      value[2] === DATA_TYPE &&
      value[3] === 0x02
    ) {
      confirmStartD2 = true;
      dataLength = value[startPos + 1];
      // if error, reset process
      if (!dataLength || dataLength < 4) {
        resetBufferData1234();
      } else {
        // push values to array
        for (let i = startPos; i < value.length; i++) {
          data2.push(value[i]);
          if (data2.length >= dataLength) {
            hasD2 = true;
            break;
          }
        }
      }
    } else {
      resetBufferData1234();
    }
  } else {
    // get the rest data2
    for (let i = 0; i < value.length; i++) {
      data2.push(value[i]);
      if (data2.length >= dataLength) {
        hasD2 = true;
        break;
      }
    }
  }
}

function extractData3(value: number[], startPos: number = 0): void {
  // get data3
  if (!confirmStartD3) {
    const DATA_TYPE = 0x03;
    if (
      value[0] === 0xfa &&
      value[1] === 0xfa &&
      value[2] === DATA_TYPE &&
      value[3] === 0x02
    ) {
      confirmStartD3 = true;
      dataLength = value[startPos + 1];
      // if error, reset process
      if (!dataLength || dataLength < 4) {
        resetBufferData1234();
      } else {
        // push values to array
        for (let i = startPos; i < value.length; i++) {
          data3.push(value[i]);
          if (data3.length >= dataLength) {
            hasD3 = true;
            break;
          }
        }
      }
    } else {
      resetBufferData1234();
    }
  } else {
    // get the rest data3
    for (let i = 0; i < value.length; i++) {
      data3.push(value[i]);
      if (data3.length >= dataLength) {
        hasD3 = true;
        break;
      }
    }
  }
}

function extractData4(value: number[], startPos: number = 0): void {
  // get data4
  if (!confirmStartD4) {
    const DATA_TYPE = 0x04;
    if (
      value[0] === 0xfa &&
      value[1] === 0xfa &&
      value[2] === DATA_TYPE &&
      value[3] === 0x02
    ) {
      confirmStartD4 = true;
      dataLength = value[startPos + 1];
      // if error, reset process
      if (!dataLength || dataLength < 4) {
        resetBufferData1234();
      } else {
        // push values to array
        for (let i = startPos; i < value.length; i++) {
          data4.push(value[i]);
          if (data4.length >= dataLength) {
            hasD4 = true;
            break;
          }
        }
      }
    } else {
      resetBufferData1234();
    }
  } else {
    // get the rest data4
    for (let i = 0; i < value.length; i++) {
      data4.push(value[i]);
      if (data4.length >= dataLength) {
        hasD4 = true;
        break;
      }
    }
  }
}
