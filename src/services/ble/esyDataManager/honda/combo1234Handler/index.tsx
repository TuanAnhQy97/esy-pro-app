import ver1 from "./ver1";
import ver2 from "./ver2";

import ble from "../../../index";

const extractData1234 = (value: number[]) => {
  switch (ble.esyVersion) {
    case "ver1":
      return ver1.extractData1234(value);

    case "ver2":
      return ver2.extractData1234(value);

    default:
      break;
  }
};

const resetBufferData1234 = () => {
  switch (ble.esyVersion) {
    case "ver1":
      ver1.resetBufferData1234();
      break;

    case "ver2":
      ver2.resetBufferData1234();
      break;

    default:
      break;
  }
};

const combo1234Handler = {
  extractData1234,
  resetBufferData1234
};
export default combo1234Handler;
