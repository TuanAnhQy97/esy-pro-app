import eventBus from '../../../eventBus';
import {EVENT_BUS, AUTO_CHECK_ONE_PROGRESS, BLE_ERROR} from '../../../../constants';
import {tools as OverLayEraseError_ResetEcm_tools} from '../../../../Components/Popup/OverLayEraseError_ResetEcm';
import dashboardHandler from './dashboardHandler';
import ble from '../..';
import errorhandler from './errorhandler';
import idleHandler from './idleHandler';
import autoCheckOneHandler from './autoCheckOneHandler';
import autoCheckTwoHandler from './autoCheckTwoHandler';
import combo1234Handler from './combo1234Handler';
import {compareArrays} from '../../../../SharedFunctions';

const ARRAY_WAIT_FOR_ESY_REINIT = Object.freeze([[0x69, 0x69, 0x69]]);
const ARRAY_KLINE_ERROR = Object.freeze([0xfa, 0xfa, 0xff, 0xff]);

type MODE = '' | 'idle' | 'dashboard' | 'readError' | 'reset' | 'erase' | 'autoCheckOne' | 'autoCheckTwo' | 'updateECU';

let currentMode: MODE = '';

export const handleUpdateValueForCharacteristic = (data: any) => {
  const {value} = data;
  if (compareArrays(value, ARRAY_WAIT_FOR_ESY_REINIT)) {
    console.warn('WAIT_FOR_ESY_REINIT');
    return;
  }

  if (compareArrays(value, ARRAY_KLINE_ERROR)) {
    console.warn('KLINE_ERROR');
    eventBus.fireEvent(EVENT_BUS.BLE_CHANGE_ERROR, BLE_ERROR.KLINE_ERROR);
    return;
  }

  switch (currentMode) {
    case 'idle':
      idleHandler.handleIdle(value);
      break;

    case 'dashboard':
      dashboardHandler.handleDashboardData(value);
      break;

    case 'readError':
      errorhandler.handleErrorFI(value);
      break;

    case 'autoCheckOne':
      autoCheckOneHandler.handleData(value);
      break;

    case 'autoCheckTwo':
      autoCheckTwoHandler.handleData(value);
      break;

    default:
      break;
  }
};

const jobWithTryCatch = async (job: Function) => {
  eventBus.fireEvent(EVENT_BUS.BLE_CHANGE_DATA_MODE, true);
  try {
    await job();
  } catch (e) {
    eventBus.fireEvent(EVENT_BUS.BLE_CHANGE_ERROR, e);
  }
  eventBus.fireEvent(EVENT_BUS.BLE_CHANGE_DATA_MODE, false);
};

export const toIdleMode = async () => {
  currentMode = 'idle';
  await jobWithTryCatch(async () => {
    await ble.sendData({data: ble.generateCommand.goToCombo1234Mode()});
  });
};

export const toDashboardMode = async () => {
  currentMode = 'dashboard';
  combo1234Handler.resetBufferData1234();
  await jobWithTryCatch(() => ble.sendData({data: ble.generateCommand.goToCombo1234Mode()}));
};

export const toReadErrorMode = async () => {
  currentMode = 'readError';
  errorhandler.resetErrorsData();
  await jobWithTryCatch(() => ble.sendData({data: ble.generateCommand.goToReadError()}));
};

export const toAutoCheckOne = async () => {
  autoCheckOneHandler.resetDataType();
  autoCheckOneHandler.resetAutoCheckOneBuffer();
  errorhandler.resetErrorsData();
  currentMode = 'autoCheckOne';
  eventBus.fireEvent(EVENT_BUS.BIKE_AUTO_CHECK_ONE_PROGRESS_CHANGE, AUTO_CHECK_ONE_PROGRESS.INIT);
  await jobWithTryCatch(() => ble.sendData({data: ble.generateCommand.goToReadError()}));
};

export const toAutoCheckOneRealTime = async () => {
  currentMode = 'autoCheckOne';
  combo1234Handler.resetBufferData1234();
  await jobWithTryCatch(() => ble.sendData({data: ble.generateCommand.goToCombo1234Mode()}));
};

export const toAutoCheckTwo = async () => {
  currentMode = 'autoCheckTwo';
  combo1234Handler.resetBufferData1234();
  await jobWithTryCatch(() => ble.sendData({data: ble.generateCommand.goToCombo1234Mode()}));
};

export const toResetECU = async () => {
  eventBus.fireEvent(EVENT_BUS.BIKE_ERASE_RESET_PROGRESS_CHANGE, 'doing');
  OverLayEraseError_ResetEcm_tools.showPopup(true, 'reset');
  currentMode = 'reset';
  setTimeout(
    () =>
      jobWithTryCatch(async () => {
        let response = await ble.sendData({data: ble.generateCommand.goToResetECU(), waitForResponse: true}) as number[];
        response = ble.extractDataFromResponse(response);
        while (!response || !compareArrays(response.slice(0, 3), [2, 5, 96])) {
          response = await ble.sendData({ data: [], waitForResponse: true }) as number[];
          response = ble.extractDataFromResponse(response);
        }
        await new Promise(r => setTimeout(r, 3000));
        eventBus.fireEvent(EVENT_BUS.BIKE_ERASE_RESET_PROGRESS_CHANGE, 'done');
        toIdleMode();
      }),
    0,
  );
};

export const toClearErrorROM = async () => {
  eventBus.fireEvent(EVENT_BUS.BIKE_ERASE_RESET_PROGRESS_CHANGE, 'doing');
  OverLayEraseError_ResetEcm_tools.showPopup(true, 'erase');
  currentMode = 'erase';
  setTimeout(
    () =>
      jobWithTryCatch(async () => {
        let response = await ble.sendData({ data: ble.generateCommand.goToClearErrorROM(), waitForResponse: true }) as number[];
        response = ble.extractDataFromResponse(response);
        while (!response || !compareArrays(response.slice(0, 3), [2, 5, 96])) {
          response = await ble.sendData({ data: [], waitForResponse: true }) as number[];
          response = ble.extractDataFromResponse(response);
        }
        await new Promise(r => setTimeout(r, 3000));
        eventBus.fireEvent(EVENT_BUS.BIKE_ERASE_RESET_PROGRESS_CHANGE, 'done');
        toIdleMode();
      }),
    0,
  );
};

export const toUpdateECU = (on: boolean) => {
  if (on) {
    jobWithTryCatch(async () => {
      await ble.sendData({data: ble.generateCommand.goToUpdateEcu()});
      ble.fireEventWhenDisconnect(false);
      currentMode = 'updateECU';
    });
  } else {
    ble.disconnect();
    ble.fireEventWhenDisconnect(true);
  }
};
