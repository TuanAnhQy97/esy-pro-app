import combo1234Handler from "./combo1234Handler";
import bikeLibHelper from "../../../bikeLibHelper";
import eventBus from "../../../eventBus";
import { EVENT_BUS } from "../../../../constants";

const handleIdle = (value: number[]) => {
  const data1234 = combo1234Handler.extractData1234(value);

  if (data1234) {
    const data1 = bikeLibHelper.honda.convert.convertData1(data1234.data1);
    const extraData3 = bikeLibHelper.honda.convert.convertData3Pro(
      data1234.data3
    );

    const mill = extraData3[0];
    const battery = data1[4];
    const rpm = data1[1];

    eventBus.fireEvent(EVENT_BUS.BIKE_IDLE_MODE_DATA_CHANGE, {
      mill,
      battery,
      rpm
    });
  }
};

const idleHandler = {
  handleIdle,
  resetBuffer: combo1234Handler.resetBufferData1234
};
export default idleHandler;
