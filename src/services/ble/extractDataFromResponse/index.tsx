import { BLE_RESPONSE_DATA_TYPE } from "../../../constants";
import { esyVersion } from "../ble";

export default (data: number[], dataType?: string) => {
  switch (esyVersion) {
    case "ver1":
      return exactForVer1(data, dataType);
    case "ver2":
      return exactForVer2(data, dataType);

    default:
      return data;
  }
};

const exactForVer1 = (data: number[], dataType?: string) => {
  switch (dataType) {
    case BLE_RESPONSE_DATA_TYPE.SECURITY:
      return data;
    case BLE_RESPONSE_DATA_TYPE.ECU_ID:
      return data;
    default:
      return data;
  }
};

const exactForVer2 = (data: number[], dataType?: string) => {
  switch (dataType) {
    case BLE_RESPONSE_DATA_TYPE.SECURITY:
      return data.slice(2);
    case BLE_RESPONSE_DATA_TYPE.ECU_ID:
      return data.slice(7, 12);
    default:
      return data.slice(2);
  }
};
