/**
 * @flow
 */
import {
  RemoteMessage,
  Notification,
  NotificationOpen
} from "react-native-firebase";

import firebase from "react-native-firebase";
import { Linking, Platform } from "react-native";

let onTokenRefreshListener;
let messageListener;
let notificationDisplayedListener;
let notificationListener;
let notificationOpenedListener;

const NOTIFICATION_DEFAULT_CHANNEL_ID = "esyProNotification";

function setUpNotificationChannel() {
  // Build a channel
  const channel = new firebase.notifications.Android.Channel(
    NOTIFICATION_DEFAULT_CHANNEL_ID,
    "ESY Pro notification",
    firebase.notifications.Android.Importance.Max
  ).setDescription("ESY Pro channel");

  // Create the channel
  firebase.notifications().android.createChannel(channel);
}

/**
 * Setup every basic thing to use noti
 */
export async function initNotification() {
  const permission = await firebase.messaging().hasPermission();
  if (permission) {
    // user has permissions
    console.log("User has permission");
    registerNotificationListener();
  } else {
    try {
      await firebase.messaging().requestPermission();
      console.log("User has authorised");
      // User has authorised
      setUpNotificationChannel();
    } catch (error) {
      console.log("User has rejected permissions");
      // User has rejected permissions
      // Alert something
    }
  }
}

/**
 * Retrieve the current registration token
 */
async function retrieveFCMtoken() {
  const fcmToken = await firebase.messaging().getToken();
  if (fcmToken) {
    console.log("fcmToken", fcmToken);
    // user has a device token
  } else {
    // user doesn't have a device token yet
  }
  return fcmToken;
}
/**
 * Register Callback when token is refreshed
 * @Use when app start
 */
function registerOnTokenRefreshCallback() {
  onTokenRefreshListener = firebase
    .messaging()
    .onTokenRefresh((fcmToken: string) => {
      console.log("registerOnTokenRefreshCallback", fcmToken);
    });
}
/**
 * Cancel Callback when token is refreshed
 * @Use when app close
 */
function cancelOnTokenRefreshCallback() {
  onTokenRefreshListener();
}
/**
 * Listen for FCM messages
 * A message will trigger the onMessage listener
 * when the application receives a message in the foreground.
 * @Use when app start
 */
function listenForFCMmessages() {
  messageListener = firebase.messaging().onMessage((message: RemoteMessage) => {
    // Process your message as required
    console.log("listenForFCMmessages", message);
  });
}
/**
 * Triggered when a particular notification has been displayed
 */
function onNotificationDisplayedCallback() {
  notificationDisplayedListener = firebase
    .notifications()
    .onNotificationDisplayed((notification: Notification) => {
      console.log("onNotificationDisplayedCallback", notification);
      // Process your notification as required
      // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
    });
}
/**
 * Triggered when a particular notification has been received
 */
function onNotificationReceived() {
  notificationListener = firebase
    .notifications()
    .onNotification((notification: Notification) => {
      console.log("onNotificationReceived", notification);
      // Process your notification as required
      if (Platform.OS === "android") {
        const channel = new firebase.notifications.Android.Channel(
          NOTIFICATION_DEFAULT_CHANNEL_ID,
          "ESY Pro",
          firebase.notifications.Android.Importance.Max
        ).setDescription("ESY Pro");
        // Create the channel
        firebase.notifications().android.createChannel(channel);
      }
      const _notification = new firebase.notifications.Notification()
        .setNotificationId(`reDisplay${Date.now().toString()}`)
        .setTitle(notification.title)
        .setSubtitle(notification.subtitle)
        .setBody(notification.body)
        .setData(notification.data);
      _notification.android
        .setChannelId(NOTIFICATION_DEFAULT_CHANNEL_ID)
        .android.setSmallIcon("ic_stat_ic_notification")
        .android.setLargeIcon("ic_stat_ic_notification");
      firebase.notifications().displayNotification(_notification);
    });
}
/**
 * Listen for a Notification being opened
 * If your app is in the foreground, or background,
 * you can listen for when a notification is clicked / tapped / opened as follows:
 */
function onNotificationOpened() {
  notificationOpenedListener = firebase
    .notifications()
    .onNotificationOpened((notificationOpen: NotificationOpen) => {
      // Get the action triggered by the notification being opened
      const { action } = notificationOpen;
      // Get information about the notification that was opened
      const { notification } = notificationOpen;
      openLinkFromNoti(notification);
    });
}
/**
 * If your app is closed,
 * you can check if it was opened by a notification
 * being clicked / tapped / opened as follows:
 * @Use when App start
 */
export async function checkIfAppIsOpenedByNotification(
  onlyCheck: boolean = false
) {
  const notificationOpen: NotificationOpen = await firebase
    .notifications()
    .getInitialNotification();
  console.log(
    "checkIfAppIsOpenedByNotification",
    "onlyCheck",
    onlyCheck,
    notificationOpen
  );
  if (notificationOpen) {
    // App was opened by a notification
    // Get the action triggered by the notification being opened
    const { action } = notificationOpen;
    // Get information about the notification that was opened
    const { notification } = notificationOpen;
    if (!onlyCheck) {
      await openLinkFromNoti(notification);
    }
    return true;
  }
  return false;
}
/**
 *
 */
function registerNotificationListener() {
  listenForFCMmessages();
  onNotificationDisplayedCallback();
  onNotificationReceived();
  onNotificationOpened();
}

/**
 * Cancel listener
 * @Use when app stop
 */
export function cancelListenerForFCMmessages() {
  messageListener();
  notificationDisplayedListener();
  notificationListener();
  notificationOpenedListener();
}

const openLinkFromNoti = async (noti: Notification) => {
  const { data } = noti;
  const { link } = data;
  if (link) {
    await Linking.openURL(link);
  }
};
