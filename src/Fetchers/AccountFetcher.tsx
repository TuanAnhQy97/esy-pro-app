/**
 * @flow
 */
import { ROOT_DOMAIN } from "./../Config";
import { executeFunction, post } from "./Shared";

const URL_CREATE_NEW_ACCOUNT = ROOT_DOMAIN + "account/create",
  URL_IS_PHONE_EXIST = ROOT_DOMAIN + "account/isPhoneExist",
  URL_RESET_PASSWORD = ROOT_DOMAIN + "account/resetPw",
  URL_GET_USER_INFO = ROOT_DOMAIN + "account/getInfo";

/**
 * Tạo tài khoản mới
 * @param {*} activeToken active token từ server
 * @param {*} idToken từ firebase sau khi xác nhận số đt
 */
async function createNewAccount(
  activeToken: string,
  idToken: any,
  userInfo: Object,
  deviceId: number[]
) {
  let res;
  res = await executeFunction(() =>
    post(URL_CREATE_NEW_ACCOUNT, {
      activeToken: activeToken,
      idToken,
      userInfo,
      deviceId
    })
  );
  return res;
}
/**
 * Kiểm tra xem số điện thoại đã tồn tại hay chưa
 * @param {*} phone
 */
async function isPhoneNumberExist(phone: string) {
  let res;
  res = await executeFunction(() =>
    post(URL_IS_PHONE_EXIST, {
      phone: phone
    })
  );
  return res;
}
/**
 * Đặt lại mật khẩu
 * @param {*} newPass
 * @param {*} idToken
 */
async function resetPassword(newPass: string, idToken: string) {
  let res;
  res = await executeFunction(() =>
    post(URL_RESET_PASSWORD, {
      newPass,
      idToken
    })
  );
  return res;
}
/**
 * Lấy về thông tin người dùng
 */
async function getInfo() {
  let res;
  res = await executeFunction(() => post(URL_GET_USER_INFO));
  return res;
}

export const AccountFetcher = {
  createNewAccount,
  isPhoneNumberExist,
  resetPassword,
  getInfo
};
