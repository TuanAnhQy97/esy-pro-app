/**
 * @flow
 */
import { ROOT_DOMAIN } from "./../Config";
import { executeFunction, post } from "./Shared";

const URL_GET_BIKELIB = ROOT_DOMAIN + "device/getLibVer2",
  URL_GET_BIKELIBPRO = ROOT_DOMAIN + "device/getLibProVer2",
  URL_UPDATE_DEVICE_VER = ROOT_DOMAIN + "device/updateInfo/deviceVer",
  URL_LOAD_DEVICE = ROOT_DOMAIN + "device/mydevice",
  URL_CHECK_SEED_KEY = ROOT_DOMAIN + "device/checkSeedKey",
  URL_GET_BIKELIBVER3 = ROOT_DOMAIN + "device/getLibVer3";

/**
 * =============================================================================
 *                        FOR PAIRED
 * =============================================================================
 */

/**
 * Get Bike lib from server
 * @param {*} ecuId ecu id or that bike
 */
async function getBikeLibFromServer(ecuId: number[]) {
  let res = await executeFunction(() => post(URL_GET_BIKELIB, { ecuId }));
  return res;
}
/**
 * Get Bike lib pro from server
 * @param {*} ecuId ecu id or that bike
 */
async function getBikeLibProFromServer(ecuId: number[]) {
  let res = await executeFunction(() => post(URL_GET_BIKELIBPRO, { ecuId }));
  return res;
}
/**
 * Get Bike lib VER 3 from server
 * @param {*} ecuId ecu id or that bike
 */
async function getBikeLibVer3FromServer(ecuId: number[]) {
  let res = await executeFunction(() => post(URL_GET_BIKELIBVER3, { ecuId }));
  return res;
}
/**
 * Fetch owned ESY from Server
 */
async function getPairedDevice() {
  let res = await executeFunction(() => post(URL_LOAD_DEVICE));
  return res;
}
/**
 * Send update info of device to server
 */
async function updateDeviceVerOnServer(deviceId: number[], version: number[]) {
  let res;
  res = await executeFunction(() =>
    post(URL_UPDATE_DEVICE_VER, { deviceId, version })
  );
  return res;
}
/**
 * Check seed key pair
 * @param {*} s1 random seed from app
 * @param {*} s2 received key from ESY
 */
async function checkSeedKeyThroughServer(s1: number[], s2: number[]) {
  let res;
  res = await executeFunction(() => post(URL_CHECK_SEED_KEY, { s1, s2 }));
  return res;
}

export const fetchDataForPaired = {
  getPairedDevice,
  getBikeLibFromServer,
  getBikeLibProFromServer,
  checkSeedKeyThroughServer,
  getBikeLibVer3FromServer
};
/**
 * =============================================================================
 *                        FOR ACTIVE NEW
 * =============================================================================
 */
const URL_CHECK_DEVICE = ROOT_DOMAIN + "device/checkBrandNew",
  URL_GET_KEY_S3 = ROOT_DOMAIN + "device/getKeyS3",
  URL_GET_ACTIVE_TOKEN = ROOT_DOMAIN + "device/getActiveToken",
  URL_CREATE_NEW_ACCOUNT = ROOT_DOMAIN + "account/create";
/**
 * Kiểm tra xem thiết bị có còn zin không?
 * @param {*} deviceId
 */
async function checkDevice(deviceId: number[]) {
  let res;
  // res = { data: true };
  res = await executeFunction(() => post(URL_CHECK_DEVICE, { deviceId }));
  return res;
}
/**
 * Lấy key s3 cho việc xác nhận active device
 * @param {*} deviceId
 * @param {*} s1 key s1
 * @param {*} s2 key s2
 */
async function getKeyS3(deviceId: number[], s1: number[], s2: number[]) {
  let res;
  // res = { data: [0, 0, 0, 3] };
  res = await executeFunction(() => post(URL_GET_KEY_S3, { deviceId, s1, s2 }));
  return res;
}
/**
 * Lấy token active device
 * @param {*} deviceId
 * @param {*} s1 key s1
 * @param {*} s2 key s2
 * @param {*} s3 key s3
 */
async function getActiveToken(
  deviceId: number[],
  s1: number[],
  s2: number[],
  s3: number[]
) {
  let res;
  // res = { data: "tokenBladsadas" };
  res = await executeFunction(() =>
    post(URL_GET_ACTIVE_TOKEN, { deviceId, s1, s2, s3 })
  );
  return res;
}
/**
 * Tạo tài khoản mới
 * @param {*} activeToken active token từ server
 * @param {*} user từ firebase sau khi xác nhận số đt
 */
async function createNewAccount(activeToken: string, user: any) {
  let res;
  res = await executeFunction(() =>
    post(URL_CREATE_NEW_ACCOUNT, { token: activeToken, user })
  );
  return res;
}
export const fetchDataToActive = {
  checkDevice,
  getKeyS3,
  getActiveToken
};
