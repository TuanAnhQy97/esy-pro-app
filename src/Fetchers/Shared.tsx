/**
 * @flow
 */

import { Alert } from "react-native";
import { logout } from "./../SharedFunctions";
import { ROOT_DOMAIN } from "./../Config";
import ASHelper from "../ASHelper";
import { Credentials } from "../Model/Credentials";

const ERROR_NO_NETWORK = 1,
  ERROR_NOT_LOGIN_YET = 2,
  ERROR_NOT_VALID_TOKEN = 3,
  ERROR_SERVER_DOWN = 4,
  ERROR_SERVER_UNKNOWN = 5,
  ERROR_NO_INFO = 6;

export const FETCH_ERROR = {
  ERROR_NO_NETWORK,
  ERROR_NOT_LOGIN_YET,
  ERROR_NOT_VALID_TOKEN,
  ERROR_SERVER_DOWN,
  ERROR_SERVER_UNKNOWN,
  ERROR_NO_INFO
};

const URL_LOGIN_LOCAL = `${ROOT_DOMAIN  }login/local`;

/**
 * Fetch to server under POST method
 * @param {*} url server address
 * @param {*} body object contains content
 * @return data: Object or []
 */
export async function post(
  url: string,
  body?: Object
): Promise<{ data: any; error: number; succeed: boolean }> {
  if (!body) body = {};

  let res, error;
  [error, res] = await to(
    fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      credentials: "include",
      body: JSON.stringify({ ...body })
    })
  );
  if (error) {
    return createErrorForm(ERROR_NO_NETWORK);
  } else {
    if (res.status == 404) {
      return createErrorForm(ERROR_SERVER_DOWN);
    } else {
      try {
        return res.json();
      } catch (error) {
        return createErrorForm(ERROR_SERVER_DOWN);
      }
    }
  }
}

/**
 * Do any communication to server
 * @return data or succeed if succeed. It can be
 * - Object
 * - Array
 * @return error if er
 * @param {Function} func function needed to execute
 */
export async function executeFunction(
  func: Function
): Promise<{ data: any; error: number; succeed: boolean }> {
  let res = await func();
  if (res.error === ERROR_NOT_LOGIN_YET) {
    // Login with saved credentials
    const credentials = await ASHelper.loadCredentialsFromAS();
    if (credentials === null) {
      // Không có token
      dealWithInvalidToken();
      return { error: ERROR_NOT_VALID_TOKEN };
    }

    const resLoginLocal = await loginLocal(credentials);
    if (resLoginLocal.error) {
      if (resLoginLocal.error === ERROR_NOT_VALID_TOKEN) {
        // Tài khoản không còn đúng nữa
        dealWithInvalidToken();
      }
      return resLoginLocal;
    } else {
      // Login succeed -> retry task
      res = await func();
      return res;
    }
  } else if (res.error === ERROR_NOT_VALID_TOKEN) {
    // Tài khoản không còn đúng nữa
    dealWithInvalidToken();
    return res;
  } else {
    // / done
    return res;
  }
  function dealWithInvalidToken() {
    console.log("dealWithInvalidToken");
    // -> Alert
    Alert.alert(
      "Thông tin tài khoản đã bị thay đổi",
      "Đăng nhập lại để tiếp tục sử dụng",
      [
        {
          text: "Đồng ý",
          onPress: () => {}
        }
      ],
      { cancelable: false }
    );
    // -> Logout
    logout();
  }
}
/**
 * Login to ESY account
 * @param {*} esyCredentials phone and password
 * @returns Succeed { succeed: true }
 * @returns error { error: error_type }
 */
export async function loginLocal(esyCredentials: Credentials) {
  const res = await post(URL_LOGIN_LOCAL, esyCredentials);
  return res;
}

/**
 * Create response for error in standard from
 * @param {*} error_type error
 */
function createErrorForm(
  error_type: number
): { error: number; data: any; succeed: boolean } {
  return { error: error_type, data: {}, succeed: false };
}
/*
 // help catch error when await a promise http://blog.grossman.io/how-to-write-async-await-without-try-catch-blocks-in-javascript/
  EXAMPLE
     [err, savedTask] = await to(TaskModel({userId: user.id, name: 'Demo Task'}));
     if(err) return cb('Error occurred while saving task');
  */
function to(promise: any) {
  return promise
    .then(data => {
      return [null, data];
    })
    .catch(err => [err]);
}
