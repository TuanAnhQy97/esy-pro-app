/**
 * @flow
 */
import NetInfo from "@react-native-community/netinfo";
import Store from "../Store";
/**
 * Internet is on = true
 */
let internetState = false;
/**
 *                  INTERNET CHANGE HANLDER
 */
export function getInternetState() {
  return internetState;
}

export function setInternetChangeHandler() {
  NetInfo.isConnected.addEventListener(
    "connectionChange",
    onInitialNetConnection.bind(this)
  );
  function onInitialNetConnection(isConnected) {
    internetState = isConnected;

    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      onInitialNetConnection
    );

    Store.dispatch({ type: "UPDATE_NETINFO_INTERNET", values: internetState });
    // Update state
  }
}
