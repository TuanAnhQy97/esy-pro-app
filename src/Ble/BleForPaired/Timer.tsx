/**
 * @flow
 */
import { Platform } from "react-native";
import Store from "../../Store";
import Ble from ".";
import Errorhandler from "./Errorhandler";

const k = Platform.OS === "android" ? 2000 : 0;
const SCAN_TIMEOUT = 5, // s
  DATA_RESPONSE_TIMEOUT = 5000, // ms
  WRITE_CHARA_TIMEOUT = 3000 + k, // ms
  RETRIEVE_SERVICES_TIMEOUT = 4000 + k, // ms
  CONNECTING_TIMEOUT = Platform.OS === "android" ? 20000 + k : 2500, //ms
  NOTIFY_TIMEOUT = 3000 + k; //ms
const DELETE_BOND_TIMEOUT = 10000; //ms
const RETRY_CONNECT_AFTER_BOND = 5500; // ms

type TimeOutHelper = {
  id: TimeoutID,
  startTimer: Function,
  clearTimer: Function
};
/**
 * Timer manage timeout Retrieve Services
 */
export const Timer_retrieveServices: TimeOutHelper = {
  id: setTimeout(() => {}, 0),
  startTimer: function(callback) {
    this.clearTimer();
    this.id = setTimeout(() => {
      console.log("Timer_retrieveServices", "Timeout");
      try {
        callback();
      } catch (e) {}
    }, RETRIEVE_SERVICES_TIMEOUT);
  },
  clearTimer: function() {
    try {
      clearTimeout(this.id);
    } catch (e) {
      console.log("Timer_retrieveServices", "clear ERROR", e);
    }
  }
};

/**
 * Timer manage timeout Write Chara
 */
export const Timer_writeChara: TimeOutHelper = {
  id: setTimeout(() => {}, 0),
  startTimer: function(callback) {
    this.clearTimer();
    this.id = setTimeout(() => {
      console.log("Timer_writeChara", "Timeout");
      try {
        callback();
      } catch (e) {}
    }, WRITE_CHARA_TIMEOUT);
  },
  clearTimer: function() {
    try {
      clearTimeout(this.id);
    } catch (e) {
      console.log("Timer_writeChara", "clear ERROR", e);
    }
  }
};

/**
 * Timer manage timeout data response
 */
export const Timer_dataResponse: TimeOutHelper = {
  id: setTimeout(() => {}, 0),
  startTimer: function() {
    // this.clearTimer();
    // this.id = setTimeout(() => {
    //   console.log("Timer_dataResponse", "Timeout");
    //   Ble.handleDataResponseTimeout();
    // }, DATA_RESPONSE_TIMEOUT);
  },
  clearTimer: function() {
    // try {
    //   clearTimeout(this.id);
    // } catch (e) {
    //   console.log("Timer_dataResponse", "clear ERROR", e);
    // }
  }
};
/**
 * Timer manage timeout data bộ nhớ lỗi
 */
export const Timer_dataErrorFI: TimeOutHelper = {
  id: setTimeout(() => {}, 0),
  startTimer: function() {
    this.clearTimer();
    this.id = setTimeout(() => {
      console.log("Timer_dataErrorFI", "Timeout");
      // Reset biến error -> Gửi lênh nhận lại từ đầu
      Errorhandler.resetErrorsData();
      Ble.sendDataToSavy(Ble.COMMAND_TO_ESY.READ_ERRORS);
    }, DATA_RESPONSE_TIMEOUT);
  },
  clearTimer: function() {
    try {
      clearTimeout(this.id);
    } catch (e) {
      console.log("Timer_dataErrorFI", "clear ERROR", e);
    }
  }
};
/**
 * Timer manage timeout connect
 */
export const Timer_connect: TimeOutHelper = {
  id: setTimeout(() => {}, 0),
  startTimer: function() {
    this.clearTimer();
    this.id = setTimeout(() => {
      console.log("Timer_connect", "Timeout");
      Ble.resetBleConnection();
    }, CONNECTING_TIMEOUT);
  },
  clearTimer: function() {
    try {
      clearTimeout(this.id);
    } catch (e) {
      console.log("Timer_connect", "clear ERROR", e);
    }
  }
};
/**
 * Timer manage timeout res key
 */
export const Timer_resKey: TimeOutHelper = {
  id: setTimeout(() => {}, 0),
  startTimer: function() {
    this.clearTimer();
    this.id = setTimeout(() => {
      console.log("Timer_resKey", "Timeout");
      Ble.resetBleConnection();
    }, DATA_RESPONSE_TIMEOUT);
  },
  clearTimer: function() {
    try {
      clearTimeout(this.id);
    } catch (e) {
      console.log("Timer_resKey", "clear ERROR", e);
    }
  }
};
/**
 * Timer manage timeout notify unsecured Chara
 */
export const Timer_notifyUnsecuredChara: TimeOutHelper = {
  id: setTimeout(() => {}, 0),
  startTimer: function(callback) {
    this.clearTimer();
    this.id = setTimeout(() => {
      console.log("Timer_notifyUnsecuredChara", "Timeout");
      try {
        callback();
      } catch (e) {}
    }, NOTIFY_TIMEOUT);
  },
  clearTimer: function() {
    try {
      clearTimeout(this.id);
    } catch (e) {
      console.log("Timer_notifyUnsecuredChara", "clear ERROR", e);
    }
  }
};
/**
 * Timer manage timeout notify secured Chara
 */
export const Timer_notifySecuredChara: TimeOutHelper = {
  id: setTimeout(() => {}, 0),
  startTimer: function(callback) {
    this.clearTimer();
    this.id = setTimeout(() => {
      console.log("Timer_notifySecuredChara", "Timeout");
      try {
        callback();
      } catch (e) {}
    }, NOTIFY_TIMEOUT);
  },
  clearTimer: function() {
    try {
      clearTimeout(this.id);
    } catch (e) {
      console.log("Timer_notifyUnSecureChara", "clear ERROR", e);
    }
  }
};
/**
 * Timer manage timeout delete bond
 */
export const Timer_deleteBond: TimeOutHelper = {
  id: setTimeout(() => {}, 0),
  startTimer: function(callback) {
    this.clearTimer();
    this.id = setTimeout(() => {
      console.log("Timer_deleteBond", "Timeout");
      try {
        callback();
      } catch (e) {}
    }, DELETE_BOND_TIMEOUT);
  },
  clearTimer: function() {
    try {
      clearTimeout(this.id);
    } catch (e) {
      console.log("Timer_deleteBond", "clear ERROR", e);
    }
  }
};
/**
 * [Android]
 * Timer cho việc kết nối lại sau khi bond mà dis sau vài giây (3-5s)
 * - Sau vài giây -> Tắt cờ cho phép thử kết nối lại
 */
export const Timer_retryConnectAfterBond: TimeOutHelper = {
  id: setTimeout(() => {}, 0),
  startTimer: function(callback) {
    this.clearTimer();
    this.id = setTimeout(() => {
      console.log("Timer_retryConnectAfterBond", "Timeout");
      try {
        callback();
      } catch (e) {}
    }, RETRY_CONNECT_AFTER_BOND);
  },
  clearTimer: function() {
    try {
      clearTimeout(this.id);
    } catch (e) {
      console.log("Timer_retryConnectAfterBond", "clear ERROR", e);
    }
  }
};

/**
 * Timer delay reset ECU
 */
export const Timer_delayReset_EraseEcu: TimeOutHelper = {
  id: setTimeout(() => {}, 0),
  startTimer: function(callback) {
    this.clearTimer();
    this.id = setTimeout(() => {
      console.log("Timer_delayReset_EraseEcu", "Timeout");
      try {
        callback();
      } catch (e) {}
    }, 2000);
  },
  clearTimer: function() {
    try {
      clearTimeout(this.id);
    } catch (e) {
      console.log("Timer_delayReset_EraseEcu", "clear ERROR", e);
    }
  }
};
