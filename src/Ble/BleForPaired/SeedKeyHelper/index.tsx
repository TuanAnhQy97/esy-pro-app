/**
 * @flow
 */
import Store from "../../../Store";
import { fetchDataForPaired } from "../../../Fetchers/DeviceFetcher";
import Ble from "../../BleForPaired";
import { compareArrays } from "../../../SharedFunctions";
import { FETCH_ERROR } from "../../../Fetchers/Shared";
/**
 *            CONSTANTS
 */
const seedKeyLength = 4; // bytes
const FLAG = Object.freeze({
  WAIT_SEED_2_FROM_ESY: 0,
  WAIT_CONFIRM_FROM_ESY: 1,
  DONE: 3
});
const ARRAY_WRONG_KEY = Object.freeze([255, 255, 255]);
const ARRAY_TRUE_KEY = Object.freeze([1, 1, 1]);
/**
 *            BUFFERS
 */
let seed1 = [],
  seed2 = [],
  seed3 = []; //Buffer to store seed
let hasSeedKey = false;
/**
 * reset buffer of seed key
 */
function resetSeedKey() {
  seed1 = [];
  seed2 = [];
  seed3 = []; //Buffer to store seed
  hasSeedKey = false;
}
let progressFlag = FLAG.WAIT_SEED_2_FROM_ESY; // Flag manage process exchange seed key

/**
 * Random a seed to send to ESY
 */
export function generateSeedForPhone() {
  progressFlag = FLAG.WAIT_SEED_2_FROM_ESY; // Set flag
  let seed;
  if (!hasSeedKey) {
    seed = randomArrayNumber();
    seed1 = seed; // store to buffer
  } else {
    seed = seed1;
  }
  return seed;
}
/**
 * Random a array of seedKeyLength elements from 0 to 255
 */
function randomArrayNumber() {
  let seed = [];
  for (let i = 0; i < seedKeyLength; i++) {
    let index = Math.floor(Math.random() * 255);
    seed.push(index);
  }
  return seed;
}

export async function dealWithSeedKey(key: number[]) {
  console.log("dealWithSeedKey", "hasSeedKey", hasSeedKey);
  switch (progressFlag) {
    case FLAG.WAIT_SEED_2_FROM_ESY:
      console.log("dealWithSeedKey", "FLAG.WAIT_SEED_2_FROM_ESY", key);
      if (!hasSeedKey) {
        // Check Key received from ESY. Send this seed key pair to Server to check
        let checkSeedKey = await fetchDataForPaired.checkSeedKeyThroughServer(
          seed1,
          key
        );
        if (checkSeedKey.error) {
          if (checkSeedKey.error === FETCH_ERROR.ERROR_NO_NETWORK) {
            // Không có mạng
            console.log("dealWithSeedKey", "Lấy key S3 lỗi mạng");
            Ble.resetBleConnection();
            Store.dispatch({
              type: "UPDATE_BLE_ERROR",
              values: Ble.BLE_PROGRESS.ERROR.NETWORK_ERROR
            });
          } else if (checkSeedKey.error === FETCH_ERROR.ERROR_NO_INFO) {
            console.log("dealWithSeedKey", "Sai seed 2");
            Ble.resetBleConnection();
            Store.dispatch({
              type: "UPDATE_BLE_ERROR",
              values: Ble.BLE_PROGRESS.ERROR.FAILURE
            });
          }
        } else {
          seed2 = key;
          seed3 = checkSeedKey.data;
          progressFlag = FLAG.WAIT_CONFIRM_FROM_ESY; // Set flag
          // Send key 2
          let res = await Ble.sendSeedToESY(checkSeedKey.data);
          if (!res) {
            Ble.resetBleConnection();
            Store.dispatch({
              type: "UPDATE_BLE_ERROR",
              values: Ble.BLE_PROGRESS.ERROR.FAILURE
            });
          }
        }
      } else {
        if (compareArrays(key, seed2)) {
          progressFlag = FLAG.WAIT_CONFIRM_FROM_ESY; // Set flag
          let res = await Ble.sendSeedToESY(seed3);
          if (!res) {
            Ble.resetBleConnection();
            Store.dispatch({
              type: "UPDATE_BLE_ERROR",
              values: Ble.BLE_PROGRESS.ERROR.FAILURE
            });
          }
        } else {
          console.log("dealWithSeedKey", "Sai seed 2");
          Ble.resetBleConnection();
          Store.dispatch({
            type: "UPDATE_BLE_ERROR",
            values: Ble.BLE_PROGRESS.ERROR.NEED_MANUAL_RETRY
          });
        }
      }
      break;
    case FLAG.WAIT_CONFIRM_FROM_ESY:
      console.log("dealWithSeedKey", "FLAG.WAIT_CONFIRM_FROM_ESY", key);
      // Wrong key
      if (compareArrays(key, ARRAY_WRONG_KEY)) {
        console.log("dealWithSeedKey", "Sai seed 3");
        resetSeedKey();
        progressFlag = FLAG.DONE;
        Ble.resetBleConnection();
        Store.dispatch({
          type: "UPDATE_BLE_ERROR",
          values: Ble.BLE_PROGRESS.ERROR.FAILURE
        });
      } else if (compareArrays(key, ARRAY_TRUE_KEY)) {
        hasSeedKey = true;
        progressFlag = FLAG.DONE;
        Ble.setBleJobType(Ble.BLE_JOB_MODE.STANDBY);
        Ble.startExchangeAfterConnectOkay();
      }
      break;
    default:
      console.log("dealWithSeedKey", "FLAG other", key);
      break;
  }
}
