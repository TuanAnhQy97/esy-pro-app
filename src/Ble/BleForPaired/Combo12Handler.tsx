/**
 * @flow
 */

import Ble from "../../Ble/BleForPaired";
import { compareArrays } from "../../SharedFunctions";
import BikeHelper from "../../Library/BikeHelper";

/**
 * @flow
 */

/**
 *      BUFFERs
 */
let confirmStart = false;
let confirmStartD1 = false,
  confirmStartD2 = false;
let hasD1 = false,
  hasD2 = false;
let dataLength;
let data1 = [],
  data2 = [];

export function extractData12(value: number[]) {
  if (!confirmStart) {
    if (compareArrays(value, Ble.ARRAY_START)) {
      confirmStart = true;
      return;
    }
  } else {
    if (!hasD1) {
      extractData1(value);
      return;
    } else if (!hasD2) {
      // has data1, now get data2
      extractData2(value, 0);
    }
  }
  if (hasD1 && hasD2) {
    let data12 = { data1: [...data1], data2: [...data2] };
    resetBufferData12();
    return data12;
  } else return undefined;
}

function extractData1(value: number[]) {
  // get data1
  if (!confirmStartD1) {
    if (value[0] === 2) {
      confirmStartD1 = true;
      dataLength = value[1];
      // if error, reset process
      if (dataLength === null || dataLength === undefined || dataLength < 4) {
        resetBufferData12();
        return;
      } else {
        // push values to array
        for (let i = 0; i < value.length; i++) {
          data1.push(value[i]);
          // if has enough data, skip to data2
          if (data1.length >= dataLength) {
            hasD1 = true;
            break;
          }
        }
        return;
      }
    } else {
      resetBufferData12();
      return;
    }
  } else {
    // get the rest data1
    for (let i = 0; i < value.length; i++) {
      data1.push(value[i]);
      if (data1.length >= dataLength) {
        hasD1 = true;
        break;
      }
    }
    return;
  }
}
function extractData2(value: number[], startPos: number): void {
  if (startPos === null || startPos === undefined) startPos = 0;
  // get data2
  if (!confirmStartD2) {
    if (value[startPos] === 2) {
      confirmStartD2 = true;
      dataLength = value[startPos + 1];
      // if error, reset process
      if (dataLength === null || dataLength === undefined || dataLength < 4) {
        resetBufferData12();
        return;
      } else {
        // push values to array
        for (let i = startPos; i < value.length; i++) {
          data2.push(value[i]);
          if (data2.length >= dataLength) {
            hasD2 = true;
            break;
          }
        }
        return;
      }
    } else {
      resetBufferData12();
      return;
    }
  } else {
    // get the rest data2
    for (let i = 0; i < value.length; i++) {
      data2.push(value[i]);
      if (data2.length >= dataLength) {
        hasD2 = true;
        break;
      }
    }
    return;
  }
}

export function resetBufferData12(): void {
  confirmStart = false;
  confirmStartD1 = false;
  confirmStartD2 = false;
  hasD1 = false;
  hasD2 = false;
  data1 = [];
  data2 = [];
}
