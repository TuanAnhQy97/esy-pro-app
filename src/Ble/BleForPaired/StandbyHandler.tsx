/**
 * @flow
 */
import { compareArrays } from "../../SharedFunctions";
import BikeHelper from "../../Library/BikeHelper";
import BikeHelperPro from "../../Library/BikeHelperPro";
import { extractData1234 } from "./Combo1234Handler";
import { Timer_dataResponse } from "./Timer";

import Store from "../../Store";

export async function handleStandbyData(value: number[]) {
  Timer_dataResponse.startTimer();
  let data1234 = extractData1234(value);
  if (data1234) {
    let convertData1 = BikeHelper.convertData1(data1234.data1),
      convertData2 = BikeHelper.convertData2(data1234.data2);

    let extraData1 = BikeHelperPro.convertData1(data1234.data1),
      extraData2 = BikeHelperPro.convertData2(data1234.data2),
      extraData3 = BikeHelperPro.convertData3(data1234.data3),
      extraData4 = BikeHelperPro.convertData4(data1234.data4);

    Store.dispatch({
      type: "UPDATE_DASHBOARD_DATA",
      values: {
        /**
         * Normal Data
         */
        data1: convertData1,
        data2: convertData2,
        /**
         * Extra Data
         */
        extraData1: extraData1,
        extraData2: extraData2,
        extraData3: extraData3,
        extraData4: extraData4
      }
    });
  }
}
