/**
 * @flow
 */
import BikeHelper from "../../Library/BikeHelper";
import { ECU_TYPE } from "../../Library/BasicBikeLib";
import { Timer_dataResponse, Timer_dataErrorFI } from "./Timer";
import Store from "../../Store";
import Ble from ".";

class MyInstance {
  confirmStart = false;
  errors: number[][] = [];
  errorLineCount = 0;
  async handleErrorFI(value: number[]) {
    Timer_dataResponse.startTimer();
    Timer_dataErrorFI.startTimer(); // Bật hoặc làm mới timer này
    let extractedErrors = this.extractErrorData(value);
    if (extractedErrors) {
      Timer_dataErrorFI.clearTimer(); // Xong rồi -> Tắt
      Store.dispatch({ type: "UPDATE_ERROR_FI_DATA", values: extractedErrors });
      // Lấy lỗi tiếp
      this.resetErrorsData();
      let res = await Ble.sendDataToSavy(Ble.COMMAND_TO_ESY.READ_ERRORS);
      if (!res) {
        Ble.resetBleConnection();
      }
    }
  }

  extractErrorData(value: number[]) {
    // Check header
    if (value[0] === 2 && value[2] === 115) {
      this.errorLineCount++;
      if (this.errorLineCount === 1) return;
      for (let i = 5; i < value.length - 1; i += 2) {
        if (value[i] !== 0) {
          let error = [value[i], value[i + 1]];
          this.errors.push(error);
        }
      }
      let maxLine = BikeHelper.lib.ecuType === ECU_TYPE.TYPE_A ? 10 : 11;
      if (this.errorLineCount === maxLine) {
        return this.errors;
      } else {
        return null;
      }
    } else {
      console.log("extractErrorData", "SKIP wrong format ");
      return null;
    }
  }

  resetErrorsData(): void {
    this.confirmStart = false;
    this.errorLineCount = 0;
    this.errors = [];
  }
}
class ErrorHandler {
  static instance: MyInstance;
  static getInstance() {
    if (!ErrorHandler.instance) {
      ErrorHandler.instance = new MyInstance();
    }
    return ErrorHandler.instance;
  }
}
export default ErrorHandler.getInstance();
