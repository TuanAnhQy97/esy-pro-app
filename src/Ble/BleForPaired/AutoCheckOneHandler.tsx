/**
 * @flow
 */
import Ble from "../../Ble/BleForPaired";
import { Timer_dataResponse, Timer_dataErrorFI } from "./Timer";
import { extractData1234, resetBufferData1234 } from "./Combo1234Handler";
import BikeHelper from "../../Library/BikeHelper";
import BikeHelperPro from "../../Library/BikeHelperPro";
import Errorhandler from "./Errorhandler";
import Store from "../../Store";
import {  DATA1234 } from "../../DiagnosticHelper/DiagnosticHelper";

import DataForReport from "../../Screens/ReportScreen/DataForReport";

const MIN_REALTIME_SAMPLE = 10;

class MyInstance {
  /**
   * Buffer
   */
  errorList: number[][] = [];
  realtimeSampleSink = [];
  realtimeAverage: DATA1234 = { data1: [], data2: [], data3: [], data4: [] };
  /**
   * Xử lý dữ liệu nhận được
   * @param {*} value
   */
  async handleAutoCheckOneData(value: number[]) {
    Timer_dataResponse.startTimer();
    //  Lấy dữ liệu lỗi FI
    if (Ble.bleDataType === Ble.BLE_DATA_TYPE.ERROR) {
      Timer_dataErrorFI.startTimer(); // Bật hoặc làm mới timer này
      let errorList = Errorhandler.extractErrorData(value);
      if (errorList) {
        Timer_dataErrorFI.clearTimer(); // Xong rồi -> Tắt
        this.errorList = errorList;
        console.log("handleAutoCheckOneData", "errorList", this.errorList);
        // Đủ dữ liệu -> Chuyển sang combo1234
        resetBufferData1234();
        Ble.setBleDataType(Ble.BLE_DATA_TYPE.REALTIME);
        let res = await Ble.sendDataToSavy(Ble.COMMAND_TO_ESY.COMBO_DATA_1234);
        if (!res) {
          Ble.resetBleConnection("handleAutoCheckOneData send Command");
          // Alert
        }
      }
    }
    // Lấy dữ liệu data list
    else if (Ble.bleDataType === Ble.BLE_DATA_TYPE.REALTIME) {
      let data1234 = extractData1234(value);
      if (data1234) {
        let convertedData1 = BikeHelper.convertData1(data1234.data1);
        let rpm = convertedData1[1];
        if (rpm !== 0) {
          // Đang nổ máy
          // Update UI
          Store.dispatch({
            type: "UPDATE_AUTO_CHECK_ONE_PROGRESS",
            values: "showWarning"
          });
          // reset buffer
          if (this.realtimeSampleSink.length !== 0) {
            this.realtimeSampleSink = [];
          }
        } else {
          // Đã tắt máy
          // Xử lý xong rồi -> k cần làm gì nữa
          if (this.realtimeSampleSink.length === MIN_REALTIME_SAMPLE) {
            return;
          }
          // Thêm vào mảng
          if (this.realtimeSampleSink.length < MIN_REALTIME_SAMPLE) {
            this.realtimeSampleSink.push(data1234);
          }
          // Đủ dữ liệu rồi -> Xử lý -> Hiển thị
          if (this.realtimeSampleSink.length === MIN_REALTIME_SAMPLE) {
            this.realtimeAverage = calculateAverage(this.realtimeSampleSink);
            console.log(
              "handleAutoCheckOneData",
              "realtimeAverage",
              this.realtimeAverage
            );
            Store.dispatch({
              type: "UPDATE_AUTO_CHECK_ONE_RESULT",
              values: {
                errorList: this.errorList,
                dataList: this.realtimeAverage
              }
            });
            // Lưu giá trị vào report
            DataForReport.setAutoCheckOne({
              errorList: this.errorList,
              dataList: this.realtimeAverage
            });
          }
        }
      }
    }
  }
  /**
   * Reset các buffer của Auto Check One
   */
  resetAutoCheckOneBuffer() {
    this.errorList = [];
    this.realtimeSampleSink = [];
    this.realtimeAverage = { data1: [], data2: [], data3: [], data4: [] };
  }
}

function calculateAverage(sampleSink: Object[]) {
  let sumOfData1 = [],
    sumOfData2 = [],
    sumOfData3 = [],
    sumOfData4 = [];
  // Calculate sum
  for (let i = 0; i < sampleSink.length; i++) {
    let data1 = sampleSink[i].data1,
      data2 = sampleSink[i].data2,
      data3 = sampleSink[i].data3,
      data4 = sampleSink[i].data4;

    mergeValue2Array(sumOfData1, data1);
    mergeValue2Array(sumOfData2, data2);
    mergeValue2Array(sumOfData3, data3);
    mergeValue2Array(sumOfData4, data4);
  }
  // Calculate aver.
  divideElementTo(sumOfData1, sampleSink.length);
  divideElementTo(sumOfData2, sampleSink.length);
  divideElementTo(sumOfData3, sampleSink.length);
  divideElementTo(sumOfData4, sampleSink.length);

  return {
    data1: sumOfData1,
    data2: sumOfData2,
    data3: sumOfData3,
    data4: sumOfData4
  };
  /**
   * Cộng đè giá trị của 2 mảng số
   * @param {*} arr1 đích
   * @param {*} arr2 nguồn
   */
  function mergeValue2Array(arr1: number[], arr2: number[]) {
    for (let i = 0; i < arr2.length; i++) {
      // Nếu chưa được khời tạo giá trị
      if (arr1[i] === undefined) {
        arr1[i] = 0;
      }
      arr1[i] += arr2[i];
    }
  }
  /**
   * Chia tất cả phần tử của mảng cho 1 số
   * @param {*} arr mảng số
   * @param {*} n số chia
   */
  function divideElementTo(arr: number[], n: number) {
    for (let i = 0; i < arr.length; i++) {
      arr[i] = arr[i] / n;
    }
    return arr;
  }
}

class AutoCheckOneHandler {
  static instance: MyInstance;
  static getInstance() {
    if (!AutoCheckOneHandler.instance) {
      AutoCheckOneHandler.instance = new MyInstance();
    }
    return AutoCheckOneHandler.instance;
  }
}
export default AutoCheckOneHandler.getInstance();
