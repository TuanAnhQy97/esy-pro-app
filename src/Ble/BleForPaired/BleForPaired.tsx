/**
 * @flow
 */
import {
  NativeModules,
  NativeEventEmitter,
  Platform,
  Alert
} from "react-native";

import Store from "../../Store";

import BleManager from "react-native-ble-manager";
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

import { DeviceInfo } from "../../Model/DeviceInfo";

import BasicBikeLib, { ECU_TYPE } from "../../Library/BasicBikeLib";
import BasicBikeLibPro from "../../Library/BasicBikeLibPro";

import BikeHelper from "../../Library/BikeHelper";
import BikeHelperPro from "../../Library/BikeHelperPro";

import { fetchDataForPaired } from "../../Fetchers/DeviceFetcher";
import { getInternetState } from "../../Fetchers/InternetChangeHandler";

import ErrorHandler from "./Errorhandler";

import { handleStandbyData } from "./StandbyHandler";
import AutoCheckOneHandler from "./AutoCheckOneHandler";

import { checkUpdate as GetEcuNextVer } from "../../Screens/UpdateEcu/UpdateModule/src/utilities/check";

import { currentRouteName } from "../../../App";

import TrackUser from "../../TrackUser";

/**
 *        DEFINE ERROR
 */
const ERROR_NOT_FOUND = "not_found",
  ERROR_ALREADY_WRITTING = "You're already writing",
  ERROR_133 = "133",
  ERROR_129 = "129";

const ERROR_BOND_CANCEL = 1,
  ERROR_BOND_TIMEOUT = 2,
  BOND_PROCESS_START = 3;
/**
 *        DEFINE BLE CHARACTERISTIC & SERVICE & DATA
 */
const SERVICE_MAIN = "6e400001-b5a3-f393-e0a9-e50e24dcca9e",
  SERVICE_CONFIG_ESY = "6e400010-b5a3-f393-e0a9-e50e24dcca9e";

const CHARA_CONTROL = "6e400006-b5a3-f393-e0a9-e50e24dcca9e",
  CHARA_SEND_KEY = "6e401089-b5a3-f393-e0a9-e50e24dcca9e",
  CHARA_CHECK_CONNECTION = "00001111-1212-efde-1523-785fef13d123";
export const CHARA_SECURE_RESPONSE = "6e400003-b5a3-f393-e0a9-e50e24dcca9e";
export const CHARA_NORMAL_RESPONSE = "6e401008-b5a3-f393-e0a9-e50e24dcca9e";
export const CHARA_CONFIG_ECU_TYPE = "6e400005-b5a3-f393-e0a9-e50e24dcca9e";
const CHARA_CONFIG_REQUEST_DATA = "6e400012-b5a3-f393-e0a9-e50e24dcca9e";

export const BLE_MAX_ACCEPTED_PING = 140000;
export const BLE_MIN_AI_SAMPLE_FOR_HIGH_PING =
  Platform.OS === "android" ? 4 : 5;
export const BLE_MIN_AI_SAMPLE_FOR_NORMAL = Platform.OS === "android" ? 6 : 11;

const k = 1000;
const SCAN_DURATION = Platform.OS === "android" ? 4 : 3, // s
  CHECK_CONNECTION_ANDROID_INTERVAL = 2000 + k; // ms
const DELAY_AFTER_BLE_ERROR = 1000; // ms

const MAX_COUNT_TRY_TO_REFRESH_BLE = 3;
/**
 * Command to send to ESY
 */
export const COMMAND_TO_ESY = Object.freeze({
  ERASE_ERROR_MEM: [0x69],
  READ_ID: [0x04],
  READ_DATA4: [0x06],
  READ_ERRORS: [0x68],
  COMBO_DATA12: [0x64],
  COMBO_DATA_14: [0x0e],
  COMBO_DATA_1234: [0x66],
  RESET_ECU: [0x67]
});
import { isTheSameDay, compareArrays, wait } from "../../SharedFunctions";

import { tools as OverLayWaitingKline_tools } from "../../Components/Popup/OverLayWaitingKline";
import { tools as OverLayEraseError_ResetEcm_tools } from "../../Components/Popup/OverLayEraseError_ResetEcm";
import { tools as OverlayConfirmErase_tools } from "../../Components/Popup/OverlayConfirmErase";
import { tools as OverlayConfirmReset_tools } from "../../Components/Popup/OverlayConfirmReset";
import { tools as OverlayExplainBleError_tools } from "../../Components/Popup/OverlayExplainBleError";
import { tools as OverlayWarningEcuError_tools } from "../../Components/Popup/OverlayWarningEcuError";

import {
  Timer_retrieveServices,
  Timer_writeChara,
  Timer_dataResponse,
  Timer_connect,
  Timer_resKey,
  Timer_notifySecuredChara,
  Timer_notifyUnsecuredChara,
  Timer_retryConnectAfterBond,
  Timer_delayReset_EraseEcu,
  Timer_dataErrorFI
} from "./Timer";
import CheckPermission from "../../CheckPermission";
import { handleDashboardData } from "./DashboardHandler";
import { resetBufferData1234 } from "./Combo1234Handler";
import { generateSeedForPhone, dealWithSeedKey } from "./SeedKeyHelper";
import AutoCheckOne from "../../Screens/AutoCheckOne/AutoCheckOne";
import DataForReport from "../../Screens/ReportScreen/DataForReport";
import navigationService from "../../services/navigationService";

export const BLE_CONNECT_STATE = Object.freeze({
  DISCONNECTED: 0,
  CONNECTING: 1,
  CONNECTED: 2,
  READY: 3 // Connected + bond + notify okay
});

export const BLE_JOB_MODE = Object.freeze({
  STANDBY: -1,
  DASHBOARD: 1,
  READ_ERROR: 2,
  RESET_ECU: 3,
  ERASE_ERROR: 4,
  EXCHANGE_SEED_KEY: 5,
  AUTO_CHECK_ONE: 6,
  AUTO_CHECK_TWO: 7
});
export const BLE_DATA_TYPE = Object.freeze({
  DO_NOTHING: -2,
  ORDINARY: -1,
  ERROR: 0,
  REALTIME: 2,
  DELETE_ROM: 4,
  RESET_ECU: 10
});
/**
 * Trạng thái của hành động gì đó hoặc mã lỗi của hành động
 */
export const BLE_PROGRESS = Object.freeze({
  PROGRESS: {
    OKAY: -1,
    INIT: 0,
    // Processes
    SCANNING: 4,
    DOING_JOB: 7,
    CONNECTING: 8
  },
  ERROR: {
    // ERRORS
    NOT_FOUND: 1,
    FAILURE: 3,
    ECU_ID_CHANGE: 11,
    ESY_NEED_RESET: 12,
    KLINE_ERROR: 13,
    BLE_129: 14,
    BLE_TIMEOUT_CONNECT: 15,
    BLE_RETRIEVE_SERVICE: 16,
    DATA_TIMEOUT: 17,
    STUCK_ACCOUNT: 18,
    BLE_NOTIFY_CHAR: 19,
    BLE_ERROR_AFTER_LOG_OUT: 20,
    BIKE_NOT_SUPPORT: 21,
    NEED_MANUAL_RETRY: 22,
    //
    NO_BLE: 23,
    NO_INTERNET: 24,
    NETWORK_ERROR: 25,

    ECU_NEED_RESTART: 25,

    NOT_PURCHASED_YET: 26 // Chưa có bản quyền cho gói xe
  }
});

export const ARRAY_START = Object.freeze([0xff, 0xff, 0xff]);
const ARRAY_WAIT_FOR_ESY_REINIT = Object.freeze([0x69, 0x69, 0x69]);
const ARRAY_KLINE_ERROR = Object.freeze([0x70, 0x70, 0x70]);
const ARRAY_NO_LIBRARY = Object.freeze([0x96, 0x96, 0x96]);
const ARRAY_ECU_NEED_RESTART = Object.freeze([0x33, 0x33, 0x33]);

const ARRAY_INIT_FAILURE_WHEN_ADVER = Object.freeze([
  0x00,
  0x00,
  0x00,
  0x00,
  0x00
]);
/*
          bleJobMode:
           - 
           -
            bleDataType:
               - BLE_DATA_TYPE.STANDBY
               - BLE_DATA_TYPE.ERROR
               - BLE_DATA_TYPE.REALTIME;
    */

/**
 * States of BLE that must be reset when disconnect
 */

class DefaultBleFlag {
  // Main flags
  bleIsOn: boolean;
  connectState: number;
  isScanning: boolean;
  isUpdatingEcu: boolean;

  notifiedDataChara: boolean;
  notifiedSeedKeyChara: boolean;
  isNotifying: boolean;

  isWriting: boolean;

  // Buffer flags
  found: boolean; // for scan process
  retryConnect: boolean; // for connect proccess [Android]
  needRefreshBle: boolean; // true if retrieve services error. Usually happen in Android

  isCancelingCurrentJob: boolean; // true if user want to canel current Job: Sync, Dashboard, Ad. Ana ...
  constructor() {
    // Main flags
    this.bleIsOn = false;
    this.connectState = BLE_CONNECT_STATE.DISCONNECTED;
    this.isScanning = false;
    this.isUpdatingEcu = false;

    this.notifiedDataChara = false;
    this.notifiedSeedKeyChara = false;
    this.isNotifying = false;

    this.isWriting = false;

    // Buffer flags
    this.found = false;
    this.retryConnect = false;
    this.needRefreshBle = false;

    this.isCancelingCurrentJob = false;
  }
  resetWhenDis() {
    this.connectState = BLE_CONNECT_STATE.DISCONNECTED;
    this.notifiedDataChara = false;
    this.notifiedSeedKeyChara = false;
    this.isNotifying = false;
    this.isWriting = false;
  }
  resetForNewConnection() {
    // Main flags
    this.connectState = BLE_CONNECT_STATE.DISCONNECTED;
    this.notifiedDataChara = false;
    this.notifiedSeedKeyChara = false;
    this.isNotifying = false;

    this.isWriting = false;

    // Buffer flags
    this.found = false;
    this.retryConnect = false;

    this.isCancelingCurrentJob = false;
  }
}

let state: DefaultBleFlag;
let bleJobMode: number;
let bleDataType: number;
let foundList: DeviceInfo[] = [];

let previousEcuId = []; // Backup ecu id
let previousLib: BasicBikeLib; // Backup lib
let previousLibPro: BasicBikeLibPro; // Backup lib pro

let deviceInfo = new DeviceInfo();

let vBatt: null | number = null; // Last battery voltage của thiết bị được kết nối
const MIN_BATTERY_VOLTAGE_FOR_ESY = 7.5;
/**
 * Cập nhật lại giá trị mới nhất của điện áp xe
 * @param {*} value  Voltage
 */
export function setCurrentVBatt(value: number) {
  vBatt = value;
}

let handlerStopScan,
  handlerDiscover,
  handlerConnected,
  handlerDisconnected,
  handlerUpdateState,
  handlerUpdateCharaValue;

export {
  setupEnvironmentForDevice,
  destroyBle,
  toStandbyMode,
  scanToConnectOnly,
  enableBle,
  connectBLE,
  stopScanning
};
export { deviceInfo, bleDataType, bleJobMode };

/**
 *  GET SET
 */
export function setBleDataType(type: number) {
  bleDataType = type;
}
export function setBleJobType(type: number) {
  bleJobMode = type;
}
/**
 *                              MAIN METHODS
 */
export async function initBle(requireEnableBle: boolean = true) {
  console.log("initBle FOR PAIR DEVICE");
  deviceInfo = new DeviceInfo();
  state = new DefaultBleFlag();

  // Success code
  console.log("Module initialized");
  addBleEvents();
  CheckPermission.checkBlePermission();
  if (requireEnableBle) {
    await enableBle();
  }
  BleManager.checkState();
  await wait(175); // Delay to check state
  if (getBleState() && getInternetState()) {
    // Bluetooth on Internet on -> Bắt đầu tìm thiết bị
    scanToConnectOnly();
  } else {
    if (!getBleState()) {
      // Không Ble
      dealWithBleOff();
    } else {
      // Không Internet
      dealWithInternetOff();
    }
  }
}

/**
 * Destroy current ble connection
 */
export function resetBleConnection() {
  console.log("resetBleConnection");
  // Stop scanning
  stopScanning();
  disconnect(); // disconnect from current device if exists
}
function removeAllTimeoutHandler() {
  Timer_resKey.clearTimer();
  Timer_retrieveServices.clearTimer();
  Timer_writeChara.clearTimer();
  Timer_notifySecuredChara.clearTimer();
  Timer_notifyUnsecuredChara.clearTimer();
  Timer_dataResponse.clearTimer();
  Timer_connect.clearTimer();

  Timer_delayReset_EraseEcu.clearTimer();
}
/**
 * Stop Everything of BLE
 */
function destroyBle() {
  console.log("destroyBle");
  state = new DefaultBleFlag();
  removeBleEvents();
  forceDisconnectAll();
  removeAllTimeoutHandler();
}
/**
 * Try to disconnect All connected devices
 */
async function forceDisconnectAll() {
  removeAllTimeoutHandler(); // remove all timeout
  let list = await BleManager.getConnectedPeripherals();
  console.log("forceDisconnectAll", "length", list.length);
  let error, res;
  for (let i = 0; i < list.length; i++) {
    [error, res] = await to(BleManager.disconnect(list[i].id));
  }
}
/**
 * Return BLE status, on or off
 */
export function getBleState(): boolean {
  return state.bleIsOn;
}
/**
 * Force check Ble state, result will be return in BleManagerDidUpdateState event
 * @Use getBleState after that to get result
 */
export function forceCheckBleState() {
  BleManager.checkState();
}
/**
 * Kết nối và ở trạng thái chờ
 */
async function scanToConnectOnly() {
  console.log("scanToConnectOnly", "isScanning", state.isScanning);
  stopScanning(); // Tất scanning nếu có trước

  if (!getBleState()) {
    dealWithBleOff();
    return;
  }

  bleJobMode = BLE_JOB_MODE.STANDBY;
  bleDataType = BLE_DATA_TYPE.ORDINARY;
  Store.dispatch({
    type: "UPDATE_BLE_PROGRESS",
    values: BLE_PROGRESS.PROGRESS.SCANNING
  });
  await disconnect();
  vBatt = null; // Reset vBatt
  // reset signal info of device info
  deviceInfo.rssi = {
    value: 0,
    sampleCount: 0,
    lastTimeStamp: 0
  };
  state.resetForNewConnection();
  BleManager.scan([], SCAN_DURATION, true).then(() => {
    // Success code
    state.isScanning = true;
    console.log("Scan started");
  });
}
/**
 * Kết nối và trao đổi dữ liệu
 */
function scanToConnect() {
  disconnect();
  Store.dispatch({
    type: "UPDATE_BLE_PROGRESS",
    values: BLE_PROGRESS.PROGRESS.SCANNING
  });
  console.log("scanToConnect");
  vBatt = null; // Reset vBatt
  // reset signal info of device info
  deviceInfo.rssi = {
    value: 0,
    sampleCount: 0,
    lastTimeStamp: 0
  };
  state.resetForNewConnection();
  BleManager.scan([], SCAN_DURATION, true).then(() => {
    // Success code
    state.isScanning = true;
    console.log("Scan started");
  });
}

function connectBLE(addr: string) {
  if (
    state.connectState === BLE_CONNECT_STATE.CONNECTED ||
    state.connectState === BLE_CONNECT_STATE.READY
  ) {
    return;
  }
  state.connectState = BLE_CONNECT_STATE.CONNECTING;
  console.log("connectBLE", addr);
  // reset signal info of device info
  deviceInfo.rssi = {
    value: 0,
    sampleCount: 0,
    lastTimeStamp: 0
  };
  Store.dispatch({
    type: "UPDATE_BLE_PROGRESS",
    values: BLE_PROGRESS.PROGRESS.CONNECTING
  });
  state.retryConnect = false; // set to default value
  Timer_connect.startTimer();
  BleManager.connect(addr).catch(async (error: string) => {
    // Failure code
    console.log("connectBLE", error);
    if (error.includes("Connection")) {
      if (!state.isCancelingCurrentJob) {
        state.retryConnect = true;
        resetBleConnection();
        await wait(1000);
        connectBLE(addr);
      }
    }
    if (error.includes("disconnected")) {
      console.log("connectBLE", "Dis khi đang connecting");
      Store.dispatch({
        type: "UPDATE_BLE_ERROR",
        values: BLE_PROGRESS.ERROR.FAILURE
      });
      resetBleConnection();
    } else {
      resetBleConnection();
    }
  });
}

/**
 * Check if device is connected or not through BLE
 */
async function isConnected() {
  console.log("isConnected", state.connectState);
  if (state.connectState === BLE_CONNECT_STATE.DISCONNECTED) {
    return false;
  } else if (
    state.connectState === BLE_CONNECT_STATE.CONNECTED ||
    state.connectState === BLE_CONNECT_STATE.READY
  ) {
    return true;
  } else if (state.connectState === BLE_CONNECT_STATE.CONNECTING) {
    disconnect();
    state.resetForNewConnection();
    return false;
  }
}

async function enableBle() {
  console.log("enableBle");
  if (Platform.OS === "android") {
    let [error, enableReq] = await to(BleManager.enableBluetooth());
    if (error) {
      // Failure code
      console.log("The user refuse to enable bluetooth");
    } else {
      console.log("The bluetooh is already enabled or the user confirm");
    }
  }
}

/**
 * Change data stream to standby mode
 */
async function toStandbyMode() {
  console.log("toStandbyMode");
  Timer_dataResponse.clearTimer();
  Timer_dataErrorFI.clearTimer(); // Xoá timer đọc lỗi nếu có
  if (bleJobMode !== BLE_JOB_MODE.DASHBOARD) {
    Store.dispatch({ type: "UPDATE_BLE_CHANGING_MODE", values: true });
    // Đang ở mode khác combo 1234 -> Quay về mode này
    bleJobMode = BLE_JOB_MODE.STANDBY;
    bleDataType = BLE_DATA_TYPE.ORDINARY;
    let SCK = await sendDataToSavy(COMMAND_TO_ESY.COMBO_DATA_1234);
    Timer_dataResponse.startTimer();
    await wait(300);
    Store.dispatch({ type: "UPDATE_BLE_CHANGING_MODE", values: false });
    if (SCK) {
      return true;
    } else {
      return false;
    }
  } else {
    // Đang có combo 1234 rồi
    bleJobMode = BLE_JOB_MODE.STANDBY;
    bleDataType = BLE_DATA_TYPE.ORDINARY;
    Timer_dataResponse.startTimer();
  }
}
function stopScanning() {
  if (state.isScanning)
    BleManager.stopScan()
      .then(() => {
        state.isScanning = false;
      })
      .catch(error => {
        console.log("stopScanning", error);
      });
}

export async function sendDataToSavy(
  key: number[],
  chara: string = CHARA_CONTROL,
  service: string = SERVICE_MAIN
) {
  if (
    state.connectState === BLE_CONNECT_STATE.CONNECTING ||
    state.connectState === BLE_CONNECT_STATE.DISCONNECTED
  ) {
    return;
  }
  if (state.isWriting) {
    console.log(
      "sendDataToSavy",
      key[0],
      "INGORE cause previous writing not complete yet"
    );
    return true;
  } else {
    state.isWriting = true;
  }
  console.log("sendDataToSavy", key[0]);
  Timer_writeChara.startTimer(() => {
    resetBleConnection();
  });
  let error, res;
  [error, res] = await to(
    BleManager.write(deviceInfo.addr, service, chara, key)
  );
  state.isWriting = false;
  if (error) {
    console.log("sendDataToSavy", key, "error", error);
    if (error.includes("disconnected")) {
      // Device not connected
      resetBleConnection();
      Store.dispatch({
        type: "UPDATE_BLE_ERROR",
        values: BLE_PROGRESS.ERROR.FAILURE
      });
    }
    Timer_writeChara.clearTimer(); // clear timer
    return false;
  } else {
    console.log("sendDataToSavy", key[0], "OKAY");
    Timer_writeChara.clearTimer(); // clear timer
    // startCheckConnectionIntervalAndroid(true); // turn it on when send process completes
    return true;
  }
}

async function startNotifyBikeDataChara() {
  state.isNotifying = true;
  if (state.notifiedDataChara) {
    console.log("startNotifySecuredChara", "already notified");
    return true;
  }
  console.log("startNotifySecuredChara");
  let error, res;
  [error, res] = await to(
    BleManager.startNotification(
      deviceInfo.addr,
      SERVICE_MAIN,
      CHARA_SECURE_RESPONSE
    )
  );
  if (error) {
    console.log("startNotifySecuredChara", error);
    if (error.includes(ERROR_133)) {
      // ANDROID if error = 133 -> do again
      // TODO: Lỗi BLE
    } else if (error.includes(ERROR_129)) {
      resetBleConnection();
      Store.dispatch({
        type: "UPDATE_BLE_ERROR",
        values: BLE_PROGRESS.ERROR.BLE_129
      });
    } else if (error.includes("disconnected")) {
      // Device not connected
      resetBleConnection();
      Store.dispatch({
        type: "UPDATE_BLE_ERROR",
        values: BLE_PROGRESS.ERROR.FAILURE
      });
    } else if (error.includes("Could not find service with UUID")) {
      // Không tìm thấy chara -> Cần tắt bật BLE để refrésh [iOS]
      resetBleConnection();
      Alert.alert(
        "Tắt và bật lại Bluetooth",
        "Ứng dụng cần khởi động lại Bluetooth để làm mới bộ nhớ đệm.\nVào “Cài đặt” -> “Bluetooth” -> Tắt và bật lại Bluetooth"
      );
      return;
    }
    return false;
  } else {
    console.log("startNotifySecuredChara", "succeed");
    state.notifiedDataChara = true;
    state.isNotifying = false;
    return true;
  }
}

function addBleEvents() {
  console.log("addBleEvents");
  handlerDisconnected = bleManagerEmitter.addListener(
    "BleManagerDisconnectPeripheral",
    handleDisconnectedPeripheral
  );
  handlerUpdateState = bleManagerEmitter.addListener(
    "BleManagerDidUpdateState",
    handleUpdateState
  );
  handlerStopScan = bleManagerEmitter.addListener(
    // The scanning for peripherals is ended.
    "BleManagerStopScan",
    handleStopScan
  );
  handlerDiscover = bleManagerEmitter.addListener(
    "BleManagerDiscoverPeripheral",
    handleDiscoverPeripheral
  );
  handlerConnected = bleManagerEmitter.addListener(
    "BleManagerConnectPeripheral",
    handleConnectedPeripheral
  );
  handlerUpdateCharaValue = bleManagerEmitter.addListener(
    "BleManagerDidUpdateValueForCharacteristic",
    handleUpdateCharaValue
  );
}

function removeBleEvents(): void {
  console.log("removeBleEvents");
  handlerDisconnected.remove();
  handlerUpdateState.remove();
  handlerStopScan.remove();
  handlerDiscover.remove();
  handlerConnected.remove();
  handlerUpdateCharaValue.remove();
}

/**
 *                      General methods
 */
async function handleDisconnectedPeripheral(data: Object) {
  // peripheral - String - the id of the peripheral
  // let addr = data.peripheral;
  if (state.connectState === BLE_CONNECT_STATE.DISCONNECTED) {
    console.log("handleDisconnectedPeripheral ", "triggered twice, hence skip");
    return;
  }
  console.log("handleDisconnectedPeripheral ");
  state.connectState = BLE_CONNECT_STATE.DISCONNECTED;

  DataForReport.resetAllResults();
  // Nếu đang ở mode cập nhật ECU
  if (state.isUpdatingEcu) {
    return;
  }

  removeAllTimeoutHandler();
  OverLayWaitingKline_tools.showPopup(false); // Tắt overlay này nếu có
  OverlayWarningEcuError_tools.showPopup(false); // Tắt popup lỗi ECU nếu có
  OverlayConfirmErase_tools.showPopup(false);
  OverlayConfirmReset_tools.showPopup(false);

  let error = Store.getState().BleStateReducer.error;

  let autoRetry = false;

  if (!getBleState()) {
    // Những trường hợp dừng, không làm gì nữa
    // Update UI
  } else if (error) {
    switch (error) {
      // Những trường hợp dừng, không làm gì nữa
      case BLE_PROGRESS.ERROR.BIKE_NOT_SUPPORT:
      case BLE_PROGRESS.ERROR.KLINE_ERROR:
      case BLE_PROGRESS.ERROR.ECU_NEED_RESTART:
      case BLE_PROGRESS.ERROR.NETWORK_ERROR:
      case BLE_PROGRESS.ERROR.NOT_PURCHASED_YET:
        break;
      case BLE_PROGRESS.ERROR.NEED_MANUAL_RETRY:
        break;
      // Những trường hợp đợi chút rồi thử lại
      case BLE_PROGRESS.ERROR.BLE_129:
        break;
      // Tiếp tục kết nối, thử lại
      default:
        autoRetry = true;
        break;
    }
  } else {
    Store.dispatch({
      type: "UPDATE_BLE_ERROR",
      values: BLE_PROGRESS.ERROR.FAILURE
    });
    autoRetry = true;
  }

  if (autoRetry) {
    switch (bleJobMode) {
      case BLE_JOB_MODE.ERASE_ERROR:
      case BLE_JOB_MODE.RESET_ECU:
        console.log("DIS WHEN RESET ECU");
        // Báo lỗi trên Popup
        Store.dispatch({ type: "ECU_RESET_AND_ERASE_STATUS", values: "error" });
        await wait(150);
        scanToConnectOnly();
        break;
      default:
        switch (currentRouteName) {
          // Không quay về khi ở những màn sau
          case "UserInfo":
          case "Setting":
            break;
          // Quay về Main
          default:
            navigationService.navigate("Main");
            break;
        }
        await wait(150);
        scanToConnectOnly();
        break;
    }
  } else {
    // Update UI
    navigationService.navigate("Main");
    OverlayExplainBleError_tools.showPopup(false); // Tắt popup này nếu có
  }

  state.resetWhenDis();
}
/**
 * Kiểm tra điện áp ắc quy khi bị dis
 */
function checkBatteryHealthWhenDis() {
  if (vBatt) {
    // Có giá trị Ắc quy cuối
    if (vBatt <= MIN_BATTERY_VOLTAGE_FOR_ESY) {
      // Điện áp ắc quy sụt nhiều -> có thể gây reset ESY
      Alert.alert(
        "Nguồn điện của xe yếu",
        `ESY phát hiện nguồn điện của xe vừa chạm mốc ${vBatt.toFixed(
          1
        )}Volt\nĐiều này khiến xe và ESY hoạt động không ổn định\nVui lòng kiểm tra nguồn điện của xe`,
        [{ text: "Đồng ý" }],
        { cancelable: false }
      );
    }
  }
}

function handleUpdateState(args: Object): void {
  // state - String - the new BLE state ('on'/'off').
  state.bleIsOn = args.state === "on" ? true : false;
  console.log("handleUpdateState", args.state, state.bleIsOn);

  Store.dispatch({ type: "UPDATE_NETINFO_BLE", values: state.bleIsOn });

  if (!state.bleIsOn) {
    dealWithBleOff();
  }
}
function dealWithBleOff() {
  stopScanning();
  removeAllTimeoutHandler();
  // Update UI
  Store.dispatch({
    type: "UPDATE_BLE_ERROR",
    values: BLE_PROGRESS.ERROR.NO_BLE
  });
}
function dealWithInternetOff() {
  stopScanning();
  removeAllTimeoutHandler();
  // Update UI
  Store.dispatch({
    type: "UPDATE_BLE_ERROR",
    values: BLE_PROGRESS.ERROR.NO_INTERNET
  });
}
async function handleConnectedPeripheral(data: Object) {
  console.log("handleConnectedPeripheral");
  if (state.connectState === BLE_CONNECT_STATE.CONNECTED) {
    console.log("handleConnectedPeripheral may trigger twice. Hence, skip");
    return;
  }
  state.connectState = BLE_CONNECT_STATE.CONNECTED;

  // Nếu đang ở mode cập nhật ECU
  if (state.isUpdatingEcu) {
    return;
  }

  Timer_connect.clearTimer();
  // retrieve ble services
  // if (Platform.OS === "android") {
  //   await wait(250); // delay a little bit -> Avoid Error 133 on Android
  //   // if (state.needRefreshBle) {
  //   console.log("refreshCache", "start");
  //   let [e, r] = await to(BleManager.refreshCache(deviceInfo.addr));
  //   if (e) {
  //     console.log("refreshCache", e);
  //   } else {
  //     console.log("refreshCache", "done");
  //   }
  //   await wait(250); // delay a little bit -> Avoid Error 133 on Android
  //   // }
  // }
  let RS = await retrieveServices();
  if (!RS) {
    console.log("retrieveServices FAIL");
    return;
  }
  // if (Platform.OS === "android") {
  //   await wait(150); // delay a little bit -> Avoid Error 133 on Android
  //   await requestHighConnectionPriority();
  //   await wait(150); // delay a little bit -> Avoid Error 133 on Android
  // }
  console.log("retrieveServices OKAY");

  let res = await startNotifySeedKeyChara();
  if (!res) {
    resetBleConnection();
    Store.dispatch({
      type: "UPDATE_BLE_ERROR",
      values: BLE_PROGRESS.ERROR.FAILURE
    });
    // Alert
    return;
  }

  res = await startNotifyBikeDataChara();
  if (!res) {
    resetBleConnection();
    Store.dispatch({
      type: "UPDATE_BLE_ERROR",
      values: BLE_PROGRESS.ERROR.FAILURE
    });
    // Alert
    return;
  }

  startExchangeSeedKey();
}
/**
 * Start exchange seed and key
 */
async function startExchangeSeedKey() {
  bleJobMode = BLE_JOB_MODE.EXCHANGE_SEED_KEY;
  bleDataType = BLE_DATA_TYPE.ORDINARY;
  let bufferSeed = generateSeedForPhone();
  let res = await sendSeedToESY(bufferSeed);
}
/**
 * [Android] Request Highest priority for Ble connection
 */
async function requestHighConnectionPriority() {
  console.log("requestHighConnectionPriority");
  let [error, res] = await to(
    BleManager.requestConnectionPriority(deviceInfo.addr, 1)
  );
  if (error) {
    console.log("requestHighConnectionPriority ERROR", error);
  } else {
    console.log("requestHighConnectionPriority OKAY");
  }
}

async function handleUpdateCharaValue(data: Object) {
  // peripheral - String - the id of the peripheral
  // characteristic - String - the UUID of the characteristic
  // value - Array - the read value

  // Nếu đang ở mode cập nhật ECU
  if (state.isUpdatingEcu) {
    return;
  }

  if (bleDataType === BLE_DATA_TYPE.DO_NOTHING) {
    console.log("handleUpdateCharaValue", "DO_NOTHING");
    return;
  }

  let characteristic = data.characteristic.toLowerCase();
  switch (characteristic) {
    case CHARA_NORMAL_RESPONSE:
      dealWithSeedKey(data.value);
      return;
    default:
      break;
  }
  // filter array when ECU NEED RESTART
  if (compareArrays(data.value, ARRAY_ECU_NEED_RESTART)) {
    resetBleConnection();
    Store.dispatch({
      type: "UPDATE_BLE_ERROR",
      values: BLE_PROGRESS.ERROR.ECU_NEED_RESTART
    });
    return;
  }
  // filter array when K line is gone. NEED TO CHECK
  if (compareArrays(data.value, ARRAY_KLINE_ERROR)) {
    resetBleConnection();
    Store.dispatch({
      type: "UPDATE_BLE_ERROR",
      values: BLE_PROGRESS.ERROR.KLINE_ERROR
    });
    return;
  }
  // Filter if need to wait for K line
  if (compareArrays(data.value, ARRAY_WAIT_FOR_ESY_REINIT)) {
    console.log("WAIT FOR ESY REINIT");
    Timer_dataResponse.startTimer();
    OverLayWaitingKline_tools.showPopup(true);
    return;
  } else {
    OverLayWaitingKline_tools.showPopup(false);
  }
  // Other case
  switch (bleJobMode) {
    case BLE_JOB_MODE.STANDBY:
      // Chế độ chờ
      handleStandbyData(data.value);
      break;
    default:
      handleUpdateCharaValue4Active(data);
      break;
  }
}

export async function sendDataMorethan20bytes(data: number[], chara?: string) {
  let length = data.length;
  let pos = 0;
  while (pos < length) {
    let smallDataPack = [];
    for (let i = 0; i < 20; i++) {
      if (pos === length) break;
      smallDataPack.push(data[pos]);
      pos++;
    }
    let res = await sendDataToSavy(smallDataPack, chara);
    if (!res) return false;
  }
  return true;
}

async function startNotifySeedKeyChara() {
  state.isNotifying = true;
  if (state.notifiedSeedKeyChara) {
    console.log("startNotifySeedKeyChara", "already notified");
    return true;
  }
  console.log("startNotifySeedKeyChara");
  Timer_notifyUnsecuredChara.startTimer(() => {
    console.log("startNotifySeedKeyChara", "timeout");
    resetBleConnection();
    // TODO: Lỗi BLE
  });

  let error, res;
  [error, res] = await to(
    BleManager.startNotification(
      deviceInfo.addr,
      SERVICE_MAIN,
      CHARA_NORMAL_RESPONSE
    )
  );
  Timer_notifyUnsecuredChara.clearTimer();
  if (error) {
    console.log("startNotifySeedKeyChara", error);
    if (error.includes(ERROR_133)) {
      // ANDROID if error = 133 -> do again
      // TODO: Lỗi BLE
    } else if (error.includes(ERROR_129)) {
      // resetBleConnection();
      // Store.dispatch({
      //   type: "UPDATE_BLE_ERROR",
      //   values: BLE_PROGRESS.ERROR.BLE_129
      // });
      // TODO: Lỗi BLE
    } else if (error.includes("Peripheral did disconnect")) {
      // iOS Ngắt kết nối bất ngờ khi notify
      resetBleConnection();
      Store.dispatch({
        type: "UPDATE_BLE_ERROR",
        values: BLE_PROGRESS.ERROR.FAILURE
      });
    } else if (error.includes("disconnected")) {
      // Device not connected
      resetBleConnection();
      Store.dispatch({
        type: "UPDATE_BLE_ERROR",
        values: BLE_PROGRESS.ERROR.FAILURE
      });
    } else if (error.includes("Could not find service with UUID")) {
      // Không tìm thấy chara -> Cần tắt bật BLE để refrésh [iOS]

      resetBleConnection();
      navigationService.navigate("Main");
      Alert.alert(
        "Tắt và bật lại Bluetooth",
        "Ứng dụng cần khởi động lại Bluetooth để làm mới bộ nhớ đệm.\nVào “Cài đặt” -> “Bluetooth” -> Tắt và bật lại Bluetooth"
      );
      return;
    }
    return false;
  } else {
    state.notifiedSeedKeyChara = true;
    state.isNotifying = false;
    console.log("startNotifySeedKeyChara", "succeed");
    return true;
  }
}
async function retrieveServices() {
  console.log("retrieveServices", "start");
  // start timer
  Timer_retrieveServices.startTimer(() => {
    console.log("retrieveServices", "timeout");
    state.needRefreshBle = true;
    resetBleConnection();
  });

  let error, res;
  [error, res] = await to(BleManager.retrieveServices(deviceInfo.addr));
  if (error) {
    console.log("retrieveServices", error, deviceInfo.addr);
    if (error.includes("Could not find service with UUID")) {
      // Không tìm thấy chara -> Cần tắt bật BLE để refrésh [iOS]

      resetBleConnection();
      navigationService.navigate("Main");
      Alert.alert(
        "Tắt và bật lại Bluetooth",
        "Ứng dụng cần khởi động lại Bluetooth để làm mới bộ nhớ đệm.\nVào “Cài đặt” -> “Bluetooth” -> Tắt và bật lại Bluetooth"
      );
      return;
    } else if (error.includes("Peripheral not found or not connected")) {
      resetBleConnection();
      console.log("Lỗi wtf");
      Store.dispatch({
        type: "UPDATE_BLE_ERROR",
        values: BLE_PROGRESS.ERROR.BLE_ERROR_AFTER_LOG_OUT
      });
    }
    state.needRefreshBle = true; // retry
    Timer_retrieveServices.clearTimer();
    return false;
  } else {
    Timer_retrieveServices.clearTimer();
    return true;
  }
}

function doContain(element: Object, foundList: DeviceInfo[]) {
  // array.forEach((value, index) => {
  //   if(element.id===value.id) return true;
  // });
  let pos = -1;
  for (let index = 0; index < foundList.length; index++) {
    let value = foundList[index];
    if (value.addr === element.id) {
      pos = index;
      break;
    }
  }
  return pos;
}

/**
 *                   HANDLE METHODS
 *
 * */
function handleDiscoverPeripheral(data: Object): void {
  // Nếu đang ở mode cập nhật ECU
  if (state.isUpdatingEcu) {
    return;
  }

  if (state.found) {
    return;
  }
  if (!data.name) {
    return;
  }
  let adverPackage = data.advertising;
  // filter with adverPackage
  // let isEsyStuckInDfuMode = DfuHandler.isEsyStuckInDfuMode(data);
  // if (isEsyStuckInDfuMode !== "no") {
  //   console.log("ESY stuck in DFU mode");
  //   state.found = true;
  //   BleManager.stopScan()
  //     .then(() => {
  //       state.isScanning = false;
  //       Alert.alert(
  //         "Thiết bị ESY lỗi phần mềm",
  //         "Cần cập nhật lại thiết bị để sử dụng bình thường",
  //         [
  //           { text: "Bỏ qua", onPress: navigationService.navigateGoBack },
  //           {
  //             text: "Cập nhật",
  //             onPress: async () => {
  //               resetBleConnection();
  //               navigationService.navigateGoBack();
  //               await wait(1000);
  //               navigationService.navigate("DFU", {
  //                 deviceAddr: data.id,
  //                 currentVersion: isEsyStuckInDfuMode
  //               });
  //             }
  //           }
  //         ],
  //         { cancelable: false }
  //       );
  //     })
  //     .catch(error => {
  //       console.log(error);
  //     });
  // } else
  if (isSavyDevice(adverPackage)) {
    // So sánh device Id
    let deviceId = getDeviceId(adverPackage); // get device Id, return Id if device is free and SAVY
    if (deviceId === null) return;
    if (!compareArrays(deviceId, deviceInfo.deviceId)) return;
    let deviceVersion = getDeviceVersion(adverPackage);
    if (!deviceVersion) {
      deviceInfo.version = [1, 0, 0];
    } else {
      if (!deviceInfo.version) {
        // Chưa có thông tin device version trong app -> lưu vào app, gửi về server
        deviceInfo.version = deviceVersion;
        // updateDeviceVerOnServer(deviceInfo.deviceId, deviceVersion); // Gửi lên server
        // updateDeviceInfoOnAS(deviceInfo); // Lưu vào app
      } else {
        deviceInfo.version = deviceVersion;
      }
    }
    // So sánh ecu Id
    let ecuId = getEcuId(adverPackage);
    if (ecuId === null) {
      return;
    } else {
      deviceInfo.ecuId = ecuId;
    }
    // if (ecuId[2] != 0xf1) {
    //   return;
    // }
    if (compareArrays(ecuId, ARRAY_INIT_FAILURE_WHEN_ADVER)) {
      // Phần cứng mất kline khi khởi động -> thông báo người dùng
      resetBleConnection();
      Store.dispatch({
        type: "UPDATE_BLE_ERROR",
        values: BLE_PROGRESS.ERROR.KLINE_ERROR
      });
      return;
    }
    state.found = true;
    deviceInfo.addr = data.id;
    BleManager.stopScan()
      .then(async () => {
        state.isScanning = false;
        await wait(150);
        connectBLE(deviceInfo.addr); // connect to device
      })
      .catch(error => {
        console.log(error);
      });
  }
}

async function handleStopScan() {
  // handle when scan time out
  console.log("handleStopScan", "isScanning", state.isScanning);
  state.isScanning = false;

  // Nếu đang ở mode cập nhật ECU
  if (state.isUpdatingEcu) {
    return;
  }

  await forceDisconnectAll(); // Ngắt kết nối các kết nối đang có
  Store.dispatch({
    type: "UPDATE_BLE_ERROR",
    values: BLE_PROGRESS.ERROR.NOT_FOUND
  });
  await wait(DELAY_AFTER_BLE_ERROR); // Delay một chút
  scanToConnectOnly(); // Bắt đầu scan tiếp
}
/**
 * Start exchange bike Data
 */
export async function startExchangeAfterConnectOkay() {
  // this is the same for both ios and android
  console.log("startExchangeAfterConnectOkay");
  state.connectState = BLE_CONNECT_STATE.READY;

  // So sánh xem đã dữ liệu xe đã có trong ram chưa?
  if (previousLib && compareArrays(deviceInfo.ecuId, previousEcuId)) {
    // Đã có rồi -> Mang ra dùng
    setupEnvironmentForDevice(deviceInfo, previousLib, previousLibPro);
  } else {
    // Chưa có -> Tải về
    if (!getInternetState()) {
      // Không có mạng
       navigationService.showAlertNoInternet();
      dealWithBleOff();
      return;
    }
    let trimEcuId = deviceInfo.ecuId.slice(0, 4);
    // Thư viện thường
    let libRes = await fetchDataForPaired.getBikeLibVer3FromServer(trimEcuId);
    if (libRes.error) {
      // Chưa có bản quyền
      if (libRes.error === "not_purchased_yet") {
        Store.dispatch({
          type: "UPDATE_BLE_ERROR",
          values: BLE_PROGRESS.ERROR.NOT_PURCHASED_YET
        });
      } else if (libRes.error === 6) {
        // Not support yet
        Store.dispatch({
          type: "UPDATE_BLE_ERROR",
          values: BLE_PROGRESS.ERROR.BIKE_NOT_SUPPORT
        });
        resetBleConnection();
        return;
      }
      // Khác
      else {
        Store.dispatch({
          type: "UPDATE_BLE_ERROR",
          values: BLE_PROGRESS.ERROR.NETWORK_ERROR
        });
      }
      resetBleConnection();
      return;
      // Alert
    }

    previousEcuId = [...deviceInfo.ecuId]; // Backup
    previousLib = libRes.data.normalLib;
    previousLibPro = libRes.data.proLib;
    setupEnvironmentForDevice(deviceInfo, previousLib, previousLibPro);
  }

  //  ECU có bị lỗi không?
  let ecuVer1 = deviceInfo.ecuId[3];
  let ecuVer2 = deviceInfo.ecuId[4];
  if (ecuVer1 === 0 && ecuVer2 === 0) {
    console.log("startExchangeAfterConnectOkay", "ECU ERROR on Update");
    let cbConfirm = () => {
      let params = {
        ecuId: [...deviceInfo.ecuId],
        ecuError: true
      };
      navigationService.navigate("UpdateEcu_CheckVer", params);
    };
    let cbCancel = resetBleConnection;
    OverlayWarningEcuError_tools.showPopup(true, cbConfirm, cbCancel);
    return;
  }

  // Gửi dữ liệu config ESY
  let chipConfig;
  let lib: BasicBikeLib = previousLib;
  let res;
  // Là chip A hoặc B
  if (lib.ecuType !== ECU_TYPE.TYPE_C) {
    if (lib.ecuType === ECU_TYPE.TYPE_A) {
      chipConfig = [0x01];
    } else {
      chipConfig = [0x02];
    }
    res = await sendDataToSavy(chipConfig, CHARA_CONFIG_ECU_TYPE);
    if (!res) {
      resetBleConnection();
      // Alert
      Store.dispatch({
        type: "UPDATE_BLE_ERROR",
        values: BLE_PROGRESS.ERROR.FAILURE
      });
      return;
    }
  }
  // Là chip C
  else {
    chipConfig = [0x03];
    // Gửi lệnh config chip C
    res = await sendDataToSavy(chipConfig, CHARA_CONFIG_ECU_TYPE);
    if (!res) {
      resetBleConnection();
      // Alert
      Store.dispatch({
        type: "UPDATE_BLE_ERROR",
        values: BLE_PROGRESS.ERROR.FAILURE
      });
      return;
    }
    // Gửi lệnh config Request data 1 2 3 4 nếu có
    let dataConfigArr = [];
    if (lib.overideReq1) {
      let length = lib.overideReq1.length + 3; // Header + Length + Checksum
      let dataConfig = [0xf1, length, ...lib.overideReq1, 0x00];
      dataConfigArr.push(dataConfig);
    }
    if (lib.overideReq2) {
      let length = lib.overideReq2.length + 3; // Header + Length + Checksum
      let dataConfig = [0xf2, length, ...lib.overideReq2, 0x00];
      dataConfigArr.push(dataConfig);
    }
    if (lib.overideReq3) {
      let length = lib.overideReq3.length + 3; // Header + Length + Checksum
      let dataConfig = [0xf3, length, ...lib.overideReq3, 0x00];
      dataConfigArr.push(dataConfig);
    }
    if (lib.overideReq4) {
      let length = lib.overideReq4.length + 3; // Header + Length + Checksum
      let dataConfig = [0xf4, length, ...lib.overideReq4, 0x00];
      dataConfigArr.push(dataConfig);
    }
    for (let i = 0; i < dataConfigArr.length; i++) {
      res = await sendDataToSavy(
        dataConfigArr[i],
        CHARA_CONFIG_REQUEST_DATA,
        SERVICE_CONFIG_ESY
      );
      if (!res) {
        resetBleConnection();
        // Alert
        Store.dispatch({
          type: "UPDATE_BLE_ERROR",
          values: BLE_PROGRESS.ERROR.FAILURE
        });
        return;
      }
    }
    // Kết thúc config
    res = await sendDataToSavy(
      [0xff],
      CHARA_CONFIG_REQUEST_DATA,
      SERVICE_CONFIG_ESY
    );
    if (!res) {
      resetBleConnection();
      // Alert
      Store.dispatch({
        type: "UPDATE_BLE_ERROR",
        values: BLE_PROGRESS.ERROR.FAILURE
      });
      return;
    }
  }
  Store.dispatch({
    type: "UPDATE_BLE_PROGRESS",
    values: BLE_PROGRESS.PROGRESS.DOING_JOB
  });
  OverlayExplainBleError_tools.showPopup(false); // Tắt popup hiện lỗi nếu có
  // continue exchange info
  if (bleJobMode === BLE_JOB_MODE.STANDBY) {
    res = await sendDataToSavy(COMMAND_TO_ESY.COMBO_DATA_1234);
    if (!res) {
      resetBleConnection();
      // Alert
      Store.dispatch({
        type: "UPDATE_BLE_ERROR",
        values: BLE_PROGRESS.ERROR.FAILURE
      });
      return;
    }
  } else {
    switch (bleJobMode) {
      case BLE_JOB_MODE.DASHBOARD:
        readEngineValue();
        break;
      case BLE_JOB_MODE.READ_ERROR:
        readErrorFI();
        break;
    }
  }
}

export async function sendSeedToESY(seed: number[]) {
  console.log("sendSeedToESY");
  Timer_writeChara.startTimer();
  let error, res;
  [error, res] = await to(
    BleManager.write(deviceInfo.addr, SERVICE_MAIN, CHARA_SEND_KEY, seed)
  );
  if (error) {
    Timer_writeChara.clearTimer();
    console.log(error);
    return false;
  } else {
    Timer_writeChara.clearTimer();
    return true;
  }
}
async function handleUpdateCharaValue4Active(data: Object) {
  // Chế độ đọc thông số
  if (bleJobMode === BLE_JOB_MODE.DASHBOARD) {
    switch (bleDataType) {
      case BLE_DATA_TYPE.REALTIME:
        handleDashboardData(data.value);
        break;
      default:
        break;
    }
  }
  // Chế độ đọc lỗi
  else if (bleJobMode === BLE_JOB_MODE.READ_ERROR) {
    ErrorHandler.handleErrorFI(data.value);
  }
  // Chế độ phân tích xe chưa nổ
  else if (bleJobMode === BLE_JOB_MODE.AUTO_CHECK_ONE) {
    AutoCheckOneHandler.handleAutoCheckOneData(data.value);
  } else if (bleJobMode === BLE_JOB_MODE.RESET_ECU) {
    // So sánh đúng chuỗi chưa?
    if (compareArrays(data.value.slice(0, 3), [2, 5, 96])) {
      console.log("handleUpdateCharaValue4Active", "RESET_ECU", "Has response");
      // Đúng có nghĩa Thành công -> Update UI
      let res = await toStandbyMode();
      if (!res) {
        Store.dispatch({ type: "ECU_RESET_AND_ERASE_STATUS", values: "error" });
        resetBleConnection();
        return;
      } else {
        TrackUser.logEvent("tool_reset_ecu");
        Store.dispatch({ type: "ECU_RESET_AND_ERASE_STATUS", values: "done" });
      }
    }
  } else if (bleJobMode === BLE_JOB_MODE.ERASE_ERROR) {
    // So sánh đúng chuỗi chưa?
    if (compareArrays(data.value.slice(0, 3), [2, 5, 96])) {
      console.log(
        "handleUpdateCharaValue4Active",
        "ERASE_ERROR",
        "Has response"
      );
      // Đúng có nghĩa Thành công -> Update UI
      let res = await toStandbyMode();
      if (!res) {
        Store.dispatch({ type: "ECU_RESET_AND_ERASE_STATUS", values: "error" });
        resetBleConnection();
        return;
      } else {
        TrackUser.logEvent("tool_erase_error");
        Store.dispatch({ type: "ECU_RESET_AND_ERASE_STATUS", values: "done" });
      }
    }
  }
}

/**
 *                          Others
 */
function isSavyDevice(adverPackage: Object): boolean {
  try {
    if (Platform.OS === "ios") {
      if (!adverPackage.manufacturerData) return false; // device not have kCBAdvDataManufacturerData
      let raw = adverPackage.manufacturerData.bytes;
      if (raw[0] == 0xff && raw[1] == 0xff) {
        return true;
      }
      return false;
    } else if (Platform.OS === "android") {
      if (!adverPackage.manufacturerData) return false; // device not have kCBAdvDataManufacturerData
      let raw = adverPackage.manufacturerData.bytes;
      if (raw[4] == 0xff && raw[5] == 0xff) {
        return true;
      } else return false;
    }
    return false;
  } catch (e) {
    return false;
  }
}

function getDeviceId(adverPackage: Object): any {
  try {
    if (Platform.OS === "ios") {
      let deviceId = []; // array length = 8
      let raw = adverPackage.manufacturerData.bytes;
      for (let i = 2; i <= 9; i++) {
        if (raw[i] === undefined) return null;
        deviceId.push(raw[i]);
      }
      return deviceId;
    } else if (Platform.OS === "android") {
      let deviceId = []; // array length = 8
      if (!adverPackage.manufacturerData) return false; // device not have kCBAdvDataManufacturerData
      let raw = adverPackage.manufacturerData.bytes;
      for (let i = 7; i <= 14; i++) {
        if (raw[i] === undefined) return null;
        deviceId.push(raw[i]);
      }
      return deviceId;
    }
  } catch (e) {
    return null;
  }
}
function getEcuId(adverPackage: Object): any {
  try {
    if (Platform.OS === "ios") {
      let ecuId = []; // array length = 4
      let raw = adverPackage.manufacturerData.bytes;
      for (let i = 10; i <= 14; i++) {
        if (raw[i] === undefined) return null;
        ecuId.push(raw[i]);
      }
      return ecuId;
    } else if (Platform.OS === "android") {
      let ecuId = []; // array length = 8
      let raw = adverPackage.manufacturerData.bytes;
      for (let i = 15; i <= 19; i++) {
        if (raw[i] === undefined) return null;
        ecuId.push(raw[i]);
      }
      return ecuId;
    }
  } catch (e) {
    return null;
  }
}
function getDeviceVersion(adverPackage: Object) {
  try {
    if (Platform.OS === "ios") {
      let deviceVersion = []; // array length = 4
      let raw = adverPackage.manufacturerData.bytes;
      for (let i = 15; i <= 17; i++) {
        if (raw[i] === undefined) return null;
        deviceVersion.push(raw[i]);
      }
      return deviceVersion;
    } else if (Platform.OS === "android") {
      let deviceVersion = []; // array length = 8
      let raw = adverPackage.manufacturerData.bytes;
      for (let i = 20; i <= 22; i++) {
        if (raw[i] === undefined) return null;
        deviceVersion.push(raw[i]);
      }
      return deviceVersion;
    }
  } catch (e) {
    return null;
  }
}

async function disconnect() {
  console.log("disconnect", "connectState", state.connectState);
  removeAllTimeoutHandler(); // remove all timeout
  let error, res;
  [error, res] = await to(BleManager.disconnect(deviceInfo.addr));
}

/**
 * Prepare Bike Lib, BLE for a Savy device
 * @use when select a device or connect to a device
 * @param {*} nextDeviceInfo  deviceInfo Savy
 * @return true if previous device and next device are the same.
 * else @return false
 */
function setupEnvironmentForDevice(
  nextDeviceInfo: DeviceInfo,
  lib?: BasicBikeLib,
  libPro?: BasicBikeLibPro
): boolean {
  if (lib) {
    BikeHelper.prepareLib(lib);
  }
  if (libPro) {
    BikeHelperPro.prepareLib(libPro);
  }
  // Bỏ thông tin tín hiệu khỏi deviceInfo
  nextDeviceInfo.rssi = {
    value: 0,
    sampleCount: 0,
    lastTimeStamp: 0
  };
  if (compareArrays(deviceInfo.deviceId, nextDeviceInfo.deviceId)) {
    return true; // same device -> do nothing
  }
  resetBleConnection(); // destroy connection with previous device if exists
  deviceInfo = nextDeviceInfo;
  return false;
}

function handleConnectingTimeout(): void {
  console.log(
    "handleConnectingTimeout",
    " connectState: " + state.connectState
  );
  if (
    state.connectState === BLE_CONNECT_STATE.CONNECTING ||
    state.connectState === BLE_CONNECT_STATE.DISCONNECTED
  ) {
    resetBleConnection();
  }
}
/**
 * Deal with Ble data response timeout
 */
export function handleDataResponseTimeout() {
  console.log("handleDataResponseTimeout", bleJobMode, bleDataType);
  // Nếu đang trạng thái nghỉ -> Không cần báo lỗi

  Store.dispatch({
    type: "UPDATE_BLE_ERROR",
    values: BLE_PROGRESS.ERROR.DATA_TIMEOUT
  });

  resetBleConnection();
}

function to(promise) {
  // help catch error when await a promise http://blog.grossman.io/how-to-write-async-await-without-try-catch-blocks-in-javascript/
  /* EXAMPLE
         [err, savedTask] = await to(TaskModel({userId: user.id, name: 'Demo Task'}));
         if(err) return cb('Error occurred while saving task');
      */
  return promise
    .then(data => {
      return [null, data];
    })
    .catch(err => [err]);
}

/***
 *  ================================================================================================
 * *  ================================================================================================
 * *  ================================================================================================
 *
 */
/**
 * Bắt đầu nhận dữ liệu Dashboard
 */
export async function readEngineValue() {
  console.log("readEngineValue");

  let res;
  bleJobMode = BLE_JOB_MODE.DASHBOARD;
  bleDataType = BLE_DATA_TYPE.REALTIME;
  resetBufferData1234();
  res = await sendDataToSavy(COMMAND_TO_ESY.COMBO_DATA_1234);
  if (!res) {
    resetBleConnection();
    return;
    // Alert
  }
  Store.dispatch({
    type: "UPDATE_BLE_PROGRESS",
    values: BLE_PROGRESS.PROGRESS.DOING_JOB
  });
}
/**
 * Bắt đầu nhận dữ liệu lỗi FI
 */
export async function readErrorFI() {
  console.log("readErrorFI");

  Timer_dataResponse.startTimer();
  Timer_dataErrorFI.startTimer();
  ErrorHandler.resetErrorsData();
  Store.dispatch({
    type: "UPDATE_BLE_PROGRESS",
    values: BLE_PROGRESS.PROGRESS.DOING_JOB
  });
  Store.dispatch({ type: "UPDATE_ERROR_FI_LOADING" });

  let res;
  bleJobMode = BLE_JOB_MODE.READ_ERROR;
  bleDataType = BLE_DATA_TYPE.ERROR;
  res = await sendDataToSavy(COMMAND_TO_ESY.READ_ERRORS);
  if (!res) {
    resetBleConnection();
    return;
  }
}
/**
 * Xoá lỗi FI
 */
export async function eraseErrorFI() {
  console.log("eraseErrorFI");

  Store.dispatch({ type: "ECU_RESET_AND_ERASE_JOB", values: "erase" });
  Store.dispatch({ type: "ECU_RESET_AND_ERASE_STATUS", values: "doing" });
  OverLayEraseError_ResetEcm_tools.showPopup(true);
  bleJobMode = BLE_JOB_MODE.ERASE_ERROR;
  bleDataType = BLE_DATA_TYPE.DELETE_ROM;
  Timer_delayReset_EraseEcu.startTimer(async () => {
    let res;
    bleJobMode = BLE_JOB_MODE.ERASE_ERROR;
    bleDataType = BLE_DATA_TYPE.DELETE_ROM;
    res = await sendDataToSavy(COMMAND_TO_ESY.ERASE_ERROR_MEM);
    if (!res) {
      Store.dispatch({ type: "ECU_RESET_AND_ERASE_STATUS", values: "error" });
      resetBleConnection();
      return;
    }
  });
}
/**
 * Reset ECU
 */
export async function resetECU() {
  console.log("resetECU");

  Store.dispatch({ type: "ECU_RESET_AND_ERASE_JOB", values: "reset" });
  Store.dispatch({ type: "ECU_RESET_AND_ERASE_STATUS", values: "doing" });
  OverLayEraseError_ResetEcm_tools.showPopup(true);
  bleJobMode = BLE_JOB_MODE.RESET_ECU;
  bleDataType = BLE_DATA_TYPE.RESET_ECU;
  Timer_delayReset_EraseEcu.startTimer(async () => {
    let res;
    bleJobMode = BLE_JOB_MODE.RESET_ECU;
    bleDataType = BLE_DATA_TYPE.RESET_ECU;
    res = await sendDataToSavy(COMMAND_TO_ESY.RESET_ECU);
    if (!res) {
      Store.dispatch({ type: "ECU_RESET_AND_ERASE_STATUS", values: "error" });
      resetBleConnection();
      return;
    }
  });
}

/**
 * Auto check one - kiểm tra hệ thống điện khi không nổ
 */
export async function autoCheckOne() {
  console.log("autoCheckOne");
  Store.dispatch({
    type: "UPDATE_AUTO_CHECK_ONE_PROGRESS",
    values: "init"
  });

  let res;
  Timer_dataErrorFI.startTimer();
  ErrorHandler.resetErrorsData();
  AutoCheckOneHandler.resetAutoCheckOneBuffer();
  bleJobMode = BLE_JOB_MODE.AUTO_CHECK_ONE;
  bleDataType = BLE_DATA_TYPE.ERROR;
  res = await sendDataToSavy(COMMAND_TO_ESY.READ_ERRORS);
  if (!res) {
    resetBleConnection();
    // Alert
  }
  Store.dispatch({
    type: "UPDATE_AUTO_CHECK_ONE_PROGRESS",
    values: "processing"
  });
}
/**
 * Auto check one - kiểm tra hệ thống điện khi không nổ
 */
export async function autoCheckTwo() {
  console.log("autoCheckTwo");

  let res;
  bleJobMode = BLE_JOB_MODE.DASHBOARD;
  bleDataType = BLE_DATA_TYPE.REALTIME;
  res = await sendDataToSavy(COMMAND_TO_ESY.COMBO_DATA_1234);
  if (!res) {
    resetBleConnection();
    return;
  }
}
/**
 * Chuyển sang mode update ECU
 * @param {*} event
 */
export async function updateEcuMode(event: "start" | "stop") {
  console.log("updateEcuMode", event);
  switch (event) {
    case "start":
      state.isUpdatingEcu = true;
      removeAllTimeoutHandler();
      break;
    case "stop":
      state.isUpdatingEcu = false;
      disconnect();
      await wait(150);
      scanToConnectOnly();
      break;
    default:
      break;
  }
}
