/**
 * @flow
 */
import Ble from "../../Ble/BleForPaired";
import { compareArrays } from "../../SharedFunctions";
import BikeHelper from "../../Library/BikeHelper";

/**
 * @flow
 */

/**
 *      BUFFERs
 */
let confirmStart = false;
let confirmStartD1 = false,
  confirmStartD4 = false;
let hasD1 = false,
  hasD4 = false;
let dataLength;
let data1 = [],
  data4 = [];

export function extractData14(value: number[]) {
  if (!confirmStart) {
    if (compareArrays(value, [238, 238, 238])) {
      confirmStart = true;
      return;
    }
  } else {
    if (!hasD1) {
      extractData1(value);
      return;
    } else if (!hasD4) {
      // has data1, now get data4
      extractData4(value, 0);
    }
  }
  if (hasD1 && hasD4) {
    let result = {
      data1: [...data1],
      data4: [...data4]
    };
    resetBufferData14();
    return result;
  } else return undefined;
}

function extractData1(value: number[]) {
  // get data1
  if (!confirmStartD1) {
    if (value[0] === 2) {
      confirmStartD1 = true;
      dataLength = value[1];
      // if error, reset process
      if (dataLength === null || dataLength === undefined || dataLength < 4) {
        resetBufferData14();
        return;
      } else {
        // push values to array
        for (let i = 0; i < value.length; i++) {
          data1.push(value[i]);
          // if has enough data, skip to data2
          if (data1.length >= dataLength) {
            hasD1 = true;
            break;
          }
        }
        return;
      }
    } else {
      resetBufferData14();
      return;
    }
  } else {
    // get the rest data1
    for (let i = 0; i < value.length; i++) {
      data1.push(value[i]);
      if (data1.length >= dataLength) {
        hasD1 = true;
        break;
      }
    }
    return;
  }
}

function extractData4(value: number[], startPos: number): void {
  if (startPos === null || startPos === undefined) startPos = 0;
  // get data2
  if (!confirmStartD4) {
    if (value[startPos] === 2) {
      confirmStartD4 = true;
      dataLength = value[startPos + 1];
      // if error, reset process
      if (dataLength === null || dataLength === undefined || dataLength < 4) {
        resetBufferData14();
        return;
      } else {
        // push values to array
        for (let i = startPos; i < value.length; i++) {
          data4.push(value[i]);
          if (data4.length >= dataLength) {
            hasD4 = true;
            break;
          }
        }
        return;
      }
    } else {
      resetBufferData14();
      return;
    }
  } else {
    // get the rest data2
    for (let i = 0; i < value.length; i++) {
      data4.push(value[i]);
      if (data4.length >= dataLength) {
        hasD4 = true;
        break;
      }
    }
    return;
  }
}

export function resetBufferData14(): void {
  confirmStart = false;
  confirmStartD1 = false;
  confirmStartD4 = false;
  hasD1 = false;
  hasD4 = false;
  data1 = [];
  data4 = [];
}
