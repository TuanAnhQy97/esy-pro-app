/**
 * @flow
 */
import Ble from "../../Ble/BleForPaired";
import { compareArrays } from "../../SharedFunctions";
import BikeHelper from "../../Library/BikeHelper";
import BikeHelperPro from "../../Library/BikeHelperPro";
import { extractData1234 } from "./Combo1234Handler";
import { Timer_dataResponse } from "./Timer";

import Store from "../../Store";

/**
 *      BUFFERs
 */
let t1, t2;
let ton1, ton2;
let rpm1, rpm2;
let T1, T2;

let v1 = 0,
  v2 = 0;

let distanceArr = [];
let itArr = [];

const SAMPLE_AMOUNT_FOR_INSTANCE_CONSUMPTION = 30; // ~ 300ms / sample

export async function handleDashboardData(value: number[]) {
  if (Ble.bleDataType === Ble.BLE_DATA_TYPE.REALTIME) {
    Timer_dataResponse.startTimer();
    let data1234 = extractData1234(value);
    if (data1234) {
      let convertData1 = BikeHelper.convertData1(data1234.data1),
        convertData2 = BikeHelper.convertData2(data1234.data2);

      let extraData1 = BikeHelperPro.convertData1(data1234.data1),
        extraData2 = BikeHelperPro.convertData2(data1234.data2),
        extraData3 = BikeHelperPro.convertData3(data1234.data3),
        extraData4 = BikeHelperPro.convertData4(data1234.data4);

      Store.dispatch({
        type: "UPDATE_DASHBOARD_DATA",
        values: {
          /**
           * Normal Data
           */
          data1: convertData1,
          data2: convertData2,
          /**
           * Extra Data
           */
          extraData1: extraData1,
          extraData2: extraData2,
          extraData3: extraData3,
          extraData4: extraData4
        }
      });
    }
  }
}

function calculateInstanceConsumption(convertData1: number[]) {
  if (!BikeHelper.lib.feature.hasVS) {
    // Không có vận tốc bỏ
    return 0;
  } else {
    let speed = convertData1[0];
    if (speed === 0) {
      // Vận tốc bằng 0 -> reset hết
      resetDashboardBuffer();
      return 0;
    } else if (!t1) {
      // Chưa có mẫu đầu tiên
      t1 = Date.now();
      rpm1 = convertData1[1];
      ton1 = convertData1[3];
      T1 = rpmToT(rpm1); // ms
      v1 = speed;
      return 0;
    } else {
      t2 = Date.now();
      rpm2 = convertData1[1];
      ton2 = convertData1[3];
      T2 = rpmToT(rpm2); // ms
      v2 = speed; // km/h
      let detalT = t2 - t1; // ms
      let distance = (v2 * detalT + ((v2 - v1) * detalT) / 2) / 3600000; // ms -> s -> h
      let sum_Ton = (detalT / (T1 + T2)) * (ton1 + ton2);
      // Push to buffer array
      distanceArr.push(distance);
      itArr.push(sum_Ton);
      // let buffer at t1 = buffer at t2
      t1 = t2;
      rpm1 = rpm2;
      ton1 = ton2;
      T1 = T2;
      v1 = v2;
      if (distanceArr.length > SAMPLE_AMOUNT_FOR_INSTANCE_CONSUMPTION) {
        // Thừa mẫu, bỏ bớt mẫu đầu
        distanceArr.shift();
        itArr.shift();
      }
      if (distanceArr.length >= 2) {
        // Tính tổng quãng đường
        let sumDis = calculateSumOfArray(distanceArr); // km
        // Tính tổng tgian phun
        let sumIT = calculateSumOfArray(itArr); // ms
        if (sumDis === 0 || sumIT === 0) {
          // Không đủ điều kiện
          return 0;
        } else {
          // Tính tổng xăng
          let sumFuel = (sumIT * BikeHelper.lib.nozzle) / 1000; // ms -> ml -> l
          // Tính toán tiêu hao tức thời
          return sumDis / sumFuel;
        }
      } else {
        // Chưa đủ mẫu
        return 0;
      }
    }
  }
}
/**
 * Reset all buffer used in dashboard BLE receiver
 */
export function resetDashboardBuffer() {
  t1 = undefined;
  t2 = undefined;
  ton1 = undefined;
  ton2 = undefined;
  rpm1 = undefined;
  rpm2 = undefined;
  T1 = undefined;
  T2 = undefined;
  v1 = 0;
  v2 = 0;
  distanceArr = [];
  itArr = [];
}

/**
 * Convert rpm to Circle of ignition (ms)
 * @param {*} rpm
 */
function rpmToT(rpm: number) {
  return (2 * 60 * 1000) / rpm;
}
/**
 * Calculate sum of all elements in an Array number
 * @param {*} arr
 */
function calculateSumOfArray(arr: number[]) {
  let sum = 0;
  for (let i = 0; i < arr.length; i++) {
    sum += arr[i];
  }
  return sum;
}
