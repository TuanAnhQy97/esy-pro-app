# Định nghĩa khung truyền, config cho ESY PRO

## Config thư viện mỗi khi kết nối

1. Gửi dữ liệu vào char:
2. Cấu trúc dữ liệu:
   - Header + Dữ liệu + Footer
     | | Vị trí | Giá trị | Ý nghĩa |
     |--- |--- |--- |--- |
     |HEADER |0 |0x01 | Mặc định |
     | |1 |0xXX | Độ dài |
     | |2 |0xXX | Kiểu dữ liệu 1 |
     | |3 |0xXX | Kiểu dữ liệu 2 |
     |BODY |... |0xXX | |
     | |n |0xXX | |
     |FOOTER |n+1 |0xXX |Checksum |
     ```
     VD: 0x01, 0x05, 0x00, 0x01, 0x00, 0xF9
     ```
   - Các kiểu dữ liệu
     | Kiểu | Giá trị |
     |--- |--- |
     |Loại chip (A hoặc B)|0x0000 |
     |Config data 1 request|0x0101 |
     |Config data 2 request|0x0102 |
     |Config data 3 request|0x0103 |
     |Config data 4 request|0x0104 |
