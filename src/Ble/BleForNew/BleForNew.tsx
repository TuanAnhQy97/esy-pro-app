/**
 * @flow
 */
import {NativeModules, NativeEventEmitter, Platform, Alert} from 'react-native';

import BleManager from 'react-native-ble-manager';
import Store from '../../Store';

import {DeviceInfo} from '../../Model/DeviceInfo';

import {compareArrays, wait, compareVersions} from '../../SharedFunctions';

import {
  Timer_retrieveServices,
  Timer_writeChara,
  Timer_dataResponse,
  Timer_connect,
  Timer_notifyUnsecuredChara,
} from './Timer';
import CheckPermission from '../../CheckPermission';
import {generateSeedForPhone, dealWithSeedKey} from './SeedKeyForNew';
import {getInternetState} from '../../Fetchers/InternetChangeHandler';
import {fetchDataToActive} from '../../Fetchers/DeviceFetcher';
import navigationService from '../../services/navigationService';
import {DEVICE_VERSION_2} from '../../constants';
import ble from '../../services/ble';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

/**
 *        DEFINE BLE CHARACTERISTIC & SERVICE & DATA
 */
const SERVICE_MAIN = '6e400001-b5a3-f393-e0a9-e50e24dcca9e';
const CHARA_SEND_KEY = '6e401089-b5a3-f393-e0a9-e50e24dcca9e';

const CHARA_KEY_RESPONSE = '6e401008-b5a3-f393-e0a9-e50e24dcca9e';

const k = 1000;
const SCAN_DURATION = Platform.OS === 'android' ? 4 : 3; // s
const CHECK_CONNECTION_ANDROID_INTERVAL = 2000 + k; // ms
const DELAY_AFTER_BLE_ERROR = 1000; // ms

const MAX_RETRY_CONNECT = 3;

const ERROR_NOT_FOUND = 'not_found';
const ERROR_ALREADY_WRITTING = "You're already writing";
const ERROR_133 = '133';
const ERROR_129 = '129';

export const BLE_CONNECT_STATE = Object.freeze({
  DISCONNECTED: 0,
  CONNECTING: 1,
  CONNECTED: 2,
  READY: 3, // Connected + bond + notify okay
});

/**
 * Trạng thái của hành động gì đó hoặc mã lỗi của hành động
 */
export const BLE_PROGRESS = Object.freeze({
  PROGRESS: {
    OKAY: -1,
    INIT: 0,
    // Processes
    SCANNING: 4,
    DOING_JOB: 7,
    CONNECTING: 8,
    VERIFY_DEVICE: 9,
  },
  ERROR: {
    // ERRORS
    NOT_FOUND: 1,
    FAILURE: 3,
    ECU_ID_CHANGE: 11,
    ESY_NEED_RESET: 12,
    KLINE_ERROR: 13,
    BLE_129: 14,
    BLE_TIMEOUT_CONNECT: 15,
    BLE_RETRIEVE_SERVICE: 16,
    DATA_TIMEOUT: 17,
    STUCK_ACCOUNT: 18,
    BLE_NOTIFY_CHAR: 19,
    BLE_ERROR_AFTER_LOG_OUT: 20,
    BIKE_NOT_SUPPORT: 21,
    NEED_MANUAL_RETRY: 22,
    DEVICE_NOT_COMPATIBLE: 23,
    DEVICE_IS_USED: 24,

    NO_BLE: 25,
    NO_INTERNET: 26,
    NETWORK_ERROR: 27,
  },
});

/**
 * States of BLE that must be reset when disconnect
 */

class DefaultBleFlag {
  // Main flags
  bleIsOn: boolean;

  connectState: number;

  isScanning: boolean;

  notifiedSeedKeyChara: boolean;

  isNotifying: boolean;

  isWriting: boolean;

  countRetry: number;

  // Buffer flags
  found: boolean;

  // for scan process
  connectionErrorAndroid: boolean;

  // for connect proccess [Android]
  isCanceled: boolean; // Dừng hết lại

  constructor() {
    // Main flags
    this.bleIsOn = false;
    this.connectState = BLE_CONNECT_STATE.DISCONNECTED;
    this.isScanning = false;
    this.notifiedSeedKeyChara = false;
    this.isNotifying = false;

    this.isWriting = false;
    this.countRetry = 0;

    // Buffer flags
    this.found = false;
    this.connectionErrorAndroid = false;
    this.isCanceled = false;
  }

  resetWhenDis() {
    this.connectState = BLE_CONNECT_STATE.DISCONNECTED;
    this.notifiedSeedKeyChara = false;
    this.isNotifying = false;
    this.isWriting = false;
  }

  resetForNewConnection() {
    // Main flags
    this.connectState = BLE_CONNECT_STATE.DISCONNECTED;
    this.notifiedSeedKeyChara = false;
    this.isNotifying = false;

    this.isWriting = false;
    this.countRetry = 0;

    // Buffer flags
    this.found = false;
    this.connectionErrorAndroid = false;
    this.isCanceled = false;
  }
}

let state: DefaultBleFlag;

let deviceInfo = new DeviceInfo();
let foundList: DeviceInfo[] = [];

let vBatt: null | number = null; // Last battery voltage của thiết bị được kết nối
const MIN_BATTERY_VOLTAGE_FOR_ESY = 7.5;
/**
 * Cập nhật lại giá trị mới nhất của điện áp xe
 * @param {*} value  Voltage
 */
export function setCurrentVBatt(value: number) {
  vBatt = value;
}

let handlerStopScan;
let handlerDiscover;
let handlerConnected;
let handlerDisconnected;
let handlerUpdateState;
let handlerUpdateCharaValue;

export {destroyBle};
export {deviceInfo};

export function setBleConnectState(connectState: number) {
  state.connectState = connectState;
}
/**
 *                              MAIN METHODS
 */
export async function initBle() {
  console.log('initBle FOR active new device');
  state = new DefaultBleFlag();

  addBleEvents();
  CheckPermission.checkBlePermission();
  await enableBle();
  BleManager.checkState();
  await wait(175); // Delay to check state

  startAddNewDevice();
}

function removeAllTimeoutHandler() {
  Timer_retrieveServices.clearTimer();
  Timer_writeChara.clearTimer();
  Timer_notifyUnsecuredChara.clearTimer();
  Timer_dataResponse.clearTimer();
  Timer_connect.clearTimer();
}
/**
 * Stop Everything of BLE
 */
function destroyBle() {
  console.log('destroyBle');
  stopScanning();
  state = new DefaultBleFlag();
  removeBleEvents();
  removeAllTimeoutHandler();
  disconnect();
}
/**
 * Alert lỗi xảy ra trong quá trình kết nối
 * @param {*} errorCode
 */
export function alertError(errosrCode: number) {
  const title = translateError(errosrCode);
  const message = '';
  Alert.alert(
    title,
    message,
    [{text: 'Thử lại', onPress: startAddNewDevice}, {text: 'Huỷ bỏ', onPress: navigationService.goBack}],
    {cancelable: false},
  );
}
/**
 * Giải mã từ error code
 * @param {*} error
 */
function translateError(error: number) {
  let str;
  let numStr;
  if (error < 10) {
    numStr = `0${error}`;
  } else {
    numStr = error;
  }
  switch (error) {
    case 0:
      str = '';
      break;

    case BLE_PROGRESS.ERROR.NO_BLE:
      str = `Không có kết nối Bluetooth. Vui lòng bật Bluetooth để tiếp tục (1${numStr})`;
      break;

    case BLE_PROGRESS.ERROR.NO_INTERNET:
      str = `Không có kết nối Internet. Vui lòng kết nối mạng để tiếp tục (1${numStr})`;
      break;

    case BLE_PROGRESS.ERROR.NETWORK_ERROR:
      str = `Kết nối mạng gián đoán. Vui lòng thử lại sau (1${numStr})`;
      break;

    case BLE_PROGRESS.ERROR.NOT_FOUND:
      str = `Không tìm thấy thiết bị (1${numStr})`;
      break;

    case BLE_PROGRESS.ERROR.DEVICE_NOT_COMPATIBLE:
      str = `Thiết bị không tương thích (1${numStr})`;
      break;

    case BLE_PROGRESS.ERROR.FAILURE:
      str = `Lỗi kết nối thiết bị (1${numStr})`;
      break;

    case BLE_PROGRESS.ERROR.DEVICE_IS_USED:
      str = `Thiết bị đã được sử dụng (1${numStr})`;
      break;

    default:
      str = `Lỗi không xác định (1${numStr})`;
      break;
  }
  return str;
}
/**
 * Try to disconnect All connected devices
 */
async function forceDisconnectAll() {
  removeAllTimeoutHandler(); // remove all timeout
  const list = await BleManager.getConnectedPeripherals();
  console.log('forceDisconnectAll', 'length', list.length);
  let error;
  let res;
  for (let i = 0; i < list.length; i++) {
    [error, res] = await to(BleManager.disconnect(list[i].id));
  }
}
/**
 * Return BLE status, on or off
 */
export function getBleState(): boolean {
  return state.bleIsOn;
}
/**
 * Force check Ble state, result will be return in BleManagerDidUpdateState event
 * @Use getBleState after that to get result
 */
export function forceCheckBleState() {
  BleManager.checkState();
}
/**
 * Kết nối và ở trạng thái chờ
 */
async function startAddNewDevice() {
  console.log('startAddNewDevice');
  if (getBleState() && getInternetState()) {
  } else {
    if (!getBleState()) {
      // Không có Bluetooth
      alertError(BLE_PROGRESS.ERROR.NO_BLE);
    } else {
      // Không có Internet
      alertError(BLE_PROGRESS.ERROR.NO_INTERNET);
    }
    return;
  }

  deviceInfo = new DeviceInfo();
  foundList = [];
  Store.dispatch({
    type: 'BLE_FOR_ACTIVE_PROGRESS',
    values: BLE_PROGRESS.PROGRESS.SCANNING,
  });
  vBatt = null; // Reset vBatt
  // reset signal info of device info
  state.resetForNewConnection();
  BleManager.scan([], SCAN_DURATION, true).then(() => {
    // Success code
    state.isScanning = true;
    console.log('Scan started');
  });
}
/**
 * Dis để kết nối lại
 */
export function resetBleConnection() {
  disconnect();
}
export function cancelBleJob() {
  state.isCanceled = true;
  disconnect();
}
/**
 * Sử dụng sau khi xác nhận thiết bị thành công
 * @param {*} token active token
 */
export function verifySucceedCallback(token: string) {
  console.log('verifySucceedCallback');
  navigationService.navigate('AuthInputPhone', {
    activeToken: token,
    deviceId: deviceInfo.deviceId,
  });
}

function connectBLE(addr: string) {
  if (state.connectState === BLE_CONNECT_STATE.CONNECTED || state.connectState === BLE_CONNECT_STATE.READY) {
    return;
  }
  state.connectState = BLE_CONNECT_STATE.CONNECTING;
  console.log('connectBLE', addr);
  Store.dispatch({
    type: 'BLE_FOR_ACTIVE_PROGRESS',
    values: BLE_PROGRESS.PROGRESS.CONNECTING,
  });
  state.connectionErrorAndroid = false; // set to default value
  Timer_connect.startTimer();
  BleManager.connect(addr).catch(async (error: string) => {
    // Failure code
    console.log('connectBLE', error);
    if (error.includes('Connection')) {
      state.connectionErrorAndroid = true;
      await wait(1000);
      connectBLE(addr);
    }
    if (error.includes('disconnected')) {
      console.log('connectBLE', 'Dis khi đang connecting');
    } else {
    }
  });
}

async function enableBle() {
  console.log('enableBle');
  if (Platform.OS === 'android') {
    const [error, enableReq] = await to(BleManager.enableBluetooth());
    if (error) {
      // Failure code
      console.log('The user refuse to enable bluetooth');
    } else {
      console.log('The bluetooh is already enabled or the user confirm');
    }
  }
}

function stopScanning() {
  if (state.isScanning)
    BleManager.stopScan()
      .then(() => {
        state.isScanning = false;
      })
      .catch(error => {
        console.log('stopScanning', error);
      });
}

export async function sendDataToSavy(key: number[]) {
  const chara = CHARA_SEND_KEY;
  if (state.connectState === BLE_CONNECT_STATE.CONNECTING || state.connectState === BLE_CONNECT_STATE.DISCONNECTED) {
    return;
  }
  if (state.isWriting) {
    console.log('sendDataToSavy', key[0], 'INGORE cause previous writing not complete yet');
    return true;
  }
  state.isWriting = true;

  console.log('sendDataToSavy', key[0]);
  Timer_writeChara.startTimer(() => {});
  let error;
  let res;
  [error, res] = await to(BleManager.write(deviceInfo.addr, SERVICE_MAIN, chara, key));
  state.isWriting = false;
  if (error) {
    console.log('sendDataToSavy', key, 'error', error);
    if (error.includes('disconnected')) {
      // Device not connected
    }
    Timer_writeChara.clearTimer(); // clear timer
    return false;
  }
  console.log('sendDataToSavy', key[0], 'OKAY');
  Timer_writeChara.clearTimer(); // clear timer
  // startCheckConnectionIntervalAndroid(true); // turn it on when send process completes
  return true;
}

function addBleEvents() {
  console.log('addBleEvents');
  handlerDisconnected = bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', handleDisconnectedPeripheral);
  handlerUpdateState = bleManagerEmitter.addListener('BleManagerDidUpdateState', handleUpdateState);
  handlerStopScan = bleManagerEmitter.addListener(
    // The scanning for peripherals is ended.
    'BleManagerStopScan',
    handleStopScan,
  );
  handlerDiscover = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', handleDiscoverPeripheral);
  handlerConnected = bleManagerEmitter.addListener('BleManagerConnectPeripheral', handleConnectedPeripheral);
  handlerUpdateCharaValue = bleManagerEmitter.addListener(
    'BleManagerDidUpdateValueForCharacteristic',
    handleUpdateCharaValue,
  );
}

function removeBleEvents(): void {
  console.log('removeBleEvents');
  handlerDisconnected.remove();
  handlerUpdateState.remove();
  handlerStopScan.remove();
  handlerDiscover.remove();
  handlerConnected.remove();
  handlerUpdateCharaValue.remove();
}

/**
 *                      General methods
 */
async function handleDisconnectedPeripheral(data: Object) {
  // peripheral - String - the id of the peripheral
  // let addr = data.peripheral;
  if (state.connectState === BLE_CONNECT_STATE.DISCONNECTED) {
    console.log('handleDisconnectedPeripheral ', 'triggered twice, hence skip');
    return;
  }
  console.log('handleDisconnectedPeripheral ');
  state.connectState = BLE_CONNECT_STATE.DISCONNECTED;

  removeAllTimeoutHandler();
  if (state.isCanceled) {
    // Do nothing
    return;
  }
  if (state.connectionErrorAndroid) {
    // Không xử lý lỗi này ở đây
  } else if (state.countRetry < MAX_RETRY_CONNECT) {
    state.countRetry++;
    await wait(100);
    connectBLE(deviceInfo.addr);
  } else {
    // Dừng lại -> quá số lần thử lại rồi
    // Update UI
    alertError(BLE_PROGRESS.ERROR.FAILURE);
  }

  state.resetWhenDis();
}
/**
 * Kiểm tra điện áp ắc quy khi bị dis
 */
function checkBatteryHealthWhenDis() {
  if (vBatt) {
    // Có giá trị Ắc quy cuối
    if (vBatt <= MIN_BATTERY_VOLTAGE_FOR_ESY) {
      // Điện áp ắc quy sụt nhiều -> có thể gây reset ESY
      Alert.alert(
        'Nguồn điện của xe yếu',
        `ESY phát hiện nguồn điện của xe vừa chạm mốc ${vBatt.toFixed(
          1,
        )}Volt\nĐiều này khiến xe và ESY hoạt động không ổn định\nVui lòng kiểm tra nguồn điện của xe`,
        [{text: 'Đồng ý'}],
        {cancelable: false},
      );
    }
  }
}

function handleUpdateState(args: Object): void {
  // state - String - the new BLE state ('on'/'off').
  state.bleIsOn = args.state === 'on';
  console.log('handleUpdateState', args.state, state.bleIsOn);

  Store.dispatch({type: 'UPDATE_NETINFO_BLE', values: state.bleIsOn});

  if (!state.bleIsOn) {
  }
}

async function handleConnectedPeripheral(data: Object) {
  console.log('handleConnectedPeripheral');
  if (state.connectState === BLE_CONNECT_STATE.CONNECTED) {
    console.log('handleConnectedPeripheral may trigger twice. Hence, skip');
    return;
  }
  state.connectState = BLE_CONNECT_STATE.CONNECTED;
  Timer_connect.clearTimer();
  const RS = await retrieveServices();
  if (!RS) {
    console.log('retrieveServices FAIL');
    return;
  }
  console.log('retrieveServices OKAY');

  const res = await startNotifySeedKeyChara();
  if (!res) {
    // Alert
    return;
  }

  startExchangeSeedKey();
}
/**
 * Start exchange seed and key
 */
async function startExchangeSeedKey() {
  Store.dispatch({
    type: 'BLE_FOR_ACTIVE_PROGRESS',
    values: BLE_PROGRESS.PROGRESS.VERIFY_DEVICE,
  });
  const bufferSeed = generateSeedForPhone();
  const res = await sendSeedToESY(bufferSeed);
}

async function handleUpdateCharaValue(data: Object) {
  // peripheral - String - the id of the peripheral
  // characteristic - String - the UUID of the characteristic
  // value - Array - the read value

  const characteristic = data.characteristic.toLowerCase();
  switch (characteristic) {
    case CHARA_KEY_RESPONSE:
      dealWithSeedKey(data.value, deviceInfo.deviceId);

    default:
      break;
  }
}

async function startNotifySeedKeyChara() {
  state.isNotifying = true;
  if (state.notifiedSeedKeyChara) {
    console.log('startNotifySeedKeyChara', 'already notified');
    return true;
  }
  console.log('startNotifySeedKeyChara');
  Timer_notifyUnsecuredChara.startTimer(() => {
    console.log('startNotifySeedKeyChara', 'timeout');
    // TODO: Lỗi BLE
  });

  let error;
  let res;
  [error, res] = await to(BleManager.startNotification(deviceInfo.addr, SERVICE_MAIN, CHARA_KEY_RESPONSE));
  Timer_notifyUnsecuredChara.clearTimer();
  if (error) {
    console.log('startNotifySeedKeyChara', error);
    if (error.includes(ERROR_133)) {
      // ANDROID if error = 133 -> do again
      // TODO: Lỗi BLE
    } else if (error.includes(ERROR_129)) {
      // TODO: Lỗi BLE
    } else if (error.includes('Peripheral did disconnect')) {
      // iOS Ngắt kết nối bất ngờ khi notify
    } else if (error.includes('disconnected')) {
      // Device not connected
    } else if (error.includes('Could not find service with UUID')) {
      // Không tìm thấy chara -> Cần tắt bật BLE để refrésh [iOS]
      Alert.alert(
        'Tắt và bật lại Bluetooth',
        'Ứng dụng cần khởi động lại Bluetooth để làm mới bộ nhớ đệm.\nVào “Cài đặt” -> “Bluetooth” -> Tắt và bật lại Bluetooth',
      );
      return;
    }
    return false;
  }
  state.notifiedSeedKeyChara = true;
  state.isNotifying = false;
  console.log('startNotifySeedKeyChara', 'succeed');
  return true;
}
async function retrieveServices() {
  console.log('retrieveServices', 'start');
  // start timer
  Timer_retrieveServices.startTimer(() => {
    console.log('retrieveServices', 'timeout');
  });

  let error;
  let res;
  [error, res] = await to(BleManager.retrieveServices(deviceInfo.addr));
  if (error) {
    console.log('retrieveServices', error, deviceInfo.addr);
    if (error.includes('Could not find service with UUID')) {
      // Không tìm thấy chara -> Cần tắt bật BLE để refrésh [iOS]
      Alert.alert(
        'Tắt và bật lại Bluetooth',
        'Ứng dụng cần khởi động lại Bluetooth để làm mới bộ nhớ đệm.\nVào “Cài đặt” -> “Bluetooth” -> Tắt và bật lại Bluetooth',
      );
      return;
    }
    if (error.includes('Peripheral not found or not connected')) {
      console.log('Lỗi wtf');
    }
    Timer_retrieveServices.clearTimer();
    return false;
  }
  Timer_retrieveServices.clearTimer();
  return true;
}

/**
 *                   HANDLE METHODS
 *
 * */
function handleDiscoverPeripheral(data: Object): void {
  if (state.found) {
    return;
  }
  if (!data.name) {
    return;
  }
  const adverPackage = data.advertising;

  if (isSavyDevice(adverPackage)) {
    // So sánh device Id
    const deviceId = getDeviceId(adverPackage); // get device Id, return Id if device is free and SAVY
    if (deviceId === null) return;
    const deviceVersion = getDeviceVersion(adverPackage);
    if (!deviceVersion) {
      deviceInfo.version = [1, 0, 0];
    } else if (!deviceInfo.version) {
      // Chưa có thông tin device version trong app -> lưu vào app, gửi về server
      deviceInfo.version = deviceVersion;
      // updateDeviceVerOnServer(deviceInfo.deviceId, deviceVersion); // Gửi lên server
      // updateDeviceInfoOnAS(deviceInfo); // Lưu vào app
    } else {
      deviceInfo.version = deviceVersion;
    }
    // Xem có ecu Id không
    const ecuId = getEcuId(adverPackage) || [];
    if (ecuId === null) {
      return;
    }
    deviceInfo.ecuId = ecuId;

    console.log('deviceVersion', deviceVersion);

    const pos = doContain(data, foundList);
    if (pos === -1) {
      // Chưa có trong list
      const _foundDevice = new DeviceInfo();
      _foundDevice.addr = data.id;
      _foundDevice.deviceId = deviceId;
      _foundDevice.version = deviceVersion || [1, 0, 0];
      // Lấy giá trị cường độ
      _foundDevice.rssi.value = data.rssi;
      _foundDevice.rssi.sampleCount++;
      // Thêm vào list
      foundList.push(_foundDevice);
    } else {
      // Tính trung bình giá trị cường độ
      foundList[pos].rssi.value =
        (foundList[pos].rssi.value * foundList[pos].rssi.sampleCount + data.rssi) /
        (foundList[pos].rssi.sampleCount + 1);
      // Tăng mẫu lên 1
      foundList[pos].rssi.sampleCount++;
    }
  }
}

function doContain(element: Object, foundList: DeviceInfo[]) {
  // array.forEach((value, index) => {
  //   if(element.id===value.id) return true;
  // });
  let pos = -1;
  for (let index = 0; index < foundList.length; index++) {
    const value = foundList[index];
    if (value.addr === element.id) {
      pos = index;
      break;
    }
  }
  return pos;
}

async function handleStopScan() {
  // handle when scan time out
  console.log('handleStopScan', 'found', foundList.length);
  state.isScanning = false;

  // Không tìm thấy
  if (foundList.length === 0) {
    await forceDisconnectAll(); // Ngắt kết nối các kết nối đang có
    alertError(BLE_PROGRESS.ERROR.NOT_FOUND);
  } else {
    // Tìm thấy 1 list thiết bị -> Update UI, check trên server
    Store.dispatch({
      type: 'BLE_FOR_ACTIVE_PROGRESS',
      values: BLE_PROGRESS.PROGRESS.VERIFY_DEVICE,
    });
    // Check trên server
    const newList: DeviceInfo[] = [];
    for (let i = 0; i < foundList.length; i++) {
      const device = foundList[i];
      const res = await fetchDataToActive.checkDevice(device.deviceId);
      if (res.error) {
        // Xảy ra lỗi trong quá trình check
        // -> Dừng, báo lỗi
        alertError(BLE_PROGRESS.ERROR.FAILURE);
        return;
      }
      if (res.data) {
        // Thiết bị hoàn toàn mới
        newList.push(device); // Thêm vào mảng buffer
      }
    }
    // Xem xét list thiết bị mới hoàn toàn tìm thấy
    let okayDevice = null;
    if (newList.length === 0) {
      // Thiết bị đã được sử dụng để kích hoạt tài khoản khác
    } else if (newList.length === 1) {
      // Có duy nhất 1 thiết bị thoả mãi
      okayDevice = newList[0];
    } else {
      // Có nhiều hơn 1 thiết bị
      // -> Chọn thiết bị gần nhất (rssi cao nhất)
      // So sánh lấy thiết bị có rssi cao nhất
      okayDevice = newList[0];
      for (let i = 1; i < newList.length; i++) {
        if (newList[i].rssi.value > okayDevice.rssi.value) {
          okayDevice = newList[i];
        }
      }
    }

    if (okayDevice) {
      // Kết nối và xác thực thiết bị trên qua BLE
      deviceInfo = okayDevice;

      if (compareVersions(deviceInfo.version, DEVICE_VERSION_2) >= 0) {
        ble.setESYVersion('ver2');
      } else {
        ble.setESYVersion('ver1');
      }
      connectBLE(deviceInfo.addr);
    } else {
      // Thiết bị đã được dùng
      alertError(BLE_PROGRESS.ERROR.DEVICE_IS_USED);
    }
  }
}

export async function sendSeedToESY(seed: number[]) {
  console.log('sendSeedToESY');
  Timer_writeChara.startTimer();
  let error;
  let res;
  const data = ble.generateCommand.keyS1(seed);
  [error, res] = await to(BleManager.write(deviceInfo.addr, SERVICE_MAIN, CHARA_SEND_KEY, data));
  if (error) {
    Timer_writeChara.clearTimer();
    console.log(error);
    return false;
  }
  Timer_writeChara.clearTimer();
  return true;
}

/**
 *                          Others
 */
function isSavyDevice(adverPackage: Object): boolean {
  try {
    if (Platform.OS === 'ios') {
      if (!adverPackage.manufacturerData) return false; // device not have kCBAdvDataManufacturerData
      const raw = adverPackage.manufacturerData.bytes;
      if (raw[0] == 0xff && raw[1] == 0xff) {
        return true;
      }
      return false;
    }
    if (Platform.OS === 'android') {
      if (!adverPackage.manufacturerData) return false; // device not have kCBAdvDataManufacturerData
      const raw = adverPackage.manufacturerData.bytes;
      if (raw[4] == 0xff && raw[5] == 0xff) {
        return true;
      }
      return false;
    }
    return false;
  } catch (e) {
    return false;
  }
}

function getDeviceId(adverPackage: Object): any {
  try {
    if (Platform.OS === 'ios') {
      const deviceId = []; // array length = 8
      const raw = adverPackage.manufacturerData.bytes;
      for (let i = 2; i <= 9; i++) {
        if (raw[i] === undefined) return null;
        deviceId.push(raw[i]);
      }
      return deviceId;
    }
    if (Platform.OS === 'android') {
      const deviceId = []; // array length = 8
      if (!adverPackage.manufacturerData) return false; // device not have kCBAdvDataManufacturerData
      const raw = adverPackage.manufacturerData.bytes;
      for (let i = 7; i <= 14; i++) {
        if (raw[i] === undefined) return null;
        deviceId.push(raw[i]);
      }
      return deviceId;
    }
  } catch (e) {
    return null;
  }
}
function getEcuId(adverPackage: Object): any {
  try {
    if (Platform.OS === 'ios') {
      const ecuId = []; // array length = 4
      const raw = adverPackage.manufacturerData.bytes;
      for (let i = 10; i <= 13; i++) {
        if (raw[i] === undefined) return null;
        ecuId.push(raw[i]);
      }
      return ecuId;
    }
    if (Platform.OS === 'android') {
      const ecuId = []; // array length = 8
      const raw = adverPackage.manufacturerData.bytes;
      for (let i = 15; i <= 18; i++) {
        if (raw[i] === undefined) return null;
        ecuId.push(raw[i]);
      }
      return ecuId;
    }
  } catch (e) {
    return null;
  }
}
function getDeviceVersion(adverPackage: Object) {
  const raw = adverPackage.manufacturerData.bytes;
  try {
    let startPos;
    if (Platform.OS === 'ios') {
      switch (raw.length) {
        case 13: // Ver 2
          startPos = 10;
          break;

        default:
          startPos = 15;
          break;
      }
    } else {
      switch (raw.length) {
        case 18: // Ver 2
          startPos = 15;
          break;

        default:
          startPos = 20;
          break;
      }
    }
    const _deviceVersion = []; // array length = 4
    for (let i = startPos; i <= startPos + 2; i++) {
      if (raw[i] === undefined) return null;
      _deviceVersion.push(raw[i]);
    }
    return _deviceVersion;
  } catch (e) {
    return null;
  }
}

async function disconnect() {
  console.log('disconnect', 'connectState', state.connectState);
  removeAllTimeoutHandler(); // remove all timeout
  let error;
  let res;
  [error, res] = await to(BleManager.disconnect(deviceInfo.addr));
}

function handleConnectingTimeout(): void {
  console.log('handleConnectingTimeout', ` connectState: ${state.connectState}`);
  if (state.connectState === BLE_CONNECT_STATE.CONNECTING || state.connectState === BLE_CONNECT_STATE.DISCONNECTED) {
  }
}

function to(promise) {
  // help catch error when await a promise http://blog.grossman.io/how-to-write-async-await-without-try-catch-blocks-in-javascript/
  /* EXAMPLE
           [err, savedTask] = await to(TaskModel({userId: user.id, name: 'Demo Task'}));
           if(err) return cb('Error occurred while saving task');
        */
  return promise
    .then(data => {
      return [null, data];
    })
    .catch(err => [err]);
}
