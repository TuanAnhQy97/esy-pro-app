/**
 * @flow
 */
import Store from '../../../Store';
import {fetchDataToActive} from '../../../Fetchers/DeviceFetcher';

import {compareArrays} from '../../../SharedFunctions';
import BleForNew from '..';
import {FETCH_ERROR} from '../../../Fetchers/Shared';
import {BLE_RESPONSE_DATA_TYPE} from '../../../constants';
import ble from '../../../services/ble';
/**
 *            CONSTANTS
 */
const seedKeyLength = 4; // bytes
const FLAG = Object.freeze({
  WAIT_SEED_2_FROM_ESY: 0,
  WAIT_CONFIRM_FROM_ESY: 1,
  DONE: 3,
});
const ARRAY_WRONG_KEY = Object.freeze([255, 255, 255]);
const ARRAY_TRUE_KEY = Object.freeze([1, 1, 1]);
/**
 *            BUFFERS
 */
let seed1 = [];
let seed2 = [];
let seed3 = []; // Buffer to store seed
let hasSeedKey = false;
/**
 * reset buffer of seed key
 */
function resetSeedKey() {
  seed1 = [];
  seed2 = [];
  seed3 = []; // Buffer to store seed
  hasSeedKey = false;
}
let progressFlag = FLAG.WAIT_SEED_2_FROM_ESY; // Flag manage process exchange seed key

/**
 * Random a seed to send to ESY
 */
export function generateSeedForPhone() {
  progressFlag = FLAG.WAIT_SEED_2_FROM_ESY; // Set flag
  let seed;
  if (!hasSeedKey) {
    seed = randomArrayNumber();
    seed1 = seed; // store to buffer
  } else {
    seed = seed1;
  }
  return seed;
}
/**
 * Random a array of seedKeyLength elements from 0 to 255
 */
function randomArrayNumber() {
  const seed = [];
  for (let i = 0; i < seedKeyLength; i++) {
    const index = Math.floor(Math.random() * 255);
    seed.push(index);
  }
  return seed;
}

export async function dealWithSeedKey(data: number[], deviceId: number[]) {
  const key = ble.extractDataFromResponse(data, BLE_RESPONSE_DATA_TYPE.SECURITY);
  console.log('dealWithSeedKey', 'hasSeedKey', hasSeedKey);
  switch (progressFlag) {
    case FLAG.WAIT_SEED_2_FROM_ESY:
      console.log('dealWithSeedKey', 'FLAG.WAIT_CONFIRM_FROM_ESY', key);
      if (!hasSeedKey) {
        // Check Key received from ESY. Send this seed key pair to Server to check
        const checkSeedKey = await fetchDataToActive.getKeyS3(deviceId, seed1, key);
        if (checkSeedKey.error) {
          // Không có mạng
          if (checkSeedKey.error === FETCH_ERROR.ERROR_NO_NETWORK) {
            console.log('dealWithSeedKey', 'Lấy key S3 lỗi mạng');
            BleForNew.cancelBleJob();
            BleForNew.alertError(BleForNew.BLE_PROGRESS.ERROR.NETWORK_ERROR);
          }
          // Không đúng seed key
          else if (checkSeedKey.error === FETCH_ERROR.ERROR_NO_INFO) {
            console.log('dealWithSeedKey', 'Sai seed 2');
            BleForNew.cancelBleJob();
            BleForNew.alertError(BleForNew.BLE_PROGRESS.ERROR.DEVICE_NOT_COMPATIBLE);
          }
        } else {
          seed2 = key;
          seed3 = checkSeedKey.data;
          const seed3Data = ble.generateCommand.keyS3(seed3);
          progressFlag = FLAG.WAIT_CONFIRM_FROM_ESY; // Set flag
          // Send key 2
          const res = await BleForNew.sendSeedToESY(seed3Data);
          if (!res) {
            BleForNew.resetBleConnection();
          }
        }
      } else if (compareArrays(key, seed2)) {
        progressFlag = FLAG.WAIT_CONFIRM_FROM_ESY; // Set flag
        const seed3Data = ble.generateCommand.keyS3(seed3);
        const res = await BleForNew.sendSeedToESY(seed3Data);
        if (!res) {
          BleForNew.resetBleConnection();
        }
      } else {
        console.log('dealWithSeedKey', 'Sai seed 2');
        BleForNew.cancelBleJob();
        BleForNew.alertError(BleForNew.BLE_PROGRESS.ERROR.DEVICE_NOT_COMPATIBLE);
      }
      break;
    case FLAG.WAIT_CONFIRM_FROM_ESY:
      console.log('dealWithSeedKey', 'FLAG.WAIT_CONFIRM_FROM_ESY', key);
      // Wrong key
      if (compareArrays(key, ARRAY_WRONG_KEY)) {
        console.log('dealWithSeedKey', 'Sai seed 3');
        resetSeedKey();
        progressFlag = FLAG.DONE;
        BleForNew.cancelBleJob();
        BleForNew.alertError(BleForNew.BLE_PROGRESS.ERROR.DEVICE_NOT_COMPATIBLE);
      } else if (compareArrays(key, ARRAY_TRUE_KEY) || compareArrays(key, [0])) {
        hasSeedKey = true;
        progressFlag = FLAG.DONE;
        const token = await fetchDataToActive.getActiveToken(deviceId, seed1, seed2, seed3);
        if (token.error) {
          // Không có mạng
          if (token.error === FETCH_ERROR.ERROR_NO_NETWORK) {
            BleForNew.cancelBleJob();
            BleForNew.alertError(BleForNew.BLE_PROGRESS.ERROR.NETWORK_ERROR);
          }
          // Lỗi khác
          else {
            BleForNew.cancelBleJob();
            BleForNew.alertError(BleForNew.BLE_PROGRESS.ERROR.FAILURE);
          }
        } else {
          BleForNew.verifySucceedCallback(token.data);
        }
      }
      break;
    default:
      console.log('dealWithSeedKey', 'FLAG other', key);
      break;
  }
}
