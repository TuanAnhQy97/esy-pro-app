/**
 * @flow
 */
import { NordicDFU, DFUEmitter } from "react-native-nordic-dfu";
import {
  Platform,
  Alert,
  NativeModules,
  NativeEventEmitter
} from "react-native";
import BleManager from "react-native-ble-manager";

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

import { to, checkLastestESYFWver } from "../Internet";
import { wait, compareArrays } from "../SharedFunctions";
import { resetBleConnection, deviceInfo } from "../Ble";
import { Timer_deleteBond } from "./Timer";

class MyInstance {
  callbackDFUProgress: Function = () => {};
  callbackDFUStateChange: Function = () => {};
  ignoreCheckUpdate = false;

  handlerDiscover: any;

  /**
   * Check if ESY HW have any Update
   * @param {*} currentVersion current version of ESY HW known through Adver. Package
   */
  async checkUpdateForHW(currentVersion: number[]) {
    console.log("checkUpdateForHW", currentVersion);
    if (!currentVersion) {
      // Phiên bản hiện tại không xác định
      return;
    }
    if (this.ignoreCheckUpdate) {
      return;
    }
    if (compareArrays(currentVersion, [1, 0, 0])) {
      // Với iOS, không cập nhật cho ESY 1.0.0, do bootloader không tương thích
      if (Platform.OS === "ios") {
        return;
      }
    }
    // Get lastest Version number from Server
    let lastestVersion = await checkLastestESYFWver();
    if (lastestVersion.error) {
      console.log("checkUpdateForHW", "error", lastestVersion.error);
      return;
    } else {
      lastestVersion = lastestVersion.data.version;
    }
    // Compare
    let needUpdate = compareESYVersion(currentVersion, lastestVersion);
    // If higher -> Alert
    if (needUpdate) {
      Alert.alert(
        "Cập nhật phần mềm cho phần cứng",
        "Phần cứng gắn trên xe có cập nhật mới. Bạn nên thực hiện ngay. Cập nhật miễn phí, quá trình diễn ra từ 3 đến 5 phút",
        [
          {
            text: "Để sau",
            onPress: () => {
              this.ignoreCheckUpdate = true;
            }
          },
          {
            text: "Cập nhật ngay",
            onPress: async () => {
              navigatePopToTop();
              resetBleConnection();
              await wait(1000); // Delay
              navigate("DFU", {
                deviceAddr: deviceInfo.addr,
                currentVersion: currentVersion,
                lastestVersion: lastestVersion
              });
            }
          }
        ],
        { cancelable: false }
      );
    }
  }
  /**
   * Check if ESY is stuck in DFU mode. Happen when DFU fails
   * @param {*} data - Advertising package of ESY
   */
  isEsyStuckInDfuMode(data: Object) {
    let adverPackage = data.advertising;
    let name = data.name;
    if (name === "DfuTarg") {
      //Check advertising package
      let ecuId = getEcuId(adverPackage);
      if (ecuId === null) {
        return [1, 0, 0];
      } else if (ecuId[0] !== 1) {
        return [1, 0, 0];
      }
      return "no";
    } else if (name === "ESYDFU") {
      // check device ID in advertising package
      let deviceId = getDeviceId(adverPackage);
      if (deviceId) {
        if (compareArrays(deviceId, deviceInfo.deviceId)) {
          let ecuId = getEcuId(adverPackage);
          if (ecuId === null) {
            return [1, 0, 1];
          } else if (ecuId[0] !== 1) {
            return [1, 0, 1];
          }
          return "no";
        }
      } else return "no";
    } else return "no";
  }
  /**
   * Start DFU progress
   * @param {*} deviceAddress MAC address (Android) / UUID (iOS)
   * @param {*} filePath FW location on storage
   */
  async startDFU(deviceAddress: string, filePath: string) {
    console.log("startDFU", deviceAddress, filePath);
    Timer_deleteBond.startTimer(onDeleteBondFailure);
    let res = await deleteBond(deviceAddress);
    Timer_deleteBond.clearTimer();
    if (!res) {
      onDeleteBondFailure();
      return;
    }
    NordicDFU.startDFU({
      deviceAddress: deviceAddress,
      deviceName: "ESY",
      filePath: filePath
    })
      .then(res => {
        console.log("Transfer done:", res);
      })
      .catch(e => {
        console.log("startDFU error ", e.code, e);
      });
  }
  /**
   * Callback for DFU progress changes
   * @param {*} callback callback for DFU progress
   * @example  ({ percent, currentPart, partsTotal, avgSpeed, speed }) => {
        console.log("DFU progress: " + percent + "%");
      }
   */
  registerDFUProgressCallback(callback: Function) {
    this.callbackDFUProgress = callback;
    DFUEmitter.addListener("DFUProgress", this.callbackDFUProgress);
  }
  /**
   * Callback when DFU state changes
   * @param {*} callback
   * @example 
   *  ({ state }) => {
        console.log("DFU State:", state);
      });
      
     @note value of state:
      - "DFU_FAILED"
   */
  registerDFUStateChangedCallback(callback: Function) {
    this.callbackDFUStateChange = callback;
    DFUEmitter.addListener("DFUStateChanged", this.callbackDFUStateChange);
  }
  unresgisterAllCallback() {
    DFUEmitter.removeListener("DFUProgress", this.callbackDFUProgress);
    DFUEmitter.removeListener("DFUStateChanged", this.callbackDFUStateChange);
  }
  /**
   * Add on Discover BLE event
   */
  addListenerBLE() {
    this.handlerDiscover = bleManagerEmitter.addListener(
      "BleManagerDiscoverPeripheral",
      this.onDiscover
    );
  }
  /**
   * Remove on Discover BLE event
   */
  removeListenerBLE() {
    this.handlerDiscover.remove();
  }
  onDiscover(data: Object) {
    if (data.name) console.log("onDiscoverDFU", data.name, data.id);
  }
}
async function deleteBond(deviceAddr: string) {
  if (Platform.OS === "android") {
    let [error, res] = await to(BleManager.getBondedPeripherals());
    if (error) {
      return false;
    } else {
      for (let i = 0; i < res.length; i++) {
        console.log("bonded device", res[i]);
        if (res[i].id === deviceAddr) {
          let [errorRemove, removeRes] = await to(
            BleManager.removeBond(deviceAddr)
          );
          if (errorRemove) {
            console.log("remove bond error ", errorRemove);
            return false;
          }
        } else {
          console.log("remove bond okay");
          return true;
        }
      }
      console.log("Not bond yet");
      return true;
    }
  } else return true;
}
/**
 * Do when delete bond not work. Display Alert
 */
function onDeleteBondFailure() {
  Alert.alert(
    "Lỗi bỏ ghép đôi",
    'Vào "Cài đặt" của điện thoại -> "Bluetooth" -> Chọn thiết bị ESY -> "Bỏ ghép đôi" và thử lại cập nhật"',
    [{ text: "Đồng ý", onPress: navigatePopToTop }],
    { cancelable: false }
  );
}
/**
 * Compare ESY Version. Current vs Lastest
 * @return true if lastest is higher
 * @note ESY version must contain 3 parts xx.xx.xx
 * @param {*} _current
 * @param {*} _lastest
 */
function compareESYVersion(_current: number[], _lastest: number[]) {
  console.log("compareESYVersion", _current, _lastest);
  let c = _current;
  let l = _lastest;

  if (l[0] > c[0]) {
    return true;
  } else if (l[0] === c[0]) {
    if (l[1] > c[1]) {
      return true;
    } else if (l[1] === c[1]) {
      if (l[2] > c[2]) {
        return true;
      } else if (l[2] === c[2]) {
        return false;
      } else {
        // l2 < c2
        return false;
      }
    } else {
      // l1 < c1
      return false;
    }
  } else {
    // l0 < c0
    return false;
  }
}
/**
 * Get device Id from advertising package of ESY
 * @param {*} adverPackage
 */
function getDeviceId(adverPackage: Object): any {
  try {
    if (Platform.OS === "ios") {
      let deviceId = []; // array length = 8
      let raw = adverPackage.manufacturerData.bytes;
      for (let i = 2; i <= 9; i++) {
        if (raw[i] === undefined) return null;
        deviceId.push(raw[i]);
      }
      return deviceId;
    } else if (Platform.OS === "android") {
      let deviceId = []; // array length = 8
      if (!adverPackage.manufacturerData) return false; // device not have kCBAdvDataManufacturerData
      let raw = adverPackage.manufacturerData.bytes;
      for (let i = 7; i <= 14; i++) {
        if (raw[i] === undefined) return null;
        deviceId.push(raw[i]);
      }
      return deviceId;
    }
  } catch (e) {
    return null;
  }
}
/**
 * Get ECU Id from advertising package of ESY
 * @param {*} adverPackage
 */
function getEcuId(adverPackage: Object): any {
  try {
    if (Platform.OS === "ios") {
      let ecuId = []; // array length = 4
      let raw = adverPackage.manufacturerData.bytes;
      for (let i = 10; i <= 13; i++) {
        if (raw[i] === undefined) return null;
        ecuId.push(raw[i]);
      }
      return ecuId;
    } else if (Platform.OS === "android") {
      let ecuId = []; // array length = 8
      let raw = adverPackage.manufacturerData.bytes;
      for (let i = 15; i <= 18; i++) {
        if (raw[i] === undefined) return null;
        ecuId.push(raw[i]);
      }
      return ecuId;
    }
  } catch (e) {
    return null;
  }
}

class DfuHandler {
  static instance: MyInstance;
  static getInstance() {
    if (!DfuHandler.instance) {
      DfuHandler.instance = new MyInstance();
    }
    return DfuHandler.instance;
  }
}
export default DfuHandler.getInstance();
