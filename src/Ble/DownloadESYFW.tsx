/**
 * @flow
 */
import RNFetchBlob from "rn-fetch-blob";
import { Platform } from "react-native";

import RNFS from "react-native-fs";
import { rootDomain } from "../Config";
import { loginLocal } from "../Internet";
import Store from "../Store";

/* eslint-disable */
class MyInstance {
  path: string;
  task: any;
  process: number;
  isDownloading: boolean = false;
  /**
   * @param process return quá trình download
   * @returns Promise<{
   *                    rsLogin: {code: number, err: string} | null,
   *                    path: string | null
   *                  }>
   * Nếu có rsLogin thì không có path và ngược lại.
   * Khi có rsLogin cần navigate đến LoginScreen để tiến hành đăng nhập,
   * đăng nhập thành công thì quay lại download, lúc này sẽ có path và không có rsLogin
   * @throws return null
   */
  async download(
    process?: Function = (value: number) => {
      console.log("downloadProgress ", value);
    },
    currentVersion: number[]
  ): Promise<{ errorCode: Object | null, path: string | null }> {
    this.isDownloading = true;
    let errorCode = null;
    this.path = "";

    // Config download
    this.task = RNFetchBlob.config({
      path: `${RNFS.DocumentDirectoryPath}/DFU.zip`
    }).fetch(
      "POST",
      rootDomain + "get/ESYFW",
      {
        "Content-Type": "application/json"
      },
      JSON.stringify({ currentVersion })
    );

    // Config progress callback
    this.task.progress({ interval: 75 }, (received, total) => {
      process((received / total) * 100);
    });

    // Execute download
    await this.task
      .then(async res => {
        console.log("DOWNLOAD task res ", res);
        this.isDownloading = false;
        try {
          console.log("download res type", typeof res);
          let json;
          if (Platform.OS === "android") {
            if (typeof res === "string") {
              json = await res.json(); // Thử json kết quả trả về
            }
          } else {
            json = await res.json(); // Thử json kết quả trả về
          }
          console.log("json", json);
          // json okay
          if (json.error) {
            // Có lỗi
            errorCode = json.error;
          } else {
            if (res.respInfo.status === 200) {
              this.path = res.path();
            } else {
              errorCode = -1;
            }
          }
        } catch (error) {
          console.log("DOWNLOAD OKAY");
          // Không json được -> Download Okay
          if (res.respInfo.status === 200) {
            this.path = res.path();
          } else {
            errorCode = -1;
          }
        }
      })
      .catch(error => {
        console.log("DOWNLOAD error: ", error);
      });
    console.log("errorCode", errorCode);
    if (errorCode) {
      if (errorCode === 2) {
        /// Try login if need
        console.log("retryLogin start");
        let loginRes = await retryLogin();
        if (loginRes) {
          console.log("retryLogin Okay");
          // Login okay -> retry
        } else {
          console.log("retryLogin error");
          // Login failed -> Stop
          return { errorCode: { error: -1 }, path: null };
        }
      }
      console.log("Retry download");
      // Retry download
      // Config download
      this.task = RNFetchBlob.config({
        path: `${RNFS.DocumentDirectoryPath}/DFU.zip`
      }).fetch(
        "POST",
        rootDomain + "get/ESYFW",
        {
          "Content-Type": "application/json"
        },
        JSON.stringify({ currentVersion })
      );

      // Config progress callback
      this.task.progress({ interval: 75 }, (received, total) => {
        process((received / total) * 100);
      });

      this.isDownloading = true;
      await this.task.then(async resRetry => {
        this.isDownloading = false;
        try {
          let json;
          if (Platform.OS === "android") {
            if (typeof resRetry === "string") {
              json = await resRetry.json(); // Thử json kết quả trả về
            }
          } else {
            json = await resRetry.json(); // Thử json kết quả trả về
          }
          console.log("Retry download", "json", json);
          if (json.error) {
            errorCode = json.error;
          } else {
            if (resRetry.respInfo.status === 200) this.path = resRetry.path();
          }
        } catch (e) {
          // Download Okay
          console.log("Retry download", "Okay");
          if (resRetry.respInfo.status === 200) {
            this.path = resRetry.path();
            errorCode = null;
          }
        }
      });
    }
    // Check flag to return result
    if (errorCode !== null) {
      return { errorCode: errorCode, path: null };
    } else if (this.path !== "") {
      return { errorCode: null, path: this.path };
    } else {
      console.log("ERROR unknown");
      return { errorCode: { error: -1 }, path: null };
    }
  }
  /**
   * Return download status: doing or not
   */
  getDownloading = (): boolean => {
    return this.isDownloading;
  };
  /**
   * Remove downloaded file
   */
  removeDownloadedFile() {
    try {
      RNFetchBlob.fs.unlink(`${RNFS.DocumentDirectoryPath}/DFU.zip`);
    } catch (e) {}
  }
  /**
   * Hủy download
   * Khi dùng hàm này cần bỏ qua giá trị của hàm download trả về
   */
  cancel() {
    this.task.cancel();
  }
}
async function retryLogin() {
  let resLoginLocal = await loginLocal(Store.getState().DucReducer.credentials);
  if (resLoginLocal.error) {
    // Lỗi đăng nhập
    return false;
  } else {
    // Đăng nhập okay
    return true;
  }
}

class DownloadZip {
  static instance: MyInstance;
  static getInstance(): MyInstance {
    if (!this.instance) {
      this.instance = new MyInstance();
      return this.instance;
    }
    return this.instance;
  }
}
export default DownloadZip.getInstance();
