/**
 * @flow
 */

import React, { Component } from "react";
import Orientation from "react-native-orientation-locker";
import BikeLoading from "../Components/Animates/BikeLoading";
import ASHelper from "../ASHelper";
import { wait } from "../SharedFunctions";

class AppLoadingScreen extends React.PureComponent<{ navigation: any }, {}> {
  render() {
    return <BikeLoading />;
  }

  async componentDidMount() {
    console.log("AuthLoadingScreen");
    await wait(1500);
    // check token from storage
    const credentials = await ASHelper.loadCredentialsFromAS();
    if (credentials === null) {
      // set Login screen
      ASHelper.clearAS(); // clear all info of all
      Orientation.lockToPortrait();
      this.props.navigation.navigate("Auth");
    } else {
      // has token
      const device = await ASHelper.loadDeviceInfoFromAS();
      if (device === null) {
        ASHelper.clearAS(); // clear all info of all
        this.props.navigation.navigate("AppLoading");
      } else {
        this.props.navigation.navigate("Menu", { device });
      }
    }
  }
}

export default AppLoadingScreen;
