/**
 * @flow
 */
import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  KeyboardAvoidingView,
  Keyboard
} from "react-native";
import Text from "../../../../Components/Text";
import { horizontalScale, verticalScale } from "../../../../ScaleUtility";
import { colors } from "../../../../styles/colors";
import { getInternetState } from "../../../../Fetchers/InternetChangeHandler";
import TextInputWrapper from "../../../../Components/TextInputWrapper";
import OverLayWaiting, {
  tools as OverLayWaiting_tools
} from "../CreateAcc/OverLayWaiting";
import { loginLocal } from "../../../../Fetchers/Shared";
import { Credentials } from "../../../../Model/Credentials";
import { fetchDataForPaired } from "../../../../Fetchers/DeviceFetcher";
import { AccountFetcher } from "../../../../Fetchers/AccountFetcher";
import ASHelper from "../../../../ASHelper";
import navigationService from "../../../../services/navigationService";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };
const hitSlop_input = { top: 5, bottom: 5, left: 5, right: 5 };

type Props = {};

type States = {
  phone: string;
  password: string;
  warning: string;
  autoLogin: boolean;
};

class LoginScreen extends React.PureComponent<Props, States> {
  containerPhone: any;

  containerPw: any;

  inputPw: any;

  inputPhone: any;

  constructor(props: Props) {
    super(props);
    const phone = props.navigation.getParam("phone", "");
    const password = props.navigation.getParam("password", "");
    const autoLogin = props.navigation.getParam("autoLogin", false);
    this.state = { phone, password, warning: "", autoLogin };
  }

  componentDidMount() {
    if (this.state.autoLogin) {
      this.onPressLogin();
    }
  }

  componentWillUnmount() {}

  render() {
    const {} = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: colors.primary_light }}>
        {this.renderBody()}
        <OverLayWaiting
          ref={_ref => {
            OverLayWaiting_tools.setRef(_ref);
          }}
        />
      </View>
    );
  }

  renderBody() {
    const logoSize = verticalScale(91);
    const inputIcon = horizontalScale(14);
    return (
      <SafeAreaView style={styles.rootContainer}>
        <KeyboardAvoidingView behavior="padding">
          {/* Logo + App name  */}
          <View
            style={{ marginBottom: verticalScale(68), alignItems: "center" }}
          >
            {/* Logo   */}
            <ImageBackground
              style={{ height: logoSize, width: logoSize }}
              resizeMode="contain"
              source={require("../../../../../assets/Images/Share/logoLogin.png")}
            />
            {/*  App name  */}
            <View
              style={{ flexDirection: "row", marginTop: verticalScale(23) }}
            >
              <Text
                style={{
                  fontSize: horizontalScale(24),
                  fontWeight: "bold",
                  color: colors.secondary_dark
                }}
              >
                ESY{" "}
              </Text>
              <Text
                style={{
                  fontSize: horizontalScale(24),
                  fontWeight: "bold",
                  color: colors.third_dark
                }}
              >
                PRO
              </Text>
            </View>
          </View>
          {/* Thông tin đăng nhập + Các nút */}
          {/* <Text style={styles.headerText}>Số điện thoại</Text> */}
          <TextInputWrapper
            ref={ref => {
              this.containerPhone = ref;
            }}
          >
            <View style={[styles.inputWrapper, {}]}>
              <ImageBackground
                imageStyle={{ tintColor: colors.third_dark }}
                style={{
                  width: inputIcon,
                  height: inputIcon,
                  marginRight: 17.5
                }}
                source={require("../../../../../assets/Images/Share/phone.png")}
                resizeMode="contain"
              />
              <TextInput
                ref={_ref => {
                  this.inputPhone = _ref;
                }}
                value={this.state.phone}
                maxLength={10}
                hitSlop={hitSlop_input}
                placeholderTextColor={colors.third_dark}
                onChangeText={this.onChangeTextPhone.bind(this)}
                onSubmitEditing={this.onSubmitEditingPhone.bind(this)}
                style={styles.inputPhone}
                placeholder="Số điện thoại"
                underlineColorAndroid="transparent"
                keyboardType="number-pad"
                autoCapitalize="words"
                autoCorrect={false}
                returnKeyType="next"
              />
            </View>
          </TextInputWrapper>

          {/* <Text style={styles.headerText}>Mật khẩu</Text> */}
          <TextInputWrapper
            ref={ref => {
              this.containerPw = ref;
            }}
          >
            <View style={[styles.inputWrapper, { marginBottom: 0 }]}>
              <ImageBackground
                imageStyle={{ tintColor: colors.third_dark }}
                style={{
                  width: inputIcon,
                  height: inputIcon,
                  marginRight: 17.5
                }}
                source={require("../../../../../assets/Images/Share/lock.png")}
                resizeMode="contain"
              />
              <TextInput
                ref={_ref => {
                  this.inputPw = _ref;
                }}
                value={this.state.password}
                maxLength={6}
                hitSlop={hitSlop_input}
                placeholderTextColor={colors.third_dark}
                onChangeText={this.onChangeTextPassword.bind(this)}
                onSubmitEditing={this.onSubmitEditingPassword.bind(this)}
                style={styles.inputPw}
                underlineColorAndroid="transparent"
                placeholder="******"
                keyboardType="number-pad"
                returnKeyType="done"
                secureTextEntry
              />
            </View>
          </TextInputWrapper>
        </KeyboardAvoidingView>
        {this.renderWarning()}
        {/* Nút đăng nhập */}
        <TouchableOpacity
          onPress={this.onPressLogin.bind(this)}
          style={styles.loginBut}
        >
          <Text style={styles.loginText}>Đăng nhập</Text>
        </TouchableOpacity>
        {/* Quên mật khẩu */}
        <TouchableOpacity
          style={styles.forgetPwBut}
          onPress={this.onPressForgetPw}
        >
          <Text style={styles.forgetText}>Quên mật khẩu ?</Text>
        </TouchableOpacity>
        {/* Seperate line */}
        <View style={styles.line} />
        {/* Tạo tài khoản */}
        <TouchableOpacity
          style={styles.createAccBut}
          onPress={this.onPressCreateAcc}
        >
          <Text style={styles.forgetText}>Chưa có tài khoản ? </Text>
          <Text style={styles.createAccText}>ĐĂNG KÝ</Text>
        </TouchableOpacity>

        {/* Dùng thử */}
        <TouchableOpacity
          style={styles.trialBut}
          onPress={() => {
            navigationService.navigate("TrialList");
          }}
        >
          <Text style={styles.createAccText}>Dùng thử</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }

  renderWarning() {
    const str = this.state.warning;

    if (str) {
      return <Text style={styles.warningText}>{str}</Text>;
    }
    return <View style={styles.warningEmpty} />;
  }

  onChangeTextPhone(text: string) {
    this.setState({ phone: text });
  }

  onChangeTextPassword(text: string) {
    this.setState({ password: text });
  }

  async onPressLogin() {
    Keyboard.dismiss();
    if (!getInternetState()) {
      navigationService.showAlertNoInternet();
    } else {
      const { phone, password } = this.state;
      const isPhoneValid = validatePhone(phone);
      if (isPhoneValid) {
        const isPwValid = validatePassword(password);
        if (isPwValid) {
          // Có vẻ đúng form -> Tiến hành đăng nhập
          this.setState({ warning: "" });
          OverLayWaiting_tools.showPopup(true);
          // Xem số điện thoại có tồn tại hay không
          // Đăng nhập
          const phoneWithRegionCode = `+84${phone.slice(1)}`;
          const credential = new Credentials();
          credential.phone = phoneWithRegionCode;
          credential.password = password;
          const resLogin = await loginLocal(credential);
          if (resLogin.error) {
            OverLayWaiting_tools.showPopup(false);
            this.setState({
              warning: "Tài khoản không đúng hoặc không tồn tại"
            });
            return;
          }
          // Lấy thông tin thiết bị
          const device = await fetchDataForPaired.getPairedDevice();

          if (device.error) {
            OverLayWaiting_tools.showPopup(false);
            this.setState({
              warning:
                "Xảy ra lỗi trong quá trình đăng nhập. Vui lòng thử lại sau"
            });
            return;
          }
          // Lấy thông tin người dùng
          const userInfo = await AccountFetcher.getInfo();
          if (userInfo.error) {
            OverLayWaiting_tools.showPopup(false);
            this.setState({
              warning:
                "Xảy ra lỗi trong quá trình đăng nhập. Vui lòng thử lại sau"
            });
            return;
          }
          // Lưu thông tin đăng nhập
          await ASHelper.replaceDeviceInfoOnAS(device.data);
          await ASHelper.updateCredentialsOnAS(credential);
          await ASHelper.replaceUserInfoOnAS(userInfo.data);
          // Chuyển màn
          OverLayWaiting_tools.showPopup(false);
          navigationService.navigate("AppLoading");
        } else {
          // Mật khẩu không đúng
          this.setState({ warning: "Mật khẩu hợp lệ" });
          this.containerPw.shake();
        }
      } else {
        // Số đt không đúng form
        this.setState({ warning: "Số điện thoại không hợp lệ" });
        this.containerPhone.shake();
      }
    }
  }

  onSubmitEditingPhone() {
    // To password
    this.inputPw.focus();
  }

  onSubmitEditingPassword() {
    // To press login
    this.onPressLogin();
  }

  onPressCreateAcc() {
    if (!getInternetState()) {
      navigationService.showAlertNoInternet();
    } else {
      navigationService.navigate("ActiveDevice");
    }
  }

  onPressForgetPw() {
    if (!getInternetState()) {
      navigationService.showAlertNoInternet();
    } else {
      navigationService.navigate("ForgetPW");
    }
  }
}
const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    justifyContent: "flex-end",
    marginHorizontal: horizontalScale(44)
  },
  headerText: {
    fontSize: horizontalScale(18),
    color: colors.secondary_dark,
    marginBottom: verticalScale(10)
  },
  inputWrapper: {
    flexDirection: "row",
    alignItems: "center",
    borderBottomColor: colors.third_dark,
    borderBottomWidth: 1,
    marginBottom: verticalScale(13)
  },
  inputPhone: {
    padding: 0,
    fontSize: horizontalScale(18),
    height: horizontalScale(40),
    flex: 1
    // color: colors.primary_light,
  },
  inputPw: {
    padding: 0,
    fontSize: horizontalScale(18),
    height: horizontalScale(40),
    flex: 1
    // color: colors.primary_light,
  },
  loginBut: {
    padding: horizontalScale(15),
    alignItems: "center",
    backgroundColor: colors.secondary_dark,
    borderRadius: 8
  },
  loginText: {
    color: colors.primary_light,
    fontSize: horizontalScale(16)
  },
  forgetPwBut: {
    alignItems: "center",
    paddingBottom: verticalScale(24),
    paddingTop: verticalScale(29)
  },
  forgetText: {
    color: colors.third_dark,
    fontSize: horizontalScale(14)
  },
  line: {
    borderTopColor: "black",
    borderTopWidth: 1
  },
  createAccText: {
    color: colors.primary_dark,
    fontWeight: "bold",
    fontSize: horizontalScale(14)
  },
  createAccBut: {
    flexDirection: "row",
    justifyContent: "center",
    paddingBottom: verticalScale(28),
    paddingTop: verticalScale(34)
  },
  trialBut: {
    paddingTop: 15,
    paddingBottom: 15,
    flexDirection: "row",
    justifyContent: "center"
  },
  warningText: {
    color: colors.error,
    fontSize: horizontalScale(16),
    marginVertical: verticalScale(21)
  },
  warningEmpty: {
    height: verticalScale(38)
  }
});
export default LoginScreen;
/**
 * Kiểm tra số điện thoại
 * @param {*} phone
 */
function validatePhone(phone: string) {
  let valid = true;
  if (phone.length === 10) {
    if (/^\d+$/.test(phone)) {
      // Chỉ bao gồm số
    } else {
      // Gồm ký tự khác
      valid = false;
    }
  } else {
    // Độ dài không đủ
    valid = false;
  }
  return valid;
}
/**
 * Kiểm tra mật khẩu
 * @param {*} password
 */
function validatePassword(password: string) {
  let valid = true;
  if (password.length === 6) {
    if (/^\d+$/.test(password)) {
      // Chỉ bao gồm số
    } else {
      // Gồm ký tự khác
      valid = false;
    }
  } else {
    // Độ dài không đủ
    valid = false;
  }
  return valid;
}
