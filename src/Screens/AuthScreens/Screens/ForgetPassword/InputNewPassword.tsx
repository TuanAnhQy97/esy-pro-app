/**
 * @flow
 */
import React, { PureComponent } from "react";
import {
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Alert,
  KeyboardAvoidingView,
  Keyboard
} from "react-native";
import Text from "../../../../Components/Text";
import { horizontalScale } from "../../../../ScaleUtility";
import { colors } from "../../../../styles/colors";

import TextInputWrapper from "../../../../Components/TextInputWrapper";
import BaseScreen from "../../../../Components/BaseScreen";
import NavBar from "../../SharedCom/NavBar";
import { AccountFetcher } from "../../../../Fetchers/AccountFetcher";
import OverLayWaiting, {
  tools as OverLayWaiting_tools
} from "./OverLayWaiting";
import ASHelper from "../../../../ASHelper";
import { DeviceInfo } from "../../../../Model/DeviceInfo";
import { Credentials } from "../../../../Model/Credentials";
import navigationService from "../../../../services/navigationService";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

type States = {
  idToken: string,
  phone: string,
  /**
   * Input info
   */
  password: string,
  confirmPw: string,
  // showWarning
  warning_password: boolean,
  warning_confirmPw: boolean
};
type Props = { navigation: Object };

type INPUT_FIELD = "password" | "confirmPw";

class InputNewAccInfo extends PureComponent<Props, States> {
  containerRef_password: any;
  containerRef_confirmPassword: any;

  inputRef_confirmPw: any;

  constructor(props: Object) {
    super(props);
    let idToken = props.navigation.getParam("idToken");
    let phone = props.navigation.getParam("phone");
    this.state = {
      idToken,
      phone,
      /**
       * Input info
       */
      password: "",
      confirmPw: "",
      // showWarning
      warning_password: false,
      warning_confirmPw: false
    };
  }

  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    const {} = this.state;
    return (
      <View style={{ flex: 1 }}>
        <BaseScreen
          statusbarStyle="dark-content"
          backgroundColor={colors.primary_light}
        >
          <NavBar onLeftPress={this.onPressBack} title="Nhập mật khẩu mới" />
          {this.renderBody()}
        </BaseScreen>
        <OverLayWaiting
          ref={_ref => {
            OverLayWaiting_tools.setRef(_ref);
          }}
        />
      </View>
    );
  }

  renderBody() {
    return (
      <View
        style={{
          padding: 20
        }}
      >
        {/* Input Forms */}
        <KeyboardAvoidingView behavior="padding">
          {this.renderFormInput()}
        </KeyboardAvoidingView>
        {/* Continue Button */}
        <TouchableOpacity
          hitSlop={hitSlop}
          style={styles.continueBut}
          onPress={this.onPressContinue.bind(this)}
        >
          <Text style={styles.continueText}>Tiếp tục</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderFormInput() {
    return (
      <View>
        {/* Password */}
        <Text style={styles.headerText}>Mật khẩu mới (6 chữ số)</Text>
        <TextInputWrapper
          ref={ref => {
            this.containerRef_password = ref;
          }}
        >
          <TextInput
            onSubmitEditing={this.onSubmitEditingPw.bind(this)}
            maxLength={6}
            hitSlop={hitSlop}
            style={styles.inputText}
            secureTextEntry={true}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            returnKeyType="next"
            onChangeText={(text: string) => {
              this.onChangeText.bind(this)("password", text);
            }}
          />
        </TextInputWrapper>
        {this.renderWarning("password")}

        {/* Store name */}
        <Text style={styles.headerText}>Xác nhận mật khẩu</Text>
        <TextInputWrapper
          ref={ref => {
            this.containerRef_confirmPassword = ref;
          }}
        >
          <TextInput
            ref={_ref => {
              this.inputRef_confirmPw = _ref;
            }}
            onSubmitEditing={this.onPressContinue.bind(this)}
            maxLength={6}
            hitSlop={hitSlop}
            style={styles.inputText}
            secureTextEntry={true}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            returnKeyType="done"
            onChangeText={(text: string) => {
              this.onChangeText.bind(this)("confirmPw", text);
            }}
          />
        </TextInputWrapper>
        {this.renderWarning("confirmPw")}
      </View>
    );
  }

  renderWarning(field: INPUT_FIELD) {
    let str;
    let notOkay = false;
    switch (field) {
      case "password":
        notOkay = this.state.warning_password;
        str = "Mật khẩu không hợp lệ";
        break;

      case "confirmPw":
        notOkay = this.state.warning_confirmPw;
        str = "Mật khẩu không khớp";
        break;

      default:
        str = "";
        break;
    }

    if (notOkay) {
      return <Text style={styles.warningText}>{str}</Text>;
    } else {
      return null;
    }
  }

  onChangeText(field: INPUT_FIELD, text: string) {
    switch (field) {
      case "password":
        this.setState({ password: text });
        break;

      case "confirmPw":
        this.setState({ confirmPw: text });
        break;

      default:
        break;
    }
  }

  onSubmitEditingPw() {
    this.inputRef_confirmPw.focus();
  }

  onPressContinue() {
    Keyboard.dismiss();
    // Validate Infomation
    const { password, phone, idToken } = this.state;
    let warning_password = false;
    // Validate Password
    if (password.length === 6) {
      if (/^\d+$/.test(password)) {
        // Chỉ bao gồm số
      } else {
        // Gồm ký tự khác
        warning_password = true;
      }
    } else {
      // Độ dài không đủ
      warning_password = true;
    }
    // Validate Store name
    const { confirmPw } = this.state;
    let warning_confirmPw = false;
    // Validate Password
    if (confirmPw.trim()) {
      if (password === confirmPw) {
      } else {
        // Khác nhau
        warning_confirmPw = true;
      }
    } else {
      // Độ dài không đủ
      warning_confirmPw = true;
    }
    // Update UI
    this.setState(
      {
        warning_password,
        warning_confirmPw
      },
      async () => {
        if (warning_password || warning_confirmPw) {
          // Shake if need
          if (warning_password) {
            this.containerRef_password.shake();
          }
          if (warning_confirmPw) {
            this.containerRef_confirmPassword.shake();
          }
        } else {
          // Everything is okay
          // Reset password on server
          OverLayWaiting_tools.showPopup(true);
          let res = await AccountFetcher.resetPassword(password, idToken);
          console.log("resetPw", res);
          OverLayWaiting_tools.showPopup(false);
          if (res.error) {
            // Có lỗi xảy ra
            let title = "Đổi mật khẩu không thành công";
            let message = "Kiểm tra internet và thử lại";
            Alert.alert(title, message, [{ text: "Đồng ý" }], {
              cancelable: false
            });
          } else {
            // Đổi okay
            // Về màn Login với phone và mật khẩu
            let p = { phone, password };
            navigationService.navigate("Login", p);
          }
        }
      }
    );
  }

  onPressBack() {
    Alert.alert("Bạn muốn thay đổi số điện thoại?", "", [
      {
        text: "Đồng ý",
        onPress: navigationService.goBack
      },
      { text: "Bỏ qua" }
    ]);
  }
}

const styles = StyleSheet.create({
  explainText: {
    fontSize: horizontalScale(16),
    color: colors.third_dark
  },
  headerText: {
    fontSize: horizontalScale(18)
    // color: colors.secondary_dark
  },
  inputText: {
    paddingTop: 10,
    fontSize: horizontalScale(18),
    borderBottomColor: "black",
    borderBottomWidth: 1,
    marginBottom: 13,
    minHeight: 44
  },
  continueBut: {
    fontSize: horizontalScale(18),
    marginTop: 36,
    marginBottom: 50,
    alignSelf: "flex-end"
  },
  continueText: {
    fontSize: horizontalScale(18),
    color: colors.secondary_dark
  },
  warningText: {
    color: colors.error,
    marginBottom: 13
  }
});
export default InputNewAccInfo;
