/**
 * @flow
 */
import React, { PureComponent } from "react";
import {
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  KeyboardAvoidingView,
  Keyboard
} from "react-native";
import Text from "../../../../Components/Text";
import { horizontalScale } from "../../../../ScaleUtility";
import { colors } from "../../../../styles/colors";
import { to } from "../../../../SharedFunctions";

import firebase from "react-native-firebase";
import TextInputWrapper from "../../../../Components/TextInputWrapper";
import MyActivityIndicator from "../../../../Components/MyActivityIndicator";
import NavBar from "../../SharedCom/NavBar";
import BaseScreen from "../../../../Components/BaseScreen";
import { AccountFetcher } from "../../../../Fetchers/AccountFetcher";
import navigationService from "../../../../services/navigationService";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

type States = {
  isLoading: boolean,
  codeInput: string,
  phoneNumber: string,
  phoneWithRegionCode: string,
  confirmResult: null | Object,
  warning: string
};
type Props = {
  navigation: Object
};

class InputPhoneNumber extends PureComponent<Props, States> {
  anti_multi_afterHasUser: boolean = false;

  unsubscribe: Function;
  inputRef: any;

  constructor(props: Props) {
    super(props);
    this.state = {
      isLoading: true,
      phoneNumber: "",
      phoneWithRegionCode: "",
      confirmResult: null,
      codeInput: "",
      warning: ""
    };
  }

  async componentDidMount() {
    await to(firebase.auth().signOut()); // Bỏ đăng nhập trước (nếu có)

    // Nghe sự kiện auth change
    this.unsubscribe = firebase.auth().onAuthStateChanged(user => {
      if (user) {
        console.log("onAuthStateChanged", "verify phone succeed");
        this.afterHasUser.bind(this)(user);
      } else {
        // User has been signed out, reset the state
      }
    });
    this.setState({ isLoading: false });
  }

  componentWillUnmount() {
    // Bỏ nghe sự kiện auth change
    if (this.unsubscribe) {
      this.unsubscribe();
    }
  }

  render() {
    const { isLoading } = this.state;
    if (isLoading) {
      return <MyActivityIndicator />;
    } else {
      return (
        <BaseScreen
          statusbarStyle="dark-content"
          backgroundColor={colors.primary_light}
        >
          <NavBar
            onLeftPress={this.onPressBack.bind(this)}
            title="Quên mật khẩu"
          />
          {this.renderBody()}
        </BaseScreen>
      );
    }
  }

  renderBody() {
    return (
      <View style={{ flex: 1, paddingHorizontal: 20 }}>
        {this.renderExplain()}
        {this.renderInputPhone()}
        {this.renderInputCode()}
      </View>
    );
  }

  renderExplain() {
    let explain;
    const { confirmResult } = this.state;
    if (!confirmResult) {
      explain = "Vui lòng nhập số điện thoại tài khoản của bạn";
    } else {
      explain = "Nhập mã xác nhận trong tin nhắn gửi đến số điện thoại đã nhập";
    }
    return (
      <View style={styles.explainContainer}>
        <Text style={styles.headerText}>Xác nhận số điện thoại</Text>
        <Text style={styles.explainText}>{explain}</Text>
      </View>
    );
  }

  renderInputPhone() {
    const { confirmResult } = this.state;
    if (!confirmResult) {
      return (
        <View>
          <KeyboardAvoidingView
            behavior="padding"
            style={{}}
            keyboardVerticalOffset={64}
            enabled={true}
          >
            <Text style={styles.headerText}>Số điện thoại</Text>
            <TextInputWrapper
              ref={ref => {
                this.inputRef = ref;
              }}
            >
              <TextInput
                maxLength={10}
                onSubmitEditing={this.submitPhone.bind(this)}
                hitSlop={hitSlop}
                style={styles.inputText}
                keyboardType="number-pad"
                returnKeyType="done"
                onChangeText={(text: string) => {
                  this.onChangeTextPhone.bind(this)(text);
                }}
              />
            </TextInputWrapper>
            {this.renderWarning()}
          </KeyboardAvoidingView>
          <TouchableOpacity
            hitSlop={hitSlop}
            style={styles.continueBut}
            onPress={this.submitPhone.bind(this)}
          >
            <Text style={styles.continueText}>Tiếp tục</Text>
          </TouchableOpacity>
        </View>
      );
    }
  }

  renderInputCode() {
    const { confirmResult } = this.state;
    if (confirmResult) {
      return (
        <View>
          <KeyboardAvoidingView
            behavior="padding"
            style={{}}
            keyboardVerticalOffset={64}
            enabled={true}
          >
            <Text style={styles.headerText}>Mã xác nhận</Text>
            <TextInputWrapper
              ref={ref => {
                this.inputRef = ref;
              }}
            >
              <TextInput
                onSubmitEditing={this.confirmCode.bind(this)}
                hitSlop={hitSlop}
                style={styles.inputText}
                placeholder=""
                keyboardType="number-pad"
                returnKeyType="done"
                onChangeText={(text: string) => {
                  this.onChangeTextCode.bind(this)(text);
                }}
              />
            </TextInputWrapper>
            {this.renderWarning()}
          </KeyboardAvoidingView>
          <TouchableOpacity
            hitSlop={hitSlop}
            style={styles.continueBut}
            onPress={this.confirmCode.bind(this)}
          >
            <Text style={styles.continueText}>Tiếp tục</Text>
          </TouchableOpacity>
        </View>
      );
    }
  }

  renderWarning() {
    if (this.state.warning) {
      return <Text style={styles.warningText}>{this.state.warning}</Text>;
    } else {
      return null;
    }
  }

  onChangeTextPhone(text: string) {
    this.setState({ phoneNumber: text });
  }

  onChangeTextCode(text: string) {
    this.setState({ codeInput: text });
  }

  async submitPhone() {
    Keyboard.dismiss(); // Tắt bàn phím
    const { phoneNumber } = this.state;
    // Validate phone number
    let validate = this.validatePhoneFormat(phoneNumber);
    if (validate) {
      let phoneWithRegionCode = `+84${phoneNumber.slice(1)}`;
      console.log("phoneWithRegionCode", phoneWithRegionCode);
      this.setState({ isLoading: true });
      // Kiểm tra xem số điện thoại đã tồn tại hay chưa?
      let isPhoneExist = await AccountFetcher.isPhoneNumberExist(
        phoneWithRegionCode
      );
      if (isPhoneExist.error) {
        this.setState({
          warning: "Lỗi xác thực số điện thoại. Kiểm tra internet và thử lại",
          isLoading: false
        });
        return;
      }
      // Số điện thoại đã tồn lại
      if (!isPhoneExist.data) {
        this.setState({
          warning: "Tài khoản không đúng hoặc không tồn tại",
          isLoading: false
        });
        return;
      }

      // Gửi tín hiệu cho Firebase -> đợi tin nhắn
      firebase
        .auth()
        .signInWithPhoneNumber(phoneWithRegionCode)
        .then(confirmResult => {
          this.setState({
            confirmResult,
            phoneWithRegionCode,
            warning: "",
            isLoading: false
          });
        })
        .catch(error => {
          console.log("submit", "verifyphone error");
          this.setState({
            warning: `Lỗi xác thực số điện thoại: ${error.message}`,
            isLoading: false
          });
        });
    } else {
      this.setState(
        { warning: "Số điện thoại không hợp lệ" },
        this.inputRef.shake
      );
    }
  }

  validatePhoneFormat(text: string) {
    console.log("validatePhoneFormat", text, text.length);
    // consider length
    if (text.length < 10) {
      return false;
    }
    // contain number only??
    if (!/^\d+$/.test(text)) {
      return false;
    }
    return true;
  }

  validateCodeFormat(text: string) {
    // consider length
    if (text.length !== 6) {
      return false;
    }
    // contain number only??
    if (!/^\d+$/.test(text)) {
      return false;
    }
    return true;
  }

  confirmCode() {
    Keyboard.dismiss(); // Tắt bàn phím
    const { codeInput, confirmResult, phoneNumber } = this.state;
    // Validate code number
    let validate = this.validateCodeFormat(codeInput);
    if (validate) {
      if (confirmResult) {
        this.setState({ isLoading: true });
        confirmResult
          .confirm(codeInput)
          .then(user => this.afterHasUser(user))
          .catch(error => {
            this.setState({ isLoading: false }, () => {
              this.setState(
                { warning: `Lỗi mã xác nhận: ${error.message}` },
                this.inputRef.shake
              );
            });
          });
      }
    } else {
      this.setState({ warning: "Mã xác nhận không đúng" }, this.inputRef.shake);
    }
  }

  async afterHasUser(user: any) {
    if (this.anti_multi_afterHasUser) {
      console.log("afterHasUser", "SKIP");
      return;
    } else {
      this.anti_multi_afterHasUser = true;
      setTimeout(() => {
        this.anti_multi_afterHasUser = false;
      }, 120);
    }
    console.log("afterHasUser", "Proccess");

    const { phoneNumber } = this.state;

    let [error, idToken] = await to(user.getIdToken());
    if (error) {
      this.setState({
        isLoading: false,
        phoneNumber: "",
        phoneWithRegionCode: "",
        confirmResult: null,
        codeInput: "",
        warning: "Xảy ra lỗi trong quá trình xác thực token"
      });
    } else {
      let params = {
        phone: phoneNumber,
        idToken: idToken
      };
      // Reset và chuyển tiếp trang
      this.setState(
        {
          isLoading: false,
          phoneNumber: "",
          confirmResult: null,
          codeInput: "",
          warning: ""
        },
        () => {
          navigationService.navigate("InputNewPassword", params);
        }
      );
    }
  }

  signOut() {
    firebase.auth().signOut();
  }

  onPressBack() {
    navigationService.navigate("Login");
  }
}
const styles = StyleSheet.create({
  explainContainer: { marginTop: 40, alignItems: "center", marginBottom: 90 },
  explainText: {
    fontSize: horizontalScale(16),
    color: colors.third_dark,
    textAlign: "center"
  },
  headerText: {
    fontSize: horizontalScale(18),
    color: colors.secondary_dark,
    marginBottom: 10
  },
  inputText: {
    fontSize: horizontalScale(18),
    borderBottomColor: "black",
    borderBottomWidth: 1,
    marginBottom: 13,
    minHeight: 44
  },
  continueBut: {
    fontSize: horizontalScale(18),
    marginTop: 36,
    alignSelf: "flex-end"
  },
  continueText: {
    fontSize: horizontalScale(18),
    color: colors.secondary_dark
  },
  warningText: { color: colors.error }
});
export default InputPhoneNumber;
