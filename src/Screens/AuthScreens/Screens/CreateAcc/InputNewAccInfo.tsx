/**
 * @flow
 */
import React, { PureComponent } from "react";
import {
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Alert,
  Keyboard
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Text from "../../../../Components/Text";
import { horizontalScale } from "../../../../ScaleUtility";
import { colors } from "../../../../styles/colors";

import TextInputWrapper from "../../../../Components/TextInputWrapper";
import BaseScreen from "../../../../Components/BaseScreen";
import NavBar from "../../SharedCom/NavBar";
import { AccountFetcher } from "../../../../Fetchers/AccountFetcher";
import OverLayWaiting, {
  tools as OverLayWaiting_tools
} from "./OverLayWaiting";
import ASHelper from "../../../../ASHelper";
import { DeviceInfo } from "../../../../Model/DeviceInfo";
import { Credentials } from "../../../../Model/Credentials";

import TrackUser from "../../../../TrackUser";
import navigationService from "../../../../services/navigationService";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

type States = {
  activeToken: string,
  idToken: string,
  deviceId: number[],
  phone: string,
  /**
   * Input info
   */
  password: string,
  confirmPw: string,
  storename: string,
  addr: string,
  ward: string,
  district: string,
  city: string,
  // showWarning
  warning_password: boolean,
  warning_confirmPw: boolean,
  warning_storename: boolean,
  warning_addr: boolean,
  warning_ward: boolean,
  warning_district: boolean,
  warning_city: boolean
};
type Props = { navigation: any };

type INPUT_FIELD =
  | "password"
  | "confirmPw"
  | "storename"
  | "addr"
  | "ward"
  | "district"
  | "city";

class InputNewAccInfo extends PureComponent<Props, States> {
  anti_multiPress_continue: boolean = false;

  containerRef_password: any;
  containerRef_confirmPassword: any;
  containerRef_storename: any;
  containerRef_addr: any;
  containerRef_ward: any;
  containerRef_district: any;
  containerRef_city: any;

  inputRef_confirmPassword: any;
  inputRef_storename: any;
  inputRef_addr: any;
  inputRef_ward: any;
  inputRef_district: any;
  inputRef_city: any;

  constructor(props: Props) {
    super(props);
    let activeToken = props.navigation.getParam("activeToken");
    let idToken = props.navigation.getParam("idToken");
    let deviceId = props.navigation.getParam("deviceId");
    let phone = props.navigation.getParam("phone");
    this.state = {
      activeToken,
      idToken,
      deviceId,
      phone,
      /**
       * Input info
       */
      password: "",
      confirmPw: "",
      storename: "",
      addr: "",
      ward: "",
      district: "",
      city: "",
      // showWarning
      warning_password: false,
      warning_confirmPw: false,
      warning_storename: false,
      warning_addr: false,
      warning_ward: false,
      warning_district: false,
      warning_city: false
    };
  }

  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    const {} = this.state;
    return (
      <View style={{ flex: 1 }}>
        <BaseScreen statusbarStyle="dark-content">
          <NavBar onLeftPress={this.onPressBack} title="Tạo tài khoản mới" />
          {this.renderBody()}
        </BaseScreen>
        <OverLayWaiting
          ref={_ref => {
            OverLayWaiting_tools.setRef(_ref);
          }}
        />
      </View>
    );
  }

  renderBody() {
    return (
      <ScrollView
        style={{
          padding: 20
        }}
      >
        <Text style={{ ...styles.explainText, marginBottom: 16 }}>
          Nhập thông tin tài khoản
        </Text>
        {/* Input Forms */}
        <KeyboardAwareScrollView keyboardDismissMode="interactive">
          {this.renderFormInput()}
        </KeyboardAwareScrollView>
        {/* Continue Button */}
        <TouchableOpacity
          hitSlop={hitSlop}
          style={styles.continueBut}
          onPress={this.onPressContinue.bind(this)}
        >
          <Text style={styles.continueText}>Tiếp tục</Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }

  renderFormInput() {
    return (
      <View>
        {/* Password */}
        <Text style={styles.headerText}>Mật khẩu (6 chữ số)</Text>
        <TextInputWrapper
          ref={ref => {
            this.containerRef_password = ref;
          }}
        >
          <TextInput
            onSubmitEditing={() => {
              this.onSubmitEdting.bind(this)("password");
            }}
            maxLength={6}
            hitSlop={hitSlop}
            style={styles.inputText}
            secureTextEntry={true}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            returnKeyType="next"
            onChangeText={(text: string) => {
              this.onChangeText.bind(this)("password", text);
            }}
          />
        </TextInputWrapper>
        {this.renderWarning("password")}
        <Text style={styles.headerText}>Nhập lại mật khẩu</Text>
        <TextInputWrapper
          ref={ref => {
            this.containerRef_confirmPassword = ref;
          }}
        >
          <TextInput
            ref={_ref => {
              this.inputRef_confirmPassword = _ref;
            }}
            onSubmitEditing={() => {
              this.onSubmitEdting.bind(this)("confirmPw");
            }}
            maxLength={6}
            hitSlop={hitSlop}
            style={styles.inputText}
            secureTextEntry={true}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            returnKeyType="next"
            onChangeText={(text: string) => {
              this.onChangeText.bind(this)("confirmPw", text);
            }}
          />
        </TextInputWrapper>
        {this.renderWarning("confirmPw")}
        {/* Store name */}
        <Text style={styles.headerText}>Tên cửa hàng</Text>
        <TextInputWrapper
          ref={ref => {
            this.containerRef_storename = ref;
          }}
        >
          <TextInput
            ref={_ref => {
              this.inputRef_storename = _ref;
            }}
            onSubmitEditing={() => {
              this.onSubmitEdting.bind(this)("storename");
            }}
            hitSlop={hitSlop}
            style={styles.inputText}
            underlineColorAndroid="transparent"
            keyboardType="default"
            autoCapitalize="words"
            autoCorrect={false}
            returnKeyType="next"
            onChangeText={(text: string) => {
              this.onChangeText.bind(this)("storename", text);
            }}
          />
        </TextInputWrapper>
        {this.renderWarning("storename")}

        {/* Địa chỉ */}
        <Text style={styles.headerText}>Địa chỉ</Text>
        <TextInputWrapper
          ref={ref => {
            this.containerRef_addr = ref;
          }}
        >
          <TextInput
            ref={_ref => {
              this.inputRef_addr = _ref;
            }}
            onSubmitEditing={() => {
              this.onSubmitEdting.bind(this)("addr");
            }}
            hitSlop={hitSlop}
            style={styles.inputText}
            underlineColorAndroid="transparent"
            keyboardType="default"
            autoCapitalize="words"
            autoCorrect={false}
            returnKeyType="next"
            onChangeText={(text: string) => {
              this.onChangeText.bind(this)("addr", text);
            }}
          />
        </TextInputWrapper>
        {this.renderWarning("addr")}

        {/* Phường */}
        <Text style={styles.headerText}>Phường (Xã)</Text>
        <TextInputWrapper
          ref={ref => {
            this.containerRef_ward = ref;
          }}
        >
          <TextInput
            ref={_ref => {
              this.inputRef_ward = _ref;
            }}
            onSubmitEditing={() => {
              this.onSubmitEdting.bind(this)("ward");
            }}
            hitSlop={hitSlop}
            style={styles.inputText}
            underlineColorAndroid="transparent"
            keyboardType="default"
            autoCapitalize="words"
            autoCorrect={false}
            returnKeyType="next"
            onChangeText={(text: string) => {
              this.onChangeText.bind(this)("ward", text);
            }}
          />
        </TextInputWrapper>
        {this.renderWarning("ward")}

        {/* Quận */}
        <Text style={styles.headerText}>Quận (Huyện)</Text>
        <TextInputWrapper
          ref={ref => {
            this.containerRef_district = ref;
          }}
        >
          <TextInput
            ref={_ref => {
              this.inputRef_district = _ref;
            }}
            onSubmitEditing={() => {
              this.onSubmitEdting.bind(this)("district");
            }}
            hitSlop={hitSlop}
            style={styles.inputText}
            underlineColorAndroid="transparent"
            keyboardType="default"
            autoCapitalize="words"
            autoCorrect={false}
            returnKeyType="next"
            onChangeText={(text: string) => {
              this.onChangeText.bind(this)("district", text);
            }}
          />
        </TextInputWrapper>
        {this.renderWarning("district")}

        {/* Thành phố */}
        <Text style={styles.headerText}>Thành phố (Tỉnh)</Text>
        <TextInputWrapper
          ref={ref => {
            this.containerRef_city = ref;
          }}
        >
          <TextInput
            ref={_ref => {
              this.inputRef_city = _ref;
            }}
            onSubmitEditing={() => {
              this.onSubmitEdting.bind(this)("city");
            }}
            hitSlop={hitSlop}
            style={styles.inputText}
            underlineColorAndroid="transparent"
            keyboardType="default"
            autoCapitalize="words"
            autoCorrect={false}
            returnKeyType="done"
            onChangeText={(text: string) => {
              this.onChangeText.bind(this)("city", text);
            }}
          />
        </TextInputWrapper>
        {this.renderWarning("city")}
      </View>
    );
  }

  renderWarning(field: INPUT_FIELD) {
    let str;
    let notOkay = false;
    switch (field) {
      case "password":
        notOkay = this.state.warning_password;
        str = "Mật khẩu không hợp lệ";
        break;
      case "confirmPw":
        notOkay = this.state.warning_confirmPw;
        str = "Mật khẩu không khớp";
        break;

      case "storename":
        notOkay = this.state.warning_storename;
        str = "Tên cửa hàng không hợp lệ";
        break;

      case "addr":
        notOkay = this.state.warning_addr;
        str = "Địa chỉ không hợp lệ";
        break;

      case "ward":
        notOkay = this.state.warning_ward;
        str = "";
        break;

      case "district":
        notOkay = this.state.warning_district;
        str = "";
        break;

      case "city":
        notOkay = this.state.warning_city;
        str = "Nội dung không hợp lệ";
        break;

      default:
        str = "";
        break;
    }

    if (notOkay) {
      return <Text style={styles.warningText}>{str}</Text>;
    } else {
      return null;
    }
  }

  onChangeText(field: INPUT_FIELD, text: string) {
    switch (field) {
      case "password":
        this.setState({ password: text });
        break;
      case "confirmPw":
        this.setState({ confirmPw: text });
        break;

      case "storename":
        this.setState({ storename: text });
        break;

      case "addr":
        this.setState({ addr: text });
        break;

      case "ward":
        this.setState({ ward: text });
        break;

      case "district":
        this.setState({ district: text });
        break;

      case "city":
        this.setState({ city: text });
        break;

      default:
        break;
    }
  }

  onSubmitEdting(field: INPUT_FIELD) {
    switch (field) {
      case "password":
        this.inputRef_confirmPassword.focus();
        break;

      case "confirmPw":
        this.inputRef_storename.focus();
        break;

      case "storename":
        this.inputRef_addr.focus();
        break;

      case "addr":
        this.inputRef_ward.focus();
        break;

      case "ward":
        this.inputRef_district.focus();
        break;

      case "district":
        this.inputRef_city.focus();
        break;

      case "city":
        this.onPressContinue();
        break;

      default:
        break;
    }
  }

  onPressContinue() {
    Keyboard.dismiss(); // Tắt bàn phím
    // Chống dội phím
    if (this.anti_multiPress_continue) {
      console.log("InputNewAccInfo", "onPressContinue", "skip");
      return;
    }
    // Xử lý bấm
    else {
      console.log("InputNewAccInfo", "onPressContinue", "proccess");
      this.anti_multiPress_continue = true;
      setTimeout(() => {
        this.anti_multiPress_continue = false;
      }, 200);
    }
    // Validate Infomation
    const { password, phone, confirmPw } = this.state;
    let warning_password = false,
      warning_confirmPw = false;
    // Validate Password
    if (password.length === 6) {
      if (/^\d+$/.test(password)) {
        // Chỉ bao gồm số
      } else {
        // Gồm ký tự khác
        warning_password = true;
      }
    } else {
      // Độ dài không đủ
      warning_password = true;
    }
    if (!warning_password) {
      if (password !== confirmPw) {
        warning_confirmPw = true;
      }
    }
    // Validate Store name
    let { storename } = this.state;
    let warning_storename = false;
    storename = storename.trim();
    if (storename) {
    } else {
      // Độ dài không đủ
      warning_storename = true;
    }
    // Validate Addr
    let { addr } = this.state;
    let warning_addr = false;
    addr = addr.trim();
    if (addr) {
    } else {
      // Độ dài không đủ
      warning_addr = true;
    }
    // Validate Ward
    let { ward } = this.state;
    let warning_ward = false;
    ward = ward.trim();
    if (ward) {
    } else {
      // Độ dài không đủ
      warning_ward = true;
    }
    // Validate District
    let { district } = this.state;
    let warning_district = false;
    district = district.trim();
    if (district) {
    } else {
      // Độ dài không đủ
      warning_district = true;
    }
    // Validate City
    let { city } = this.state;
    let warning_city = false;
    city = city.trim();
    if (city) {
    } else {
      // Độ dài không đủ
      warning_city = true;
    }
    // Update UI
    this.setState(
      {
        warning_password,
        warning_confirmPw,
        warning_storename,
        warning_addr,
        warning_ward,
        warning_district,
        warning_city
      },
      async () => {
        if (
          warning_password ||
          warning_confirmPw ||
          warning_storename ||
          warning_addr ||
          warning_ward ||
          warning_district ||
          warning_city
        ) {
          // Shake if need
          if (warning_password) {
            this.containerRef_password.shake();
          }
          if (warning_confirmPw) {
            this.containerRef_confirmPassword.shake();
          }
          if (warning_addr) {
            this.containerRef_addr.shake();
          }
          if (warning_city) {
            this.containerRef_city.shake();
          }
        } else {
          // Everything is okay
          let userInfo = {
            password,
            storeName: storename,
            addr,
            ward,
            district,
            city
          };
          // Create new account with active token, user, input user info
          OverLayWaiting_tools.showPopup(true);
          let res = await AccountFetcher.createNewAccount(
            this.state.activeToken,
            this.state.idToken,
            userInfo,
            this.state.deviceId
          );
          console.log("createNewAccount", res);
          OverLayWaiting_tools.showPopup(false);
          if (res.error) {
            // Có lỗi xảy ra
            let title = "Tạo tài khoản không thành công";
            let message = "Kiểm tra internet và thử lại";
            Alert.alert(title, message, [{ text: "Đồng ý" }], {
              cancelable: false
            });
          } else {
            let params = {
              phone: "0" + phone.slice(3),
              password,
              autoLogin: true
            };
            // Tạo tài khoản okay
            // Quay lại Auth, tự chạy login
            console.log("params", params);
            console.log("phone", phone);
            navigationService.navigate("Login", params);
            TrackUser.logEvent("new_account_created");
          }
        }
      }
    );
  }

  onPressBack() {
    Alert.alert("Bạn muốn thay đổi số điện thoại?", "", [
      {
        text: "Đồng ý",
        onPress: navigationService.goBack
      },
      { text: "Bỏ qua" }
    ]);
  }
}

const styles = StyleSheet.create({
  explainText: {
    fontSize: horizontalScale(16),
    color: colors.third_dark
  },
  headerText: {
    fontSize: horizontalScale(18)
    // color: colors.secondary_dark
  },
  inputText: {
    paddingTop: 10,
    fontSize: horizontalScale(18),
    borderBottomColor: "black",
    borderBottomWidth: 1,
    marginBottom: 13,
    minHeight: 45
  },
  continueBut: {
    fontSize: horizontalScale(18),
    marginTop: 36,
    marginBottom: 50,
    alignSelf: "flex-end"
  },
  continueText: {
    fontSize: horizontalScale(18),
    color: colors.secondary_dark
  },
  warningText: {
    color: colors.error,
    marginBottom: 13
  }
});
export default InputNewAccInfo;
