/**
 * @flow
 */
import React, { Component } from "react";
import { View, StyleSheet } from "react-native";

import { colors } from "../../../../styles/colors";

import { SkypeIndicator } from "react-native-indicators";

import Text from "../../../../Components/Text";

class OverLayWaiting extends React.PureComponent<{}, { isShowing: boolean }> {
  constructor(props: any) {
    super(props);
    this.state = {
      isShowing: false
    };
  }

  componentDidMount() {
    showed = false;
  }
  componentWillUnmount() {
    showed = false;
  }

  render() {
    if (this.state.isShowing) {
      return (
        <View
          style={[
            StyleSheet.absoluteFillObject,
            {
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }
          ]}
        >
          {/* Background color */}
          <View
            style={[
              StyleSheet.absoluteFillObject,
              { backgroundColor: colors.third_dark, opacity: 0.87 }
            ]}
          />
          {/* Content */}
          {this.renderContent()}
        </View>
      );
    } else {
      return null;
    }
  }
  renderContent() {
    return (
      <View>
        <SkypeIndicator size={75} color={colors.primary_light} />
        <Text
          style={{
            color: colors.primary_light,
            alignSelf: "center",
            fontSize: 16,
            marginTop: 20
          }}
        >
          Vui lòng đợi giây lát
        </Text>
      </View>
    );
  }

  show() {
    this.setState({ isShowing: true });
  }
  dismiss() {
    this.setState({ isShowing: false });
  }
}

export default OverLayWaiting;

/**
 * ================================================================================

 * ================================================================================
 */
let popupRef,
  showed = false;

/**
 * Set ref cho popup
 * @param {*} ref
 */
function setRef(ref: any) {
  popupRef = ref;
}
/**
 * Hiển thị popup
 * @param {*} on
 * @param {*} _callbackConfirm
 * @param {*} _callbackCancel
 */
function showPopup(on?: boolean = false) {
  if (showed !== on) {
    showed = on;
    if (on) {
      popupRef.show();
    } else {
      popupRef.dismiss();
    }
  }
}
/**
 * Có đang hiển thị không?
 */
function isShowing() {
  return showed;
}

export const tools = { setRef, showPopup, isShowing };
