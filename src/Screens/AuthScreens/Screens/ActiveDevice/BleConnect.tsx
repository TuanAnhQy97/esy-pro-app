/**
 * @flow
 */
import React, { PureComponent } from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import { connect } from "react-redux";
import Text from "../../../../Components/Text";
import { horizontalScale } from "../../../../ScaleUtility";
import { colors } from "../../../../styles/colors";

import BleForNew from "../../../../Ble/BleForNew";
import { TYPE_STORE_DATA } from "../../../../Store";
import BaseScreen from "../../../../Components/BaseScreen";
import ActiveDeviceAnimate from "./ActiveDeviceAnimate";
import navigationService from "../../../../services/navigationService";
import ble from "../../../../services/ble";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

type Props = { navigation: any; progress: number; error: number };
type States = {};

class BleConnect extends PureComponent<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    BleForNew.initBle();
    ble.removeListener();
  }

  componentWillUnmount() {
    // Huỷ hết BLE
    BleForNew.destroyBle();
    ble.addListener();
  }

  render() {
    const { error, progress } = this.props;
    return (
      <BaseScreen
        statusbarStyle="dark-content"
        backgroundColor={colors.primary_light}
      >
        {this.renderBody()}
      </BaseScreen>
    );
  }

  renderBody() {
    return (
      <View style={{ flex: 1, justifyContent: "space-between" }}>
        <View>
          {/* Header */}
          <Text style={styles.headerText}>Xác thực tài khoản qua thiết bị</Text>

          {/* Quá trình */}
          <Text style={styles.progressText}>
            {translateProgress(this.props.progress)}
          </Text>
        </View>
        {/* Hình minh hoạ */}
        <ActiveDeviceAnimate />
        {/* Nút huỷ */}
        <TouchableOpacity
          hitSlop={hitSlop}
          style={styles.continueBut}
          onPress={this.onPressCancel.bind(this)}
        >
          <Text style={styles.continueText}>Huỷ bỏ</Text>
        </TouchableOpacity>
      </View>
    );
  }

  onPressCancel() {
    navigationService.goBack();
  }
}
/**
 * Giải mã từ progress code
 * @param {*} progress
 */
function translateProgress(progress: number) {
  let str;
  switch (progress) {
    case BleForNew.BLE_PROGRESS.PROGRESS.SCANNING:
      str = "Đang tìm thiết bị";
      break;

    case BleForNew.BLE_PROGRESS.PROGRESS.CONNECTING:
      str = "Đang kết nối";
      break;

    case BleForNew.BLE_PROGRESS.PROGRESS.VERIFY_DEVICE:
      str = "Đang xác nhận thiết bị";
      break;

    default:
      str = "";
      break;
  }
  return str;
}
/**
 * Giải mã từ error code
 * @param {*} error
 */
function translateError(error: number) {
  let str;
  let numStr;
  if (error < 10) {
    numStr = `0${error}`;
  } else {
    numStr = error;
  }
  switch (error) {
    case 0:
      str = "";
      break;

    case BleForNew.BLE_PROGRESS.ERROR.NOT_FOUND:
      str = `Không tìm thấy thiết bị (1${numStr})`;
      break;

    case BleForNew.BLE_PROGRESS.ERROR.DEVICE_NOT_COMPATIBLE:
      str = `Thiết bị không tương thích (1${numStr})`;
      break;

    case BleForNew.BLE_PROGRESS.ERROR.FAILURE:
      str = `Lỗi kết nối thiết bị (1${numStr})`;
      break;

    case BleForNew.BLE_PROGRESS.ERROR.DEVICE_IS_USED:
      str = `Thiết bị đã được sử dụng (1${numStr})`;
      break;

    default:
      str = `Lỗi không xác định (1${numStr})`;
      break;
  }
  return str;
}

const styles = StyleSheet.create({
  headerText: {
    marginTop: 44,
    fontSize: horizontalScale(14),
    alignSelf: "center"
  },
  progressText: {
    marginTop: 10,
    fontSize: horizontalScale(18),
    color: colors.secondary_dark,
    alignSelf: "center"
  },
  continueBut: {
    padding: 10,
    marginBottom: 15,
    alignSelf: "center"
  },
  continueText: {}
});

function mapStateToProps(state: TYPE_STORE_DATA) {
  return {
    progress: state.BleForActive.progress,
    error: state.BleForActive.error
  };
}
export default connect(mapStateToProps)(BleConnect);
