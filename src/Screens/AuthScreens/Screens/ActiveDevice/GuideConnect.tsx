/**
 * @flow
 */
import React, { PureComponent } from "react";
import {
  View,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Linking,
  Alert,
  Platform
} from "react-native";
import Text from "../../../../Components/Text";
import { horizontalScale } from "../../../../ScaleUtility";
import { colors } from "../../../../styles/colors";
import BaseScreen from "../../../../Components/BaseScreen";
import NavBar from "../../SharedCom/NavBar";
import navigationService from "../../../../services/navigationService";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

class GuideConnect extends PureComponent<{}, {}> {
  constructor(props: Object) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    const {} = this.state;
    return (
      <BaseScreen
        statusbarStyle="dark-content"
        backgroundColor={colors.primary_light}
      >
        <NavBar
          onLeftPress={() => {
            navigationService.navigate("Login"); // Quay lại login
          }}
          title="Tạo tài khoản mới"
        />
        {this.renderBody()}
      </BaseScreen>
    );
  }

  renderBody() {
    return (
      <View
        style={{
          flex: 1,
          padding: 20,
          justifyContent: "space-between"
        }}
      >
        {this.renderExplain()}
        <TouchableOpacity
          hitSlop={hitSlop}
          style={styles.continueBut}
          onPress={this.onPressContinue.bind(this)}
        >
          <Text style={styles.continueText}>Tiếp tục</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderExplain() {
    const iconSize = horizontalScale(200);
    return (
      <View style={styles.explainContainer}>
        <Text style={styles.headerText}>
          Hướng dẫn tạo tài khoản với thiết bị mới
        </Text>
        <ImageBackground
          source={require("../../../../../assets/Images/Share/guideConnectDLC.png")}
          style={{
            height: iconSize,
            width: iconSize
          }}
          resizeMode="contain"
        />
        <View style={{ alignItems: "center" }}>
          <Text style={styles.explainText}>
            Kết nối ESY Pro với đầu đọc lỗi{"\n"}trên xe máy Honda rồi bật khoá
            điện
          </Text>
          <TouchableOpacity
            hitSlop={hitSlop}
            onPress={() => {
              Linking.openURL("https://youtu.be/07cFiGabijU");
            }}
            style={{ marginTop: 5 }}
          >
            <Text
              style={{
                color: colors.secondary_dark,
                fontSize: horizontalScale(16),
                textAlign: "center"
              }}
            >
              Xem hướng dẫn chi tiết
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  onPressContinue = async () => {
    if (Platform.OS === "android") {
      const CheckGpsState = require("../../../../../NativeModules/CheckGpsState");
      const isGpsOn = await CheckGpsState.isGpsOn();
      if (isGpsOn) {
        navigationService.navigate("AuthBleConnect");
      } else {
        Alert.alert(
          "GPS không được bật",
          "Nếu bạn ứng dụng không thể tìm thấy thiết bị. Vui lòng bật GPS và thử lại",
          [
            { text: "Bỏ qua" },
            { text: "Bật GPS", onPress: CheckGpsState.openGpsSetting }
          ]
        );
      }
    } else {
      navigationService.navigate("AuthBleConnect");
    }
  };
}
const styles = StyleSheet.create({
  explainContainer: {
    flex: 1,
    marginTop: 40,
    alignItems: "center",
    marginBottom: 90,
    justifyContent: "space-between"
  },
  explainText: {
    fontSize: horizontalScale(16),
    color: colors.third_dark,
    textAlign: "center"
  },
  headerText: { fontSize: horizontalScale(18), color: colors.secondary_dark },
  continueBut: {
    padding: 10,
    alignSelf: "center"
  },
  continueText: {
    fontSize: horizontalScale(18),
    color: colors.secondary_dark
  }
});
export default GuideConnect;
