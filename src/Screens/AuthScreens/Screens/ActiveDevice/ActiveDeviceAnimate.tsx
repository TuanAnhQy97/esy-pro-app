/**
 * @flow
 */
import React from "react";
import {
  View,
  Animated,
  Easing,
  ImageBackground,
  StyleSheet
} from "react-native";
import { horizontalScale, verticalScale } from "../../../../ScaleUtility";
import { colors } from "../../../../styles/colors";

const CIRCLE_COUNT = 2;

type Props = {};
type States = {};

class ActiveDeviceAnimate extends React.PureComponent<Props, States> {
  animateValueArr = [];

  constructor(props: any) {
    super(props);
    for (let i = 0; i < CIRCLE_COUNT; i++) {
      this.animateValueArr.push(new Animated.Value(0));
    }
  }

  componentDidMount() {
    // this.animateValueArr.forEach(value => {
    //   this.animate(value);
    // });
    this.animateStagger();
  }

  render() {
    const minSize = horizontalScale(160);
    const maxSize = horizontalScale(265);
    return (
      <View
        style={[
          {
            alignSelf: "center",
            height: maxSize,
            width: maxSize,
            justifyContent: "center"
          },
          //
          {
            // position: "absolute"
          }
        ]}
      >
        {this.animateValueArr.map((value, index) => {
          const size = this.animateValueArr[index].interpolate({
            inputRange: [0, 1],
            outputRange: [minSize, maxSize]
          });
          const borderWidth = this.animateValueArr[index].interpolate({
            inputRange: [0, 0.1, 1],
            outputRange: [0, 0.8, 1.5]
          });
          const opacity = this.animateValueArr[index].interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0.3]
          });
          return (
            <Animated.View
              key={index.toString()}
              style={[
                {
                  position: "absolute",
                  alignSelf: "center",
                  borderColor: colors.secondary_dark,
                  borderRadius: maxSize
                },
                {
                  //   Animated attributes
                  borderWidth: borderWidth,
                  width: size,
                  height: size,
                  opacity
                }
              ]}
            />
          );
        })}
        <ImageBackground
          style={{
            width: horizontalScale(160),
            height: verticalScale(85),
            alignSelf: "center"
          }}
          resizeMode="contain"
          source={require("../../../../../assets/Images/Share/ESYproCase.png")}
        />
      </View>
    );
  }

  animate(value: any) {
    value.setValue(0);
    Animated.timing(value, {
      toValue: 1.5,
      duration: 2000,
      easing: Easing.linear
    }).start(() => {
      this.animate(value);
    });
  }

  animateStagger() {
    const config = {
      toValue: 1.5,
      duration: 2000,
      easing: Easing.linear
    };
    Animated.stagger(
      800,
      this.animateValueArr.map(value => {
        value.setValue(0);
        return Animated.timing(value, config);
      })
    ).start(() => {
      this.animateStagger();
    });
  }
}

export default ActiveDeviceAnimate;
