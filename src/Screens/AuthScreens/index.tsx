/**
 * @flow
 */
import Router, { SCREEN_KEY } from "./Router/Router";

export type AUTH_SCREEN_KEY = SCREEN_KEY;
export default Router;
