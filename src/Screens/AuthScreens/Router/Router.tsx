/**
 * @flow
 */
import { createSwitchNavigator, createStackNavigator } from "react-navigation";

import LoginScreen from "../Screens/Login/LoginScreen";

import GuideConnection from "../Screens/ActiveDevice/GuideConnect";
import BleConnect from "../Screens/ActiveDevice/BleConnect";

import VerifyPhoneNumber from "../Screens/CreateAcc/VerifyPhoneNumber";
import InputNewAccInfo from "../Screens/CreateAcc/InputNewAccInfo";

import ForgetPassword from "../Screens/ForgetPassword/ForgetPassword";
import InputNewPassword from "../Screens/ForgetPassword/InputNewPassword";

export type SCREEN_KEY =
  /**
   * Root switch
   */
  | "Login"
  | "ActiveDevice"
  | "CreateNewAcc"
  | "ForgetPW"
  /**
   * ActiveDevice
   */
  | "AuthGuideConnect"
  | "AuthBleConnect"
  /**
   * Create new Acc
   */
  | "CreateNewAcc"
  | "AuthInputNewAccInfo"
  | "AuthInputPhone"
  /**
   * Forget Password
   */
  | "ForgetPassword"
  | "InputNewPassword";

const ActiveDeviceStack = createStackNavigator(
  {
    AuthGuideConnect: {
      screen: GuideConnection
    },
    AuthBleConnect: {
      screen: BleConnect
    }
  },
  {
    mode: "card",
    headerMode: "none",
    navigationOptions: { gesturesEnabled: false }
  }
);
const CreateNewAcc = createStackNavigator(
  {
    AuthInputPhone: {
      screen: VerifyPhoneNumber
    },
    AuthInputNewAccInfo: {
      screen: InputNewAccInfo
    }
  },
  {
    // initialRouteName: "AuthInputNewAccInfo",
    mode: "card",
    headerMode: "none",
    navigationOptions: { gesturesEnabled: false }
  }
);

const ForgetPW = createStackNavigator(
  {
    ForgetPassword: {
      screen: ForgetPassword
    },
    InputNewPassword: {
      screen: InputNewPassword
    }
  },
  {
    // initialRouteName: "AuthInputNewAccInfo",
    mode: "card",
    headerMode: "none",
    navigationOptions: { gesturesEnabled: false }
  }
);

const rootRoute = createSwitchNavigator(
  {
    Login: {
      screen: LoginScreen
    },
    ActiveDevice: {
      screen: ActiveDeviceStack
    },
    CreateNewAcc: {
      screen: CreateNewAcc
    },
    ForgetPW: {
      screen: ForgetPW
    }
  },
  {
    // initialRouteName: "CreateNewAcc"
  }
);

export default rootRoute;
