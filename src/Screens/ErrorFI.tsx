// @flow
import React, { Component } from "react";
import {
  View,
  FlatList,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import Text from "../Components/Text";
import BaseScreen from "../Components/BaseScreen";

import NavBar from "../Components/NavBar";
import { horizontalScale } from "../ScaleUtility";
import ExplainError from "../Library/ExplainError";
import MyActivityIndicator from "../Components/MyActivityIndicator";

import OverlayExplainFIError, {
  tools as OverlayExplainFIError_tools
} from "../Components/Popup/OverlayExplainFIError";
import { tools as OverLayWaitingKline_tools } from "../Components/Popup/OverLayWaitingKline";
import { colors } from "../styles/colors";
import ble from "../services/ble";
import navigationService from "../services/navigationService";
import eventBus from "../services/eventBus";
import { EVENT_BUS } from "../constants";

type ErrorProps = {};

type States = {
  backPress?: boolean;
  errorCode: number;
  errorState: number;
  errorList: number[][];
  isLoading: boolean;
};

class ErrorFI extends React.PureComponent<ErrorProps, States> {
  constructor(props: ErrorProps) {
    super(props);
    this.state = {
      errorCode: 0,
      errorState: 1,
      errorList: [],
      isLoading: true
    };
  }

  componentDidMount() {
    eventBus.addListener(
      EVENT_BUS.BIKE_ERROR_FI_LIST_CHANGE,
      this._onDataChange
    );
  }

  componentWillUnmount() {
    if (!this.state.backPress) {
      // Back do swipe gesture
      ble.esyDataManager.honda.toIdleMode();
    }
    eventBus.removeListener(this._onDataChange);
  }

  _onDataChange = (errorList: number[][]) => {
    this.setState({ isLoading: false, errorList });
    ble.esyDataManager.honda.toReadErrorMode();
  };

  handleBackPress = () => {
    console.log("ErrorFI", "handleBackPress");
    // works best when the goBack is async
    this.onBack();
    return true;
  };

  onBack = async () => {
    console.log("ErrorFI", "onBack");
    /**
     * Nếu đang hiển thị popup -> Tắt popup
     */
    // Đang chờ Kline
    const isWaitingKline = OverLayWaitingKline_tools.isShowing();
    if (isWaitingKline) {
      // Không làm gì cả
      return;
    }
    // Popup Giải thích
    const isShowExplain = OverlayExplainFIError_tools.isShowing();
    if (isShowExplain) {
      OverlayExplainFIError_tools.showPopup(false);
      return;
    }

    this.setState({ backPress: true }, () => {
      ble.esyDataManager.honda.toIdleMode();
      navigationService.goBack();
    });
  };

  renderBody = () => {
    if (this.state.isLoading) {
      return <MyActivityIndicator />;
    }
    if (this.state.errorList.length === 0) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <ImageBackground
            resizeMode="contain"
            style={{ height: 135, width: 135 }}
            source={require("../../assets/Images/Share/data-storage.png")}
          />
          <Text
            style={{
              marginTop: 20,
              fontSize: 20
            }}
          >
            BỘ NHỚ LỖI TRỐNG
          </Text>
        </View>
      );
    }
    return this.renderErrorList();
  };

  renderErrorList = () => {
    const { errorList } = this.state;
    return (
      <View style={{ flex: 1, padding: 10 }}>
        <View
          style={{
            paddingTop: 5,
            backgroundColor: colors.primary_light,
            flexDirection: "row"
          }}
        >
          <Text
            style={{
              width: horizontalScale(100),
              paddingVertical: 10,
              textAlign: "center",
              fontSize: horizontalScale(18),
              color: colors.secondary_dark
            }}
          >
            MÃ LỖI
          </Text>
          <View
            style={{
              width: 1,
              height: "100%",
              backgroundColor: "gray",
              opacity: 0.1
            }}
          />
          <Text
            style={{
              flex: 1,
              paddingVertical: 10,
              paddingLeft: horizontalScale(10),
              textAlign: "center",
              fontSize: horizontalScale(18)
            }}
          >
            TÊN LỖI
          </Text>
        </View>
        <FlatList
          style={{ paddingTop: 5, flex: 1, marginTop: 23 }}
          data={errorList}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderItem}
        />
      </View>
    );
  };

  renderItem = ({ item }) => {
    const explain = ExplainError.getExplain(item[0], item[1]);
    return (
      <TouchableOpacity
        hitSlop={{ top: 5, bottom: 5, left: 5, right: 5 }}
        style={{
          width: "100%",
          flexDirection: "row",
          marginBottom: 15,
          minHeight: 44
        }}
        onPress={() => {
          this.setState({ errorCode: item[0], errorState: item[1] }, () => {
            OverlayExplainFIError_tools.showPopup(true);
          });
        }}
      >
        <Text
          style={{
            width: horizontalScale(100),
            paddingVertical: 10,
            textAlign: "center",
            fontSize: horizontalScale(18),
            color: colors.primary_light,
            backgroundColor: colors.secondary_dark
          }}
        >
          {`${item[0]}-${item[1]}`}
        </Text>
        <Text
          style={{
            flex: 1,
            paddingVertical: 10,
            paddingLeft: horizontalScale(10),
            textAlign: "center",
            fontSize: horizontalScale(18),
            backgroundColor: colors.primary_light
          }}
        >
          {explain.name}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <BaseScreen
        statusbarStyle="light-content"
        bodyBackgroundColor="#F5F5F5"
        handleBackPress={this.handleBackPress}
      >
        {/* NavBar */}
        <NavBar onLeftPress={this.handleBackPress} title="Danh sách lỗi" />
        {this.renderBody()}
        <OverlayExplainFIError
          ref={_ref => {
            OverlayExplainFIError_tools.setRef(_ref);
          }}
          errorCode={this.state.errorCode}
          errorState={this.state.errorState}
        />
      </BaseScreen>
    );
  }
}

export default ErrorFI;
