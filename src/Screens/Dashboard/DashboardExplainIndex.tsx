/**
 * @flow
 */

export const name_unit_arr = [
  /**
   * Normal Data
   */
  ["Vận tốc", "km/h", "Giá trị vận tốc đạt được khi vận hành"],
  [
    "Vòng tua máy",
    "vòng/phút",
    "Là giá trị biểu diễn của tốc độ quay của trục khuỷu khi động cơ hoạt động"
  ],
  [
    "Nhiệt độ động cơ",
    "°C",
    "Là nhiệt độ tạo ra bởi ma sát kim loại kèm theo hiệu ứng nhiệt đồng đều lan tỏa từ buồng đốt tới toàn động cơ"
  ],
  ["Nhiệt độ động cơ (Điện áp)", "V", "Giá trị cảm biến dưới dạng điện áp"],
  [
    "Thời gian phun",
    "ms",
    "Là thời gian phun xăng ở từng chu kỳ sinh công của động cơ"
  ],
  [
    "Ắc quy",
    "V",
    "Là mức điện áp mà ecu đo được giá trị thực tế được cấp từ sau công tắc đánh lửa"
  ],
  [
    "Góc đánh lửa",
    "°",
    "Là thời điểm phát sinh tia lửa điện để đốt cháy nhiên liệu theo một góc được tạo bởi cánh tay đòn trục khuỷu và tâm bánh đà"
  ],
  [
    "Góc mở bướm ga",
    "°",
    "Là giá trị biểu diễn góc mở cửa nạp oxy chính theo sự điều khiển của tay ga"
  ],
  ["Góc mở bướm ga (Điện áp)", "V", "Giá trị cảm biến dưới dạng điện áp"],
  [
    "Áp suất khí nạp",
    "kPa",
    "Là giá trị biểu diễn áp suất dòng không khí di chuyển với tốc độ cao trong họng hút khí khi động cơ vận hành"
  ],
  ["Áp suất khí nạp (Điện áp)", "V", "Giá trị cảm biến dưới dạng điện áp"],
  [
    "Nhiệt độ khí nạp",
    "°C",
    "Là giá trị đo được khi dòng không khí di chuyển với tốc độ cao và tạo ma sát sinh nhiệt"
  ],
  ["Nhiệt độ khí nạp (Điện áp)", "V", "Giá trị cảm biến dưới dạng điện áp"],
  [
    "Tín hiệu Oxy",
    "mV",
    "Là tín hiệu được tạo ra để đo đạc độ giàu hoặc nghèo nhiên liệu ở mỗi chu kỳ sinh công"
  ],
  /**
   * Extra Data
   */
  // DATA 1
  ["Vị trí van IACV", "bước", "Là đơn vị đo độ mở cửa nạp khí phụ"],
  ["Lưu lượng khí nạp cầm chừng", "l/phút", "Lượng khí nạp ở cửa khí phụ"],
  // Data 2
  [
    "Tỉ lệ xăng/gió",
    "",
    "Là tỉ lệ tham chiếu với tỉ lệ tiêu chuẩn 14,7 gram Oxy / 1 gram xăng"
  ],
  [
    "Bộ nung Oxy",
    "",
    "Là bộ phận gia nhiệt tăng tính phản ứng tích cực khi kiểm soát khí thải"
  ],
  // Data 3
  [
    "Bộ nhớ mã lỗi (Mill)",
    "",
    "Khi ở giá trị BẬT bằng ECU đang báo đã ghi nhận xảy ra sự cố. TẮT khi bộ nhớ lỗi trống"
  ],
  ["Tín hiệu cảm biến góc nghiêng", "V"],
  ["Cảm biến MAP trung bình", "kPa"],
  // Data 4
  ["Bộ bơm xăng", "", "Hoạt động theo sự điều khiển của ECU"],
  ["Chân chống cạnh", "", "Chức năng cảnh báo an toàn khi vận hành động cơ"],
  ["Công tắc khởi động", "", "Lệnh yêu cầu ECU thực hiện chế độ khởi động"],
  ["Quạt gió", "", "Hoạt động theo sự điều khiển của ECU"],
  [
    "Đầu nối tắt DLC",
    "",
    "Giá trị MỞ khi không gọi dịch vụ cài đặt ECU. ĐÓNG khi gọi dịch vụ cài đặt ECU"
  ],
  ["Lệnh đề", "", "Lệnh Thông báo Hệ thống ACG khởi động"],
  ["Rơ-le khởi động", "", "Hoạt động khi ECU nhận được yêu cầu từ Lệnh đề"],
  ["Van EVAP", "", "Van kiểm soát việc ngưng tụ hơi xăng"],
  ["Cảm biến đổ xe", ""],
  [
    "Van trung hoà khí thải (PCV)",
    "",
    "Là bộ phân cấp thêm Oxy để đốt hết nhiên liệu dư thừa trong ống xả trước khi xả thải ra môi trường"
  ],
  ["Bộ hãm khởi động", "", ""]
];

type ExplainStructure = {
  name: string,
  englishName?: string,
  def: string
};

class DashboardExplainIndex {
  static instance: DashboardExplainIndex;
  static getInstance(): DashboardExplainIndex {
    if (!DashboardExplainIndex.instance) {
      DashboardExplainIndex.instance = new DashboardExplainIndex();
    }
    return DashboardExplainIndex.instance;
  }
  getExplain(indexOrder: number): ExplainStructure {
    let explain = name_unit_arr[indexOrder];
    if (explain) {
      return { name: explain[0], def: explain[2] };
    } else {
      return { name: "Unknow (" + indexOrder + ")", def: "Unknown" };
    }
  }
}
export default DashboardExplainIndex.getInstance();
