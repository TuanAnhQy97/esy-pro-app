/**
 * @flow
 */
import React, { Component } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ImageBackground
} from "react-native";

import Text from "../../../Components/Text";

import { colors } from "../../../styles/colors";
import { verticalScale, horizontalScale } from "../../../ScaleUtility";

import Explain from "../DashboardExplainIndex";

type Props = { indexOrder: number };

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

class OverlayExplainDashboard extends React.PureComponent<
  Props,
  { isShowing: boolean }
> {
  constructor(props: any) {
    super(props);
    this.state = { isShowing: false };
  }
  componentDidMount() {
    console.log("OverlayExplainDashboard", "componentDidMount");
    showed = false;
  }
  componentWillUnmount() {
    showed = false;
  }
  render() {
    if (this.state.isShowing) {
      return (
        <View
          style={[
            StyleSheet.absoluteFillObject,
            { flex: 1, justifyContent: "center", alignItems: "center" }
          ]}
        >
          {/* Background color */}
          <View
            style={[
              StyleSheet.absoluteFillObject,
              { backgroundColor: colors.third_dark, opacity: 0.87 }
            ]}
          />
          {/* Content */}
          {this.renderBody.bind(this)()}
        </View>
      );
    } else {
      return null;
    }
  }
  renderBody() {
    if (this.state.isShowing) {
      let explain = Explain.getExplain(this.props.indexOrder);
      let def = explain.def ? explain.def : "Không có giải thích";
      return (
        <View>
          <View
            style={{
              backgroundColor: colors.primary_light,
              width: horizontalScale(350),
              borderBottomLeftRadius: 8,
              borderBottomRightRadius: 8,
              alignSelf: "center",
              paddingTop: verticalScale(20),
              paddingBottom: verticalScale(30),
              paddingHorizontal: horizontalScale(10)
            }}
          >
            {/* NAME */}
            <Text
              style={{
                marginBottom: verticalScale(7),
                fontSize: horizontalScale(18)
              }}
            >
              {explain.name}
            </Text>
            <View
              style={{
                marginTop: verticalScale(5),
                height: 1,
                width: "100%",
                alignSelf: "center",
                backgroundColor: "gray"
              }}
            />
            {/* DEF */}
            <Text
              style={{
                marginVertical: verticalScale(14),
                fontSize: horizontalScale(18)
              }}
            >
              {def}
            </Text>
          </View>

          {/* Close Button */}
          <TouchableOpacity
            hitSlop={hitSlop}
            onPress={() => {
              showPopup(false);
            }}
            style={{
              alignSelf: "center",
              justifyContent: "center",
              alignItems: "center",
              marginTop: 17,
              height: 44,
              width: 44,
              borderRadius: 44,
              backgroundColor: colors.primary_light
            }}
          >
            <ImageBackground
              style={{
                width: 14,
                height: 14
              }}
              imageStyle={{
                resizeMode: "contain",
                transform: [{ rotateZ: "-90deg" }]
              }}
              source={require("../../../../assets/Images/Share/next.png")}
            />
          </TouchableOpacity>
        </View>
      );
    } else {
      return null;
    }
  }

  show() {
    this.setState({ isShowing: true });
  }
  dismiss() {
    this.setState({ isShowing: false });
  }
}
const styles = StyleSheet.create({
  content: {
    fontSize: horizontalScale(18),
    color: "gray"
  }
});

export default OverlayExplainDashboard;

/**
 * ================================================================================

 * ================================================================================
 */

let popupRef,
  showed = false;

/**
 * Set ref cho popup
 * @param {*} ref
 */
function setRef(ref: any) {
  popupRef = ref;
}
/**
 * Hiển thị popup
 * @param {*} on
 */
function showPopup(on?: boolean = false) {
  if (showed !== on) {
    showed = on;
    if (on) {
      popupRef.show();
    } else {
      popupRef.dismiss();
    }
  }
}
/**
 * Có đang hiển thị không?
 */
export function isShowing() {
  return showed;
}
export const tools = { setRef, showPopup, isShowing };
