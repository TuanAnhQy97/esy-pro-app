// @flow
import React from "react";
import { View, FlatList } from "react-native";

import BaseScreen from "../../Components/BaseScreen";
import ThreeColumnInRow from "../../Components/Table/ThreeColumnInRow";

import NavBar from "../../Components/NavBar";
import { tools as OverLayWaitingKline_tools } from "../../Components/Popup/OverLayWaitingKline";
import OverlayExplainDashboard, {
  tools as OverlayExplainDashboard_tools
} from "./OverlayExplainDashboard";
import { name_unit_arr } from "./DashboardExplainIndex";
import ble from "../../services/ble";
import eventBus from "../../services/eventBus";
import { EVENT_BUS } from "../../constants";
import navigationService from "../../services/navigationService";

type Props = {};

type States = {
  backPress?: boolean;
  selectedPos: number;

  data1: number[];
  data2: number[];
  /**
   * Extra
   */
  extraData1: number[];
  extraData2: number[];
  extraData3: number[];
  extraData4: number[];
};

class Dashboard extends React.PureComponent<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = {
      selectedPos: 0,

      data1: [],
      data2: [],
      /**
       * Extra
       */
      extraData1: [],
      extraData2: [],
      extraData3: [],
      extraData4: []
    };
  }

  componentDidMount() {
    console.log("componentDidMount", "Dashboard");
    eventBus.addListener(
      EVENT_BUS.BIKE_DASHBOARD_VALUE_CHANGE,
      this._onValueChange
    );
  }

  componentWillUnmount() {
    if (!this.state.backPress) {
      // Back do swipe gesture
      ble.esyDataManager.honda.toIdleMode();
    }
    eventBus.removeListener(this._onValueChange);
  }

  _onValueChange = (values: any) => this.setState({ ...values });

  renderBody() {
    return this.renderEngineIndex();
  }

  renderEngineIndex() {
    return (
      <View style={{ flex: 1 }}>
        {this.renderDataListHeader()}
        {this.renderDataListBody()}
      </View>
    );
  }

  renderDataListHeader() {
    return (
      <View
        style={{
          paddingTop: 5,
          backgroundColor: "green"
        }}
      >
        <ThreeColumnInRow
          value1="Tên"
          value2="Giá trị"
          value3="Đơn vị"
          disabled
        />
      </View>
    );
  }

  renderDataListBody() {
    const {
      data1,
      data2,
      extraData1,
      extraData2,
      extraData3,
      extraData4
    } = this.state;
    const data = arrangePosition(
      data1,
      data2,
      extraData1,
      extraData2,
      extraData3,
      extraData4
    );

    const filteredData: any = [];
    let sum = 0;
    data.forEach((value, index) => {
      if (value !== null && value !== undefined) {
        filteredData.push({
          posOnFlatlist: sum++,
          pos: index,
          value
        });
      }
    });
    return (
      <FlatList
        style={{
          paddingTop: 5,
          height: "100%"
        }}
        data={filteredData}
        keyExtractor={item => item.pos.toString()}
        renderItem={this.renderItem.bind(this)}
      />
    );
  }

  renderItem({ item }) {
    const name = name_unit_arr[item.pos][0];
    const unit = name_unit_arr[item.pos][1];
    if (item.value === null || item.value === undefined || item.value === NaN) {
      return null;
    }
    let value;
    let pass = false;
    // Làm tròn theo tên
    if (!pass) {
      try {
        switch (name) {
          /**
           * Data 1
           */
          case "Ắc quy":
            value = item.value.toFixed(1);
            pass = true;
            break;
          /**
           * Data 2
           */
          case "Tỉ lệ xăng/gió":
            value = item.value.toFixed(3);
            pass = true;
            break;
          case "Bộ nung Oxy":
            switch (item.value) {
              case 1:
                value = "Bật";
                break;
              default:
                value = "Tắt";
                break;
            }
            pass = true;
            break;
          /**
           * Data 3
           */
          case "Bộ nhớ mã lỗi (Mill)":
            if (item.value) {
              value = "Có";
            } else {
              value = "Trống";
            }
            pass = true;
            break;
          /**
           * Data 4
           */
          case "Van trung hoà khí thải (PCV)":
          case "Cảm biến đổ xe":
          case "Van EVAP":
          case "Rơ-le khởi động":
          case "Lệnh đề":
          case "Công tắc khởi động":
          case "Bộ bơm xăng":
          case "Quạt gió":
            switch (item.value) {
              case 1:
                value = "Bật";
                break;
              default:
                value = "Tắt";
                break;
            }
            pass = true;
            break;
          case "Chân chống cạnh":
            switch (item.value) {
              case 1:
                value = "Gạt xuống";
                break;
              default:
                value = "Gạt lên";
                break;
            }
            pass = true;
            break;
          case "Đầu nối tắt DLC":
            switch (item.value) {
              case 1:
                value = "Đóng";
                break;
              default:
                value = "Mở";
                break;
            }
            pass = true;
            break;

          default:
            value = item.value;
            break;
        }
      } catch (e) {
        value = item.value;
      }
    }
    // Làm tròn theo đơn vị
    if (!pass) {
      try {
        switch (unit) {
          case "V":
            value = item.value.toFixed(3);
            pass = true;
            break;
          case "mV":
            value = item.value.toFixed(0);
            pass = true;
            break;
          case "l/phút":
            value = item.value.toFixed(0);
            pass = true;
            break;
          default:
            value = item.value;
            break;
        }
      } catch (e) {
        value = item.value;
      }
    }
    let backgroundColor;
    if (item.posOnFlatlist % 2) {
      backgroundColor = "#C8E6C9";
    } else {
    }
    return (
      <ThreeColumnInRow
        value1={name}
        value2={value}
        value3={unit}
        backgroundColor={backgroundColor}
        onPress={() => {
          this.onPressDetail(item.pos);
        }}
      />
    );
  }

  onPressDetail(selectedPos: number) {
    this.setState({ selectedPos }, () => {
      OverlayExplainDashboard_tools.showPopup(true);
    });
  }

  handleBackPress = () => {
    console.log("Dashboard", "handleBackPress");
    // works best when the goBack is async
    this.onBack();
    return true;
  };

  async onBack() {
    /**
     * Nếu đang hiển thị popup -> Tắt popup
     */
    // Đang chờ Kline
    const isWaitingKline = OverLayWaitingKline_tools.isShowing();
    if (isWaitingKline) {
      // Không làm gì cả
      return;
    }

    this.setState({ backPress: true }, () => {
      ble.esyDataManager.honda.toIdleMode();
      navigationService.goBack();
    });
  }

  render() {
    return (
      <BaseScreen
        statusbarStyle="light-content"
        handleBackPress={this.handleBackPress}
      >
        {/* NavBar */}
        <NavBar onLeftPress={this.handleBackPress} title="Thông số" />
        {this.renderBody()}
        <OverlayExplainDashboard
          indexOrder={this.state.selectedPos}
          ref={_ref => {
            OverlayExplainDashboard_tools.setRef(_ref);
          }}
        />
      </BaseScreen>
    );
  }
}

/**
 * Sắp xếp lại thứ tự các thông số động cơ
 * @param {*} data Dashboard data
 */
function arrangePosition(
  data1: number[],
  data2: number[],
  /**
   * Extra
   */
  extraData1: number[],
  extraData2: number[],
  extraData3: number[],
  extraData4: number[]
): number[] {
  let finalArr;
  // Đã có dữ liệu
  if (data1.length !== 0) {
    // Lấy các số chính trong combo data 1
    finalArr = data1.slice(0, 9); // SUM = 9
    // // Chèn giá trị điện áp
    // // Temp Volt
    finalArr.splice(3, 0, extraData1[1]);
    // // TPS Volt
    finalArr.splice(8, 0, extraData1[0]);
    // // MAP Volt
    finalArr.splice(10, 0, extraData1[3]);
    // // IAT Volt
    finalArr.splice(12, 0, extraData1[2]); // SUM = 9 + 4 = 13

    // Đẩy nốt thông số còn lại
    //  Data 2 - Oxy
    finalArr.push(data2[0]); // SUM = 14
    //  ex Data 1 - Pull
    finalArr.push(extraData1[4]);
    finalArr.push(extraData1[5]); // SUM = 16
    //  ex Data 2 - Tỉ lệ xăng/gió, Nung Oxy
    finalArr.push(extraData2[0]);
    finalArr.push(extraData2[1]); // SUM = 18
    // ex Data 3 - Bộ nhớ mã lỗi (Mill), BA, MAP trung bình
    finalArr.push(extraData3[0]);
    finalArr.push(extraData3[1]); // SUM =
    finalArr.push(extraData3[2]);
    // ex Data 4 - Bộ bơm xăng, Chân chống cạnh, Nút Đề, Quạt gió...
    finalArr.push(...extraData4.slice(0, 11)); // SUM  =
  }
  // Chưa có dữ liệu
  else {
    finalArr = [];
  }

  return finalArr;
}

export default Dashboard;
