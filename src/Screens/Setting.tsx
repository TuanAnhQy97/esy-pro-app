/**
 * @flow
 */
import React from "react";
import { View, TouchableOpacity, ImageBackground, Alert } from "react-native";
import Text from "../Components/Text";
import BaseScreen from "../Components/BaseScreen";
import NavBar from "../Components/NavBar";

import { logout } from "../SharedFunctions";
import { colors } from "../styles/colors";

type Props = {};
type States = {};

class Setting extends React.PureComponent<Props, States> {
  render() {
    return (
      <BaseScreen>
        <NavBar title="Cài đặt" />
        {this.renderBody()}
      </BaseScreen>
    );
  }

  renderBody() {
    return (
      <View style={{ flex: 1, backgroundColor: `${colors.third_dark}11` }}>
        {this.renderButton(
          "Đổi mật khẩu",
          "Thay đổi mật khẩu tài khoản",
          this.onPressChangePass
        )}
        {this.renderButton(
          "Đổi thông tin cửa hàng",
          "Cập nhật thông tin về cửa hàng như tên cửa hàng, địa chỉ…",
          this.onPressChangeStoreInfo
        )}
        {this.renderButton(
          "Đăng xuất tài khoản",
          "Thoát tài khoản khỏi ứng dụng và xoá hết dữ liệu tài khoản trên điện thoại",
          this.onPressLogout
        )}
      </View>
    );
  }

  renderButton(title: string, explain: string, onPress: Function) {
    return (
      <View>
        <TouchableOpacity
          style={{
            backgroundColor: "white",
            marginTop: 14,
            minHeight: 44,
            justifyContent: "space-between",
            alignItems: "center",
            paddingHorizontal: 10,
            paddingVertical: 15,
            borderColor: "gray",
            borderTopWidth: 1,
            borderBottomWidth: 1,
            flexDirection: "row"
          }}
          onPress={onPress}
        >
          <Text style={{ fontSize: 18 }}>{title}</Text>
          <ImageBackground
            style={{ height: 20, width: 10 }}
            resizeMode="contain"
            source={require("../../assets/Images/Share/next.png")}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontSize: 14,
            marginLeft: 10,
            marginTop: 3,
            color: colors.third_dark
          }}
        >
          {explain}
        </Text>
      </View>
    );
  }

  onPressChangePass() {
    Alert.alert("Chức năng đang phát triển", "", [{ text: "Đồng ý" }]);
  }

  onPressChangeStoreInfo() {
    Alert.alert(
      "Thay đổi thông tin cửa hàng",
      "Vui lòng liên hệ với Fangia Savy để được hướng dẫn",
      [{ text: "Đồng ý" }]
    );
  }

  onPressLogout() {
    Alert.alert("Đăng xuất tài khoản", "", [
      { text: "Huỷ" },
      { text: "Đồng ý", onPress: logout }
    ]);
  }
}

export default Setting;
