import React, { PureComponent } from "react";
import {
  View,
  StyleSheet,
  Text,
  ToastAndroid,
  BackHandler,
  Alert
} from "react-native";
import LottieView from "lottie-react-native";

import ble from "../../services/ble";
import PopupConnectionError, {
  setRef,
  responsePopup
} from "../../Components/PopupConnectionError";
import {
  BLE_ERROR,
  DEVICE_VERSION_2,
  ESY_VER1_LOST_KLINE_ECU_ID
} from "../../constants";
import withSafeArea from "../../Components/withSafeViewArea";
import navigationService from "../../services/navigationService";
import { AccountFetcher } from "../../Fetchers/AccountFetcher";
import {
  translateBleError,
  compareVersions,
  compareArrays
} from "../../SharedFunctions";

const LatestFW = [2, 1, 0]

type Props = {};
type States = { deviceId: number[]; userInfo: any };

class ScanningScreen extends PureComponent<Props, States> {
  backPressed: any;

  constructor(props: Props) {
    super(props);
    const device = props.navigation.getParam("device", {});
    const { deviceId } = device;
    this.state = {
      deviceId,
      userInfo: null
    };
  }

  async componentDidMount() {
    // Fetch user info
    const userInfo = await AccountFetcher.getInfo();
    if (userInfo.error) {
      responsePopup({
        title: "Kết nối không thành công",
        subtitle: translateBleError(BLE_ERROR.NETWORK_ERROR),
        touchOutsideToDismiss: false,
        retryCb: this.componentDidMount
      });
    } else {
      this.setState({ userInfo: userInfo.data });
      // Start connect
      setTimeout(this.startConnect, 500);
    }
  }

  startConnect = async () => {
    console.log("startConnect");

    const { deviceId, userInfo } = this.state;
    // Load default device ID
    try {
      const { deviceVersion, ecuId } = await ble.connect(deviceId);
      if (compareVersions(deviceVersion, DEVICE_VERSION_2) < 0) {
        if (compareArrays(ecuId, ESY_VER1_LOST_KLINE_ECU_ID)) {
          throw new Error(BLE_ERROR.KLINE_ERROR);
        }
      }
      if (compareVersions(deviceVersion, LatestFW) < 0) {
        await new Promise((resolve, eject) => {
          Alert.alert(
            "Nâng cấp miễn phí phần cứng",
            "Phiên bản mới cải thiện tính năng sản phẩm. Cập nhật ngay?",
            [
              {
                text: "Đồng ý",
                onPress: () => {
                  eject(new Error("dfu"));
                  navigationService.navigate("Dfu", {
                    deviceAddr: ble.deviceAddr
                  });
                }
              },
              {
                text: "Bỏ qua",
                onPress: resolve,
                style: "cancel"
              }
            ],
            { cancelable: false }
          );
        });
      }

      navigationService.navigate("SelectBikeBrandScreen", {
        deviceVersion,
        userInfo,
        ecuId
      });
    } catch (e) {
      console.warn("startConnect", e);
      if (e.message !== "dfu") {
        // Show popup fail
        responsePopup({
          title: "Kết nối không thành công",
          subtitle: translateBleError(e),
          touchOutsideToDismiss: false,
          retryCb: this.startConnect
        });
      } else {
        ble.disconnect();
      }
    }
  };

  onBackPress = () => {
    if (!this.backPressed) {
      this.backPressed = true;
      ToastAndroid.show(
        "Ấn back một lần nữa để thoát ứng dụng.",
        ToastAndroid.SHORT
      );
      setTimeout(() => {
        this.backPressed = false;
      }, 2000);
    } else {
      BackHandler.exitApp();
    }
  };

  render() {
    return (
      <View
        style={{
          flex: 1
        }}
      >
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          {/* Hình minh hoạ */}
          {/* <ActiveDeviceAnimate size={265} /> */}
          <LottieView
            style={{ width: 250, height: 250, marginBottom: 10 }}
            source={require("../../../assets/animationJson/search.json")}
            loop
            autoPlay
          />
          {/* Header */}
          <Text style={styles.headerText}>Đang kết nối với thiết bị</Text>
        </View>
        <PopupConnectionError ref={_ref => setRef(_ref)} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerText: {
    alignSelf: "center",
    padding: 10,
    fontSize: 18
  },
  progressText: {
    marginTop: 10,
    fontSize: 18,
    alignSelf: "center"
  },
  continueBut: {
    padding: 10,
    marginBottom: 15,
    alignSelf: "center"
  },
  continueText: {}
});

export default withSafeArea()(ScanningScreen);
