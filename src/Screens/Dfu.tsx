/**
 * @flow
 */

import React, { Component } from "react";
import { View, Text, Platform, Alert } from "react-native";
import Fs from "react-native-fs";
import BleManager from "react-native-ble-manager";

import DfuHandler from "../services/ble/DfuHandler";
import navigationService from "../services/navigationService";
import { colors } from "../styles/colors";
import { verticalScale, horizontalScale } from "../ScaleUtility";
import BaseScreen from "../Components/BaseScreen";
import NavBar from "../Components/NavBar";
import { wait } from "../SharedFunctions";
import DfuAnimate from "../Components/Animates/DfuAnimate";

const ResourceFolder = "dfu";
const DfuFile = `${Platform.select({
  ios: Fs.MainBundlePath,
  android: Fs.DocumentDirectoryPath
})}/${ResourceFolder}/fw.zip`;

const cloneAssetToDocument = async (path: string) => {
  const assets = await Fs.readDirAssets(path);
  console.log("cloneAssetToDocument", path, assets);

  assets.forEach(async item => {
    if (item.isFile()) {
      // copy to document now
      console.log("clone file", item.path);
      await Fs.copyFileAssets(
        item.path,
        `${Fs.DocumentDirectoryPath}/${item.path}`
      );
    } else if (item.isDirectory()) {
      // make a folder on document
      await Fs.mkdir(`${Fs.DocumentDirectoryPath}/${item.path}`);
      // dig deeper
      await cloneAssetToDocument(item.path);
    }
  });
};

const clone3DResourceFromAssetToDocument = async () => {
  const rootPath = `${Fs.DocumentDirectoryPath}/${ResourceFolder}`;
  await Fs.mkdir(rootPath);

  await cloneAssetToDocument(ResourceFolder);
};

if (Platform.OS === "android") {
  clone3DResourceFromAssetToDocument();
}

const MAX_TRY_DFU = 3;
const SCAN_DURATION = Platform.OS === "android" ? 4 : 3; // s

type State = {
  isLoading: boolean;
  isDownloading?: boolean;
  isDFU?: boolean;
  progress: string;
  failure?: boolean;
  failureCode?: number;
  DFU_state: string;
  countTry: number;
  /**
   *
   */
  deviceAddr: string;
  currentVersion: number[];
  lastestVersion: number[];
};

class DfuUpdateScreen extends React.Component<{ navigation: Object }, State> {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      isDownloading: false,
      isDFU: false,
      progress: "0%",
      failure: false,
      failureCode: 0,
      DFU_state: "",
      deviceAddr: "",
      currentVersion: [],
      lastestVersion: [],
      countTry: 0
    };
  }

  componentDidMount() {
    // Setup DFU callback
    DfuHandler.registerDFUProgressCallback(this.onDfuProgressChange);
    DfuHandler.registerDFUStateChangedCallback(this.onDfuStateChange);

    this.startDFU();
  }

  componentWillUnmount() {
    // Remove callback
    DfuHandler.unresgisterAllCallback();
  }

  onDownloadProgressChange = (percent: number) => {
    console.log(`Download progress: ${percent}%`);
    const str = `Đang chuẩn bị dữ liệu ${percent.toFixed(0)}%`;
    this.setState({ progress: str });
  };

  onDfuProgressChange = ({
    percent,
    currentPart,
    partsTotal,
    avgSpeed,
    speed
  }: Object) => {
    console.log(`DFU progress: ${percent}%`);
    const str = `Đang cài đặt ${percent}% (${currentPart}/${partsTotal})`;
    this.setState({ progress: str });
  };

  onDfuStateChange = ({ state }: Object) => {
    console.log("DFU State:", state);
    this.setState({ DFU_state: state });
    if (state === "DFU_FAILED") {
      // FAILED
      if (this.state.countTry < MAX_TRY_DFU) {
        // RETRY
        this.setState(
          {
            countTry: this.state.countTry + 1,
            progress: `Tiến hành thử lại ${this.state.countTry +
              1}/${MAX_TRY_DFU}`
          },
          this.startDFU
        );
      } else {
        // ENOUGH RETRY
        this.onFailure();
      }
    } else if (state === "DFU_COMPLETED") {
      Alert.alert(
        "Cập nhật thành công",
        "Vui lòng tắt Bluetooth và bật lại để tiếp tục sử dụng",
        [
          {
            text: "Đồng ý",
            onPress: () => navigationService.navigate("ScanningScreen")
          }
        ],
        { cancelable: false }
      );
    } else if (state === "CONNECTING") {
      this.setState({ progress: "Đang kết nối thiết bị" });
    }
  };

  onFailure = () => {
    Alert.alert(
      "Cập nhật không thành công",
      "",
      [
        {
          text: "Bỏ qua",
          onPress: () => navigationService.navigate("ScanningScreen")
        },
        {
          text: "Thử lại",
          onPress: () => {
            this.setState(
              { countTry: 0, progress: "Kiểm tra thiết bị" },
              this.startDFU
            );
          }
        }
      ],
      { cancelable: false }
    );
  };

  handleBackPress = () => {
    Alert.alert(
      "Vui lòng đợi cập nhật hoàn tất",
      "Huỷ quá trình có thể khiến thiết bị ngừng hoạt động",
      [{ text: "Đồng ý" }],
      {
        cancelable: false
      }
    );
    return true;
  };

  startDFU = async () => {
    const deviceAddr = this.props.navigation.getParam("deviceAddr");
    this.setState({ progress: "Kiểm tra thiết bị" });
    // Scan trước khi bắt đầu -> Tránh lỗi, để BLE stack nhận được thiết bị
    BleManager.scan([], SCAN_DURATION, true, { scanMode: 1 }).then(() => {
      // Success code
      console.log("Scan for DFU started");
    });
    await wait((SCAN_DURATION + 0.5) * 1000);
    DfuHandler.startDFU(deviceAddr, DfuFile);
  };

  renderBody = () => {
    if (this.state.failure) {
      return null;
    }
    return (
      <View style={{ alignItems: "center", marginTop: verticalScale(97) }}>
        <Text style={{ fontSize: horizontalScale(18) }}>
          {this.state.progress}
        </Text>
        <Text
          style={{
            color: colors.third_dark,
            marginTop: verticalScale(10),
            textAlign: "center"
          }}
        >
          Không tắt ứng dụng hoặc khoá điện
          {"\n"}
          khi cập nhật
        </Text>
        {/* Animated View */}
        <DfuAnimate />
        <Text
          style={{
            color: colors.third_dark,
            marginTop: verticalScale(69),
            textAlign: "center"
          }}
        >
          Giữ khoảng cách gần với xe của bạn.
          {"\n"} Quá trình có thể diễn ra từ 3 đến 5 phút
        </Text>
      </View>
    );
  };

  render() {
    return (
      <BaseScreen
        statusbarStyle="light-content"
        handleBackPress={this.handleBackPress}
      >
        <NavBar
          title="Cập nhật phần cứng"
          leftIconSource={null}
          onLeftPress={() => {}}
        />
        {this.renderBody.bind(this)()}
      </BaseScreen>
    );
  }
}
export default DfuUpdateScreen;
