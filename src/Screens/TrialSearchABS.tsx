/**
 * @flow
 */
import React from "react";
import {
  StyleSheet,
  Dimensions,
  FlatList,
  TouchableOpacity,
  Text,
  View
} from "react-native";
import HTML from "react-native-render-html";
import BaseScreen from "../Components/BaseScreen";
import NavBar from "../Components/NavBar";

import { horizontalScale } from "../ScaleUtility";
import { colors } from "../styles/colors";
import ASHelper from "../ASHelper";
import navigationService from "../services/navigationService";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

type Props = {};
type States = {
  error: "no_internet" | "unknown" | "";
  isLoading: boolean;

  phone: string;
  deviceId: number[];
  storeName: string;
  addr: string;
  expiredDate: number;
};

const data = [
  {
    title: "Cách xóa mã lỗi lưu trữ hệ thống ABS Honda",
    content: `Thao tác như sau :
    - Nối đầu nối SCS vào DLC.
    - Bóp tay phanh và bật khóa điện ,chờ đèn báo ABS sáng 2 giây rồi tắt.
    - Nhả tay phanh ngay khi đèn ABS tắt,sau khi nhả tay phanh đèn ABS sẽ sáng trở lại.
    - Bóp tay phanh ngay khi đèn ABS sáng ,sau khi bóp tay phanh đèn ABS sẽ tắt.
    - Nhả tay phanh ngay khi đèn ABS tắt,theo dõi đèn ABS nếu đèn nháy 2 lần và sáng là thành công. ( Làm lại 5 bước trên nếu không thành công)`
  },
  {
    title: "Cách Gọi Lỗi hệ thống ABS",
    content: `Thao tác như sau :
    - Nối đầu nối SCS vào DLC.
    - Bật khóa điện và quan sát đèn báo ABS 
    - Nếu đèn ABS sáng trong 2 giây rồi tắt và nhấp nháy ,đọc số lần nhấp nháy và tra cứu mã lỗi.
    - Nếu đèn ABS sáng liên tục không tắt, hệ thống ABS không có lỗi.
    `
  },
  {
    title: "Lỗi 1-1",
    content: `Mạch cảm biến tốc độ bánh trước 
 
    Gợi ý sửa chữa :
    - Kiểm tra mạch dây dẫn tới ECU ABS.
    - Kiểm tra ECU ABS.`
  },
  {
    title: "Lỗi 1-2",
    content: `Cảm biến bánh trước : 

    Gợi ý sửa chữa :
    - Kiểm tra vành xung đĩa cảm biến có biến dạng bất thường.
    - Nhiễm từ trường xung quanh.
    - Kiểm tra cảm biến.`
  },
  {
    title: "Lỗi 1-3 ",
    content: `Mạch cảm biến bánh sau

    Gợi ý sửa chữa :
    - Kiểm tra mạch dây dẫn tới ECU ABS.
    - Kiểm tra ECU ABS.`
  },
  {
    title: "Lỗi 1-4",
    content: `Cảm biến bánh sau : 

    Gợi ý sửa chữa :
    - Kiểm tra vành xung đĩa cảm biến có biến dạng bất thường.
    - Nhiễm từ trường xung quanh.
    - Kiểm tra cảm biến.`
  },
  {
    title: "Lỗi 1-5",
    content: `Cảm biến trước hoặc sau bị ngắn mạch
 
    Gợi ý sửa chữa :
    - Kiểm tra cảm biến ,dây dẫn và ECU ABS`
  },
  {
    title: "Lỗi 2-1",
    content: `Vòng phát xung bánh trước

    Gợi ý sửa chữa :
    - Kiểm tra biên dạng ,độ phẳng của vành xung,khoảng cách giữa vành xung và cảm biến.`
  },
  {
    title: "Lỗi 2-3",
    content: `Vòng phát xung bánh sau

    Gợi ý sửa chữa :
    Kiểm tra biên dạng ,độ phẳng của vành xung,khoảng cách giữa vành xung và cảm biến .`
  },
  {
    title: "Lỗi 3-1 ",
    content: `Van từ điều áp ABS 

    Gợi ý sửa chữa :
    Kiểm tra cụm van điều áp ABS hoặc ECU ABS`
  },
  {
    title: "Lỗi 3-2",
    content: `Van từ điều áp ABS 

    Gợi ý sửa chữa :
    Kiểm tra cụm van điều áp ABS hoặc ECU ABS`
  },
  {
    title: "Lỗi 3-3",
    content: `Van từ điều áp ABS 

    Gợi ý sửa chữa :
    Kiểm tra cụm van điều áp ABS hoặc ECU ABS`
  },
  {
    title: "Lỗi 3-4",
    content: `Van từ điều áp ABS 

    Gợi ý sửa chữa :
    Kiểm tra cụm van điều áp ABS hoặc ECU ABS`
  },
  {
    title: "Lỗi 4-1",
    content: `Khóa bánh trước

    Gợi ý sửa chữa :
    Kiểm tra lý do bánh trước không quay cùng bánh sau`
  },
  {
    title: "Lỗi 4-2",
    content: `Khóa bánh trước

    Gợi ý sửa chữa :
    Kiểm tra lý do bánh trước không quay cùng bánh sau`
  },
  {
    title: "Lỗi 4-3",
    content: `Khóa bánh sau

    Gợi ý sửa chữa :
    Kiểm tra lý do bánh sau không quay cùng bánh trước`
  },
  {
    title: "Lỗi 5-1",
    content: `Motor bơm ABS

    Gợi ý sửa chữa :
    Kiểm tra motor bơm ( bộ bơm điều áp ) hoạt động bất thường hoặc không hoạt động`
  },
  {
    title: "Lỗi 5-4",
    content: `Nguồn cấp  Motor bơm ABS

    Gợi ý sửa chữa :
    Kiểm tra mạch điện motor bơm ( bộ bơm điều áp ) hoạt động bất thường hoặc không hoạt động`
  },
  {
    title: "Lỗi 6-1",
    content: `Nguồn cấp Motor bơm ABS

    Gợi ý sửa chữa :
    Kiểm tra mạch điện motor bơm ( bộ bơm điều áp ) hoạt động bất thường hoặc không hoạt động`
  },
  {
    title: "Lỗi 6-2",
    content: `Nguồn cấp Motor bơm ABS

    Gợi ý sửa chữa :
    Kiểm tra mạch điện motor bơm ( bộ bơm điều áp ) hoạt động bất thường hoặc không hoạt động`
  },
  {
    title: "Lỗi 7-1",
    content: `Kích thước lốp sai tiêu chuẩn

    Gợi ý sửa chữa :
    Kiểm tra thông số lốp theo quy định của nhà sản xuất`
  },
  {
    title: "Lỗi 8-1",
    content: `Lỗi Bộ Điều khiển ABS

    Gợi ý sửa chữa :
    Kiểm tra bộ điều khiển hệ thống ABS và hệ thống điều áp ABS.`
  }
];

class TrialSearchABS extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  componentWillUnmount() {}

  onBack = async () => {
    console.log("TrialSearchABS", "onBack");
    navigationService.goBack();
  };

  handleBackPress = () => {
    console.log("TrialSearchABS", "handleBackPress");
    // works best when the goBack is async
    this.onBack();
    return true;
  };

  render() {
    const htmlContent = ``;

    return (
      <BaseScreen
        statusbarStyle="light-content"
        handleBackPress={this.handleBackPress}
      >
        {/* NavBar */}
        <NavBar onLeftPress={this.handleBackPress} title="Mã lỗi ABS Honda" />
        {/* Note */}
        <HTML
          html={htmlContent}
          imagesMaxWidth={Dimensions.get("window").width}
        />
        {/* List */}
        <FlatList
          keyExtractor={item => item.title}
          data={data}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity
                style={styles.item}
                onPress={() => {
                  navigationService.navigate("ABSExplain", {
                    data: item
                  });
                }}
              >
                <Text style={styles.itemText}>{item.title}</Text>
              </TouchableOpacity>
            );
          }}
          ListHeaderComponent={() => <View style={{ marginTop: 5 }} />}
          ItemSeparatorComponent={() => <View style={styles.line} />}
          ListFooterComponent={() => <View style={styles.line} />}
        />
      </BaseScreen>
    );
  }
}

export default TrialSearchABS;

const styles = StyleSheet.create({
  item: { minHeight: 50, paddingVertical: 10, paddingHorizontal: 20 },
  itemText: { fontSize: 20 },
  line: { width: "100%", height: 1, opacity: 0.3, backgroundColor: "#808080" }
});
