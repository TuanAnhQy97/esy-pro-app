/**
 * @flow
 */
import React from "react";
import {
  View,
  TouchableOpacity,
  StyleSheet,
  ImageBackground
} from "react-native";
import Text from "../../Components/Text";
import { horizontalScale } from "../../ScaleUtility";
import { colors } from "../../styles/colors";

type States = {};
type Props = {
  setStateAutoCheckTwo: Function,
  onPress: Function,
  title: string,
  unit: string,
  value: any,
  min: any,
  max: any,
  enable: boolean,
  iconSource: any,
  highLightColor?: string
};

class EngineIndex extends React.PureComponent<Props, States> {
  render() {
    let circleSize = horizontalScale(30);

    let enable = this.props.enable;
    let title = this.props.title;
    let unit = this.props.unit;
    let value = this.props.value;
    let min = this.props.min;
    let max = this.props.max;

    let highLightColor;
    let titleColor;
    // Có màu custom
    if (this.props.highLightColor) {
      highLightColor = this.props.highLightColor;
      titleColor = this.props.highLightColor;
    }
    // K có custom
    else {
      // Nếu thông số được bật
      if (enable) {
        if (value >= min && value <= max) {
          highLightColor = colors.secondary_dark + "ce";
          titleColor = colors.secondary_dark;
        } else {
          highLightColor = colors.error;
          titleColor = colors.error;
        }
      }
      // Không bật
      else {
        highLightColor = colors.third_dark;
        titleColor = colors.third_dark;
      }
    }

    return (
      <TouchableOpacity
        disabled={!enable}
        onPress={this.props.onPress}
        style={{
          overflow: "hidden",
          marginVertical: 7,
          opacity: enable ? 1 : 0.5,
          borderRadius: 8,
          borderColor: colors.third_dark,
          borderWidth: 1
        }}
      >
        <View
          style={{
            padding: 10,
            width: "100%",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <Text
            style={{
              // paddingHorizontal: 10,
              fontSize: horizontalScale(18),
              color: titleColor
            }}
          >
            {title} ({unit})
          </Text>
          <ImageBackground
            imageStyle={{ tintColor: highLightColor }}
            style={{
              width: circleSize,
              height: circleSize,
              borderRadius: circleSize
            }}
            resizeMode="contain"
            source={this.props.iconSource}
          />
        </View>
        <View
          style={{
            padding: 10,
            // paddingHorizontal: 10,
            flexDirection: "row",
            justifyContent: "space-between",
            borderTopColor: colors.third_dark,
            borderTopWidth: 1
          }}
        >
          <Text style={styles.minmaxText}>{min}</Text>
          <Text
            style={{
              ...styles.valueText,
              color: highLightColor ? highLightColor : colors.third_dark,
              fontSize: horizontalScale(20)
              // fontWeight: "bold"
            }}
          >
            {value}
          </Text>
          <Text style={{ ...styles.minmaxText, textAlign: "right" }}>
            {max}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  minmaxText: { flex: 1 },
  valueText: { flex: 1, textAlign: "center", fontSize: horizontalScale(18) }
});

export default EngineIndex;
