/**
 * @flow
 */
import React from "react";
import { View, ScrollView, Platform, Alert, ToastAndroid } from "react-native";
import BaseScreen from "../../Components/BaseScreen";
import NavBar from "../../Components/NavBar";
import EngineIndex from "./EngineIndex";

import OverlayPerformanceIndexDetail, {
  tools as OverlayPerformanceIndexDetail_tools
} from "../../Components/Popup/OverlayPerformanceIndexDetail";
import { tools as OverLayWaitingKline_tools } from "../../Components/Popup/OverLayWaitingKline";

import DataForReport from "../../Screens/ReportScreen/DataForReport";
import { analyzePerformance } from "../ReportScreen/Helpers/AnalyzePerformance";
import eventBus from "../../services/eventBus";
import { EVENT_BUS } from "../../constants";
import bikeLibHelper from "../../services/bikeLibHelper";
import ble from "../../services/ble";
import navigationService from "../../services/navigationService";

const iconArr = [
  require("../../../assets/Images/BikeComponents/pistol.png"), // 0
  require("../../../assets/Images/Share/temp.png"), // 1
  require("../../../assets/Images/BikeComponents/injector.png"), // 2
  require("../../../assets/Images/BikeComponents/battery.png"), // 3
  require("../../../assets/Images/BikeComponents/buzy.png"), // 4
  require("../../../assets/Images/BikeComponents/throttle.png"), // 5
  require("../../../assets/Images/BikeComponents/pressure.png"), // 6
  require("../../../assets/Images/BikeComponents/airTemp.png"), // 7
  require("../../../assets/Images/BikeComponents/eco.png") // 8
];

const name_unit_arr = [
  ["Vòng tua", "vòng/phút"],
  // ["Nhiệt độ", "°C"],
  ["Thời gian phun", "ms"],
  ["Điện áp bình", "V"],
  ["Góc đánh lửa", "°"],
  ["Góc bướm ga", "°"],
  ["Áp suất khí nạp", "kPa"],
  ["Nhiệt độ khí nạp", "°C"],
  ["Oxy", "mV"]
];

type States = {
  selectedIndex: number;
  levelOfIndex: number | null;
  backPress?: boolean;
  data1: number[];
  data2: number[];
};
type Props = {};

class AutoCheckTwo extends React.PureComponent<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = { selectedIndex: 0, levelOfIndex: null, data1: [], data2: [] };
  }

  componentDidMount() {
    eventBus.addListener(
      EVENT_BUS.BIKE_AUTO_CHECK_TWO_RESULT_CHANGE,
      this._onValueChange
    );
  }

  _onValueChange = (data: any) => {
    this.setState({ ...data });
  };

  componentWillUnmount() {
    if (!this.state.backPress) {
      // Back do swipe gesture
      ble.esyDataManager.honda.toIdleMode();
    }
    eventBus.removeListener(this._onValueChange);
  }

  renderBody() {
    const { data1, data2 } = this.state;
    const convertedList = [...data1, ...data2];
    const filtered = convertedList.slice(1, 10);
    const levelArr = analyzePerformance(
      filtered,
      bikeLibHelper.honda._hondaLib.standardQD
    ); // Note: Phân tích trước -> thay đổi vị trí sau
    // bỏ nhiệt độ ra riêng
    const temp = filtered.splice(1, 1)[0];
    levelArr.splice(1, 1);

    const standardQD = [...bikeLibHelper.honda._hondaLib.standardQD];
    // let standardQD = [
    //   [1600, 1800], // RPM
    //   [80, 100], // ECT
    //   [1.6, 3], // IT
    //   [12, 15], // Vbat
    //   [10, 14, 12], // GDL
    //   [-2, 2], // TPS
    //   [20, 110], // MAP
    //   [18, 65], // IAT
    //   [0, 1000] // Oxy
    // ];
    // bỏ nhiệt độ ra riêng
    let tempColor;
    const minMaxTemp = standardQD.splice(1, 1)[0];
    const warmEnough = temp >= bikeLibHelper.honda._hondaLib.qdTempCondition;
    const engineStarted = filtered[0] > 0;
    const displayOtherIndex = engineStarted && warmEnough;

    if (!warmEnough) {
      tempColor = "#1E88E5";
    }
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1 }}>
          <EngineIndex
            highLightColor={tempColor}
            iconSource={iconArr[1]}
            onPress={() => {
              this.onPressDetail(1, 0);
            }}
            enable
            setStateAutoCheckTwo={this.setStateAutoCheckTwo}
            title="Nhiệt độ"
            unit="°C"
            value={temp}
            min={minMaxTemp[0]}
            max={minMaxTemp[1]}
          />
          {filtered.map((value, index) => {
            if (value !== null) {
              let posOnPerformanceExplain = 0;
              if (index === 0) {
                posOnPerformanceExplain = 0;
              } else {
                posOnPerformanceExplain = index + 1;
              }
              // Không có dữ liệu QD của giá trị
              if (standardQD[index].length === 0) {
                return null;
              }

              let level = 0;
              if (value < standardQD[index][0]) {
                level = -1;
              } else if (value > standardQD[index][1]) {
                level = 1;
              }
              return (
                <EngineIndex
                  iconSource={iconArr[posOnPerformanceExplain]}
                  onPress={() => {
                    this.onPressDetail(
                      posOnPerformanceExplain,
                      levelArr[index]
                    );
                  }}
                  enable={displayOtherIndex}
                  key={index.toString()}
                  setStateAutoCheckTwo={this.setStateAutoCheckTwo}
                  title={name_unit_arr[index][0]}
                  unit={name_unit_arr[index][1]}
                  value={roundValue(value, index)}
                  min={standardQD[index][0]}
                  max={standardQD[index][1]}
                />
              );
            }
            return null;
          })}
          <View style={{ height: 10 }} />
        </ScrollView>
        {/* nút lưu kết quả */}
        {/* <TouchableOpacity
          style={{
            backgroundColor: displayOtherIndex
              ? colors.primary_dark
              : colors.third_dark,
            height: 44,
            justifyContent: "center",
            alignItems: "center"
          }}
          onPress={() => this.onPressSave(warmEnough)}
        >
          <Text style={{ color: colors.primary_light, fontSize: 18 }}>
            Lưu kết quả vào báo cáo
          </Text>
        </TouchableOpacity> */}
      </View>
    );
  }

  handleBackPress = () => {
    console.log("AutoCheckTwo", "handleBackPress");
    // works best when the goBack is async
    this.onBack();
    return true;
  };

  onBack = async () => {
    /**
     * Nếu đang hiển thị popup -> Tắt popup
     */
    // Đang chờ Kline
    const isWaitingKline = OverLayWaitingKline_tools.isShowing();
    if (isWaitingKline) {
      // Không làm gì cả
      return;
    }
    // Popup Giải thích
    const isShowExplain = OverlayPerformanceIndexDetail_tools.isShowing();
    if (isShowExplain) {
      OverlayPerformanceIndexDetail_tools.showPopup(false);
      return;
    }

    this.setState({ backPress: true }, () => {
      ble.esyDataManager.honda.toIdleMode();
      navigationService.goBack();
    });
  };

  /**
   * Set state of this Component
   * @param {*} params
   */
  setStateAutoCheckTwo = (params: any) => {
    this.setState({ ...params });
  };

  onPressSave = () => {
    const { data1, data2 } = this.state;
    const convertedList = [...data1, ...data2];
    const temp = convertedList[2];
    const rpm = convertedList[1];

    const warmEnough = temp >= bikeLibHelper.honda._hondaLib.qdTempCondition;
    const engineStarted = rpm > 0;

    if (warmEnough) {
      if (engineStarted) {
        // Lưu giá trị vào report
        DataForReport.setPerformance(convertedList.slice(1));
      } else {
        warning("Động cơ cần hoạt động ở chế độ không tải (cầm chừng)");
      }
    } else {
      warning(
        `Động cơ cần nhiệt độ tối thiểu ${
          bikeLibHelper.honda._hondaLib.qdTempCondition
        }°C`
      );
    }
  };

  onPressDetail = (index: number, level: number | null) => {
    this.setState({ selectedIndex: index, levelOfIndex: level }, () => {
      OverlayPerformanceIndexDetail_tools.showPopup(true);
    });
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <BaseScreen>
          <NavBar
            title="Hiệu suất động cơ"
            onLeftPress={this.handleBackPress}
            rightIconSource={require("../../../assets/Images/AutoCheckTwo/save.png")}
            onRightPress={this.onPressSave}
          />
          {this.renderBody()}
        </BaseScreen>
        <OverlayPerformanceIndexDetail
          ref={_ref => {
            OverlayPerformanceIndexDetail_tools.setRef(_ref);
          }}
          indexOrder={this.state.selectedIndex}
          level={this.state.levelOfIndex}
        />
      </View>
    );
  }
}

/**
 * Làm tròn các giá trị
 * @param {*} value giá trị
 * @param {*} index thứ tự hiển thị trong bảng
 */
function roundValue(value: number, index: number) {
  switch (index) {
    // thời gian phun
    case 1:
      return value.toFixed(2);
    // Oxy
    case 7:
      return value.toFixed(0);
    default:
      return value;
  }
}
function warning(content: string) {
  if (Platform.OS === "android") {
    ToastAndroid.show(content, ToastAndroid.SHORT);
  } else {
    Alert.alert(content, "", [{ text: "Đồng ý" }]);
  }
}

export default AutoCheckTwo;
