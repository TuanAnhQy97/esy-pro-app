/**
 * @flow
 */
import React from "react";
import {
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  Text,
  View
} from "react-native";
import BaseScreen from "../Components/BaseScreen";
import NavBar from "../Components/NavBar";

import { horizontalScale } from "../ScaleUtility";
import { colors } from "../styles/colors";
import ASHelper from "../ASHelper";
import navigationService from "../services/navigationService";
import Images from "../../assets/Images";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

type Props = {};
type States = {
  error: "no_internet" | "unknown" | "";
  isLoading: boolean;

  phone: string;
  deviceId: number[];
  storeName: string;
  addr: string;
  expiredDate: number;
};

class TrialList extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  componentWillUnmount() {}

  onBack = async () => {
    console.log("TrialList", "onBack");
    navigationService.goBack();
  };

  handleBackPress = () => {
    console.log("TrialList", "handleBackPress");
    // works best when the goBack is async
    this.onBack();
    return true;
  };

  renderBody = () => {
    return (
      <View>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => navigationService.navigate("TrialSearchABS")}
        >
          <ImageBackground
            style={styles.btnImage}
            imageStyle={styles.image}
            source={Images.abs}
          >
            <Text style={styles.btnText}>Tra cứu mã lỗi ABS Honda</Text>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => navigationService.navigate("TrialSearchSmartKey")}
        >
          <ImageBackground
            style={styles.btnImage}
            imageStyle={styles.image}
            source={Images.smartKey}
          >
            <Text style={[styles.btnText]}>Tra cứu mã lỗi SmartKey Honda</Text>
          </ImageBackground>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <BaseScreen
        statusbarStyle="light-content"
        handleBackPress={this.handleBackPress}
      >
        {/* NavBar */}
        <NavBar onLeftPress={this.handleBackPress} title="Tra cứu mã lỗi" />
        {this.renderBody()}
      </BaseScreen>
    );
  }
}

export default TrialList;

const styles = StyleSheet.create({
  btn: { width: "100%" },
  btnImage: {
    width: "100%",
    height: 150,
    justifyContent: "flex-end",
    backgroundColor: "black"
  },
  image: {
    width: "100%",
    height: 150,
    resizeMode: "cover",
    opacity: 0.8
  },
  btnText: {
    marginBottom: 15,
    marginHorizontal: 15,
    fontSize: 20,
    color: "white",
    fontWeight: "bold",
    textTransform: "uppercase"
  }
});
