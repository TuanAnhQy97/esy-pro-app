/**
 * @flow
 */
import React from 'react';
import {TouchableOpacity, View, ScrollView, ImageBackground} from 'react-native';

import BaseScreen from '../../Components/BaseScreen';
import NavBar from '../../Components/NavBar';
import Text from '../../Components/Text';
import PerformanceResult from './Components/PerformanceResult';
import AutoCheckOneResult from './Components/AutoCheckOneResult';
import CustomerInfo from './CustomerInfo';
import SharedStyles from './Components/SharedStyles';
import MyActivityIndicator from '../../Components/MyActivityIndicator';

import ShareReport from './ShareReport/src/ExportPDF';
import ASHelper from '../../ASHelper';
import {colors} from '../../styles/colors';
import navigationService from '../../services/navigationService';

const hitSlop = {top: 15, bottom: 15, left: 15, right: 15};

type Props = {};
type States = {isLoading: boolean};

class FinalReport extends React.PureComponent<Props, States> {
  otherConclusion: string;
  errorListStr: null | string[][] = null;
  notGoodListStr: null | string[] = null;
  performanceStr: null | string[] = null;
  // customer
  customername: string;
  licensePlate: string;
  customerPhone: string;
  bikeName: string;
  // store
  storeAddr: string;
  storeName: string;
  storePhone: string;

  constructor(props: any) {
    super(props);
    this.state = {isLoading: true};

    this.customername = props.navigation.getParam('customername');
    this.customerPhone = props.navigation.getParam('customerPhone');
    this.licensePlate = props.navigation.getParam('licensePlate');
    this.bikeName = props.navigation.getParam('bikeName');

    this.otherConclusion = props.navigation.getParam('otherConclusion');
    this.errorListStr = props.navigation.getParam('errorListStr');
    this.notGoodListStr = props.navigation.getParam('notGoodListStr');
    this.performanceStr = props.navigation.getParam('performanceStr');
  }

  async componentDidMount() {
    // Load store info
    let storeInfo = await ASHelper.loadUserInfoFromAS();
    if (storeInfo) {
      this.storeAddr = `${storeInfo.storeInfo.addr}, ${storeInfo.storeInfo.district}, ${storeInfo.storeInfo.city}`;
      this.storeName = storeInfo.storeInfo.name;
      this.storePhone = '0' + storeInfo.phone.slice(3);

      this.setState({isLoading: false});
    }
  }

  render() {
    return (
      <BaseScreen>
        <NavBar title="Báo cáo khách hàng" />
        {this.renderBody()}
      </BaseScreen>
    );
  }

  renderBody() {
    if (this.state.isLoading) {
      return <MyActivityIndicator />;
    } else {
      let hasOtherConclusion;
      if (this.otherConclusion) {
        hasOtherConclusion = true;
      } else {
        hasOtherConclusion = false;
      }
      return (
        <View style={{flex: 1, paddingHorizontal: 19, paddingTop: 10}}>
          <ScrollView>
            {/* Store Info */}

            {/* Customer Info */}
            <CustomerInfo
              name={this.customername}
              bikeName={this.bikeName}
              licensePlate={this.licensePlate}
              customerPhone={this.customerPhone}
            />
            {/* AutoCheckOne */}
            <AutoCheckOneResult errorList={this.errorListStr} notGoodList={this.notGoodListStr} />
            {/* AutoCheckTwo */}
            <PerformanceResult data={this.performanceStr} />
            {/* Các kết luận khác */}
            <Text
              style={{
                ...SharedStyles.header1,
                marginTop: 30,
                opacity: hasOtherConclusion ? 1 : 0.5,
              }}>
              Mục hư hỏng khác của xe
            </Text>

            <Text
              style={{
                ...SharedStyles.inputText,
                opacity: hasOtherConclusion ? 1 : 0.5,
                paddingTop: hasOtherConclusion ? 10 : 0,
              }}>
              {hasOtherConclusion ? this.otherConclusion : 'Không có'}
            </Text>
          </ScrollView>
          {/* Buttons */}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            {/* Home Button */}
            <TouchableOpacity
              hitSlop={hitSlop}
              style={{
                alignSelf: 'center',
                minHeight: 45,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              }}
              onPress={this.onPressHome.bind(this)}>
              <ImageBackground
                resizeMode="contain"
                imageStyle={{tintColor: colors.third_dark}}
                style={{width: 14, height: 14}}
                source={require('../../../assets/Images/Share/back.png')}
              />
              <Text
                style={{
                  fontSize: 16,
                  color: colors.third_dark,
                  textDecorationLine: 'underline',
                }}>
                Về màn hình chính
              </Text>
            </TouchableOpacity>
            {/* Next Button */}
            <TouchableOpacity
              hitSlop={hitSlop}
              style={{
                alignSelf: 'center',
                minHeight: 45,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              }}
              onPress={this.onPressNext.bind(this)}>
              <Text
                style={{
                  fontSize: 16,
                  color: colors.secondary_dark,
                  textDecorationLine: 'underline',
                }}>
                Chia sẻ
              </Text>
              <ImageBackground
                resizeMode="contain"
                imageStyle={{tintColor: colors.secondary_dark}}
                style={{width: 14, height: 14}}
                source={require('../../../assets/Images/Share/next.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  }

  onChangeText(text: string) {
    this.otherConclusion = text;
  }

  onPressNext() {
    ShareReport.openShare(this.genrateContentForPDF());
  }

  onPressHome() {
    navigationService.navigate('MainScreen');
  }

  genrateContentForPDF() {
    let dgcb;
    // Có dữ liệu Đánh giá cảm biến
    if (this.errorListStr) {
      let error;
      if (this.errorListStr) {
        if (this.errorListStr.length > 0) {
          error = this.errorListStr.map(value => {
            return {content: value[0]};
          });
        } else {
          error = [{content: 'Không có bản ghi nào'}];
        }
      } else {
        error = [{content: 'Chưa thực hiện kiểm tra'}];
      }
      let notGood;
      if (this.notGoodListStr) {
        if (this.notGoodListStr.length > 0) {
          notGood = this.notGoodListStr.map(value => {
            return {content: value};
          });
        } else {
          notGood = [{content: 'Không có sai lệch khác'}];
        }
      } else {
        notGood = [{content: 'Chưa thực hiện kiểm tra'}];
      }

      dgcb = [
        {
          name: 'Lỗi FI từng xảy ra',
          content: error,
        },
        {
          name: 'Sai lệch cảm biến khác',
          content: notGood,
        },
      ];
    }
    // CHưa thực hiện đánh giá cảm biến
    else {
      dgcb = [];
    }
    let performance;
    // Có dữ liệu hiệu suất
    if (this.performanceStr) {
      if (this.performanceStr.length > 0) {
        performance = this.performanceStr.map(value => {
          return {content: value};
        });
      } else {
        performance = [{content: 'Không có sai lệch khác'}];
      }
    } else {
      performance = [];
    }
    let others;
    if (this.otherConclusion) {
      others = [{content: this.otherConclusion}];
    } else {
      others = [{content: 'Không có'}];
    }

    let data = [
      {
        name: 'Thông tin',
        content: [
          {
            name: 'Cửa hàng',
            content: [
              {
                name: 'Cửa hàng',
                content: this.storeName,
              },
              {
                name: 'Địa chỉ',
                content: this.storeAddr,
              },
              {
                name: 'Số điện thoại',
                content: this.storePhone ? `link#tel:${this.storePhone}` : null,
              },
            ],
          },
          {
            name: 'Khách hàng',
            content: [
              {
                name: 'Tên khách hàng',
                content: this.customername,
              },
              {
                name: 'Điện thoại',
                content: this.customerPhone ? `link#tel:${this.customerPhone}` : null,
              },
              {
                name: 'Loại xe',
                content: this.bikeName,
              },
              {
                name: 'Biển số',
                content: this.licensePlate,
              },
            ],
          },
        ],
      },
      {
        name: 'Mục cần kiểm tra sửa chữa',
        content: [
          {
            name: 'Đánh giá cảm biến',
            content: dgcb,
          },
          {
            name: 'Hiệu suất động cơ',
            content: performance,
          },
        ],
      },
      {
        name: 'Mục kiểm tra sửa chữa bổ sung',
        content: others,
      },
    ];
    return data;
  }
}

export default FinalReport;
