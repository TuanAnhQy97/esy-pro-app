/**
 * @flow
 */
import React from "react";
import {
  TouchableOpacity,
  View,
  TextInput,
  StyleSheet,
  Keyboard
} from "react-native";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import BaseScreen from "../../Components/BaseScreen";
import NavBar from "../../Components/NavBar";
import Text from "../../Components/Text";
import SharedStyles from "./Components/SharedStyles";
import navigationService from "../../services/navigationService";
import { colors } from "../../styles/colors";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

type Props = {};
type States = {};

class InputCustomerInfo extends React.PureComponent<Props, States> {
  inputRef_phone: any;

  inputRef_licensePlate: any;

  otherConclusion: string;

  errorListStr: null | string[][] = null;

  notGoodListStr: null | string[] = null;

  performanceStr: null | string[] = null;

  name: string = "";

  customerPhone: string = "";

  licensePlate: string = "";

  bikeName: string;

  constructor(props: any) {
    super(props);
    this.name = "";
    this.licensePlate = "";
    this.bikeName = props.navigation.getParam("bikeName");

    this.otherConclusion = props.navigation.getParam("otherConclusion");
    this.errorListStr = props.navigation.getParam("errorListStr");
    this.notGoodListStr = props.navigation.getParam("notGoodListStr");
    this.performanceStr = props.navigation.getParam("performanceStr");
  }

  render() {
    return (
      <BaseScreen>
        <NavBar title="Thông tin khách hàng" />
        {this.renderBody()}
      </BaseScreen>
    );
  }

  renderBody() {
    return (
      <View style={{ flex: 1, paddingHorizontal: 19, paddingTop: 10 }}>
        <KeyboardAwareScrollView keyboardDismissMode="interactive">
          {/* Tên khách */}
          <Text style={{ ...SharedStyles.header1, marginTop: 10 }}>
            Tên khách hàng
          </Text>

          <TextInput
            onSubmitEditing={this.onSubmitEditingName.bind(this)}
            autoCorrect={false}
            hitSlop={hitSlop}
            style={SharedStyles.inputText}
            underlineColorAndroid="transparent"
            returnKeyType="next"
            autoCapitalize="words"
            onChangeText={(text: string) => {
              this.onChangeText.bind(this)("name", text);
            }}
          />
          {/* SDT */}
          <Text style={{ ...SharedStyles.header1, marginTop: 10 }}>
            Số điện thoại
          </Text>

          <TextInput
            ref={_ref => {
              this.inputRef_phone = _ref;
            }}
            maxLength={10}
            onSubmitEditing={this.onSubmitEditingPhone.bind(this)}
            autoCorrect={false}
            hitSlop={hitSlop}
            style={SharedStyles.inputText}
            underlineColorAndroid="transparent"
            returnKeyType="next"
            keyboardType="phone-pad"
            onChangeText={(text: string) => {
              this.onChangeText.bind(this)("phone", text);
            }}
          />
          {/* Biển số */}
          <Text style={{ ...SharedStyles.header1, marginTop: 30 }}>
            Biển số xe
          </Text>

          <TextInput
            ref={_ref => {
              this.inputRef_licensePlate = _ref;
            }}
            autoCorrect={false}
            hitSlop={hitSlop}
            style={SharedStyles.inputText}
            underlineColorAndroid="transparent"
            returnKeyType="done"
            autoCapitalize="characters"
            onChangeText={(text: string) => {
              this.onChangeText.bind(this)("licensePlate", text);
            }}
          />
          <Text style={{ color: colors.third_dark, marginTop: 10 }}>
            Thông tin khách hàng sẽ được hiển thị trong báo cáo
          </Text>
        </KeyboardAwareScrollView>
        {/* Next Button */}
        <TouchableOpacity
          hitSlop={hitSlop}
          style={{
            alignSelf: "center",
            minHeight: 45,
            justifyContent: "center",
            alignItems: "center"
          }}
          onPress={this.onPressNext.bind(this)}
        >
          <Text style={{ fontSize: 16 }}>Tiếp tục</Text>
        </TouchableOpacity>
      </View>
    );
  }

  onChangeText(field: "name" | "licensePlate" | "phone", text: string) {
    switch (field) {
      case "name":
        this.name = text;
        break;
      case "licensePlate":
        this.licensePlate = text;
        break;
      case "phone":
        this.customerPhone = text;
        break;
      default:
        break;
    }
  }

  onSubmitEditingName() {
    this.inputRef_phone.focus();
  }

  onSubmitEditingPhone() {
    this.inputRef_licensePlate.focus();
  }

  onPressNext() {
    Keyboard.dismiss();
    const params = {
      // Bike problem
      otherConclusion: this.otherConclusion,
      errorListStr: this.errorListStr,
      notGoodListStr: this.notGoodListStr,
      performanceStr: this.performanceStr,
      // User Info
      customername: this.name.trim(),
      licensePlate: this.licensePlate.trim(),
      bikeName: this.bikeName,
      customerPhone: this.customerPhone.trim()
    };
    navigationService.navigate("Report_final", params);
  }
}

export default InputCustomerInfo;
