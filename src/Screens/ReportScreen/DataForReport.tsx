/**
 * @flow
 */

class DataForReport {
  autoCheckOne = null;
  performance = null;
  /**
   * Set giá trị đánh giá cảm biến
   * @param {*} data
   */
  setAutoCheckOne(data: any) {
    this.autoCheckOne = data;
  }
  /**
   * Set gía trị hiệu suất
   * @param {*} data
   */
  setPerformance(data: any) {
    this.performance = data;
  }
  /**
   * Reset hết các kết quả
   */
  resetAllResults() {
    this.autoCheckOne = null;
    this.performance = null;
  }
}

class Instance {
  static instance: DataForReport;
  static getInstance() {
    if (!Instance.instance) {
      Instance.instance = new DataForReport();
    }
    return Instance.instance;
  }
}
export default Instance.getInstance();
