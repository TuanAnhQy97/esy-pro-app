# Sử dụng Module
- import ExportPDF from "ExportPDF/src" => Sử dụng hàm ```ExportPDF.openShare(data, fileName, custom?)```
- Example: cd Example -> npm i -> react-native run-android
---
# Data
#1. Có dạng: 
```
  type Item = {
    name: string | null,
    content: string | Array <Item>
  }
```
  - Nếu `(content = Array)`
  => đây là danh sách, name là tiêu đề
  - Ngược lại thì là item trong danh sách:
      - nếu có name, thì có dạng `- name: content`
      - nếu không có name: `- content `
  - Nếu `content = null`, có dạng: *`Chưa có đánh giá `*
  - Nếu content là link: có dạng `link:nội dung`
      - ví dụ: `link#tel:0327710926`
      - ví dụ: `link#https://savy.com.vn`

#2. Ví dụ
```
{
    name: "Thông tin",
    content: [
      {
        name: "Cửa hàng",
        content: [
          {
            name: "Cửa hàng",
            content: "Fangia Savy",
          },
          {
            name: "Địa chỉ",
            content: "189 Đê La Thành, Đống Đa, Hà Nội",
          },
          {
            name: "Hotline",
            content: "link#tel:0327710926",
          },
        ],
      },
      {
        name: "Khách hàng",
        content: [
          {
            name: "Tên khách hàng",
            content: "Nguyễn Anh Đức",
          },
          {
            name: "Loại xe",
            content: "Airblade 2013",
          },
          {
            name: "Biển số",
            content: "29-F1-12345",
          },
        ],
      },
    ],
  }
```