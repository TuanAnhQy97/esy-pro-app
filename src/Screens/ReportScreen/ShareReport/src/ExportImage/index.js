import React, { PureComponent } from "react";
import { View, Text, ScrollView, TouchableOpacity } from "react-native";
import ViewShot, { captureRef } from "react-native-view-shot";
import { horizontalScale } from "utilities/Tranform";
import style from "./style";

type Props = {
  navigation: Object,
};

export default class Index extends PureComponent<Props> {
  constructor(props: Props) {
    super(props);
    this.state = {};
  }
  capture = () => {
    captureRef(this.scrollView, {
      format: "jpg",
      result: "data-uri",
      snapshotContentContainer: true,
    })
      .then((uri) => {
        this.props.navigation.navigate("ShowImage", { uri });
      })
      .catch((error) => {
        console.log("capture error: ", error);
      });
    // this.viewShot
    //   .capture()
    //   .then((uri) => {
    //     releaseCapture(uri);
    //     console.log("uri: ", uri);
    //     this.props.navigation.navigate("ShowImage", { uri });
    //   })
    //   .catch((error) => {
    //     console.log("capture error: ", error);
    //   });
  };
  render() {
    return (
      <ViewShot
        ref={(node) => {
          this.viewShot = node;
        }}
        style={{ flex: 1 }}
      >
        <ScrollView
          ref={(node) => {
            this.scrollView = node;
          }}
          contentContainerStyle={{ width: horizontalScale(360), height: 1500 }}
        >
          <View style={style.container}>
            <Text>Export image</Text>
          </View>
          <View style={[style.container, { backgroundColor: "yellow" }]}>
            <Text>Export image</Text>
          </View>
          <View style={[style.container, { backgroundColor: "blue" }]}>
            <Text>Export image</Text>
          </View>
          <View style={[style.container, { backgroundColor: "green" }]}>
            <Text>Export image</Text>
          </View>
        </ScrollView>
        <View
          style={{
            position: "absolute",
            bottom: 10,
            width: "100%",
            height: 46,
            alignItems: "center",
          }}
        >
          <TouchableOpacity
            style={{
              width: 150,
              height: "100%",
              backgroundColor: "orange",
              alignItems: "center",
              justifyContent: "center",
            }}
            onPress={() => this.capture()}
          >
            <Text style={{ fontWeight: "bold" }}>Capture</Text>
          </TouchableOpacity>
        </View>
      </ViewShot>
    );
  }
}
