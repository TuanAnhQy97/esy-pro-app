import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    height: 500,
    width: "100%",
    backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center",
  },
});
