import {
  Platform,
  PermissionsAndroid,
  ToastAndroid,
  Alert,
} from "react-native";
import Share from "react-native-share";
import RNHTMLToPDF from "react-native-html-to-pdf";
import convertDataToHTML from "./html";

export type Item = {
  name?: string,
  content: Array<Item> | string,
};

type Data = Item[];

const checkPermission = (callback = () => {}) => {
  if (Platform.OS === "android" && Platform.Version >= 23) {
    PermissionsAndroid.check("android.permission.READ_EXTERNAL_STORAGE").then(
      (result) => {
        if (result) {
          callback();
        } else {
          PermissionsAndroid.requestMultiple([
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE",
          ]).then(
            (result1) => {
              switch (result1["android.permission.WRITE_EXTERNAL_STORAGE"]) {
                case PermissionsAndroid.RESULTS.GRANTED:
                  callback();
                  break;

                case PermissionsAndroid.RESULTS.DENIED:
                  checkPermission(callback);
                  break;

                case PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN:
                  Alert.alert(
                    "Chia sẻ báo cáo không thành công",
                    'Vui cấp quyền truy cập để có ứng dụng có thể hoạt động. Vào "Cài đặt" của điện thoại để thực hiện', // eslint-disable-line
                    [{ text: "Đã hiểu" }],
                  );
                  break;

                default:
                  break;
              }
            },
            (err) => {
              ToastAndroid.show(
                "Chia sẻ báo cáo không thành công",
                ToastAndroid.SHORT,
              );
              console.log("error: ", err);
            },
          );
        }
      },
      (err) => {
        ToastAndroid.show(
          "Chia sẻ báo cáo không thành công",
          ToastAndroid.SHORT,
        );
        console.log("error1: ", err);
      },
    );
  } else {
    callback();
  }
};
class Instance {
  fileName = "";
  generatePDF = (
    data: Object,
    custom,
    callback = (base64: string) => {}, // eslint-disable-line
  ) => {
    checkPermission(async () => {
      const str = convertDataToHTML(data);
      console.log("html: ", str);
      const options = {
        fileName: this.fileName,
        html: str,
        directory: "docs",
        base64: true,
        ...custom,
      };
      const tmp = await RNHTMLToPDF.convert(options);
      let { filePath, base64 } = tmp;
      if (Platform.OS === "android") {
        filePath = `file://${filePath}`;
      }
      this.path = filePath;
      console.log("Done create PDF file");
      callback(filePath);
    });
  };

  /**
   * gọi hàm này để share data
   * @param {Data} data dữ liệu xuất báo cáo
   * @param {string} fileName default "Savy"
   * @param {Object} custom iOS only: custom page pdf
   */
  openShare = (
    data: Data,
    fileName: string = "ESY Pro - Bao cao kiem tra",
    custom = { width: 612, height: 792, padding: 10 },
  ) => {
    this.fileName = fileName;
    this.generatePDF(data, custom, this.share);
  };
  share = (base64: string) => {
    console.log("START SHARE");
    Share.open({
      message: this.fileName,
      url: this.path,
      type: "application/pdf",
      showAppsToView: true,
      title: "ESY Pro - Báo cáo kiểm tra",
      subject: "Chia sẻ tới khách hàng",
    }).catch((error) => {
      console.log("SHARE ERROR", error);
    });
  };
}
class ShareSavy {
  instance: Instance;
  static getInstance(): Instance {
    if (!this.instance) {
      this.instance = new Instance();
    }
    return this.instance;
  }
}

export default ShareSavy.getInstance();
