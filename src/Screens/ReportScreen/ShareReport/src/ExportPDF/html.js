import { Item } from "./index";
import { template1 } from "../../Example/template";

// #body-review ul > li::before {
//   content: "- ";
//   padding-right: 3px;
// }
const style = `
<style>
      .color {
        color: #184423;
      }

      h4 {
        font-size: 33px;
      }

      .title0 {
        font-weight: bold;
        color: #184423;
        font-size: 36px;
        margin-top: 10px;
        padding-bottom: 2px;
        
        border-bottom: 2px solid gray;
      }
      p,
      h1,
      h2,
      ol,ul, 
      h3,
      span,
      h4 {
        padding: 0 auto;
        margin: 0 auto;
      }
      .title1 {
        font-weight: bold;
        padding: 0;
        margin:0;
        margin-top:5px;
      }
      ul  {
        padding-left: 20px;
      }
       li {
        padding: 0;
        margin: 0 ;
        list-style-type: none
      }
      .title2 {
        /* list-style-type: lower-alpha; */
        padding-left: 10px;
        font-weight: normal;
      }
      #body-review {
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        font-size: 30px;
        padding-left: 0;
        
      }

      #body-review ul {
        font-weight: normal;
        /* margin-top: 10px; */
      }
      #body-review ul>li {
        padding-top: 5px;
        padding-bottom: 2px;
      }
      #time-header {
        width: 90%;
        flex-direction: row;
        display: flex;
        justify-content: space-between;
        font-size: 30px;
      }
      #title-header {
        color: #184423;
        font-size: 33px;
        font-weight: bold;
        margin-top: 15px;
        padding-bottom: 20px;
        text-align: center;
      }
      header {
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: center;
        color: #184423;
      }
      @page{
margin-left: 0px;
margin-right: 0px;
margin-top: 5px;
margin-bottom: 2px;
padding:0px;
}
    </style>
`;

const getHeader = () => {
  const date = new Date();
  const dayOfMonth = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  const time = date.toLocaleTimeString();
  return `
    <header>
      <div id="time-header">
        <p>Ngày ${dayOfMonth} tháng ${month} năm ${year}</p>
        <p>${time}</p>
      </div>
      <p id="title-header">BÁO CÁO KHÁCH HÀNG</p>
    </header>
  `;
};
const renderItem = (item: Item, index = 0, idxOl = []) => {
  const { name } = item;
  let { content } = item;
  if (!content) content = "";
  if (typeof content === "string") {
    const check = content.search("link#");
    if (check === 0) {
      content = content.substr(5, content.length - 5);
      const regex = /[^a-zA-Z0-9][a-zA-Z0-9]/;
      const idxContent = content.search(regex);
      const content1 = content.substr(idxContent + 1, content.length - idxContent - 1);
      content = `<a href="${content}">${content1}</a>`;
    }
    if (name && name !== "") {
      /* eslint-disable */
      return `
      <li>- ${name}: ${
        content !== ""
          ? content
          : `<span style="color: grey; opacity: 0.7">(Chưa có thông tin)</span>`
      }</li>
      `;
    }
    return `
    <li>- ${content}</li>
    `;
  }

  /* eslint-enable */
  // content la Item[]
  let tmp = "";
  if (index === 0) tmp = `class="title${index}"`;
  console.log("join: ", idxOl.join("."));
  /* eslint-disable */
  let str = `
    <li>
      <span ${tmp}>${
    idxOl.length > 0 ? `${idxOl.join(".")}. ` : ""
  }${name}</span>
  `;

  const { length } = content;
  if (length > 0) {
    const subItem = content[0];
    if (!subItem.content || typeof subItem.content === "string") {
      str += `
      <ul>
      `;
      content.forEach((value) => {
        str += renderItem(value, index);
      });
      str += `</ul>
    `;
    } else {
      ++index;
      str += `
      <ol class="title${index}">
      `;
      content.forEach((value, idx) => {
        str += renderItem(value, index, [...idxOl, idx + 1]);
      });
      str += `
    </ol>
    `;
    }
  } else {
    str += `
    <ul>
      <li style="font-style: italic">
      Chưa có đánh giá
      </li>
    </ul>`;
  }
  str += "</li>";
  return str;
};

const getBody = (data: Item[]) => {
  const item: Item = data.shift();
  let str = "";
  str += renderItem(item, 0);
  if (data.length === 0) return str;
  return str + getBody(data);
};

const getHtml = (data: Item[]) => {
  if (!data) return template1;
  let str = `
    <body>
  `;
  str += style;
  str += getHeader();
  str += `<ul id="body-review">
  `; // eslint-disable-line
  str += getBody(data);
  str += `
  </ul>
  </body>
  `;
  return str;
};

export default getHtml;
/*
const style = `
<style>
.color {
  color: #184423;
}

h4 {
  font-size: 33px;
}

.title0 {
  font-weight: bold;
  color: #184423;
  font-size: 36px;
  margin-top: 10px;
  border-bottom: 2px solid gray;
}
p,
h1,
h2,
h3,
span,
h4 {
  padding: 0 auto;
  margin: 0 auto;
}
.title1 {
  font-weight: bold;
  padding: 0 15px;
  margin-top: 10px;
}
ol {
  list-style-type: none;
}
.title2 {
  padding-left: 15px;
  font-weight: normal;
}
#body-review {
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  font-size: 30px;
  padding-left: 0
}
#body-review ul {
  padding-left: 0;
  font-weight: normal;
  margin: 10px auto;
}
#body-review > li {
  list-style-type: none;
}
#body-review ul > li {
  list-style-type: none;
  padding: 3px 0;
}

#time-header {
  width: 90%;
  flex-direction: row;
  display: flex;
  justify-content: space-between;
  font-size: 30px;
}
#title-header {
  color: #184423;
  font-size: 33px;
  font-weight: bold;
  margin-top: 30px;
  text-align: center;
}
header {
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  color: #184423;
}
@page{
  margin-left: 5px;
  margin-right: 0px;
  margin-top: 5px;
  margin-bottom: 0px;
  padding:0px;
  }
</style>`;
*/
