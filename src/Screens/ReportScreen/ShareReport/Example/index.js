import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Share from "react-native-share";
import ShareSavy from "../src";
import data from "./data";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
  },
  instructions: {
    marginTop: 20,
    marginBottom: 20,
  },
});

export default class TestShare extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    // ShareSavy.getPath(data, "savytest2", (path) => {
    //   this.path = path;
    // });
    ShareSavy.openShare(data, "ShareSavy");
  }

  share = () => {
    const shareOptions = {
      title: "Savy Share",
      message: "Savy",
      url: this.path,
      subject: "Share Link", //  for email
    };
    Share.open(shareOptions);
  };
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.share}>
          <View style={styles.instructions}>
            <Text>Simple Share</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
