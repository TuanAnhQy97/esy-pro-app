/* eslint-disable */
type Data1 = [
  {
    name: string, // tiêu đề 1: Thông tin
    content: [
      // Các mục nhỏ (tiêu đề 2): Cửa hàng, Khách hàng
      {
        name: string, // Nếu content là Array thì name là tiêu đề, ngược lại: content là string, name có thể null
        // nếu content = string, name khác null thì có dạng (name: content)
        content: [
          {
            name?: string, // Cửa hàng
            content: string, // Fangia Savy
          },
        ],
      },
    ],
  },
];
/* eslint-enable */

const data = [
  {
    name: "Thông tin",
    content: [
      {
        name: "Cửa hàng",
        content: [
          {
            name: "Cửa hàng",
            content: "Fangia Savy",
          },
          {
            name: "Địa chỉ",
            content: "189 Đê La Thành, Đống Đa, Hà Nội",
          },
          {
            name: "Hotline",
            content: "link#tel:0327710926",
          },
        ],
      },
      {
        name: "Khách hàng",
        content: [
          {
            name: "Tên khách hàng",
            content: "Nguyễn Anh Đức",
          },
          {
            name: "Loại xe",
            content: "Airblade 2013",
          },
          {
            name: "Biển số",
            content: "29-F1-12345",
          },
        ],
      },
    ],
  },
  {
    name: "Mục cần kiểm tra sửa chữa",
    content: [
      {
        name: "Đánh giá cảm biến",
        content: [
          {
            name: "Lỗi FI từng xảy ra",
            content: [
              {
                content: "7-2 Cảm biến nhiệt động cơ",
              },
              {
                content: "12-1 Kim phun",
              },
            ],
          },
          {
            name: "Sai lệch cảm biến khác",
            content: [
              {
                content: "Cảm biến góc tay ga hoạt động sai lệch",
              },
              {
                content: "Nguồn điện yếu",
              },
            ],
          },
        ],
      },
      {
        name: "Hiệu suất động cơ",
        content: [
          {
            content: "Lỗi không đủ nguồn cấp cho ECU",
          },
          {
            content: "Lỗi hệ thống phun xăng không đạt tiêu chuẩn",
          },
          {
            content: "Lỗi vòng tua máy cao",
          },
        ],
      },
    ],
  },
  {
    name: "Mục kiểm tra sửa chữa nâng cao",
    content: [
      {
        content: "Má phanh mòn, thiếu nước làm mát",
      },
    ],
  },
];

export default data;
