const template = ` <body>
<style>
  .color {
    color: #184423;
  }

  h4 {
    font-size: 33px;
  }
  .title {
    font-weight: bold;
  }
  .title1 {
    color: #184423;
    font-size: 36px;
    margin-top: 10px;
    border-bottom: 2px solid gray;
  }
  p,
  h1,
  h2,
  h3,
  span,
  h4 {
    padding: 0 auto;
    margin: 0 auto;
  }
  .title2 {
    padding: 0 30px;
    margin-top: 10px;
  }
  .title3 {
    list-style-type: lower-alpha;
    padding-left: 10px;
    font-weight: normal;
  }
  #body-review {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    font-size: 30px;
  }
  #body-review ul {
    padding-left: 0;
    font-weight: normal;
    margin: 10px auto;
  }
  #body-review ul > li {
    list-style-type: none;
    padding: 3px 0;
  }
  #body-review ul > li::before {
    content: "- ";
    padding-right: 3px;
  }
  #time-header {
    width: 90%;
    flex-direction: row;
    display: flex;
    justify-content: space-between;
  }
  #title-header {
    color: #184423;
    font-size: 33px;
    font-weight: bold;
    margin-top: 10px;
    text-align: center;
  }
  header {
    width: 100%;
    height: 70px;
    display: flex;
    flex-direction: column;
    align-items: center;
    color: #184423;
  }
</style>
<header>
  <div id="time-header">
    <p>Ngày 5 tháng 12 năm 2018</p>
    <p>18 : 05</p>
  </div>
  <p id="title-header">BÁO CÁO KHÁCH HÀNG</p>
</header>
<div id="body-review">
  <div>
    <span class="title1 title"> Thông tin </span>
    <ol class="title2 title">
      <li>
        <span> Cửa hàng </span>
        <ul>
          <li>Cửa hàng: Fangia Savy</li>
          <li>Địa chỉ: 189 Đê La Thành, Đống Đa, Hà Nội</li>
        </ul>
      </li>
      <li>
        <span>Khách hàng</span>
        <ul>
          <li>Tên khách hàng: Nguyễn Anh Đức</li>
          <li>Loại xe: Airblade 2013</li>
          <li>Biển số: 29-F1-12345</li>
        </ul>
      </li>
    </ol>
  </div>
  <div>
    <span class="title1 title"> Mục cần kiểm tra sửa chữa </span>
    <ol class="title2 title">
      <li>
        <span>Đánh giá cảm biến</span>
        <ol class="title3 ">
          <li>
            <span> Lỗi FI từng xảy ra </span>
            <ul>
              <li>7-2 Cảm biến nhiệt động cơ</li>
              <li>12-1 Kim phun</li>
            </ul>
          </li>
          <li>
            <span> Sai lệch cảm biến khác </span>
            <ul>
              <li>Cảm biến góc tay ga hoạt động sai lệch</li>

              <li>Nguồn điện yếu</li>
            </ul>
          </li>
        </ol>
      </li>
      <li>
        <span>Hiệu suất động cơ</span>
        <ul>
          <li>Lỗi không đủ nguồn cấp cho ecu</li>
          <li>Lỗi hệ thống phun xăng không đạt tiêu chuẩn</li>
          <li>Lỗi vòng tua máy cao</li>
          <li>Lỗi tín hiệu kiểm soát khí thải ko đạt tiêu chuản</li>
        </ul>
      </li>
    </ol>
  </div>
  <div>
    <span class="title1 title"> Mục cần kiểm tra sửa chữa nâng cao </span>
    <ul>
      <li>Má phanh mòn, thiếu nước làm mát</li>
    </ul>
  </div>
</div>
</body> 
`;

export default template;
