// leave off @2x/@3x
/* eslint-disable */
const icons = {
  backWhite: require("../../assets/icons/backWhite.png"),
  backBlack: require("../../assets/icons/back.png"),
  success: require("../../assets/icons/success.png"),
  background: require("../../assets/icons/background.png"),
};

export default icons;
// leave off @2x/@3x
