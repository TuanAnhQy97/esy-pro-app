/**
 * @flow
 */
import React from "react";
import { TouchableOpacity, View, TextInput, StyleSheet } from "react-native";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import BaseScreen from "../../Components/BaseScreen";
import NavBar from "../../Components/NavBar";
import Text from "../../Components/Text";
import PerformanceResult from "./Components/PerformanceResult";
import AutoCheckOneResult from "./Components/AutoCheckOneResult";
import SharedStyles from "./Components/SharedStyles";
import MyActivityIndicator from "../../Components/MyActivityIndicator";
import ExplainError from "../../Library/ExplainError";
import DiagnosticHelper from "../../DiagnosticHelper/DiagnosticHelper";
import { analyzePerformance } from "./Helpers/AnalyzePerformance";

import DataForReport from "./DataForReport";
import navigationService from "../../services/navigationService";
import bikeLibHelper from "../../services/bikeLibHelper";

const DATA_STR = [
  ["Lỗi vòng tua máy thấp", "Lỗi vòng tua máy cao"],
  [
    "Nhiệt độ động cơ báo thông số sai tiêu chuẩn",
    "Nhiệt độ động cơ báo thông số sai tiêu chuẩn"
  ],
  [
    "Lỗi hệ thống phun xăng không đạt chuẩn",
    "Lỗi hệ thống phun xăng không đạt chuẩn"
  ],
  ["Lỗi không đủ nguồn cấp cho ECU", "Lỗi nguồn cấp cho ECU quá cao"],
  ["Sai góc đánh lửa tiêu chuẩn", "Sai góc đánh lửa tiêu chuẩn"],
  ["Cảm biến góc bướm ga bị lỗi", "Cảm biến góc bướm ga bị lỗi"],
  [
    "Áp suất khí nạp không đạt tiêu chuẩn",
    "Áp suất khí nạp không đạt tiêu chuẩn"
  ],
  [
    "Nhiệt độ khí nạp không đạt tiêu chuẩn",
    "Nhiệt độ khí nạp không đạt tiêu chuẩn"
  ],
  [
    "Lỗi tín hiệu kiểm soát khí thải không đạt tiêu chuẩn",
    "Lỗi tín hiệu kiểm soát khí thải không đạt tiêu chuẩn"
  ]
];

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

type Props = {};
type States = { isLoading: boolean };

class MakeReportTab extends React.PureComponent<Props, States> {
  bikeName: string;

  errorList = null;

  dataList = null;

  performanceData = null;

  otherConclusion: string = "";

  errorListStr: null | string[][] = null;

  notGoodListStr: null | string[] = null;

  performanceStr: null | string[] = null;

  constructor(props: any) {
    super(props);
    const { autoCheckOne } = DataForReport;
    if (autoCheckOne) {
      this.errorList = autoCheckOne.errorList;
      this.dataList = autoCheckOne.dataList;
    }
    this.performanceData = DataForReport.performance;

    this.otherConclusion = "";
    this.bikeName = props.navigation.getParam("bikeName");
    this.state = { isLoading: true };
  }

  componentDidMount() {
    // Xử lý mã lỗi
    if (this.errorList) {
      this.errorListStr = this.errorList.map(value => {
        const explain = ExplainError.getExplain(value[0], value[1]);
        const errorName = explain.name;
        const { effect } = explain;
        return [`${value[0]}-${value[1]} ${errorName}`, effect];
      });
    }
    // Xử lý các sai lệch cảm biến
    if (this.dataList) {
      const notGoodthings = DiagnosticHelper.analyzeAutoCheckOneData(
        this.dataList
      );
      this.notGoodListStr = notGoodthings.map(value => {
        const explain = ExplainError.getExplain(value, 0);
        return explain.name;
      });
    }
    // Xử lý hiệu suất
    if (this.performanceData) {
      const stQd = bikeLibHelper.honda._hondaLib.standardQD;
      const analyzeResult = analyzePerformance(this.performanceData, stQd);

      this.performanceStr = [];
      analyzeResult.forEach((value, index) => {
        if (value !== null) {
          if (value !== 0) {
            const pos = value < 0 ? 0 : 1;
            this.performanceStr.push(DATA_STR[index][pos]);
          }
        }
      });
    }
    this.setState({ isLoading: false });
  }

  render() {
    return (
      <BaseScreen>
        <NavBar title="Mục sửa chữa" />
        {this.renderBody()}
      </BaseScreen>
    );
  }

  renderBody() {
    if (this.state.isLoading) {
      return <MyActivityIndicator />;
    }
    return (
      <View style={{ flex: 1, paddingHorizontal: 19, paddingTop: 10 }}>
        <KeyboardAwareScrollView keyboardDismissMode="interactive">
          {/* AutoCheckOne */}
          <AutoCheckOneResult
            errorList={this.errorListStr}
            notGoodList={this.notGoodListStr}
          />
          {/* AutoCheckTwo */}
          <PerformanceResult data={this.performanceStr} />
          {/* Các kết luận khác */}
          <Text style={{ ...SharedStyles.header1, marginTop: 30 }}>
            Mục hư hỏng khác của xe
          </Text>

          <TextInput
            hitSlop={hitSlop}
            style={SharedStyles.inputText}
            placeholder="Điền thêm các vấn đề khác của xe"
            underlineColorAndroid="transparent"
            returnKeyType="done"
            autoCapitalize="sentences"
            onChangeText={(text: string) => {
              this.onChangeText.bind(this)(text);
            }}
          />
        </KeyboardAwareScrollView>
        {/* Next Button */}
        <TouchableOpacity
          hitSlop={hitSlop}
          style={{
            alignSelf: "center",
            minHeight: 45,
            justifyContent: "center",
            alignItems: "center"
          }}
          onPress={this.onPressNext.bind(this)}
        >
          <Text style={{ fontSize: 16 }}>Tiếp tục</Text>
        </TouchableOpacity>
      </View>
    );
  }

  onChangeText(text: string) {
    this.otherConclusion = text;
  }

  onPressNext() {
    const params = {
      otherConclusion: this.otherConclusion.trim(),
      errorListStr: this.errorListStr,
      notGoodListStr: this.notGoodListStr,
      performanceStr: this.performanceStr,

      bikeName: this.bikeName
    };
    navigationService.navigate("Report_fillUserInfo", params);
  }
}

export default MakeReportTab;
