/**
 * @flow
 */
import React from "react";
import { View } from "react-native";
import Text from "../../../Components/Text";
import ExplainError from "../../../Library/ExplainError";
import SharedStyles from "./SharedStyles";
import { horizontalScale } from "../../../ScaleUtility";
import DiagnosticHelper, {
  DATA1234
} from "../../../DiagnosticHelper/DiagnosticHelper";

type Props = {
  data: string[];
};
type States = {};

class Component extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <View>
        <Text style={{ ...SharedStyles.header2, marginTop: 10 }}>
          Sai lệch cảm biến khác
        </Text>
        {this.renderNotGoodIndex()}
      </View>
    );
  }

  renderNotGoodIndex() {
    // Đã phân tích xong
    if (this.props.data.length === 0) {
      // Không có sai lệch gì cả
      return (
        <View style={[SharedStyles.contentWrapper]}>
          <View style={{ flexDirection: "row" }}>
            <View style={SharedStyles.cirlceNotError} />
            <Text style={SharedStyles.contentTextNoError}>
              Không phát hiện dấu hiệu bất thường khác
            </Text>
          </View>
        </View>
      );
    }
    // Có sai lệch
    return this.props.data.map((value, index) => {
      return (
        <View key={index.toString()} style={[SharedStyles.contentWrapper]}>
          <View style={{ flexDirection: "row" }}>
            <View style={SharedStyles.circleError} />
            <View style={{ paddingRight: 17, paddingLeft: 14 }}>
              <Text
                style={{
                  fontSize: horizontalScale(18),
                  marginVertical: 9
                }}
              >
                {value}
              </Text>
            </View>
          </View>
        </View>
      );
    });
  }
}

export default Component;
