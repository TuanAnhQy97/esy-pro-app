/**
 * @flow
 */
import React from "react";
import { View } from "react-native";
import Text from "../../../Components/Text";
import SharedStyles from "./SharedStyles";

type Props = {
  data: string[][]
};
type States = {};

class Component extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
  }

  render() {
    return (
      <View>
        <Text style={{ ...SharedStyles.header2, marginTop: 10 }}>
          Lỗi FI từng xảy ra
        </Text>
        {this.renderErrorList()}
      </View>
    );
  }

  renderErrorList() {
    // trường hợp không có lỗi
    if (this.props.data.length === 0) {
      return (
        <View style={[SharedStyles.contentWrapper]}>
          <View style={{ flexDirection: "row" }}>
            <View style={SharedStyles.cirlceNotError} />
            <Text style={SharedStyles.contentTextNoError}>
              Không có bản ghi nào
            </Text>
          </View>
        </View>
      );
    } else {
      // Có lỗi
      return this.props.data.map((error, index) => {
        return (
          <View key={index.toString()} style={[SharedStyles.contentWrapper]}>
            <View style={{ flexDirection: "row" }}>
              <View style={SharedStyles.circleError} />
              <View style={{ paddingRight: 17, paddingLeft: 14 }}>
                <Text
                  style={{
                    fontSize: 18,
                    marginTop: 9,
                    paddingBottom: 6.5
                  }}
                >
                  {error[0]}
                </Text>
                {/* <View
                  style={{
                    height: 1,
                    backgroundColor: "#70707055"
                  }}
                /> 
                 <Text
                  style={{
                    fontSize: 18,
                    marginTop: 6.5,
                    marginBottom: 8
                  }}
                >
                  {error[1]}
                </Text> */}
              </View>
            </View>
          </View>
        );
      });
    }
  }
}

export default Component;
