/**
 * @flow
 */
import React from "react";
import { View } from "react-native";
import Text from "../../../Components/Text";
import SharedStyles from "./SharedStyles";
import { analyzePerformance } from "../Helpers/AnalyzePerformance";
import { horizontalScale } from "../../../ScaleUtility";

type Props = {
  data: string[] | null
};
type States = {};

class Component extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <View>
        <Text style={{ ...SharedStyles.header1, marginTop: 20 }}>
          Hiệu suất động cơ
        </Text>
        {this.renderBody()}
        <View style={SharedStyles.breakLine} />
      </View>
    );
  }

  renderBody() {
    const { data } = this.props;
    if (data) {
      // Mọi thứ OKAY
      if (data.length === 0) {
        return <Text>Thông số hiệu suất động cơ bình thường</Text>;
      }
      // Có bất thường
      else {
        return data.map((value, index) => {
          return (
            <View key={index.toString()} style={[SharedStyles.contentWrapper]}>
              <View style={{ flexDirection: "row" }}>
                <View style={SharedStyles.circleError} />
                <View style={{ paddingRight: 17, paddingLeft: 14 }}>
                  <Text
                    style={{
                      fontSize: horizontalScale(18),
                      marginVertical: 9
                    }}
                  >
                    {value}
                  </Text>
                </View>
              </View>
            </View>
          );
        });
      }
    }
    // Chưa thực hiện kiểm tra này
    else {
      return <Text>Chưa có đánh giá</Text>;
    }
  }
}

export default Component;
