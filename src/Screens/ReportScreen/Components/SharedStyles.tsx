/**
 * @flow
 */
import { StyleSheet } from "react-native";
import {} from "../../../ScaleUtility";
import { colors } from "../../../styles/colors";
const styles = StyleSheet.create({
  contentWrapper: {
    // backgroundColor: "#fffffc",
    // borderRadius: 8,
    // marginBottom: 18,
    // marginHorizontal: 13,
    // elevation: 2,
    // shadowOpacity: 0.2,
    // shadowRadius: 3,
    // shadowOffset: { height: 2, width: 2 }
  },
  contentTextNoError: {
    // color: colors.primary_light
    fontSize: 18,
    paddingVertical: 8,
    paddingHorizontal: 17
  },
  cirlceNotError: {
    height: 10,
    width: 10,
    borderRadius: 10,
    backgroundColor: colors.secondary_dark,
    marginTop: 14,
    marginLeft: 5
  },
  circleError: {
    height: 10,
    width: 10,
    borderRadius: 10,
    backgroundColor: "#FFB300",
    marginTop: 14,
    marginLeft: 5
  },
  breakLine: {
    marginTop: 10,
    height: 1,
    width: "100%",
    opacity: 0.5,
    backgroundColor: colors.third_dark
  },
  /**
   * Header
   */
  header1: { fontSize: 20, color: colors.primary_dark, fontWeight: "bold" },
  header2: { fontSize: 18 },

  /**
   * INPUT
   */
  headerInputText: {
    fontSize: 18
    // color: colors.secondary_dark
  },
  inputText: {
    paddingTop: 10,
    fontSize: 18,
    borderBottomColor: "black",
    borderBottomWidth: 1,
    marginBottom: 13,
    minHeight: 45
  }
});

export default styles;
