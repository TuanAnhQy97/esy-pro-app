/**
 * @flow
 */
import React from "react";
import { View } from "react-native";
import Text from "../../../Components/Text";
import ErrorList from "../Components/ErrorList";
import NotGoodList from "../Components/NotGoodList";
import {  DATA1234 } from "../../../DiagnosticHelper/DiagnosticHelper";
import SharedStyles from "./SharedStyles";

type Props = { errorList: string[][] | null, notGoodList: string[] | null };
type States = {};

class Component extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
  }

  render() {
    return (
      <View>
        <Text style={SharedStyles.header1}>Đánh giá cảm biến</Text>
        {this.renderBody()}
        <View style={SharedStyles.breakLine} />
      </View>
    );
  }

  renderBody() {
    if (this.props.errorList === null || this.props.notGoodList === null) {
      return <Text>Chưa có đánh giá</Text>;
    } else {
      return (
        <View>
          <ErrorList data={this.props.errorList} />
          <NotGoodList data={this.props.notGoodList} />
        </View>
      );
    }
  }
}

export default Component;
