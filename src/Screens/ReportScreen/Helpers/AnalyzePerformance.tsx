/**
 * @flow
 */
/**
 * Phân tích dữ liệu hiệu suất
 * @param {*} data data1 + data 2 bỏ vận tốc
 * @param {*} stPer
 */
export function analyzePerformance(data: number[], stPer: number[][]) {
  let levelArr = [];
  const l1 = 2,
    h1 = 3,
    l2 = 4,
    h2 = 5,
    l3 = 6,
    h3 = 7;
  for (let i = 0; i < data.length; i++) {
    let standard_low = stPer[i][0],
      standard_high = stPer[i][1];
    if (stPer[i].length === 0) {
      // Skip chỉ số k có
      levelArr[i] = null;
      continue;
    }
    /**
     * Chuẩn bị các cận cho để đánh giá
     */
    if (i === 4) {
      // Góc đánh lửa
    } else {
      // Các chỉ số khác
      stPer[i][l1] = standard_low;
      stPer[i][h1] = standard_high;
    }
    /**
     * Bắt đầu đánh giá
     */
    let value = data[i];
    if (i === 4) {
      // GDL
      if (stPer[i][h1]) {
        // GDL có khoảng ổn định cho phép
        if (value <= stPer[i][h1] && value >= stPer[i][l1]) {
          levelArr[i] = 0;
        } else {
          levelArr[i] = -1;
        }
      } else {
        // GDL chỉ có 1 giá trị
        if (value === stPer[i][l1]) {
          levelArr[i] = 0;
        } else {
          levelArr[i] = -1;
        }
      }
      continue;
    }
    // TPS
    if (i === 5) {
      if (value >= 60) {
        levelArr[i] = 3;
      } else if (value > 5) {
        levelArr[i] = 2;
      } else if (value >= 1) {
        levelArr[i] = 1;
      } else {
        levelArr[i] = 0;
      }
      continue;
    }
    // Làm tròn thời gian phun
    if (i === 2) {
      value = Math.round(value * 10) / 10;
    }
    if (stPer[i][l3] !== undefined) {
      if (value < stPer[i][l3]) {
        // < l3
        levelArr[i] = -3;
        continue;
      } else if (value >= stPer[i][l3] && value < stPer[i][l2]) {
        // l3 - l2
        levelArr[i] = -2;
        continue;
      }
    }
    if (stPer[i][l2] !== undefined) {
      if (value >= stPer[i][l2] && value < stPer[i][l1]) {
        // l2 - l1
        levelArr[i] = -1;
        continue;
      }
    }
    if (value >= stPer[i][l1] && value <= stPer[i][h1]) {
      // l1 - h1
      levelArr[i] = 0;
      continue;
    }
    if (stPer[i][h2] !== undefined) {
      if (value >= stPer[i][h1] && value < stPer[i][h2]) {
        // h1 - h2
        levelArr[i] = 1;
        continue;
      }
    }
    if (stPer[i][h3] !== undefined) {
      if (value > stPer[i][h3]) {
        // > h3
        levelArr[i] = 3;
        continue;
      } else if (value >= stPer[i][h2] && value < stPer[i][h3]) {
        // h2 - h3
        levelArr[i] = 2;
        continue;
      }
    }
    if (value < stPer[i][l1]) {
      // < l1
      levelArr[i] = -1;
      continue;
    }
    if (value > stPer[i][h1]) {
      // > h1
      levelArr[i] = 1;
      continue;
    }
  }
  return levelArr;
}
