/**
 * @flow
 */
import React from "react";
import { View, StyleSheet } from "react-native";
import Text from "../../Components/Text";
import { colors } from "../../styles/colors";
import SharedStyles from "./Components/SharedStyles";

type Props = {
  name: string,
  bikeName: string,
  licensePlate: string,
  customerPhone: string
};
type States = {};

class Component extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
  }

  render() {
    const { name, bikeName, licensePlate, customerPhone } = this.props;
    return (
      <View>
        <Text style={styles.header}>Thông tin khách hàng</Text>

        <View style={styles.contentWrapper}>
          <Text style={styles.title}>- Tên khách hàng: </Text>
          <Text style={name ? styles.content : styles.noContent}>
            {name ? name : "(Chưa có thông tin)"}
          </Text>
        </View>

        <View style={styles.contentWrapper}>
          <Text style={styles.title}>- Số điện thoại: </Text>
          <Text style={customerPhone ? styles.content : styles.noContent}>
            {customerPhone ? customerPhone : "(Chưa có thông tin)"}
          </Text>
        </View>

        <View style={styles.contentWrapper}>
          <Text style={styles.title}>- Loại xe: </Text>
          <Text style={styles.content}>{bikeName}</Text>
        </View>

        <View style={styles.contentWrapper}>
          <Text style={styles.title}>- Biển số: </Text>
          <Text style={licensePlate ? styles.content : styles.noContent}>
            {licensePlate ? licensePlate : "(Chưa có thông tin)"}
          </Text>
        </View>

        <View
          style={[SharedStyles.breakLine, { marginBottom: 20, marginTop: 10 }]}
        />
      </View>
    );
  }
}

export default Component;

const styles = StyleSheet.create({
  contentWrapper: {
    flexDirection: "row",
    paddingVertical: 3
  },
  header: { fontSize: 20, color: colors.primary_dark, fontWeight: "bold" },
  title: { fontSize: 16 },
  content: { fontSize: 16 },
  noContent: { opacity: 0.5 }
});
