/**
 * @flow
 */

import { createStackNavigator } from "react-navigation";
import MakeReport from "./MakeReport";
import InputCustomerInfo from "./InputCustomerInfo";
import FinalReport from "./FinalReport";

export type SCREEN_NAME =
  | "Report_fillInfo"
  | "Report_fillUserInfo"
  | "Report_final";

const Route = createStackNavigator(
  {
    Report_fillInfo: {
      screen: MakeReport
    },
    Report_fillUserInfo: {
      screen: InputCustomerInfo
    },
    Report_final: {
      screen: FinalReport
    }
  },
  {
    mode: "card",
    headerMode: "none"
  }
);

export default Route;
