/**
 * @flow
 */
import React from "react";
import {
  View,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  ImageBackground,
  Alert
} from "react-native";
import Text from "../Components/Text";
import BaseScreen from "../Components/BaseScreen";
import AppVersion from "../Components/AppVersion";
import NavBar from "../Components/NavBar";

import MyActivityIndicator from "../Components/MyActivityIndicator";
import { horizontalScale } from "../ScaleUtility";
import { colors } from "../styles/colors";
import { AccountFetcher } from "../Fetchers/AccountFetcher";
import { getInternetState } from "../Fetchers/InternetChangeHandler";
import { logout } from "../SharedFunctions";
import ASHelper from "../ASHelper";
import navigationService from "../services/navigationService";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

type Props = {};
type States = {
  error: "no_internet" | "unknown" | "";
  isLoading: boolean;

  phone: string;
  deviceId: number[];
  storeName: string;
  addr: string;
  expiredDate: number;
};

class UserInfo extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
    const deviceId = props.navigation.getParam("deviceId");
    this.state = {
      isLoading: true,
      error: "",
      phone: "",
      deviceId,
      storeName: "",
      addr: "",
      expiredDate: 0
    };
  }

  componentDidMount() {
    this.fetchUserInfo();
  }

  componentWillUnmount() {}

  fetchUserInfo = async () => {
    // if (!getInternetState()) {
    //   this.setState({ isLoading: false, error: "no_internet" });
    // } else {
    //   this.setState({ isLoading: true }, async () => {
    //     let res = await AccountFetcher.getInfo();
    //     if (res.error) {
    //       this.setState({ isLoading: false, error: "unknown" });
    //     } else {
    //       let data = res.data;
    //       let phone = data.phone;
    //       let expiredDate = data.expiredDate;
    //       let storeName = data.storeInfo.name;
    //       let addr = `${data.storeInfo.addr}, ${data.storeInfo.district}, ${
    //         data.storeInfo.city
    //       }`;
    //       this.setState({
    //         isLoading: false,
    //         error: "",
    //         phone,
    //         storeName,
    //         addr,
    //         expiredDate
    //       });
    //     }
    //   });
    // }
    const userInfo = await ASHelper.loadUserInfoFromAS();
    const device = await ASHelper.loadDeviceInfoFromAS();
    if (userInfo) {
      const addr = `${userInfo.storeInfo.addr}, ${
        userInfo.storeInfo.district
      }, ${userInfo.storeInfo.city}`;
      this.setState({
        isLoading: false,
        error: "",
        phone: userInfo.phone,
        expiredDate: userInfo.expiredDate,
        addr,
        storeName: userInfo.storeInfo.name,
        deviceId: device ? device.deviceId : []
      });
    } else {
      this.setState({
        isLoading: false,
        error: "unknown"
      });
    }
  };

  onBack = async () => {
    console.log("UserInfo", "onBack");
    navigationService.goBack();
  };

  handleBackPress = () => {
    console.log("UserInfo", "handleBackPress");
    // works best when the goBack is async
    this.onBack();
    return true;
  };

  renderBody() {
    // Đang load
    if (this.state.isLoading) {
      return <MyActivityIndicator />;
    }
    // Có lỗi
    if (this.state.error) {
      let str;
      switch (this.state.error) {
        case "no_internet":
          str = "Vui lòng kết nối mạng và thử lại";
          break;
        case "unknown":
          str = "Lấy thông tin không thành công\nVui lòng thử lại sau";
          break;

        default:
          str = "";
          break;
      }
      return (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ImageBackground
            style={{
              width: 83,
              height: 192
            }}
            resizeMode="contain"
            source={require("../../assets/Images/Share/internetSignal.png")}
          />
          <Text style={{ fontSize: 16, marginTop: 39, textAlign: "center" }}>
            {str}
          </Text>
          <TouchableOpacity
            hitSlop={hitSlop}
            onPress={() => {
              this.fetchUserInfo();
            }}
            style={{
              paddingVertical: 20
            }}
          >
            <Text
              style={{
                fontSize: 18,
                color: colors.secondary_dark
              }}
            >
              THỬ LẠI
            </Text>
          </TouchableOpacity>
        </View>
      );
    }
    // Lấy dữ liệu thành công

    return (
      <ScrollView style={{ flex: 1 }}>
        {/* Thông tin tài khoản */}
        <Text style={styles.title}>Thông tin tài khoản</Text>

        <View style={styles.areaContainer}>
          <Text style={styles.header}>Số điện thoại</Text>

          <View style={styles.contentContainer}>
            <Text style={styles.content}>
              {phoneToString(this.state.phone)}
            </Text>
          </View>

          <Text style={styles.header}>ID thiết bị</Text>
          <View style={styles.contentContainer}>
            <Text style={styles.content}>
              {deviceIdToString(this.state.deviceId)}
            </Text>
          </View>

          <Text style={styles.header}>Bản quyền hỗ trợ đến</Text>
          <View style={styles.contentContainer}>
            <Text style={styles.content}>
              {dateToString(this.state.expiredDate)}
            </Text>
          </View>
        </View>

        {/* Thông tin cửa hàng */}
        <Text style={styles.title}>Thông tin cửa hàng</Text>

        <View style={styles.areaContainer}>
          <Text style={styles.header}>Tên</Text>

          <View style={styles.contentContainer}>
            <Text style={styles.content}>{this.state.storeName}</Text>
          </View>

          <Text style={styles.header}>Địa chỉ</Text>
          <View style={styles.contentContainer}>
            <Text style={styles.content}>{this.state.addr}</Text>
          </View>
        </View>
      </ScrollView>
    );
  }

  render() {
    return (
      <BaseScreen
        statusbarStyle="light-content"
        handleBackPress={this.handleBackPress}
      >
        {/* NavBar */}
        <NavBar
          onLeftPress={this.handleBackPress}
          title="Thông tin tài khoản"
          rightIconSource={require("../../assets/Images/Share/settings.png")}
          onRightPress={() => {
            navigationService.navigate("Setting");
          }}
          // rightIconSource={require("../../assets/Images/Share/logout.png")}
          // onRightPress={() => {
          //   this.onPressLogout();
          // }}
        />
        {this.renderBody()}
        <AppVersion />
      </BaseScreen>
    );
  }
}

export default UserInfo;

const styles = StyleSheet.create({
  title: {
    width: "100%",
    backgroundColor: colors.secondary_dark,
    fontSize: horizontalScale(20),
    fontWeight: "bold",
    color: colors.primary_light,
    marginTop: 15,
    paddingVertical: 10,
    paddingHorizontal: 18
  },
  header: {
    fontSize: horizontalScale(18),
    marginTop: 15,
    // fontWeight: "bold",
    color: colors.third_dark
  },
  content: {
    fontSize: horizontalScale(18)
    // fontWeight: "bold",
    // color: colors.secondary_dark
  },
  contentContainer: {
    marginTop: 10,
    padding: 10,
    width: "100%",
    backgroundColor: "#fffffe",
    borderRadius: 4,
    elevation: 2,
    shadowColor: colors.third_dark,
    shadowOffset: { height: 2, width: 2 },
    shadowOpacity: 0.35,
    shadowRadius: 3
  },
  areaContainer: {
    paddingHorizontal: 18
  }
});

function deviceIdToString(deviceId: number[]) {
  let str = "";
  for (let i = 0; i < deviceId.length - 1; i += 2) {
    const fb = byteToString(deviceId[i]);
    const sb = byteToString(deviceId[i + 1]);
    str += ` ${fb}${sb}`;
  }
  str = str.toUpperCase();
  return str;
}
function byteToString(b: number) {
  if (b < 16) {
    return `0${b.toString(16)}`;
  }
  return b.toString(16);
}

function dateToString(_date: number) {
  const d = new Date(_date);
  return `${d.getDate()} - ${d.getMonth() + 1} - ${d.getFullYear()}`;
}

function phoneToString(phone: string) {
  return `0${phone.slice(3)}`;
}
