/**
 * @flow
 */
import React from "react";
import { View, ScrollView, TouchableOpacity, StyleSheet } from "react-native";
import Text from "../../Components/Text";
import BaseScreen from "../../Components/BaseScreen";
import NavBar from "../../Components/NavBar";
import { horizontalScale } from "../../ScaleUtility";
import { colors } from "../../styles/colors";
import { checkUpdate } from "./UpdateModule/src/utilities/check";

import BottomSpaceForScrollView from "../../Components/BottomSpaceForScrollView";
import navigationService from "../../services/navigationService";
import bikeLibHelper from "../../services/bikeLibHelper";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

type Props = { navigation: Object };
type States = {
  currentVer: number[];
  nextVer: number[][];
  ecuError: boolean;
};

class CheckUpdateEcu extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
    const ecuError = props.navigation.getParam("ecuError", false);
    const currentVer = props.navigation.getParam("ecuId");
    const nextVerCode = checkUpdate(currentVer).options;
    const nextVerId = nextVerCode.map(value => {
      const next = currentVer.slice(0, 4);
      next.push(value);
      return next;
    });
    this.state = { currentVer, nextVer: nextVerId, ecuError };
  }

  componentDidMount() {}

  render() {
    const { ecuError } = this.state;
    return (
      <BaseScreen
        statusbarStyle="light-content"
        handleBackPress={this.handleBackPress}
      >
        {/* NavBar */}
        <NavBar
          title="Cập nhật ECU"
          onLeftPress={this.handleBackPress}
          leftIconSource={ecuError ? null : undefined}
        />
        {this.renderBody()}
      </BaseScreen>
    );
  }

  renderBody() {
    const { basicInfo } = bikeLibHelper.honda._hondaLib;
    const bikeName = `${basicInfo.name} ${basicInfo.gen}`;
    return (
      <ScrollView style={{ flex: 1, padding: 20 }}>
        {/* Thông tin xe */}
        <Text style={styles.header}>ĐỜI XE </Text>
        <Text style={styles.content}>{bikeName}</Text>
        {/* Phiên bản hiện tại */}
        <Text style={styles.header}>PHIÊN BẢN HIỆN TẠI</Text>
        <Text style={styles.content}>
          {versionToString(this.state.currentVer)}
        </Text>

        {/* Phiên bản tiếp theo */}
        <Text style={styles.header}>PHIÊN BẢN KHẢ DỤNG</Text>
        {this.renderNextVer()}
        {this.renderUpdateButton()}
        <BottomSpaceForScrollView />
      </ScrollView>
    );
  }

  renderNextVer() {
    const { nextVer } = this.state;
    if (nextVer.length) {
      // Có phiên bản mới
      return this.renderNextVerList();
    }
    // Không có phiên bản mới
    return <Text style={styles.content3}>Không có phiên bản mới hơn</Text>;
  }

  renderNextVerList() {
    const { nextVer, currentVer } = this.state;
    return nextVer.map((value, index) => {
      return (
        <View key={index.toString()} style={styles.nextVerContainer}>
          <Text style={styles.content2}>{versionToString(value)}</Text>
          <TouchableOpacity
            hitSlop={hitSlop}
            onPress={() => {
              const params = {
                ecuError: this.state.ecuError,
                currentVer,
                nextVer: value
              };
              navigationService.navigate("UpdateEcu_QuailyUpdate", params);
            }}
            style={styles.updateButContainer}
          >
            <Text style={styles.updateButText}>Cập nhật</Text>
          </TouchableOpacity>
        </View>
      );
    });
  }

  renderUpdateButton() {
    const { nextVer } = this.state;
    if (nextVer.length) {
      // Có phiên bản mới
      return null;
    }
    // Không có phiên bản mới
    return null;
  }

  handleBackPress = () => {
    console.log("CheckUpdateEcu", "handleBackPress");
    // works best when the goBack is async
    this.onBack();
    return true;
  };

  async onBack() {
    /**
     * Nếu ECU lỗi -> resetBleConnection
     */
    if (this.state.ecuError) {
      // Back not allow
    } else {
      navigationService.goBack();
    }
  }
}

function versionToString(ver: number[]) {
  let hexStr = "";
  ver.forEach((value, index) => {
    let hex = value.toString(16);
    if (value < 16) {
      hex = `0${hex}`;
    }
    if (index !== 0) {
      hex = `-${hex}`;
    }
    hexStr += hex.toUpperCase();
  });
  return hexStr;
}

export default CheckUpdateEcu;

const styles = StyleSheet.create({
  header: {
    fontSize: horizontalScale(18),
    fontWeight: "bold",
    marginBottom: 10
  },
  content: {
    fontSize: horizontalScale(20),
    // color: colors.secondary_dark,
    marginBottom: 20
  },
  content2: {
    fontSize: horizontalScale(20),
    color: colors.secondary_dark
  },
  content3: {
    fontSize: horizontalScale(20),
    color: colors.secondary_dark
    // alignSelf: "center"
    // marginTop: 30
  },
  nextVerContainer: {
    borderWidth: 1,
    padding: 10,
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  updateButContainer: {
    alignItems: "center",
    padding: 10,
    backgroundColor: colors.secondary_dark,
    borderRadius: 8
  },
  updateButText: {
    fontSize: horizontalScale(16),
    color: colors.primary_light
  }
});
