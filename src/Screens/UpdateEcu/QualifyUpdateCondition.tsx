// @flow
import React, { Component } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Alert,
  TextInput
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Orientation from "react-native-orientation-locker";
import Text from "../../Components/Text";
import BottomSpaceForScrollView from "../../Components/BottomSpaceForScrollView";
import OverlayConfirmUpdate, {
  tools as OverlayConfirmUpdate_tools
} from "./OverlayConfirmUpdate";

import BaseScreen from "../../Components/BaseScreen";
import NavBar from "../../Components/NavBar";

import { horizontalScale } from "../../ScaleUtility";
import { colors } from "../../styles/colors";
import ASHelper from "../../ASHelper";

import TrackUser from "../../TrackUser";
import navigationService from "../../services/navigationService";
import ble from "../../services/ble";
import bikeLibHelper from "../../services/bikeLibHelper";
import eventBus from "../../services/eventBus";
import { EVENT_BUS } from "../../constants";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

type Props = {
  navigation: any;
};
type States = {
  mill: number;
  battery: number;
  rpm: number;
  ecuError: boolean;
  backPress?: boolean;
  currentVer: number[];
  nextVer: number[];
  licensePlate: string;
};

class QualifyUpdate extends React.PureComponent<Props, States> {
  storename: string;

  inputRef: any;

  constructor(props: Props) {
    super(props);
    const ecuError = props.navigation.getParam("ecuError", false);
    const currentVer = props.navigation.getParam("currentVer");
    const nextVer = props.navigation.getParam("nextVer");
    this.state = {
      currentVer,
      nextVer,
      ecuError,
      licensePlate: "",

      mill: 0,
      battery: 12,
      rpm: 0
    };
  }

  async componentDidMount() {
    console.log("componentDidMount", "QualifyUpdate");
    const userInfo = await ASHelper.loadUserInfoFromAS();
    if (userInfo) {
      this.storename = userInfo.storeInfo.name;
    }
    eventBus.addListener(
      EVENT_BUS.BIKE_IDLE_MODE_DATA_CHANGE,
      this.onBikeValueChange
    );
  }

  componentWillUnmount() {
    eventBus.removeListener(this.onBikeValueChange);
  }

  onBikeValueChange = ({ mill, battery, rpm }) => {
    this.setState({ mill, battery, rpm });
  };

  renderBody = () => {
    const { currentVer, nextVer } = this.state;
    const { basicInfo } = bikeLibHelper.honda._hondaLib;
    const bikeName = `${basicInfo.name} ${basicInfo.gen}`;

    return (
      <KeyboardAwareScrollView
        contentContainerStyle={{ padding: 20 }}
        keyboardDismissMode="interactive"
      >
        <Text style={styles.header}>ĐỜI XE </Text>
        <Text style={styles.content}>{bikeName}</Text>
        {/* Điều kiện cập nhật */}
        <Text style={styles.header}>ĐIỀU KIỆN CẬP NHẬT </Text>
        {this.renderRequirement()}
        {/* Phiên bản thay đổi ntn */}
        <Text style={styles.header}>PHIÊN BẢN HIỆN TẠI</Text>
        <Text style={styles.content}>{versionToString(currentVer)}</Text>
        <Text style={styles.header}>PHIÊN BẢN TIẾP THEO</Text>
        <Text style={styles.content}>{versionToString(nextVer)}</Text>
        {/* Nhập biển số xe */}
        {this.renderInputLicensePlate()}
        {/* Nút xác nhận */}
        {this.renderConfirm()}
        <BottomSpaceForScrollView />
      </KeyboardAwareScrollView>
    );
  };

  renderRequirement = () => {
    if (!this.state.ecuError) {
      const { mill, battery, rpm } = this.state;
      let dtcWarning;
      if (mill) {
        dtcWarning = "Có mã lỗi";
      } else {
        dtcWarning = "";
      }

      let rpmWarning;
      if (rpm) {
        rpmWarning = `${rpm} vòng/phút`;
      } else {
        rpmWarning = "";
      }

      let batteryWarning;
      if (battery < 10) {
        batteryWarning = `${battery} V`;
      } else {
        batteryWarning = "";
      }

      return (
        <View>
          {/* Bộ nhớ lỗi */}
          <View style={styles.nextVerContainer}>
            <Text style={styles.content2}>Bộ nhớ lỗi</Text>
            {this.renderWarning(dtcWarning)}
          </View>

          {/* Vòng tua */}
          <View style={styles.nextVerContainer}>
            <Text style={styles.content2}>Vòng tua máy</Text>
            {this.renderWarning(rpmWarning)}
          </View>

          {/* Ắc quy */}
          <View style={styles.nextVerContainer}>
            <Text style={styles.content2}>Điện áp ắc quy</Text>
            {this.renderWarning(batteryWarning)}
          </View>
        </View>
      );
    }
    return null;
  };

  renderWarning = (warning: string) => {
    if (warning) {
      return <Text style={styles.warningText}>{warning}</Text>;
    }
    const size = 25;
    return (
      <View
        style={{
          height: size,
          width: size,
          backgroundColor: colors.secondary_dark,
          borderRadius: size
        }}
      />
    );
  };

  renderInputLicensePlate() {
    return (
      <View>
        <Text style={styles.headerText}>Biển số xe (tuỳ chọn)</Text>
        <TextInput
          autoCorrect={false}
          hitSlop={hitSlop}
          style={styles.inputText}
          underlineColorAndroid="transparent"
          returnKeyType="done"
          onChangeText={(text: string) => {
            this.onChangeText(text);
          }}
        />
      </View>
    );
  }

  renderConfirm = () => {
    let qualified = true;
    if (!this.state.ecuError) {
      const { mill, battery, rpm } = this.state;
      if (mill) {
        qualified = false;
      } else if (rpm) {
        qualified = false;
      } else if (battery < 10) {
        qualified = false;
      }
    }

    return (
      <TouchableOpacity
        hitSlop={hitSlop}
        style={[
          styles.confirmButContainer,
          { backgroundColor: qualified ? colors.secondary_dark : "gray" }
        ]}
        onPress={() => {
          this.onPress(qualified);
        }}
      >
        <Text style={styles.confirmButText}>BẮT ĐẦU</Text>
      </TouchableOpacity>
    );
  };

  onPress = (qualified: boolean) => {
    if (qualified) {
      OverlayConfirmUpdate_tools.showPopup(true, this.cbConfirm);
    } else {
      Alert.alert(
        "Không đủ điều kiện cập nhật",
        "Vui lòng kiểm tra lại các điều kiện",
        [
          {
            text: "Về màn hình chính",
            onPress: () => {
              navigationService.navigate("Main");
            }
          },
          { text: "Đồng ý" }
        ]
      );
    }
  };

  cbConfirm = () => {
    // Đủ điều kiện update -> Sang module của Chiến
    const { currentVer, nextVer, licensePlate } = this.state;
    const bikeName = bikeLibHelper.honda.getBikeName();
    const params = {
      storename: this.storename,
      bikeName,
      licensePlate: licensePlate.trim().toUpperCase(),
      ecuId: currentVer,
      versionUpdate: nextVer[4],
      peripheralId: ble.deviceAddr,
      onStop: () => {
        Orientation.unlockAllOrientations();
        ble.esyDataManager.honda.toUpdateECU(false);
        navigationService.navigate("AppLoading");
      }
    };
    TrackUser.logEvent("tool_update_ecu");
    Orientation.lockToPortrait();
    ble.esyDataManager.honda.toUpdateECU(true);
    setTimeout(() => {
      navigationService.navigate("UpdateEcu_Start", params);
    }, 500);
  };

  onChangeText = (text: string) => {
    this.setState({ licensePlate: text });
  };

  handleBackPress = () => {
    console.log("QualifyUpdate", "handleBackPress");
    // works best when the goBack is async
    this.onBack();
    return true;
  };

  onBack = async () => {
    navigationService.goBack();
  };

  render() {
    return (
      <BaseScreen
        statusbarStyle="light-content"
        handleBackPress={this.handleBackPress}
      >
        {/* NavBar */}
        <NavBar onLeftPress={this.handleBackPress} title="Xác nhận cập nhật" />
        {this.renderBody()}
        <OverlayConfirmUpdate
          ref={_ref => {
            OverlayConfirmUpdate_tools.setRef(_ref);
          }}
        />
      </BaseScreen>
    );
  }
}

function versionToString(ver: number[]) {
  let hexStr = "";
  ver.forEach((value, index) => {
    let hex = value.toString(16);
    if (value < 16) {
      hex = `0${hex}`;
    }
    if (index !== 0) {
      hex = `-${hex}`;
    }
    hexStr += hex.toUpperCase();
  });
  return hexStr;
}

export default QualifyUpdate;

const styles = StyleSheet.create({
  header: {
    fontSize: horizontalScale(18),
    fontWeight: "bold",
    marginBottom: 10
  },
  content: {
    fontSize: horizontalScale(20),
    color: colors.secondary_dark,
    marginBottom: 20
  },
  content2: {
    fontSize: horizontalScale(20)
  },
  nextVerContainer: {
    borderWidth: 1,
    padding: 10,
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  confirmButContainer: {
    alignItems: "center",
    padding: 10,
    borderRadius: 8
  },
  confirmButText: {
    fontSize: horizontalScale(16),
    color: colors.primary_light
  },
  warningText: {
    color: colors.error,
    fontSize: horizontalScale(16)
  },

  headerText: {
    fontSize: horizontalScale(18)
    // color: colors.secondary_dark
  },
  inputText: {
    paddingTop: 10,
    fontSize: horizontalScale(18),
    borderBottomColor: "black",
    borderBottomWidth: 1,
    marginBottom: 13,
    minHeight: 45
  }
});
