import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  NativeAppEventEmitter,
  NativeEventEmitter,
  NativeModules,
  Platform,
  PermissionsAndroid,
  ListView,
  ScrollView,
  AppState,
  Dimensions,
} from "react-native";
import BleManager from "react-native-ble-manager";
import { check } from "../../src/utilities";

const window = Dimensions.get("window");
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

export default class App extends Component {
  constructor() {
    super();

    this.state = {
      scanning: false,
      peripherals: new Map(),
      appState: "",
    };
  }

  componentDidMount() {
    AppState.addEventListener("change", this.handleAppStateChange);

    BleManager.start({ showAlert: true });

    this.handlerDiscover = bleManagerEmitter.addListener(
      "BleManagerDiscoverPeripheral",
      this.handleDiscoverPeripheral,
    );
    this.handlerStop = bleManagerEmitter.addListener(
      "BleManagerStopScan",
      this.handleStopScan,
    );
    this.handlerConnect = bleManagerEmitter.addListener(
      "BleManagerConnectPeripheral",
      this.handlerConnect,
    );
    this.handlerDisconnect = bleManagerEmitter.addListener(
      "BleManagerDisconnectPeripheral",
      this.handleDisconnectedPeripheral,
    );
    check.checkPermission(
      () => {
        this.startScan();
      },
      (code, error) => {
        console.log(code, error);
      },
    );
  }

  handlerConnect = (peripheral) => {
    console.log(peripheral);
    this.connectPeripheral({ id: peripheral.peripheral });
  };

  handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      console.log("App has come to the foreground!");
      BleManager.getConnectedPeripherals([]).then((peripheralsArray) => {
        console.log("Connected peripherals: " + peripheralsArray.length);
      });
    }
    this.setState({ appState: nextAppState });
  };

  componentWillUnmount() {
    this.handlerDiscover.remove();
    this.handlerStop.remove();
    this.handlerDisconnect.remove();
  }

  handleDisconnectedPeripheral = (data) => {
    let peripherals = this.state.peripherals;
    let peripheral = peripherals.get(data.peripheral);
    if (peripheral) {
      peripheral.connected = false;
      peripherals.set(peripheral.id, peripheral);
      this.setState({ peripherals });
    }
    console.log("Disconnected from " + data.peripheral);
  };

  getErrorUpdate = () => {
    alert("Bạn cần tắt xe đi bật lại");
  };

  handleStopScan = () => {
    this.setState({ scanning: false });
    let check = true;
    this.state.peripherals.forEach((item) => {
      if (item.name === "VIET NAM") {
        check = false;
        // this.clickItem(item);
      }
    });
    // if (check) this.startScan();
  };

  startScan = () => {
    if (!this.state.scanning) {
      this.setState({ peripherals: new Map() });
      check.enableBluetooth(
        () => {
          BleManager.scan([], 3, true)
            .then(
              (results) => {
                this.setState({ scanning: true });
              },
              (error) => {
                console.log("error scan: ", error);
              },
            )
            .catch((error) => console.log("err: ", error));
        },
        (error) => {
          console.log("enable error: ", error);
        },
      );
    }
  };

  connectPeripheral = (peripheral) => {
    BleManager.retrieveServices(peripheral.id).then(
      (peripheralInfo) => {
        console.log(peripheralInfo);
        const { advertising } = peripheralInfo;
        const { bytes } = advertising.manufacturerData;
        const ecuId = [bytes[15], bytes[16], bytes[17], bytes[18], bytes[19]];
        this.props.navigation.navigate("Update", {
          disconnect: this.handleDisconnectedPeripheral,
          errorAlert: this.getErrorUpdate,
          peripheralId: peripheral.id,
          ecuId,
          bikeName: "SH Nhập 2018",
        });
      },
      (error) => console.log("err: ", error),
    );
  };
  handleDiscoverPeripheral = (peripheral) => {
    var peripherals = this.state.peripherals;
    if (!peripherals.has(peripheral.id)) {
      peripherals.set(peripheral.id, peripheral);
      this.setState({ peripherals });
    }
  };

  clickItem = (peripheral) => {
    if (peripheral) {
      if (peripheral.connected) {
        BleManager.disconnect(peripheral.id);
      } else {
        BleManager.connect(peripheral.id)
          .then(() => {
            let peripherals = this.state.peripherals;
            let p = peripherals.get(peripheral.id);
            if (p) {
              p.connected = true;
              peripherals.set(peripheral.id, p);
              this.setState({ peripherals });
            }
            console.log("Connected to " + peripheral.id);
            // this.connectPeripheral({ id: peripheral.id });
          })
          .catch((error) => {
            console.log("Connection error", error);
          });
      }
    }
  };

  render() {
    const list = Array.from(this.state.peripherals.values());
    const dataSource = ds.cloneWithRows(list);

    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={{
            marginTop: 40,
            margin: 20,
            padding: 20,
            backgroundColor: "#ccc",
          }}
          onPress={() => this.startScan()}
        >
          <Text>Scan Bluetooth ({this.state.scanning ? "on" : "off"})</Text>
        </TouchableOpacity>

        <ScrollView style={styles.scroll}>
          {list.length == 0 && (
            <View style={{ flex: 1, margin: 20 }}>
              <Text style={{ textAlign: "center" }}>No peripherals</Text>
            </View>
          )}
          <ListView
            enableEmptySections={true}
            dataSource={dataSource}
            renderRow={(item) => {
              const color = item.connected ? "green" : "#fff";
              return (
                <TouchableOpacity onPress={() => this.clickItem(item)}>
                  <View style={[styles.row, { backgroundColor: color }]}>
                    <Text
                      style={{
                        fontSize: 12,
                        textAlign: "center",
                        color: "#333333",
                        padding: 10,
                      }}
                    >
                      {item.name}
                    </Text>
                    <Text
                      style={{
                        fontSize: 8,
                        textAlign: "center",
                        color: "#333333",
                        padding: 10,
                      }}
                    >
                      {item.id}
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            }}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
    width: window.width,
    height: window.height,
  },
  scroll: {
    flex: 1,
    backgroundColor: "#f0f0f0",
    margin: 10,
  },
  row: {
    margin: 10,
  },
});
