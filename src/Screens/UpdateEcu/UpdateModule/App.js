import { createStackNavigator } from "react-navigation";
import Update from "./src";
import { Home } from "./example";

export default createStackNavigator(
  {
    Home: {
      screen: Home,
    },
    Update: {
      screen: Update,
    },
  },
  {
    headerMode: "none",
    initialRouteName: "Home",
  },
);
