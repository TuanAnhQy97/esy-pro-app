// import React, { PureComponent } from "react";
// import {
//   ScrollView,
//   View,
//   Text,
//   TouchableOpacity,
//   NativeModules,
//   NativeEventEmitter,
// } from "react-native";
// import BleManager from "react-native-ble-manager";
// import style from "./style";
// import { Modal } from "../../src/Component";
// // import { dataUpdate } from "../../src/utilities";

// const BleManagerModule = NativeModules.BleManager;
// const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

// export default class Peripheral extends PureComponent {
//   constructor(props) {
//     super(props);
//     this.peripheral = this.props.navigation.getParam("peripheral", {});
//     this.state = { data: [] };
//     this.index = 0;
//   }
//   componentDidMount() {
//     this.handlerDisconnect = bleManagerEmitter.addListener(
//       "BleManagerDisconnectPeripheral",
//       this.handleDisconnectedPeripheral,
//     );
//     this.handlerUpdate = bleManagerEmitter.addListener(
//       "BleManagerDidUpdateValueForCharacteristic",
//       this.handleUpdateValueForCharacteristic,
//     );
//     // this.notify(() => this.setChip(0x02, 5));
//     this.notifyUpdate(() => {
//       // this.write(dataUpdate(this.index), 8, () => this.index++);
//     });
//   }
//   componentWillUnmount() {
//     this.handlerDisconnect.remove();
//     this.handlerUpdate.remove();
//   }
//   function = 0;
//   x = 0;
//   maxSize = 0;
//   maxData = 0;
//   dataBLE = [];
//   errorBLE = [];
//   check = [];
//   dataPrint = [];

//   handleUpdateValueForCharacteristic = (data) => {
//     if (data.value.length === 1) {
//       this.maxData = data.value[0];
//       this.x = 0;
//       this.maxSize = 0;
//     } else {
//       switch (this.function) {
//         case 0x68:
//           this.errorBLE[this.x] = data.value;
//           this.x++;
//           if (this.x === 11) this.x = 0;
//           break;
//         default:
//           this.maxSize += data.value[1];
//           if (this.check[this.x]) {
//             this.dataBLE[this.x] = undefined;
//             this.check[this.x] = false;
//           }
//           if (this.dataBLE[this.x]) {
//             this.dataBLE[this.x].push(...data.value);
//           } else {
//             this.dataBLE[this.x] = data.value;
//           }
//           if (this.maxSize === this.dataBLE[this.x].length) {
//             this.maxSize = 0;
//             this.check[this.x] = true;
//             this.x++;
//           }
//           break;
//       }
//     }
//     if (this.dataPrint.length > 20) {
//       this.dataPrint = [];
//     }
//     this.dataPrint = [...this.dataPrint, data.value];
//     this.setState({
//       data: data.value,
//     });
//     // console.log(
//     //   'Received data from ' +
//     //     data.peripheral +
//     //     ' characteristic ' +
//     //     data.characteristic,
//     //   data.value,
//     // );
//   };
//   handleDisconnectedPeripheral = (data) => {
//     const disconnect = this.props.navigation.getParam("disconnect", null);
//     disconnect(data);
//     this.props.navigation.goBack();
//   };
//   retrieveServices = (success, error = () => {}) => {
//     const { peripheral } = this;
//     BleManager.retrieveServices(peripheral.id).then(
//       (peripheralInfo) => {
//         console.log(peripheralInfo);
//         success(peripheralInfo);
//       },
//       (error) => {
//         console.log(error);
//         if (error === "device disconnect") this.connect(this.notify);
//       },
//     );
//   };
//   connect = (callback) => {
//     BleManager.connect(this.peripheral.id).then(callback);
//   };
//   setChip = (value: number) => {
//     this.write(value, 5, (txt, port) => {
//       console.log("setchip: ", txt, port);
//     });
//   };

//   notify = (callback = () => {}) => {
//     const { peripheral } = this;
//     this.retrieveServices((peripheralInfo) => {
//       this.peripheralInfo = peripheralInfo;
//       var tmp = peripheralInfo.characteristics[3];
//       var service = tmp.service;
//       var characteristic = tmp.characteristic;
//       BleManager.startNotification(peripheral.id, service, characteristic)
//         .then(() => {
//           console.log("Started notification on " + peripheral.id);
//           callback();
//         })
//         .catch((error) => {
//           console.log(error);
//         });
//     });
//   };
//   write = async (
//     data: number | Array<number>,
//     port: number,
//     success = () => {},
//   ) => {
//     const { peripheralInfo, peripheral } = this;
//     const charWrite = peripheralInfo.characteristics[port];
//     if (this.function !== data) {
//       if (port === 4 && txt != 0x50) {
//         this.check = [];
//         this.dataBLE = [];
//         this.function = data;
//         this.dataPrint = [];
//       }
//       if (typeof data === "number") {
//         data = [data];
//         console.log("number");
//       } else {
//         console.log(typeof data);
//       }
//       BleManager.write(
//         peripheral.id,
//         charWrite.service,
//         charWrite.characteristic,
//         data,
//       )
//         .then(
//           () => {
//             console.log(this.index);
//             success(data, port);
//           },
//           (error) => {
//             console.log("error write: ", error);
//           },
//         )
//         .catch((error) => {
//           console.log("error: ", error);
//         });
//     }
//   };
//   getData = (value) => {
//     this.retrieveServices(() => this.write(value, 4));
//   };
//   removeError = () => {
//     this.retrieveServices(() => this.write(0x69, 4));
//   };
//   dataError = [];
//   getError = () => {
//     this.retrieveServices(() => this.write(0x68, 4));
//     setTimeout(() => {
//       this.errorBLE.forEach((itemError, index) => {
//         this.dataError[index] = itemError.splice(5, itemError.length - 6);
//       });
//       console.log("dataError: ", this.dataError);
//     }, 1200);
//   };
//   resetECU = () => {
//     this.retrieveServices(() => this.write(0x67, 4));
//   };
//   dataBLETmp = [];
//   stop = () => {
//     this.retrieveServices(() => {
//       this.write(0x50, 4, () => {
//         let index = 0;
//         let indexArray = 0;
//         this.dataBLETmp = [];
//         for (let i = 0; i < this.dataBLE.length; i++) {
//           let sizeData = this.dataBLE[indexArray][1];
//           if (this.dataBLETmp[index] === undefined) {
//             this.dataBLETmp[index] = this.dataBLE[i];
//           } else {
//             this.dataBLETmp[index] = [
//               ...this.dataBLETmp[index],
//               ...this.dataBLE[i],
//             ];
//           }
//           if (sizeData === this.dataBLETmp[index].length) {
//             indexArray = i + 1;
//             index++;
//           }
//         }
//         const dataXe = {};
//         this.dataBLETmp.forEach((dataItem, index) => {
//           dataXe[`xe${index}`] = dataItem.splice(2, dataItem[1] - 3);
//         });

//         console.log("dataXe: ", dataXe);
//       });
//     });
//   };

//   render() {
//     return (
//       <View style={{ flex: 1 }}>
//         <ScrollView
//           style={{ width: "100%", height: "40%", backgroundColor: "#FFFA" }}
//         >
//           <View style={{ flex: 1 }}>
//             {this.dataPrint.map((item, index) => (
//               <Text key={index.toString()}>
//                 {item.map((item1, index1) => (
//                   <Text key={index1.toString()}>{item1},</Text>
//                 ))}
//               </Text>
//             ))}
//           </View>
//         </ScrollView>
//         <ScrollView
//           style={{ width: "100%", height: "60%", backgroundColor: "white" }}
//           showsVerticalScrollIndicator={false}
//         >
//           <View style={style.container}>
//             {/* <TouchableOpacity
//               onPress={() => {
//                 this.getData(0x60);
//               }}
//               style={style.btn}
//             >
//               <Text>Get 1</Text>
//             </TouchableOpacity>
//             <TouchableOpacity
//               onPress={() => {
//                 this.getData(0x61);
//               }}
//               style={style.btn}
//             >
//               <Text>Get 2</Text>
//             </TouchableOpacity>
//             <TouchableOpacity
//               onPress={() => {
//                 this.getData(0x62);
//               }}
//               style={style.btn}
//             >
//               <Text>Get 3</Text>
//             </TouchableOpacity>
//             <TouchableOpacity
//               onPress={() => {
//                 this.getData(0x63);
//               }}
//               style={style.btn}
//             >
//               <Text>Get 4</Text>
//             </TouchableOpacity> */}
//             <TouchableOpacity
//               onPress={() => {
//                 this.sendData();
//               }}
//               style={style.btn}
//             >
//               <Text>send data</Text>
//             </TouchableOpacity>
//             <TouchableOpacity
//               onPress={() => {
//                 this.auto();
//               }}
//               style={style.btn}
//             >
//               <Text>auto</Text>
//             </TouchableOpacity>
//             <TouchableOpacity onPress={this.resetData} style={style.btn}>
//               <Text>Reset data</Text>
//             </TouchableOpacity>
//             {/* <TouchableOpacity onPress={this.getError} style={style.btn}>
//               <Text>Lấy lỗi</Text>
//             </TouchableOpacity>
//             <TouchableOpacity onPress={this.removeError} style={style.btn}>
//               <Text>Xóa lỗi</Text>
//             </TouchableOpacity>
//             <TouchableOpacity onPress={this.stop} style={style.btn}>
//               <Text>Stop</Text>
//             </TouchableOpacity> */}
//           </View>
//         </ScrollView>
//       </View>
//     );
//   }
// }
