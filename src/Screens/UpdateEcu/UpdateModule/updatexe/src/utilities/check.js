import { PermissionsAndroid, Platform, Alert } from "react-native";
import BleManager from "react-native-ble-manager";

const dataName = ["AB41", "ABAB", "LD125U2", "LD125U3", "SHMode", "VS02"];

const checkPermission = (
  success: Function,
  error = (code: number, error) => {},
) => {
  if (Platform.OS === "android" && Platform.Version >= 23) {
    PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
    ).then(
      (result) => {
        if (result) {
          success();
        } else {
          PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
          ).then(
            (result) => {
              if (result) {
                success();
              } else {
                error(3, null);
              }
            },
            (err) => {
              console.log("deny per", err);
              error(1, err);
            },
          );
        }
      },
      (err) => {
        console.log("check error: ", err);
        error(2, err);
      },
    );
  }
};

const enableBluetooth = (success, error) => {
  BleManager.enableBluetooth().then(success, error);
};

const retrieveServices = (id, success, error) => {
  BleManager.retrieveServices(id).then(success, error);
};

export const checkCodeLength3 = (code, handler, callback = () => {}) => {
  let message = "";
  let isError = true;
  let ignore = false; // Mặd định luôn xử lý
  let messageDev = "";
  let isDev = false;
  switch (code) {
    case 0x02:
      // Đúng seed key -> đéo làm gì cả
      isError = false;
      ignore = true;
      break;
    case 0x04:
      // callback();
      message = "Cập nhật phiên bản thành công, hãy khởi động lại xe!";
      isError = false;
      break;
    case 0x77:
      // Kết thúc quá trình write memmory suôn sẻ -> đéo làm gì cả
      isError = false;
      ignore = true;
      break;

    case 0x07:
      // Trong lúc viết memory, data truyền vào không đúng về: khung truyền hoặc dữ liệu
      message = "Cập nhật ECU không thành công. Đường truyền gián đoạn (0x07)";
      break;
    case 0x03: // SAI seed key -> Thử lại từ đầu
      message = "Cập nhật ECU không thành công. Đường truyền gián đoạn (0x03)";
      break;
    case 0x55:
      // Đứt Kline trong quá trình update trừ (khi đang viết vào bộ nhớ ECU)
      message = "Cập nhật ECU không thành công. Đường truyền gián đoạn (0x55)";
      break;
    case 0x66:
      // Kết thúc quá trình write memmory KHÔNG suôn sẻ
      message = "Cập nhật ECU không thành công. Đường truyền gián đoạn (0x66)";
      break;
    case 0x96:
      // Đứt Kline trong khi đang viết vào bộ nhớ ECU
      message = "Cập nhật ECU không thành công. Đường truyền gián đoạn (0x96)";
      break;

    default:
      console.log("checkCodeLength3", "default", code);
      message = "Lỗi chưa xác định!";
      isError = true;
      break;
  }
  return { message, isError, ignore, messageDev };
};

export const convertNameToNumber = (nameVehicle: string) => {
  switch (nameVehicle) {
    case "AB41":
      return 1;
    case "ABAB":
      return 2;
    case "LD125U2":
      return 3;
    case "LD125U3":
      return 4;
    case "SHMode":
      return 5;
    case "VS02":
      return 6;
    case "VS62":
      return 7;
    default:
      return 6; // data rỗng hoặc chưa define
  }
};

export const convertEcuIdToVehicle = (ecuId: number) => {
  let code;
  let _ecuId = ecuId.slice(0, 3);

  if (compareArrays(_ecuId, [0x01, 0x01, 0xab])) {
    code = 0xab;
  } else if (compareArrays(_ecuId, [0x01, 0x01, 0x41])) {
    code = 0x41;
  } else if (compareArrays(_ecuId, [0x01, 0x01, 0x2f])) {
    code = 0x2f;
  } else if (compareArrays(_ecuId, [0x01, 0x01, 0x62])) {
    code = 0x62;
  } else if (compareArrays(_ecuId, [0x01, 0x01, 0x02])) {
    code = 0x02;
  } else if (compareArrays(_ecuId, [0x01, 0x01, 0x46])) {
    code = 0x46;
  }

  switch (code) {
    case 0xab:
      return {
        name: "ABAB",
        maxVersion: 3,
        maxLoop: 3072,
      };
    case 0x41:
      return {
        name: "AB41",
        maxVersion: 2,
        maxLoop: 930,
      };
    case 0x2f:
      return {
        name: "LD125",
        maxVersion: 3,
        maxLoop: 3072,
      };
    // case 0x62:
    //   return {
    //     name: "VS62",
    //     maxVersion: 2,
    //     maxLoop: 3072,
    //   };
    // case 0x02:
    //   return {
    //     name: "VS02",
    //     maxVersion: 2,
    //     maxLoop: 3072,
    //   };
    case 0x46:
      return {
        name: "SHMode",
        maxVersion: 2,
        maxLoop: 3072,
      };
    default:
      return {
        name: null,
        maxVersion: 2,
        maxLoop: 384,
      };
  }
};

export const checkUpdate = (ecuId: Array<number>) => {
  const bytes = ecuId;
  const vehicleCode = bytes[2];
  const versionCode = bytes[4];
  const vehicle = convertEcuIdToVehicle(ecuId);
  const max = vehicle.maxVersion;
  const nameVehicle = vehicle.name;
  if (nameVehicle !== null) {
    let isUpdate = versionCode < max;
    let options = [max];
    if (nameVehicle === "LD125") options = [2, 3];
    return {
      isUpdate,
      options,
    };
  }
  return {
    isUpdate: false,
    options: [],
  };
};

export { checkPermission, enableBluetooth };

/**
 * Compare 2 array number
 * @param {*} arr1 array 1
 * @param {*} arr2 array 2
 * @returns true if they are the same, else return false
 */
function compareArrays(arr1: Array<number>, arr2: Array<number>): boolean {
  if (arr1.length != arr2.length) return false;
  for (let i = 0; i < arr1.length; i++) {
    if (arr1[i] != arr2[i]) return false;
  }
  return true;
}
