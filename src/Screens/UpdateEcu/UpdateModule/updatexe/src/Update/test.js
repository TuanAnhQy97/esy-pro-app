import React, { PureComponent } from "react";
import {
  View,
  ScrollView,
  TouchableOpacity,
  Text,
  TextInput,
  AsyncStorage,
  NativeModules,
  NativeEventEmitter,
  Picker,
  Alert,
  BackHandler,
  ImageBackground,
} from "react-native";
import BleManager from "react-native-ble-manager";
import { NavigationActions } from "react-navigation";
import * as LeadOption from "../data/Lead";
import SeedKey from "../SeedKey/SeedKeyCheck";
import { UpdateECU, BleFunction, check } from "../utilities";
import { checkCodeLength3, convertEcuIdToVehicle } from "../utilities/check";
import { ProgressCircle, Modal } from "../Component";
import style from "./style";
import { colors, icons } from "../themes";
import Success from "./Success";
import { verticalScale, horizontalScale } from "../utilities/Scale";

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

type Props = {
  navigation: Object,
};
const LENGTH_BUFFER = 21;
export default class Update extends PureComponent<Props> {
  constructor(props) {
    super(props);
    this.peripheralId = props.navigation.getParam("peripheralId", {});
    this.bikeName = props.navigation.getParam("bikeName", "");
    this.ecuId = props.navigation.getParam("ecuId", []);
    this.versionUpdate = props.navigation.getParam("versionUpdate", -1);
    this.onStop = props.navigation.getParam("onStop", () => {});
    this.disconnect = this.props.navigation.getParam("disconnect", () => {});
    this.portNoti = 21;
    this.portSend = 22;
    this.keyName = null;
    this.dataName = null;
    this.backButton = true;
    this.state = {
      loop: 0,
      dataName: "",
      status: "Đang khởi tạo dữ liệu...",
      isUpdate: true,
      isBack: false,
      totalMin: -1,
      isDisconnect: false,
      isSelectVehicle: false,
    };
    this.maxLoop = 1;
    this.index = 0;
    this.indexTime = 0; // số lần tính
    this.average = 0; // thời gian trung bình
    this.average1 = 0;
    this.totalMin = 100;
    this.backButton = true;
    this.arrSpeed = [];
    this.minTotal = 100;
    this.unit = "phút";
  }

  async componentDidMount() {
    this.checkVehicle();
    this.handlerDisconnect = bleManagerEmitter.addListener(
      "BleManagerDisconnectPeripheral",
      this.handleDisconnectedPeripheral,
    );
    this.handlerUpdate = bleManagerEmitter.addListener(
      "BleManagerDidUpdateValueForCharacteristic",
      this.handleUpdateValueForCharacteristic,
    );
    // this.start();
    this.handlerBackButtonPress = BackHandler.addEventListener(
      "hardwareBackPress",
      this.backButtonHandler,
    );
  }

  componentWillUnmount() {
    this.handlerUpdate.remove();
    this.handlerDisconnect.remove();
  }

  checkVehicle = () => {
    const vehicle = convertEcuIdToVehicle(this.ecuId);
    const nameVehicle = vehicle.name;
    this.version = this.ecuId;
    const byteVersion = [0x11, vehicle.maxVersion];
    console.log("nameVehicle: ", nameVehicle);
    if (!nameVehicle || nameVehicle === "VS02" || nameVehicle === "VS62") {
      this.options = Object.keys(SeedKey);
      this.select = this.options[0];
      this.setState({
        isSelectVehicle: true,
        select: this.select,
      });
      this.maxLoop = vehicle.maxLoop;
    } else {
      if (nameVehicle) {
        this.maxLoop = vehicle.maxLoop;
        if (versionCode === 0) {
          this.keyName = `${nameVehicle}U0`;
        } else {
          this.keyName = `${nameVehicle}U1`;
        }
        if (nameVehicle === "LD125") {
          if (this.versionUpdate === -1) {
            this.dataName = "LD125U3";
          } else {
            this.dataName = `${nameVehicle}U${this.versionUpdate}`;
            byteVersion[1] = this.versionUpdate;
          }
          this.start();
        } else {
          // Do chi co 1 ver => k can check ver
          this.dataName = nameVehicle;
          this.start();
        }
        this.version[3] = byteVersion[0];
        this.version[4] = byteVersion[1];
      } else {
        Alert.alert("Thông báo: ", "Chưa có dữ liệu của dòng xe này", [
          {
            text: "Đồng ý",
            onPress: () => this.backHandler(),
          },
        ]);
      }
    }
  };
  checkStartTest = () => {
    const check = this.select.search("LD125");
    if (check === -1) {
      const last = this.select.search("U");
      const dataName = this.select.substr(0, last);
      console.log("Dataname: ", dataName);
      this.dataName = dataName;
      this.keyName = this.select;
      if (dataName === "AB41") this.maxLoop = 930;
      else {
        this.maxLoop = 3072;
      }
      this.start();
    } else {
      Alert.alert("Thông báo: ", "Bạn không được chọn xe Lead");
    }
  };
  handleUpdateValueForCharacteristic = (data) => {
    if (
      data.characteristic.toUpperCase() !==
      "6E400021-B5A3-F393-E0A9-E50E24DCCA9E"
    ) {
      return;
    }
    console.log("Received data ", data.value);
    const { length } = data.value;
    switch (length) {
      case 1:
        this.checkActionWithLength1(data);
        break;
      case 3:
        this.checkActionWithLength3(data);
        break;
      case 4:
        this.checkActionWithLength4(data.value);
        break;
      case 7:
        this.checkSeed(data.value[4], data.value[5]);
        break;
      default: // TODO
    }
  };
  checkActionWithLength4 = (data) => {
    const number1 = data[2];
    const number2 = data[3];
    const tmpIndex = number1 * number2;
    this.index = tmpIndex;
    this.sendDataNext(true);
  };
  checkActionWithLength1 = (data) => {
    switch (data.value[0]) {
      case 0x63:
        this.sendDataNext(true);
        break;
      case 100:
        this.startSendData();
        break;
      case 101:
        this.sendDataNext();
        break;
      case 102:
        this.handleDisconnectedPeripheral();
        break;
      case 104:
        this.sendCodeVehicle();
        break;
      default:
        console.log(data.value[0]);
    }
    return;
  };
  checkActionWithLength3 = (data) => {
    const code = data.value[0];
    const { message, isError, ignore, messageDev } = checkCodeLength3(code);
    if (!ignore) {
      if (isError) {
        this.setState({ isError, message, msgDev: messageDev }, () => {
          this.modal.open();
        });
      } else {
        this.setState({ isError: false, isUpdate: false });
      }
    }
  };
  checkSeed = (number1: number, number2: number) => {
    this.setState({ status: "Đang kiểm tra ..." });
    let check = UpdateECU.checkSeedKey([number1, number2]);
    if (check.length === 0) {
      check = [102];
    }
    BleFunction.write(check, this.portSend);
  };
  handleDisconnectedPeripheral = (data) => {
    const { isUpdate } = this.state;
    if (isUpdate) {
      // Alert.alert("Lỗi: ", "Cập nhật bị lỗi, do khoảng cách với xe quá xa", [
      //   {
      //     text: "Đồng ý",
      //     onPress: () => this.backHandler(),
      //   },
      // ]);
      this.setState(
        {
          message: "Cập nhật bị lỗi, do khoảng cách với xe quá xa",
          isError: true,
          isBack: true,
        },
        () => {
          this.modal.open();
        },
      );
    } else {
      this.setState({ isBack: true });
    }
  };
  backButtonHandler = () => {
    if (this.state.isBack) {
      this.backHandler();
    }
    return true;
  };
  backHandler = () => {
    this.disconnect({ peripheral: this.peripheralId });
    this.onStop();
    const back = NavigationActions.back();
    this.props.navigation.dispatch(back);
  };
  connect = (callback) => {
    BleManager.connect(this.peripheralId).then(callback);
  };

  start = () => {
    // BleFunction.setService("UPDATE");
    if (this.keyName) {
      UpdateECU.setKey(this.keyName);
      if (this.dataName) {
        UpdateECU.setData(this.dataName);
        this.setState({
          optionVersion: [],
          dataName: this.dataName,
          keyName: this.keyName,
          loop: 0,
        });
        BleFunction.setId(this.peripheralId);
        BleFunction.notify(this.portNoti, () => {
          BleFunction.write(0x02, BleFunction.portChip, () => {
            BleFunction.write(0x70, BleFunction.portMode, () => {
              this.setState({
                loop: 0,
              });
            });
          });
        });
      } else {
        alert("Khong co xe nay: ", this.dataName);
      }
    } else {
      alert("Khong co key nay: ", this.keyName);
    }
  };

  calTime1 = () => {
    let average = 0;
    for (let i = 1; i < LENGTH_BUFFER; i++) {
      const before = average * (i - 1);
      const time1 = this.arrSpeed[i] - this.arrSpeed[i - 1];
      average = (time1 + before) / i;
    }
    this.average1 = average;
    this.calMin(average);
  };
  calMin = (average) => {
    const totalTime = average * (this.maxLoop - this.index);
    let time = Math.floor(totalTime / (60 * 1000));
    let unit = "phút";
    if (time === 0 && average !== 0) {
      time = Math.floor(totalTime / 1000);
      unit = "giây";
    }
    this.unit = unit;
    this.setState({ totalMin: time });
  };
  calTime = () => {
    const { length } = this.arrSpeed;
    if (length < LENGTH_BUFFER) {
      const time1 = this.arrSpeed[length - 1];
      const time2 = this.arrSpeed[length - 2];
      let dif = time1 - time2;
      if (dif < 0) dif = -dif;
      const before = this.average * this.indexTime;
      this.indexTime++;
      this.average = (before + dif) / this.indexTime;
      dif = this.average;
      this.calMin(dif);
    } else {
      this.calTime1();
    }
  };
  startSendData = () => {
    const time1 = new Date().getTime();
    this.arrSpeed.push(time1);
    this.index = 0;

    this.setState({
      loop: this.state.loop + 1,
      status: "Đang cài đặt dữ liệu ...",
    });
    this.sendData();
  };
  sendDataNext = (backup: boolean = false) => {
    // true: gói tin bị sai => gửi lại
    if (backup) {
      this.sendData();
    } else {
      this.index++;
      this.setState({ loop: this.state.loop + 1 });
      const time = new Date().getTime();
      this.arrSpeed.push(time);
      if (this.arrSpeed.length > LENGTH_BUFFER) this.arrSpeed.shift();
      if (this.index === 2 || this.index % 10 === 0) {
        this.calTime();
      }
      this.sendData();
    }
  };
  sendCodeVehicle = () => {
    this.sendData(check.convertNameToNumber(this.dataName));
  };
  sendData = async (
    data: number | Array<number> = null,
    callback = () => {},
  ) => {
    if (data === null) {
      data = UpdateECU.dataUpdate(this.index);
      if (data.length === 0) data = [103];
    }
    if (data) {
      BleFunction.write(data, this.portSend, () => {
        callback();
      });
    }
  };

  setData = (text) => {
    this.setState({
      dataName: text,
    });
    this.dataName = text;
    // this.start();
  };
  renderArray = (array: Array<number>) => {
    let str = "";
    if (typeof array !== "undefined") {
      const length = array.length - 1;
      array.forEach((number, index) => {
        let tmp = number.toString(16).toLocaleUpperCase();
        if (tmp.length === 1) {
          tmp = `0${tmp}`;
        }
        if (index !== length) {
          str += `${tmp} - `;
        } else {
          str += tmp;
        }
      });
    }
    return str;
  };
  render() {
    const {
      loop,
      totalMin,
      status,
      isUpdate,
      isBack,
      message,
      isError,
      isSelectVehicle,
    } = this.state;
    const number = loop % 9;
    let dot = ".";
    if (number >= 3) dot = ". .";
    if (number >= 6) dot = ". . .";
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: colors.white,
        }}
      >
        {isUpdate ? (
          <View
            style={{
              height: "100%",
              width: "100%",
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: colors.defaultOpacity,
            }}
          >
            {isSelectVehicle ? (
              <View
                style={{
                  position: "absolute",
                  zIndex: 5,
                  elevation: 3,
                  top: 10,
                  width: "100%",
                  alignItems: "center",
                }}
              >
                <Picker
                  selectedValue={this.state.select}
                  style={{ height: 50, width: "60%" }}
                  onValueChange={(itemValue, itemIndex) => {
                    this.select = itemValue;
                    this.setState({ select: this.select });
                  }}
                >
                  {this.options.map((item, index) => {
                    return (
                      <Picker.Item
                        label={item}
                        value={item}
                        key={index.toString()}
                      />
                    );
                  })}
                </Picker>
                <TouchableOpacity
                  style={{
                    height: 50,
                    width: "60%",
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: colors.orange,
                  }}
                  onPress={this.checkStartTest}
                >
                  <Text style={{ fontWeight: "bold", color: "white" }}>
                    Start
                  </Text>
                </TouchableOpacity>
              </View>
            ) : null}
            <Text style={style.showTxt}>{status}</Text>
            <View style={style.bottomContainer}>
              <Text style={style.note}>
                Lưu ý: Nếu điện thoại xa xe, quá trình update sẽ bị chậm lại
                hoặc lỗi
              </Text>
            </View>
            <View style={style.processContainer}>
              <ProgressCircle
                percent={(loop / this.maxLoop) * 100}
                radius={90}
                borderWidth={9}
                color={colors.orange}
                shadowColor="#999"
                bgColor={colors.text}
              >
                {totalMin !== -1 ? (
                  <Text
                    style={{
                      width: "70%",
                      textAlign: "center",
                      color: "white",
                      fontSize: 15,
                    }}
                    numberOfLines={3}
                  >
                    {this.unit !== "giây" ? (
                      <Text>
                        Còn{" "}
                        <Text style={{ fontWeight: "bold", fontSize: 17 }}>
                          {totalMin}
                        </Text>{" "}
                        {this.unit}
                      </Text>
                    ) : (
                      <Text style={{ fontWeight: "bold", fontSize: 18 }}>
                        Sắp xong
                      </Text>
                    )}
                    {`\n${dot}`}
                  </Text>
                ) : (
                  <Text
                    style={{
                      width: "70%",
                      textAlign: "center",
                      color: "white",
                    }}
                  >
                    Đang tính{"\n"}thời gian
                  </Text>
                )}
              </ProgressCircle>
            </View>
            <View
              style={{ marginTop: verticalScale(20), alignItems: "center" }}
            >
              <Text style={{ fontSize: 16, color: "white" }}>
                Xe cập nhật:{" "}
                <Text style={{ fontWeight: "bold" }}>{this.bikeName}</Text>
              </Text>
              <Text style={{ fontSize: 16, color: "white" }}>
                Phiên bản cập nhật:{" "}
                <Text style={{ fontWeight: "bold" }}>
                  {this.renderArray(this.version)}
                </Text>
              </Text>
            </View>
            <Modal
              ref={(node) => {
                this.modal = node;
              }}
              style={{
                backgroundColor: "white",
                borderRadius: 10,
                width: "90%",
                height: verticalScale(176),
                alignItems: "center",
                justifyContent: "center",
              }}
              onPress={() => {
                this.setState({ isUpdate: false });
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <ImageBackground
                  source={icons.warning}
                  style={{ width: 16, height: 16 }}
                />
                <Text
                  style={{
                    color: "#3C4562",
                    fontWeight: "bold",
                    fontSize: 17,
                    marginLeft: 5,
                  }}
                >
                  Quá trình nâng cấp xe bị lỗi
                </Text>
              </View>
              {/* <Text
                style={{ color: "green", fontWeight: "bold", fontSize: 17 }}
              >
                {this.state.messageDev}
              </Text> */}
              <TouchableOpacity
                style={{
                  backgroundColor: "red",
                  alignItems: "center",
                  justifyContent: "center",
                  height: verticalScale(40),
                  width: horizontalScale(150),
                  marginTop: verticalScale(50),
                }}
                onPress={() => {
                  this.setState({ isUpdate: false });
                }}
              >
                <Text
                  style={{ color: "white", fontWeight: "bold", fontSize: 16 }}
                >
                  Chi tiết
                </Text>
              </TouchableOpacity>
            </Modal>
          </View>
        ) : (
          <Success
            onPress={this.backHandler}
            back={isBack}
            message={message}
            isError={isError}
            versionUpdate={this.renderArray(this.version)}
            bikeName={this.bikeName}
            ecuId={this.renderArray(this.ecuId)}
          />
        )}
      </View>
    );
  }
}
