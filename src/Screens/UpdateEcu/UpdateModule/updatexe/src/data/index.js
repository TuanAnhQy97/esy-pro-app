import * as Lead from "./Lead";
import * as AB from "./AB";
import * as VS from "./VS";
import * as SH from "./SH";

const data = {
  ...Lead,
  ...AB,
  ...VS,
  ...SH,
};
export default data;
