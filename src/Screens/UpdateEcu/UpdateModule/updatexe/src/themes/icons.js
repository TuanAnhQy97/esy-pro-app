// leave off @2x/@3x
/* eslint-disable */
const icons = {
  success: require("../../assets/update/success.png"),
  warning: require("../../assets/update/warning.png"),
  error: require("../../assets/update/error.png"),
};
/* eslint-enable */

export default icons;
