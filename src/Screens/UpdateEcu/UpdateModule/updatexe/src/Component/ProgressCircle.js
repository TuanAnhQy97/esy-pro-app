import React, { PureComponent, ReactChild } from "react";
// import PropTypes from "prop-types";
import {
  StyleSheet,
  View,
  ViewPropTypes,
  I18nManager,
  Animated,
} from "react-native";

// compatability for react-native versions < 0.44
const ViewPropTypesStyle = ViewPropTypes
  ? ViewPropTypes.style
  : View.propTypes.style;
let direction = I18nManager.isRTL ? "right" : "left";
const styles = StyleSheet.create({
  container: {
    padding: 10,
    // backgroundColor: "red",
    justifyContent: "center",
    alignItems: "center",
  },
  outerCircle: {
    justifyContent: "center",
    alignItems: "center",
  },
  innerCircle: {
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
  },
  leftWrap: {
    position: "absolute",
    top: 0,
    [`${direction}`]: 0,
  },
  halfCircle: {
    position: "absolute",
    top: 0,
    left: 0,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  },
});

function percentToDegrees(percent) {
  return percent * 3.6;
}
const convertPercent = (percent) => {
  return -90 + percentToDegrees(percent);
};
type Props = {
  color: string,
  shadowColor: string,
  bgColor: string,
  radius: number,
  borderWidth: number,
  percent: number,
  children: ReactChild,
  containerStyle: ViewPropTypesStyle,
  outerCircleStyle: ViewPropTypesStyle,
};

export default class ProgressCircle extends PureComponent<Props> {
  static defaultProps = {
    color: "#f00",
    shadowColor: "#999",
    bgColor: "#e9e9ef",
    borderWidth: 2,
    children: null,
    containerStyle: null,
  };

  constructor(props) {
    super(props);
    this.state = this.getInitialStateFromProps(props);
    this.aniSize = new Animated.Value(0);
  }

  componentWillReceiveProps(nextProps) {
    this.setState(this.getInitialStateFromProps(nextProps));
  }
  componentDidMount() {
    this.animation();
  }
  animation = () => {
    const { borderWidth } = this.props;
    const containerRadius = borderWidth * 5;
    const ani0 = Animated.timing(this.aniSize, {
      duration: 0,
      toValue: 0,
      delay: 500,
    });
    const ani1 = Animated.timing(this.aniSize, {
      duration: 500,
      toValue: containerRadius - 5,
    });
    const ani2 = Animated.timing(this.aniSize, {
      duration: 0,
      toValue: 0,
    });
    const sequen = Animated.sequence([ani0, ani1, ani2]);
    Animated.loop(sequen).start();
  };
  getInitialStateFromProps(props) {
    const percent = Math.max(Math.min(100, props.percent), 0);
    const needHalfCircle2 = percent > 50;
    let halfCircle1Degree;
    let halfCircle2Degree;
    // degrees indicate the 'end' of the half circle, i.e. they span (degree - 180, degree)
    if (needHalfCircle2) {
      halfCircle1Degree = 180;
      halfCircle2Degree = percentToDegrees(percent);
    } else {
      halfCircle1Degree = percentToDegrees(percent);
      halfCircle2Degree = 0;
    }

    return {
      halfCircle1Degree,
      halfCircle2Degree,
      halfCircle2Styles: {
        // when the second half circle is not needed, we need it to cover
        // the negative degrees of the first circle
        backgroundColor: needHalfCircle2 ? props.color : props.shadowColor,
      },
    };
  }

  renderHalfCircle(rotateDegrees, halfCircleStyles) {
    const { radius, color } = this.props;
    const key = I18nManager.isRTL ? "right" : "left";
    return (
      <View
        style={[
          styles.leftWrap,
          {
            width: radius,
            height: radius * 2,
          },
        ]}
      >
        <Animated.View
          style={[
            styles.halfCircle,
            {
              width: radius,
              height: radius * 2,
              borderRadius: radius,
              overflow: "hidden",
              transform: [
                { translateX: radius / 2 },
                { rotate: `${rotateDegrees}deg` },
                { translateX: -radius / 2 },
              ],
              backgroundColor: color,
              ...halfCircleStyles,
            },
          ]}
        />
      </View>
    );
  }

  renderInnerCircle() {
    const radiusMinusBorder = this.props.radius - this.props.borderWidth;
    return (
      <View
        style={[
          styles.innerCircle,
          {
            width: radiusMinusBorder * 2,
            height: radiusMinusBorder * 2,
            borderRadius: radiusMinusBorder,
            backgroundColor: this.props.bgColor,
            ...this.props.containerStyle,
          },
        ]}
      >
        {this.props.children}
      </View>
    );
  }
  renderBorder = () => {};
  renderCircleMini = () => {
    const { percent, radius, borderWidth, color } = this.props;
    const tmp = (convertPercent(percent) / 180) * Math.PI;
    const r = radius - borderWidth / 2;
    const x = Math.cos(tmp) * r;
    const y = Math.sin(tmp) * r;
    const containerRadius = borderWidth * 5;
    const size1 = this.aniSize.interpolate({
      inputRange: [0, containerRadius],
      outputRange: [0, containerRadius / 2],
    });
    const size2 = this.aniSize.interpolate({
      inputRange: [0, containerRadius],
      outputRange: [0, containerRadius / 1.4],
    });
    return (
      <Animated.View
        style={{
          width: containerRadius,
          height: containerRadius,
          borderRadius: containerRadius / 2,
          position: "absolute",
          alignItems: "center",
          justifyContent: "center",
          overflow: "hidden",
          transform: [
            {
              translateX: x,
            },
            {
              translateY: y,
            },
          ],
        }}
      >
        <Animated.View
          style={{
            width: containerRadius / 3,
            height: containerRadius / 3,
            borderRadius: containerRadius / 6,
            position: "absolute",
            zIndex: 2,
            backgroundColor: this.props.color,
          }}
        />
        <Animated.View
          style={{
            width: this.aniSize,
            height: this.aniSize,
            borderRadius: size1,
            position: "absolute",
            zIndex: 1,
            borderWidth: 1,
            borderColor: color,
            // borderColor: this.aniSize.interpolate({
            //   inputRange: [0, containerRadius],

            //   outputRange: ["white", color],
            // }),
            opacity: this.aniSize.interpolate({
              inputRange: [0, containerRadius],
              outputRange: [0.8, 0.2],
            }),
          }}
        />
        <Animated.View
          style={{
            width: size2,
            height: size2,
            borderRadius: this.aniSize.interpolate({
              inputRange: [0, containerRadius],
              outputRange: [0, containerRadius / 2.8],
            }),
            position: "absolute",
            zIndex: 1,
            borderWidth: 1,
            borderColor: color,
            // borderColor: this.aniSize.interpolate({
            //   inputRange: [0, containerRadius],
            //   outputRange: [color, "white"],
            // }),
            opacity: this.aniSize.interpolate({
              inputRange: [0, containerRadius],
              outputRange: [1, 0.3],
            }),
          }}
        />
      </Animated.View>
    );
  };
  render() {
    const {
      halfCircle1Degree,
      halfCircle2Degree,
      halfCircle2Styles,
    } = this.state;
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.outerCircle,
            {
              width: this.props.radius * 2,
              height: this.props.radius * 2,
              borderRadius: this.props.radius,
              backgroundColor: this.props.shadowColor,
              ...this.props.outerCircleStyle,
            },
          ]}
        >
          {this.renderHalfCircle(halfCircle1Degree)}
          {this.renderHalfCircle(halfCircle2Degree, halfCircle2Styles)}
          {this.renderInnerCircle()}
        </View>
        {this.renderCircleMini()}
      </View>
    );
  }
}
