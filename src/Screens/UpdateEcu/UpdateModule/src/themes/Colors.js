/**
 * @flow
 */
const colors = {
  default: "rgba(66,183,42,1)",
  defaultOpacity: "rgba(66,183,42,0.75)",
  white: "rgba(255,255,255,1)",
  background: "rgba(245,245,245,1)",
  text: "rgba(82,82,82,1)",
  textOpacity: "rgba(153,153,153,1)",
  textOpacity10: "rgba(153,153,153,0.3)",
  orange: "#fb8c00",
};

/**
 *  #184423
    #3a9244
    #4ab743
    #8c8077
    #ebebeb
    #fffffe

    #fd7808
    #fb1106
    #ce0004
 */
export const COLORS = Object.freeze({
  primary_dark: "#184423",
  secondary_dark: "#3a9244",
  third_dark: "#8c8077",
  primary_light: "#fffffe",
  secondary_light: "#ebebeb",
  warning: "#fd7808",
  error: "#fb1106",
});

export default colors;
