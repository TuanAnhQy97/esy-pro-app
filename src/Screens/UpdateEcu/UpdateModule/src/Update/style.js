import { StyleSheet } from "react-native";
import { colors } from "../themes";
import { verticalScale, horizontalScale } from "../utilities/Scale";

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
  btn: {
    elevation: 10,
    borderRadius: 10,
    height: 70,
    width: "90%",
    margin: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
  },
  modalStyle: {
    width: "90%",
    marginLeft: "5%",
    flexDirection: "row",
    flexWrap: "wrap",
    elevation: 20,
  },
  pickerStyle: {
    height: (50),
    alignItems: "center",
    justifyContent: "center",
    padding: 30,
    width: "100%",
  },
  showTxt: {
    color: "white",
    fontWeight: "bold",
  },
  note: {
    color: "red",
    fontWeight: "bold",
    width: "70%",
    fontSize: (15),
    textAlign: "center",
  },
  bottomContainer: {
    position: "absolute",
    bottom: 0,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
  },
  processContainer: {
    marginTop: (20),
    // height: 30,
    // borderRadius: 15,
    width: "60%",
    // backgroundColor: "#cfd8dc",
    alignItems: "center",
    justifyContent: "center",
    // overflow: "hidden",
  },
  process: {
    height: "100%",
    backgroundColor: "#76ff03",
    borderBottomRightRadius: 15,
    borderTopRightRadius: 15,
    position: "absolute",
    left: 0,
  },
});

export default style;
