/**
 * @flow
 */
"use strict";
import React, { PureComponent, ReactNode } from "react";
import {
  View,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Text,
} from "react-native";
import * as d from "../utilities/Scale";
import { icons, colors } from "../themes";

type Props = {
  children: ReactNode,
  onPress: Function,
  back: boolean,
  isError: boolean,
  message: string,
  versionUpdate: string,
  bikeName: string,
  licensePlate: string,
  ecuId: string,
  storename: string,
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: colors.white,
  },
  note: {
    color: "#90a4ae",
    // fontWeight: "bold",
    textAlign: "center",
    fontSize: 19,
  },
  btnTxt: {
    color: "white",
    fontWeight: "bold",
    fontSize: d.horizontalScale(17),
  },
  btn: {
    backgroundColor: colors.default,
    width: 200,
    minWidth: 200,
    height: d.verticalScale(50),
    marginBottom: 22,
    alignItems: "center",
    justifyContent: "center",
  },
  propName: {
    width: "50%",
  },
  propContainer: {
    justifyContent: "center",
    flexDirection: "row",
    paddingVertical: 3,
  },
  txtValue: {
    width: "50%",
  },
});
type State = {
  isBack: boolean,
};
export default class Success extends PureComponent<Props, State> {
  static defaultProps = {
    onPress: () => {},
    isError: true,
    message: "",
    back: false,
  };
  state: State;
  constructor(props: Props) {
    super(props);
    const state = {
      isBack: false,
    };
    this.state = state;
  }
  componentDidMount() {
    if (this.props.back) {
      this.activeBack();
    }
  }
  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.back) {
      this.activeBack();
    }
  }
  activeBack = () => {
    setTimeout(() => {
      this.setState({ isBack: true });
    }, 700);
  };
  render() {
    const time = this.getTime();
    const date = this.getDate();
    const {
      back,
      isError,
      message,
      versionUpdate,
      bikeName,
      licensePlate,
      storename,
      ecuId,
    } = this.props;
    const { isBack } = this.state;
    return (
      <View style={style.container}>
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "space-around",
          }}
        >
          {/* ICON */}
          <ImageBackground
            source={isError ? icons.error : icons.success}
            style={{
              width: 76,
              height: 76,
            }}
            resizeMethod="resize"
            resizeMode="contain"
          />
          {/* Trạng thái cập nhật - Thành công hoặc lỗi */}
          <View style={{  }}>
            {!isError ? (
              <Text
                style={[
                  style.note,
                  { color: "green", fontWeight: "bold", textAlign: "center" },
                ]}
              >
                Cập nhật phiên bản thành công
              </Text>
            ) : (
              <Text style={[style.note, {}]}>{message}</Text>
            )}
          </View>
          {/* Thông tin cập nhật thành công (nếu có) */}
          <View
            style={{
              width: "90%",
              borderRadius: 5,
            }}
          >
            {!isError ? (
              <View
                style={{
                  backgroundColor: "#F5F5F5",
                  paddingHorizontal: 5,
                  paddingVertical: 10,
                }}
              >
                <View style={style.propContainer}>
                  <Text style={style.propName}>Tên xe:</Text>
                  <Text style={style.txtValue}>{bikeName}</Text>
                </View>
                <View style={style.propContainer}>
                  <Text style={style.propName}>Biển số:</Text>
                  <Text style={style.txtValue}>
                    {licensePlate ? licensePlate : "Không có"}
                  </Text>
                </View>
                <View style={style.propContainer}>
                  <Text style={style.propName}>Tại cửa hàng:</Text>
                  <Text style={style.txtValue}>{storename}</Text>
                </View>
                <View style={style.propContainer}>
                  <Text style={style.propName}>Thời gian hoàn thành:</Text>
                  <Text style={style.txtValue}>{time}</Text>
                </View>
                <View style={style.propContainer}>
                  <Text style={style.propName}>Ngày hoàn thành:</Text>
                  <Text style={style.txtValue}>{date}</Text>
                </View>
                <View style={style.propContainer}>
                  <Text style={style.propName}>Phiên bản hiện tại:</Text>
                  <Text style={style.txtValue}>{versionUpdate}</Text>
                </View>
                <View style={style.propContainer}>
                  <Text style={style.propName}>Phiên bản trước:</Text>
                  <Text style={style.txtValue}>{ecuId}</Text>
                </View>
              </View>
            ) : null}
          </View>
          {/* Hướng dẫn làm gì tiếp */}
          <Text
            style={{
              color: "#f44336",
              textAlign: "center",
              fontSize: 19,
            }}
          >
            Vui lòng tắt khoá điện và bật lại sau 10 giây
          </Text>
        </View>
        {/* Phím kết thúc */}
        <TouchableOpacity
          style={[
            style.btn,
            {
              backgroundColor: isBack ? "#fb8c00" : colors.defaultOpacity,
              opacity: isBack ? 1 : 0.6,
            },
          ]}
          onPress={isBack ? this.props.onPress : () => {}}
          activeOpacity={isBack ? 0.75 : 1}
          disabled={!isBack}
        >
          <Text style={[style.btnTxt, { color: isBack ? "white" : "grey" }]}>
            Xong
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  getTime() {
    let date = new Date();
    let hours = date.getHours();
    let minutes = date.getMinutes();
    if (hours < 10) {
      hours = "0" + hours;
    }
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    return `${hours}:${minutes}`;
  }

  getDate() {
    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth() + 1;
    if (day < 10) {
      day = "0" + day;
    }
    if (month < 10) {
      month = "0" + month;
    }
    return `${day}/${month}/${date.getFullYear()}`;
  }
}
