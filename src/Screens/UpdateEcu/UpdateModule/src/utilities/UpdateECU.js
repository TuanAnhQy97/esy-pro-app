/**
 * @flow
 */
import DataText from "../data";
import SeedKeyCheck from "../SeedKey/SeedKeyCheck";
import SeedKeyReturn from "../SeedKey/SeedKeyReturn";

class Instance {
  key: string = "";
  data = "";
  dataCheck = "";
  dataReturn = "";
  lengthData = 0;
  lengthCheck = 0;
  lengthReturn = 0;
  loop = 0;
  setKey = (key: string) => {
    this.key = key;
    this.dataCheck = SeedKeyCheck[this.key];
    this.dataReturn = SeedKeyReturn[this.key];
    this.lengthCheck = this.dataCheck.length;
    this.lengthReturn = this.dataReturn.length;
    // this.setData();
  };
  setData = (dataName: string) => {
    this.dataName = dataName;
    this.data = DataText[dataName];
    this.lengthData = this.data.length;

    this.loop = this.lengthData / (134 * 3 - 1);
  };
  checksum = (data: Array<number>) => {
    let sum = 0;
    data.forEach((number) => (sum += number));
    return ((sum ^ 0xff) + 1) & 0xff;
  };
  getArrayFromText = (
    index: number,
    text: string,
    len: number,
  ): Array<number> => {
    const tmpLenght = 3 * len - 1;
    const tmpText = text.substr(index * (tmpLenght + 1), tmpLenght);
    let arr = tmpText.split(" ");
    const arr1 = [];
    for (let i = 0; i < len; i++) {
      arr1[i] = parseInt(`0x${arr[i]}`);
    }
    return arr1;
  };
  getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max)) + 200;
  };
  getDataFromText = (index: number): Array<number> => {
    // 402 = 134 (byte: mỗi byte 2 số) * 2 + số dấu cách ở giữa các byte: 134
    if (index * 402 < this.lengthData) {
      let arr = this.getArrayFromText(index, this.data, 134);
      arr = [0x7c, 0x8b, 0x02, 0x06, ...arr];
      arr.push(this.checksum(arr));
      return arr;
    }
    return [];
  };
  dataUpdate = (index: number): Array<number> => {
    const dataBefore = this.getDataFromText(index);
    let idx = 0; // số thứ tự gói tin
    const subLength = 18; // 20 - 4 (4 byte header custom)
    let totalLength = dataBefore.length;
    let dataAfter = [];
    const random = this.getRandomInt(1000);
    while (totalLength > 0) {
      let tmp = dataBefore.splice(0, subLength);
      tmp = [0xff, idx, ...tmp];
      dataAfter = [...dataAfter, ...tmp];
      totalLength = dataBefore.length;
      idx++;
    }
    return dataAfter;
  };
  checkSeedKey = (array: Array<number> = []): Array<number> => {
    let index = 0;
    // 6 = 2 (byte: mỗi byte 2 số) * 2 + số dấu cách ở bên cạnh: 2
    while (index * 6 < this.lengthCheck) {
      const arr = this.getArrayFromText(index, this.dataCheck, 2);
      let count = 0;
      for (let i = 0; i < 2; i++) {
        if (arr[i] !== array[i]) {
          index++;
          break;
        }
        count++;
        if (count === 2) {
          let tmp = this.getArrayFromText(index, this.dataReturn, 2);
          if (this.dataName.search("VS") === -1) {
            tmp = [0x7b, 0x7, 0x2, 0x5, ...tmp];
          } else {
            tmp = [0x7b, 0x7, 0, 0x5, ...tmp];
          }
          tmp.push(this.checksum(tmp));
          return tmp;
        }
      }
    }
    return [];
  };
}
/* eslint-enable */
class Utilities {
  instance: Instance;
  static getInstance(): Instance {
    if (!this.instance) {
      this.instance = new Instance();
      return this.instance;
    }
    return this.instance;
  }
}
export default Utilities.getInstance();
