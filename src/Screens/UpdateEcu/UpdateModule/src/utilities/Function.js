import BleManager from "react-native-ble-manager";

type ServiceMode = "SETUP" | "UPDATE";

class Instance {
  service = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";
  portChip = 5;
  portMode = 6;
  defineService = {
    serviceSetup: "6e400001-b5a3-f393-e0a9-e50e24dcca9e",
    serviceUpdate: "6e400020-b5a3-f393-e0a9-e50e24dcca9e",
  };
  id = "";
  setPort = (portName: string, port: number) => {
    this[portName] = port;
    if (port < 10) this[portName] = "0" + port;
  };
  setId = (peripheralId: string) => {
    this.id = peripheralId;
  };
  setService = (mode: ServiceMode) => {
    switch (mode) {
      case "SETUP":
        this.service = this.defineService.serviceSetup;
        break;
      case "UPDATE":
        this.service = this.defineService.serviceUpdate;
        break;
      default: 
        console.log("setService default",mode);
        break;
    }
  };

  write = async (
    data: number | Array<number>,
    port: number,
    success = () => {},
  ) => {
    let portWrite = port;
    if (port < 10) portWrite = "0" + port;
    let service = this.defineService.serviceUpdate;
    if (port === this.portChip || port === this.portMode) {
      service = this.defineService.serviceSetup;
    }
    const charWrite = `6e4000${portWrite}-b5a3-f393-e0a9-e50e24dcca9e`;
    let tmpData = data;
    if (typeof data === "number") {
      tmpData = [data];
    }
    BleManager.writeWithoutResponse(this.id, service, charWrite, tmpData)
      .then(
        () => {
          success(data, port);
        },
        (error) => {
          console.log("error write: ", error);
        },
      )
      .catch((error) => {
        console.log("error: ", error);
      });
  };

  writeWithResponse = async (
    data: number | Array<number>,
    port: number,
    success = () => {},
  ) => {
    let portWrite = port;
    if (port < 10) portWrite = "0" + port;
    let service = this.defineService.serviceUpdate;
    if (port === this.portChip || port === this.portMode) {
      service = this.defineService.serviceSetup;
    }
    const charWrite = `6e4000${portWrite}-b5a3-f393-e0a9-e50e24dcca9e`;
    let tmpData = data;
    if (typeof data === "number") {
      tmpData = [data];
    }
    BleManager.write(this.id, service, charWrite, tmpData)
      .then(
        () => {
          success(data, port);
        },
        (error) => {
          console.log("error write: ", error);
        },
      )
      .catch((error) => {
        console.log("error: ", error);
      });
  };

  notify = (portNoti: number, callback = () => {}) => {
    let port = portNoti;
    if (portNoti < 10) port = "0" + portNoti;
    var characteristic = `6e4000${port}-b5a3-f393-e0a9-e50e24dcca9e`;
    let service = this.defineService.serviceUpdate;
    BleManager.startNotification(this.id, service, characteristic)
      .then(() => {
        console.log("Started notification on " + this.id);
        callback();
      })
      .catch((error) => {
        console.log(error);
      });
  };
}
class BleFunction {
  instance: Instance;
  static getInstance(): Instance {
    if (!this.instance) {
      this.instance = new Instance();
      return this.instance;
    }
    return this.instance;
  }
}
export default BleFunction.getInstance();
