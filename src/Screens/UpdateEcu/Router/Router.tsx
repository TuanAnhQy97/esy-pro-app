/**
 * @flow
 */
import { createStackNavigator, createSwitchNavigator } from "react-navigation";

import CheckUpdateEcu from "../CheckUpdateEcu";
import QualifyUpdateCondition from "../QualifyUpdateCondition";
import StartUpdate from "../UpdateModule/src";

export type SCREEN_KEY =
  /**
   * Root Stack
   */
  "UpdateEcu_CheckVer" | "UpdateEcu_QuailyUpdate" | "UpdateEcu_Start";

const verifyRoute = createStackNavigator(
  {
    UpdateEcu_CheckVer: {
      screen: CheckUpdateEcu
    },
    UpdateEcu_QuailyUpdate: {
      screen: QualifyUpdateCondition
    }
    // Module của Chiến
  },
  {
    headerMode: "none"
    // initialRouteName: "CreateNewAcc"
  }
);

const rootRoute = createSwitchNavigator(
  {
    Verify: {
      screen: verifyRoute
    },
    UpdateEcu_Start: {
      screen: StartUpdate,
      navigationOptions: {
        gesturesEnabled: false
      }
    }
    // Module của Chiến
  },
  {
    headerMode: "none"
    // initialRouteName: "CreateNewAcc"
  }
);

export default rootRoute;
