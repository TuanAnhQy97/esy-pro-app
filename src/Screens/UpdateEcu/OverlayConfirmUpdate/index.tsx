/**
 * @flow
 */
import React from "react";
import { TouchableOpacity, View, StyleSheet } from "react-native";
import { colors } from "../../../styles/colors";

import Text from "../../../Components/Text";
import { horizontalScale, verticalScale } from "../../../ScaleUtility";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

class Index extends React.PureComponent<{}, { isShowing: boolean }> {
  constructor(props: any) {
    super(props);
    this.state = { isShowing: false };
  }

  componentDidMount() {
    showed = false;
  }

  componentWillUnmount() {
    showed = false;
  }

  render() {
    if (this.state.isShowing) {
      return (
        <View
          style={[
            StyleSheet.absoluteFillObject,
            {
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }
          ]}
        >
          {/* Background color */}
          <View
            style={[
              StyleSheet.absoluteFillObject,
              { backgroundColor: colors.third_dark, opacity: 0.87 }
            ]}
          />
          {/* Content */}
          {this.renderContent()}
        </View>
      );
    }
    return null;
  }

  renderContent() {
    return (
      <View style={styles.rootContainer}>
        <Text style={styles.title}>Xác nhận cập nhật ECU</Text>
        <Text style={styles.message}>
          ESY PRO không chịu trách nghiệm cho hư hỏng do cập nhật ECU sai quy
          cách
        </Text>
        {/* Buttons */}
        <View style={styles.btnContainer}>
          <TouchableOpacity
            style={styles.btn}
            onPress={() => {
              showPopup(false);
            }}
          >
            <Text style={styles.cancelText}>Từ chối</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btn}
            onPress={() => {
              showPopup(false);
              cbConfirm();
            }}
          >
            <Text style={styles.confirmText}>Đồng ý</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  show() {
    this.setState({ isShowing: true });
  }

  dismiss() {
    this.setState({ isShowing: false });
  }
}
export default Index;

const styles = StyleSheet.create({
  rootContainer: {
    width: "100%",
    backgroundColor: colors.primary_light,
    paddingVertical: 20,
    paddingHorizontal: 10,
    borderRadius: 8
  },
  title: {
    fontSize: horizontalScale(20),
    fontWeight: "bold",
    marginBottom: 10,
    paddingTop: 10
  },
  message: {
    fontSize: horizontalScale(16)
  },
  btnContainer: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "center"
  },
  btn: { padding: 10, flex: 1 },
  confirmText: {
    fontSize: horizontalScale(16),
    fontWeight: "bold",
    color: colors.secondary_dark,
    alignSelf: "flex-end"
  },
  cancelText: {
    fontSize: horizontalScale(16),
    color: colors.third_dark,
    alignSelf: "flex-end"
  }
});
/**
 * ================================================================================

 * ================================================================================
 */
let popupRef;
let showed = false;
let cbConfirm = () => {};

/**
 * Set ref cho popup
 * @param {*} ref
 */
function setRef(ref: any) {
  popupRef = ref;
}
/**
 * Hiển thị popup
 * @param {*} on
 * @param {*} _callbackConfirm
 */
function showPopup(
  on?: boolean = false,
  _callbackConfirm: Function = () => {}
) {
  if (showed !== on) {
    showed = on;
    if (on) {
      popupRef.show();
      cbConfirm = _callbackConfirm;
    } else {
      popupRef.dismiss();
    }
  }
}
/**
 * Có đang hiển thị không?
 */
export function isShowing() {
  return showed;
}

export const tools = { setRef, showPopup, isShowing };
