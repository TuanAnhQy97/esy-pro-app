import React from 'react';
import {AppState, BackHandler} from 'react-native';

import BaseScreen from '../Components/BaseScreen';
import MyActivityIndicator from '../Components/MyActivityIndicator';
import Header from '../Components/MainScreen/Header';

import TabInMain from '../Components/MainScreen/Tabs/TabInMain';
import OverLayEraseError_ResetEcm, {
  tools as OverLayEraseError_ResetEcm_tools,
} from '../Components/Popup/OverLayEraseError_ResetEcm';
import OverlayWaitingChangeMode from '../Components/MainScreen/OverlayWaitingChangeMode';
import OverlayWarningEcuError, {
  tools as OverlayWarningEcuError_tools,
} from '../Components/Popup/OverlayWarningEcuError';

import OverlayConfirmReset, {tools as OverlayConfirmReset_tools} from '../Components/Popup/OverlayConfirmReset';
import OverlayConfirmErase, {tools as OverlayConfirmErase_tools} from '../Components/Popup/OverlayConfirmErase';

import AppVersionCheckUpdate from '../AppVersionCheckUpdate';
import {checkIfAppIsOpenedByNotification} from '../services/notification/FCM';
import ble from '../services/ble';
import eventBus from '../services/eventBus';
import {EVENT_BUS} from '../constants';
import navigationService from '../services/navigationService';

import {translateBleError, translateBleErrorTitle} from '../SharedFunctions';
import PopupConnectionError, {responsePopup, setRef} from '../Components/PopupConnectionError';

type Props = {
  dispatch: Function;
  changingMode: boolean;
  navigation: any;
};
type States = {
  appState: any;
  isLoading: boolean;
  isChangingMode: boolean;
  ecuId: number[];
  brand: string;
};
class MainScreen extends React.PureComponent<Props, States> {
  constructor(props: Props) {
    super(props);
    const ecuId = props.navigation.getParam('ecuId');
    const brand = props.navigation.getParam('brand');

    this.state = {
      appState: '',
      isLoading: true,
      isChangingMode: false,
      ecuId,
      brand,
    };
  }

  async componentDidMount() {
    console.log('componentDidMount', 'Main');
    const openedByNotification = await checkIfAppIsOpenedByNotification(true);
    // Add app state tracker
    AppState.addEventListener('change', this._handleAppStateChange);
    this.setState({isLoading: false});

    // Check App Update
    AppVersionCheckUpdate.isNeededToUpdate();

    eventBus.addListener(EVENT_BUS.BLE_CHANGE_DATA_MODE, this._onBleChangingDataMode);
    eventBus.addListener(EVENT_BUS.BLE_CHANGE_ERROR, this._onBleError);

    // Check if ECU is error or not
    const {ecuId} = this.state;
    const ecuVer1 = ecuId[3];
    const ecuVer2 = ecuId[4];
    if (ecuVer1 === 0 && ecuVer2 === 0) {
      console.warn('ECU ERROR on Update');
      const cbConfirm = () => {
        const params = {
          ecuId: [...ecuId],
          ecuError: true,
        };
        navigationService.navigate('UpdateEcu_CheckVer', params);
      };
      const cbCancel = () => {
        navigationService.navigate('SetupDevice');
      };
      OverlayWarningEcuError_tools.showPopup(true, cbConfirm, cbCancel);
    } else {
      // To Idle mode
      ble.esyDataManager.honda.toIdleMode();
    }
  }

  componentWillUnmount() {
    console.log('componentWillUnmount', 'Main');
    // Remove App State tracker
    AppState.removeEventListener('change', this._handleAppStateChange);
    eventBus.removeListener(this._onBleChangingDataMode);
    eventBus.removeListener(this._onBleError);
  }

  _onBleChangingDataMode = (isChangingMode: boolean) => {
    if (isChangingMode) {
      this.setState({isChangingMode});
    } else {
      setTimeout(() => {
        this.setState({isChangingMode});
      }, 75);
    }
  };

  _onBleError = (error: string | Error) => {
    navigationService.navigate('MainScreen');
    responsePopup({
      title: translateBleErrorTitle(error),
      subtitle: translateBleError(error),
      touchOutsideToDismiss: false,
      retryCb: () => navigationService.navigate('SetupDevice'),
    });
  };

  handleBackPress = () => {
    console.log('MainScreen', 'handleBackPress');
    this.onBack();
    return true;
  };

  onBack = async () => {
    /**
     * Nếu đang hiển thị popup -> Tắt popup
     */
    // Popup Lỗi ECU
    const isShowEcuError = OverlayWarningEcuError_tools.isShowing();
    if (isShowEcuError) {
      return;
    }
    // Popup confirm
    const isShowConfirmReset = OverlayConfirmReset_tools.isShowing();
    if (isShowConfirmReset) {
      OverlayConfirmReset_tools.showPopup(false);
      return;
    }
    const isShowConfirmErase = OverlayConfirmErase_tools.isShowing();
    if (isShowConfirmErase) {
      OverlayConfirmErase_tools.showPopup(false);
      return;
    }
    // Đang xoá lỗi, hoặc Reset Ecu
    const isEraseOrReset = OverLayEraseError_ResetEcm_tools.isShowing();
    if (isEraseOrReset) {
      return;
    }
    // Đang đổi mode
    const isChangingMode = this.props.changingMode;
    if (isChangingMode) {
      return;
    }
    BackHandler.exitApp(); // Thoát app
  };

  _handleAppStateChange = (nextAppState: string) => {
    // Background  -> active
    if (this.state.appState.match(/background/) && nextAppState === 'active') {
      console.log('App has come to the active from background!');
    }
    // inactive|background -> active
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the active from inactive|background!');
      // Force to check Ble State
      ble.forceCheckBleStatus();
    }
    // inactive|active -> background
    if (this.state.appState.match(/inactive|active/) && nextAppState === 'background') {
      console.log('App has come to the background!');
    }
    this.setState({appState: nextAppState});
  };

  render() {
    const {isLoading, isChangingMode} = this.state;
    if (isLoading) {
      return <MyActivityIndicator />;
    }
    return (
      <BaseScreen statusbarStyle="light-content" handleBackPress={this.handleBackPress}>
        <Header />
        <TabInMain />
        <OverlayWarningEcuError
          ref={_ref => {
            OverlayWarningEcuError_tools.setRef(_ref);
          }}
        />
        <OverlayWaitingChangeMode show={isChangingMode} />
        <OverLayEraseError_ResetEcm
          ref={_ref => {
            OverLayEraseError_ResetEcm_tools.setRef(_ref);
          }}
        />
        <OverlayConfirmReset
          ref={_ref => {
            OverlayConfirmReset_tools.setRef(_ref);
          }}
        />
        <OverlayConfirmErase
          ref={_ref => {
            OverlayConfirmErase_tools.setRef(_ref);
          }}
        />
        <PopupConnectionError ref={_ref => setRef(_ref)} />
      </BaseScreen>
    );
  }
}

export default MainScreen;
