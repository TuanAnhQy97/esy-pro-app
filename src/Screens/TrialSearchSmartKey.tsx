/**
 * @flow
 */
import React from "react";
import { StyleSheet, ScrollView } from "react-native";
import HTML from "react-native-render-html";
import BaseScreen from "../Components/BaseScreen";
import NavBar from "../Components/NavBar";

import { horizontalScale } from "../ScaleUtility";
import { colors } from "../styles/colors";
import ASHelper from "../ASHelper";
import navigationService from "../services/navigationService";

type Props = {};
type States = {
  error: "no_internet" | "unknown" | "";
  isLoading: boolean;

  phone: string;
  deviceId: number[];
  storeName: string;
  addr: string;
  expiredDate: number;
};

class TrialSearchSmartKey extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  componentWillUnmount() {}

  onBack = async () => {
    console.log("TrialSearchSmartKey", "onBack");
    navigationService.goBack();
  };

  handleBackPress = () => {
    console.log("TrialSearchSmartKey", "handleBackPress");
    // works best when the goBack is async
    this.onBack();
    return true;
  };

  renderBody = () => {
    return null;
  };

  render() {
    return (
      <BaseScreen
        statusbarStyle="light-content"
        handleBackPress={this.handleBackPress}
      >
        {/* NavBar */}
        <NavBar
          onLeftPress={this.handleBackPress}
          title="Mã lỗi Smart key Honda"
        />
        <ScrollView contentContainerStyle={{ paddingHorizontal: 20 }}>
          <HTML html={BODY} />
        </ScrollView>
      </BaseScreen>
    );
  }
}

export default TrialSearchSmartKey;

const BODY = `</html>
<h1><span style="color: #ff0000;"><strong>HỆ THỐNG Ch&igrave;a th&ocirc;ng minh Smartkey</strong></span></h1>
<h2><span style="color: #ff0000;"><strong>Quy ước kỹ thuật về m&atilde; lỗi Ch&igrave;a Smartkey HONDA</strong></span></h2>
<p><strong>M&atilde; nh&aacute;y đ&egrave;n</strong><span style="font-weight: 400;"> ch&igrave;a kh&oacute;a tr&ecirc;n đồng hồ sẽ bao gồm </span><strong>2 dạng</strong> <strong>ngắn</strong><span style="font-weight: 400;"> v&agrave; </span><strong>d&agrave;i</strong><span style="font-weight: 400;"> v&agrave; khi g&otilde; v&agrave;o &ocirc; tra cứu kỹ thuật vi&ecirc;n sẽ quan s&aacute;t đ&egrave;n b&aacute;o ch&igrave;a tr&ecirc;n đồng hồ v&agrave; g&otilde; v&agrave;o phần mềm</span></p>
<p><strong>V&iacute; dụ:</strong><strong> N-D-N-D = Ngắn - D&agrave;i - Ngắn - D&agrave;i </strong><span style="font-weight: 400;">sau</span> <span style="font-weight: 400;">đ&oacute; sẽ ấn v&agrave;o tra cứu để t&igrave;m m&atilde; lỗi</span></p>
<p>&nbsp;</p>
<h2><span style="color: #ff0000;"><strong>M&atilde; N-N-D-D&nbsp;</strong></span></h2>
<p><strong>T&ecirc;n lỗi : mất kết nối ch&igrave;a th&ocirc;ng minh FOB v&agrave; SCU.</strong></p>
<p><strong>Gợi &yacute; sửa chữa :</strong></p>
<p><strong>Kiểm tra ch&igrave;a FOB v&agrave; SCU.</strong></p>
<h2><span style="color: #ff0000;"><strong>M&atilde; N-D-D-N</strong></span></h2>
<p><strong>T&ecirc;n lỗi : Lỗi đăng k&yacute;&nbsp;</strong></p>
<p><strong>Gợi &yacute; sửa chữa :</strong></p>
<p><strong>Thực hiện lại quy tr&igrave;nh đăng k&yacute; ch&igrave;a th&ocirc;ng minh FOB</strong>&nbsp;</p>
<h2><span style="color: #ff0000;"><strong>M&atilde; N-D-N-D</strong></span></h2>
<p><strong>T&ecirc;n lỗi : Lỗi đăng k&yacute; tr&ugrave;ng lặp</strong></p>
<p><strong>Gợi &yacute; sửa chữa:</strong></p>
<p><strong>Ch&igrave;a FOB đ&atilde; được đăng k&yacute; trong SCU.</strong></p>
<h2><span style="color: #ff0000;"><strong>M&atilde; N-D-N-N</strong></span></h2>
<p><strong>T&ecirc;n lỗi : FOB đ&atilde; được sử dụng</strong></p>
<p><strong>Gợi &yacute; sửa chữa :</strong></p>
<p><strong>FOB đ&atilde; được đăng k&yacute; tr&ecirc;n một xe kh&aacute;c,sử dụng 1 ch&igrave;a kh&oacute;a mới chưa đăng k&yacute; để thực hiện đăng k&yacute; tr&ecirc;n xe n&agrave;y.</strong></p>
<h2><span style="color: #ff0000;"><strong>M&atilde; N-N-N-N</strong></span></h2>
<p><strong>T&ecirc;n lỗi : Lỗi đăng k&yacute; sai khi thay SCU hoặc ECM</strong></p>
<p><strong>Gợi &yacute; sửa chữa :</strong></p>
<p><strong>Thực hiện lại quy tr&igrave;nh đăng k&yacute; ch&igrave;a th&ocirc;ng minh FOB.</strong></p>
<h2><span style="color: #ff0000;"><strong>M&atilde; D-N-N-D</strong></span></h2>
<p><strong>T&ecirc;n lỗi : M&atilde; ID bị lỗi&nbsp;</strong></p>
<p><strong>Gợi &yacute; sửa chữa :</strong></p>
<p><strong>Kiểm tra m&atilde; ID của ECM.</strong></p>
<h2><span style="color: #ff0000;"><strong>M&atilde; N-D-D-D</strong></span></h2>
<p><strong>T&ecirc;n lỗi : ECM kh&ocirc;ng x&aacute;c nhận ID</strong></p>
<p><strong>Gợi &yacute; sửa chữa :</strong></p>
<p><strong>Kiểm tra việc truyền nhận ID từ SCU tới ECM.</strong></p>
<p><br /><br /><br /></p>
</html>
`;
