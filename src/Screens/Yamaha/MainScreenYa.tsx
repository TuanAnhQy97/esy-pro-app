import React from "react";
import { AppState, BackHandler } from "react-native";
import { checkIfAppIsOpenedByNotification } from "../../services/notification/FCM";
import AppVersionCheckUpdate from "../../AppVersionCheckUpdate";
import eventBus from "../../services/eventBus";
import { EVENT_BUS, BLE_ERROR } from "../../constants";
import PopupConnectionError, {
  responsePopup,
  setRef
} from "../../Components/PopupConnectionError";
import navigationService from "../../services/navigationService";
import ble from "../../services/ble";
import BaseScreen from "../../Components/BaseScreen";
import MyActivityIndicator from "../../Components/MyActivityIndicator";
import Header from "../../Components/MainScreenYa/Header";
import TabInMain from "../../Components/MainScreenYa/Tabs/TabInMain";
import OverlayWaitingChangeMode from "../../Components/MainScreenYa/OverlayWaitingChangeMode";

import OverLayEraseError_ResetEcm, {
  tools as OverLayEraseError_ResetEcm_tools
} from "../../Components/Popup/OverLayEraseError_ResetEcm";

import OverlayConfirmErase, {
  tools as OverlayConfirmErase_tools
} from "../../Components/Popup/OverlayConfirmErase";
import {
  translateBleError,
  translateBleErrorTitle
} from "../../SharedFunctions";
import { currentRouteName } from "../../../App";

type Props = {
  dispatch: Function;
  changingMode: boolean;
  navigation: any;
};
type States = {
  appState: any;
  isLoading: boolean;
  isChangingMode: boolean;
  ecuId: number[];
  brand: string;
};
class MainScreen extends React.PureComponent<Props, States> {
  constructor(props: Props) {
    super(props);
    const ecuId = props.navigation.getParam("ecuId");
    const brand = props.navigation.getParam("brand");

    this.state = {
      appState: "",
      isLoading: true,
      isChangingMode: false,
      ecuId,
      brand
    };
  }

  async componentDidMount() {
    await checkIfAppIsOpenedByNotification(true);
    // Add app state tracker
    AppState.addEventListener("change", this._handleAppStateChange);
    this.setState({ isLoading: false });

    // Check App Update
    AppVersionCheckUpdate.isNeededToUpdate();

    eventBus.addListener(
      EVENT_BUS.BLE_CHANGE_DATA_MODE,
      this._onBleChangingDataMode
    );
    eventBus.addListener(EVENT_BUS.BLE_CHANGE_ERROR, this._onBleError);
  }

  _onBleChangingDataMode = (isChangingMode: boolean) =>
    this.setState({ isChangingMode });

  _onBleError = (error: string | Error) => {
    let popup = true;
    if (error === BLE_ERROR.KLINE_ERROR) {
      if (currentRouteName === "MainScreenYa") {
        popup = false;
      }
    }

    if (popup) {
      navigationService.navigate("MainScreenYa");
      responsePopup({
        title: translateBleErrorTitle(error),
        subtitle: translateBleError(error),
        touchOutsideToDismiss: false,
        retryCb:
          error === BLE_ERROR.KLINE_ERROR
            ? undefined
            : () => navigationService.navigate("SetupDevice"),
        retryTitle: error === BLE_ERROR.KLINE_ERROR ? "Đồng ý" : undefined
      });
    }
  };

  componentWillUnmount() {
    // Remove App State tracker
    AppState.removeEventListener("change", this._handleAppStateChange);
    eventBus.removeListener(this._onBleChangingDataMode);
    eventBus.removeListener(this._onBleError);
  }

  handleBackPress = () => {
    this.onBack();
    return true;
  };

  onBack = async () => {
    /**
     * Nếu đang hiển thị popup -> Tắt popup
     */
    // Popup confirm
    const isShowConfirmErase = OverlayConfirmErase_tools.isShowing();
    if (isShowConfirmErase) {
      OverlayConfirmErase_tools.showPopup(false);
      return;
    }
    // Đang xoá lỗi, hoặc Reset Ecu
    const isEraseOrReset = OverLayEraseError_ResetEcm_tools.isShowing();
    if (isEraseOrReset) {
      return;
    }
    // Đang đổi mode
    const isChangingMode = this.props.changingMode;
    if (isChangingMode) {
      return;
    }
    BackHandler.exitApp(); // Thoát app
  };

  _handleAppStateChange = (nextAppState: string) => {
    // Background  -> active
    if (this.state.appState.match(/background/) && nextAppState === "active") {
      console.log("App has come to the active from background!");
    }
    // inactive|background -> active
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      console.log("App has come to the active from inactive|background!");
      // Force to check Ble State
      ble.forceCheckBleStatus();
    }
    // inactive|active -> background
    if (
      this.state.appState.match(/inactive|active/) &&
      nextAppState === "background"
    ) {
      console.log("App has come to the background!");
    }
    this.setState({ appState: nextAppState });
  };

  render() {
    const { isLoading, isChangingMode } = this.state;
    if (isLoading) {
      return <MyActivityIndicator />;
    }
    return (
      <BaseScreen
        statusbarStyle="light-content"
        handleBackPress={this.handleBackPress}
      >
        <Header />
        <TabInMain />
        <OverlayWaitingChangeMode show={isChangingMode} />
        <OverLayEraseError_ResetEcm
          ref={_ref => {
            OverLayEraseError_ResetEcm_tools.setRef(_ref);
          }}
        />
        <OverlayConfirmErase
          ref={_ref => {
            OverlayConfirmErase_tools.setRef(_ref);
          }}
        />
      </BaseScreen>
    );
  }
}

export default MainScreen;
