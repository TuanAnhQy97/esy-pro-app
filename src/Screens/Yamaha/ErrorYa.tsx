// @flow
import React, { Component } from "react";
import {
  View,
  FlatList,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import Text from "../../Components/Text";
import BaseScreen from "../../Components/BaseScreen";

import NavBar from "../../Components/NavBar";
import { horizontalScale } from "../../ScaleUtility";
import MyActivityIndicator from "../../Components/MyActivityIndicator";

import OverlayExplainFIErrorYa, {
  tools as OverlayExplainFIErrorYa_tools
} from "../../Components/Popup/OverlayExplainFIErrorYa";
import { tools as OverLayWaitingKline_tools } from "../../Components/Popup/OverLayWaitingKline";
import ble from "../../services/ble";
import eventBus from "../../services/eventBus";
import { EVENT_BUS } from "../../constants";
import navigationService from "../../services/navigationService";
import { colors } from "../../styles/colors";
import ExplainErrorYamaha from "../../Library/ExplainErrorYamaha";
import Loading from "../../Components/Loading";
import { compareArrays } from "../../SharedFunctions";

type ErrorProps = {};

type States = {
  backPress?: boolean;
  errorCode: number;
  errorMem: number;
  errorList: [number, number, 0xa1 | 0x20][];
  errorType: "kline" | "obd";
  isLoading: boolean;
  isErasing: boolean;
};

class ErrorYa extends React.PureComponent<ErrorProps, States> {
  constructor(props: ErrorProps) {
    super(props);
    this.state = {
      errorCode: 0,
      errorMem: 0,
      errorList: [],
      isLoading: true,
      isErasing: false,
      errorType: "kline"
    };
  }

  componentDidMount() {
    eventBus.addListener(
      EVENT_BUS.YAMAHA_ERROR_LIST_CHANGE,
      this._onDataChange
    );
  }

  componentWillUnmount() {
    if (!this.state.backPress) {
      // Back do swipe gesture
      ble.esyDataManager.yamaha.toIdleMode();
    }
    eventBus.removeListener(this._onDataChange);
  }

  _onDataChange = (params: {
    errorA: number[];
    errorB: number[];
    notDuplicateList: number[];
    errorA1: [number, number, 0xa1][];
    error20: [number, number, 0x20][];
    errorGen: "kline" | "obd";
  }) => {
    const {
      errorA,
      errorB,
      notDuplicateList,
      errorA1,
      error20,
      errorGen
    } = params;
    this.setState({
      isLoading: false,
      errorList: [...errorA1, ...error20],
      errorType: errorGen
    });
    ble.esyDataManager.yamaha.toReadErrorMode();
  };

  handleBackPress = () => {
    console.log("ErrorYa", "handleBackPress");
    // works best when the goBack is async
    this.onBack();
    return true;
  };

  onBack = async () => {
    console.log("ErrorYa", "onBack");
    /**
     * Nếu đang hiển thị popup -> Tắt popup
     */
    // Đang chờ Kline
    const isWaitingKline = OverLayWaitingKline_tools.isShowing();
    if (isWaitingKline) {
      // Không làm gì cả
      return;
    }
    // Popup Giải thích
    const isShowExplain = OverlayExplainFIErrorYa_tools.isShowing();
    if (isShowExplain) {
      OverlayExplainFIErrorYa_tools.showPopup(false);
      return;
    }

    this.setState({ backPress: true }, () => {
      ble.esyDataManager.yamaha.toIdleMode();
      navigationService.goBack();
    });
  };

  renderBody = () => {
    if (this.state.isLoading) {
      return <MyActivityIndicator />;
    }
    if (this.state.errorList.length === 0) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <ImageBackground
            resizeMode="contain"
            style={{ height: 135, width: 135 }}
            source={require("../../../assets/Images/Share/data-storage.png")}
          />
          <Text
            style={{
              marginTop: 20,
              fontSize: 20
            }}
          >
            BỘ NHỚ LỖI TRỐNG
          </Text>
        </View>
      );
    }
    return this.renderErrorList();
  };

  renderErrorList = () => {
    const { errorList } = this.state;
    return (
      <View style={{ flex: 1, padding: 10 }}>
        <View
          style={{
            paddingTop: 5,
            backgroundColor: colors.primary_light,
            flexDirection: "row"
          }}
        >
          <Text
            style={{
              width: horizontalScale(100),
              paddingVertical: 10,
              textAlign: "center",
              fontSize: horizontalScale(18),
              color: colors.secondary_dark
            }}
          >
            MÃ LỖI
          </Text>
          <View
            style={{
              width: 1,
              height: "100%",
              backgroundColor: "gray",
              opacity: 0.1
            }}
          />
          <Text
            style={{
              flex: 1,
              paddingVertical: 10,
              paddingLeft: horizontalScale(10),
              textAlign: "center",
              fontSize: horizontalScale(18)
            }}
          >
            TÊN LỖI
          </Text>
        </View>
        <FlatList
          style={{ paddingTop: 5, flex: 1, marginTop: 23 }}
          data={errorList}
          keyExtractor={item => item.toString()}
          renderItem={this.renderItem}
        />
      </View>
    );
  };

  renderItem = ({ item }) => {
    const explain = ExplainErrorYamaha.getExplain({
      errorCode: item[1],
      errorMem: item[0],
      type: this.state.errorType
    });
    let hexStr = "";
    if (this.state.errorType === "kline") {
      hexStr = item[1].toString(16);
      if (hexStr.length === 1) {
        hexStr = `0${hexStr}`;
      }
    } else {
      const hexArr = item.slice(0, 2).map(v => {
        let hex = v.toString(16);
        if (hex.length === 1) {
          hex = `0${hex}`;
        }
        return hex;
      });
      hexStr = `P${hexArr.join("")}`;
    }
    if (hexStr.length === 1) {
      hexStr = `0${hexStr}`;
    }
    hexStr = hexStr.toUpperCase();

    return (
      <TouchableOpacity
        hitSlop={{ top: 5, bottom: 5, left: 5, right: 5 }}
        style={{
          width: "100%",
          flexDirection: "row",
          marginBottom: 15,
          minHeight: 44
        }}
        onPress={() => {
          this.setState({ errorCode: item[1], errorMem: item[0] }, () => {
            OverlayExplainFIErrorYa_tools.showPopup(true);
          });
        }}
      >
        <Text
          style={{
            width: horizontalScale(100),
            paddingVertical: 10,
            textAlign: "center",
            fontSize: horizontalScale(18),
            color: colors.primary_light,
            backgroundColor: colors.secondary_dark
          }}
        >
          {`${hexStr}`}
        </Text>
        <Text
          style={{
            flex: 1,
            paddingVertical: 10,
            paddingLeft: horizontalScale(10),
            textAlign: "center",
            fontSize: horizontalScale(18),
            backgroundColor: colors.primary_light
          }}
        >
          {explain.name}
          {item[2] === 0x20 ? " - Đã khắc phục" : ""}
        </Text>
      </TouchableOpacity>
    );
  };

  onPressErase = async () => {
    this.setState({ isErasing: true });
    try {
      await ble.esyDataManager.yamaha.toIdleMode();

      const { errorList } = this.state;
      for (let i = 0; i < errorList.length; i += 1) {
        if (errorList[i][2] === 0xa1) {
          // Skip
        } else {
          // eslint-disable-next-line no-await-in-loop
          let res: number[] = await ble.sendData({
            data: ble.generateCommand.deleteErrorYa(
              errorList[i][0],
              errorList[i][1]
            ),
            waitForResponse: true
          });
          while (res.length !== 10 || res[7] !== errorList[i][1]) {
            res = await ble.sendData({
              data: [],
              waitForResponse: true
            });
          }
        }
      }
      await ble.esyDataManager.yamaha.toReadErrorMode();
    } catch (e) {
    } finally {
      this.setState({ isErasing: false });
    }
  };

  render() {
    return (
      <BaseScreen
        statusbarStyle="light-content"
        bodyBackgroundColor="#F5F5F5"
        handleBackPress={this.handleBackPress}
      >
        {/* NavBar */}
        <NavBar onLeftPress={this.handleBackPress} title="Danh sách lỗi" />
        {this.renderBody()}
        <OverlayExplainFIErrorYa
          ref={_ref => {
            OverlayExplainFIErrorYa_tools.setRef(_ref);
          }}
          errorCode={this.state.errorCode}
          errorMem={this.state.errorMem}
          errorType={this.state.errorType}
        />
        {this.state.isLoading ||
          (this.state.errorList.length ? (
            <TouchableOpacity
              style={{
                height: 60,
                width: 200,
                borderRadius: 3,
                backgroundColor: colors.secondary_dark,
                alignItems: "center",
                justifyContent: "center",
                alignSelf: "center"
              }}
              onPress={this.onPressErase}
            >
              <Text style={{ fontSize: 18, color: "white" }}>Xoá lỗi</Text>
            </TouchableOpacity>
          ) : null)}
        {this.state.isErasing && <Loading />}
      </BaseScreen>
    );
  }
}

export default ErrorYa;
