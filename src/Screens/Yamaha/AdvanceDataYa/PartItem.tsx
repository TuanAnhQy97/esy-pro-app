import React, { Fragment } from "react";
import { TouchableOpacity, Image, View } from "react-native";

import Text from "../../../Components/Text";

import { colors } from "../../../styles/colors";
import { horizontalScale } from "../../../ScaleUtility";
import eventBus from "../../../services/eventBus";
import { EVENT_BUS } from "../../../constants";
import { splitLineFromString } from "../../../SharedFunctions";

type Props = {
  keyPart: number;
  name: string;
  value: number;
  unit: string;
  explain: string;
  isSelected: boolean;
  onPressItem: (key: number) => void;
  onPressActivate: () => void;
  testable: boolean;
  checkMethods?: string;
  standard?: string;
};

class PartItem extends React.PureComponent<Props> {
  static defaultProps = {
    name: "Nhiệt độ động cơ",
    value: 60,
    unit: "C",
    explain: "Giá trị bla bla",
    isSelected: false,
    onPressItem: () => {},
    onPressActivate: () => {},
    testable: false
  };

  timeoutTurnOff: NodeJS.Timer | null = null;

  state = { on: false };

  componentDidMount() {
    eventBus.addListener(
      EVENT_BUS.YAMAHA_ADVANCE_DATA_TRIGGER,
      this._onTrigger
    );
  }

  componentWillUnmount() {
    eventBus.removeListener(this._onTrigger);
  }

  _onTrigger = () => {
    if (this.timeoutTurnOff) {
      clearTimeout(this.timeoutTurnOff);
    }
    if (this.props.isSelected) {
      this.setState({ on: true });
      this.timeoutTurnOff = setTimeout(() => {
        this.setState({ on: false });
      }, 250);
    }
  };

  render() {
    const {
      keyPart,
      name,
      value,
      unit,
      explain,
      isSelected,
      onPressItem,
      onPressActivate,
      testable,
      checkMethods,
      standard
    } = this.props;

    const formatedValue = typeof value === "number" ? value.toFixed(1) : value;

    if (isSelected)
      return (
        <View
          style={{
            padding: 14,
            backgroundColor: "#C8E6C9"
          }}
        >
          {/* Name + value */}
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "flex-start",
              borderBottomColor: "gray",
              borderBottomWidth: 0.5
            }}
          >
            <Text
              style={{
                fontSize: horizontalScale(16),
                flex: 1
              }}
            >
              {name}
            </Text>
          </View>

          <View
            style={{
              marginTop: 10,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  height: 23,
                  width: 23,
                  backgroundColor: this.state.on ? "green" : "gray",
                  borderRadius: 23
                }}
              />
              <Text
                style={{
                  marginLeft: 10,
                  fontSize: horizontalScale(16),
                  fontWeight: "bold"
                }}
              >
                {`${formatedValue} ${unit}`}
              </Text>
            </View>
            <TouchableOpacity
              disabled={!testable}
              onPress={onPressActivate}
              hitSlop={{ right: 15, left: 15, top: 15, bottom: 15 }}
              style={{
                minHeight: 30,
                minWidth: 170,
                padding: 10,
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: colors.secondary_dark,
                opacity: testable ? 1 : 0
              }}
            >
              <Text>Kiểm tra</Text>
            </TouchableOpacity>
          </View>

          {/* Check methods + Standard */}
          {checkMethods ? (
            <Fragment>
              <Text
                style={{
                  marginTop: 10,
                  fontWeight: "bold",
                  fontSize: horizontalScale(14)
                }}
              >
                Phương pháp kiểm tra
              </Text>
              <Text
                style={{
                  marginTop: 5,
                  fontStyle: "italic",
                  fontSize: horizontalScale(14)
                }}
              >
                {splitLineFromString(checkMethods)}
              </Text>
            </Fragment>
          ) : null}
          {standard ? (
            <Fragment>
              <Text
                style={{
                  marginTop: 10,
                  fontWeight: "bold",
                  fontSize: horizontalScale(14)
                }}
              >
                Tiêu chuẩn
              </Text>
              <Text
                style={{
                  marginTop: 5,
                  fontStyle: "italic",
                  fontSize: horizontalScale(14)
                }}
              >
                {splitLineFromString(standard)}
              </Text>
            </Fragment>
          ) : null}
        </View>
      );

    return (
      <TouchableOpacity
        onPress={() => onPressItem(keyPart)}
        hitSlop={{ right: 15, left: 15, top: 5, bottom: 5 }}
        style={{
          flexDirection: "row",
          paddingHorizontal: 14,
          paddingVertical: 10,
          justifyContent: "space-between",
          alignItems: "center",
          minHeight: 40,
          backgroundColor: "#dbdbdb"
        }}
      >
        <Text style={{ flex: 1 }}>{name}</Text>
        <Image
          style={{ height: 8, width: 22, marginLeft: 20 }}
          resizeMode="contain"
          source={require("../../../../assets/Images/Share/down.png")}
        />
      </TouchableOpacity>
    );
  }
}

export default PartItem;
