/**
 * @flow
 */

const NameUnitArr: TranslateStructure[] = [];
NameUnitArr[0x01] = {
  name: 'Tín hiệu cảm biến vị trí bướm ga 1',
  unit: '',
  explain: '',

  checkMethods: `hiển thi góc mở bướm ga 
  kiểm tra vị trí đóng hoàn toàn
  kiểm tra vị trí mở hoàn toàn`,

  standard: `0-125
  ví trị đống hoàn toán :14-20
  ví trí mở hoàn toán :97-107`,
};
NameUnitArr[0x03] = {
  name: 'Áp suất khí nạp',
  unit: 'kPa',
  explain: '',

  checkMethods: `hiển thi áp suất khí nạp
  kiểm ra áp suất cổ hút 
  nhấn công tắc khởi động và kiểm tra các thay đổi ở áp suất khí nạp`,

  standard: `0-126 kPa
  khi động cơ ngứng hoạt động : hiển thị áp suất khí quyển
  khi động cơ quay dữ hiểu hiển thị sẽ thay đổi`,
};
NameUnitArr[0x05] = {
  name: 'Nhiệt độ khí nạp',
  unit: '°C',
  explain: '',

  checkMethods: `hiển thị nhiệt độ khí nạp
  kiểm tra nhiệt độ bên trong hút và hộp lọc gió`,
  standard: `-30-120°C
  động cơ nguội : nhiệt độ gần bằng nhiệt độ môi trường
  làm nóng động cơ:nhiệt độ hiển thị cao hơn 20 độ so với nhiệt độ xung quanh`,
};
NameUnitArr[0x07] = {
  name: 'Xung tốc độ bánh xe trước sau',
  unit: '',
  explain: '',
  checkMethods: `hiển thị giá trị tích lũy xung tốc độ`,
  standard: `0-999 [xung]
  bánh xe sau ngừng : kiểm tra giá trị có thay đổi
  dùng tay quay bánh xe để kiểm tra sự thay đổi`,
};
NameUnitArr[0x09] = {
  name: 'Điện áp hệ thống nhiên liệu (điện áp ắc quy)',
  unit: '',
  explain: '',

  checkMethods: `hiển thị điện áp ắc quy`,
  standard: `0-18.7V
  dữ liệu chuẩn : 12V`,
};
NameUnitArr[0x0b] = {name: 'Nhiệt độ động cơ', unit: '', explain: ''};
NameUnitArr[0x14] = {
  name: 'Công tắc chân chống phụ Dây màu xanh và vàng kết nối với ECU',
  unit: '',
  explain: '',
};
NameUnitArr[0x1e] = {
  name: 'Xi lanh số 1 hoặc mo bin sườn số 1-1',
  unit: '',
  explain: '',
  testable: true,
};
NameUnitArr[0x24] = {
  name: 'Vòi phun số 1',
  unit: '',
  explain: '',
  testable: true,
  checkMethods: `kiểm tra sự thông mạch vòi phun
  kiểm tra tình trạng vòi phun bằng nghe âm thanh hoạt động hoặc kiểm tra bằng mắt`,
  standard: `chú ý khi kiểm tra tháo jack bơm 
  kích hoạt vòi phun 5 lần với chu ki thời gian mỗi lần 1 giây`,
};
NameUnitArr[0x34] = {
  name: 'Rơ le đèn pha',
  unit: '',
  explain: '',
  testable: true,
  checkMethods: `kiểm tra rơ le mô tơ quạt bộ tản nhiệt bằng lắng nghe âm thanh hoạt động`,
  standard: `kích hoạt rơ le mô tơ với 5 chu kì trong 5 giây`,
};
NameUnitArr[0x36] = {
  name: 'Cụm ISC (điều khiển tốc độ galanty)/ FID (Cuộn dây cụm không tải nhanh)',
  unit: '',
  explain: '',
  testable: true,
};
NameUnitArr[0x3c] = {
  name: 'Mã lỗi hiển thị EEPROM',
  unit: '',
  explain: '',
  checkMethods: `hiển thị mã lôi EEPROM cho mã lỗi 44`,
  standard: `00 :không có mã lỗi lịch sử
  01 : chế độ điều chỉnh C0 xi lanh`,
};
NameUnitArr[0x3d] = {
  name: 'Hiển thị các mã lỗi đã xảy ra từ trước',
  unit: '',
  explain: '',
  checkMethods: `hiển thị số mã lỗi`,
  standard: `00 : không có mã lỗi lịch sử`,
};
NameUnitArr[0x3e] = {
  name: 'Xoá các mã lỗi cũ',
  unit: '',
  explain: '',
  testable: true,
  checkMethods: `hiển thị tổng số lỗi bao gồm cả lỗi phát hiện tại xảy ra từ khi lịch sử lỗi cuối cùng được xóa
  xóa lịch sử lỗi`,
  standard: `00 : không có lịch sử lỗi
  mã khác : hiển thị tổng số lỗi`,
};
NameUnitArr[0x43] = {
  name: 'Xoá giá trị hiệu chỉnh ISC',
  unit: '',
  explain: '',
  testable: true,
};
NameUnitArr[0x46] = {
  name: 'Số phiên bản chương trình',
  unit: '',
  explain: '',
  checkMethods: `kiểm tra số kiểm`,
  standard: `0-254`,
};
NameUnitArr[0x57] = {
  name: 'Xoá dữ liệu phản hồi O2',
  unit: '',
  explain: '',
  testable: true,
};
NameUnitArr[0x06] = {
  name: 'Nhiệt độ dung dịch làm mát',
  unit: '°C',
  explain: '',
  checkMethods: `hiện thị nhiệt độ nước làm mát
  kiểm tra nhiệt độ dung dịch nước làm mát`,
  standard: `-30-120°C
  động cơ nguội : nhiệt độ gần bằng nhiệt độ môi trường
  làm nóng động cơ : hiển thị nhiệt độ dung dịch làm mát`,
};
NameUnitArr[0x08] = {
  name: 'Cảm biến góc nghiêng',
  unit: 'V',
  explain: '',
  checkMethods: `hiển thị điện áo đầu ra cảm biến góc nghiêng`,
  standard: `0-5 (V)
  thẳng đứng :0.4v -1.4V
  lật nghiêng :3.7v-4.4V`,
};
NameUnitArr[0x30] = {
  name: 'Mobin sườn',
  unit: '',
  explain: '',
  checkMethods: `kiểm tra sự thông mạch của mobin sườn`,
  standard: `kích hoạt mobin sườn năm lần với chu kì 1 s`,
};
NameUnitArr[0x51] = {
  name: 'Mô tơ quạt',
  testable: true,
  unit: '',
  explain: '',
  checkMethods: ``,
  standard: ``,
};

type TranslateStructure = {
  name: string;
  unit: string;
  explain: string;
  testable?: boolean;
  checkMethods?: string;
  standard?: string;
};

class AdvanceDataExplainIndexYa {
  static instance: AdvanceDataExplainIndexYa;

  static getInstance(): AdvanceDataExplainIndexYa {
    if (!AdvanceDataExplainIndexYa.instance) {
      AdvanceDataExplainIndexYa.instance = new AdvanceDataExplainIndexYa();
    }
    return AdvanceDataExplainIndexYa.instance;
  }

  getTranslate = (key: number): TranslateStructure => {
    let explain = NameUnitArr[key];
    if (!explain) {
      explain = {name: 'Unknown', unit: 'Unknown', explain: ''};
    }

    return explain;
  };
}
export default AdvanceDataExplainIndexYa.getInstance();
