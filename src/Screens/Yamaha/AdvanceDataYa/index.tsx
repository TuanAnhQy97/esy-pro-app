import React, { Component } from "react";
import {
  View,
  FlatList,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import Text from "../../../Components/Text";
import BaseScreen from "../../../Components/BaseScreen";

import NavBar from "../../../Components/NavBar";
import MyActivityIndicator from "../../../Components/MyActivityIndicator";

import { tools as OverLayWaitingKline_tools } from "../../../Components/Popup/OverLayWaitingKline";

import PartItem from "./PartItem";
import eventBus from "../../../services/eventBus";
import { EVENT_BUS } from "../../../constants";
import ble from "../../../services/ble";
import AdvanceDataExplainIndexYa from "./AdvanceDataExplainIndexYa";
import bikeLibHelper from "../../../services/bikeLibHelper";
import navigationService from "../../../services/navigationService";
import Loading from "../../../Components/Loading";

type ErrorProps = {};

type States = {
  backPress?: boolean;
  data: {
    key: number;
    value: number;
    name: string;
    unit: string;
    explain?: string;
    testable?: boolean;
    checkMethods?: string;
    standard?: string;
  }[];
  selectedPart: number | null;
  isLoading: boolean;
};

class AdvanceDataYa extends React.PureComponent<ErrorProps, States> {
  constructor(props: ErrorProps) {
    super(props);

    const partList = bikeLibHelper.yamaha.getPartList() || [];

    const data = partList.map(key => {
      const translate = AdvanceDataExplainIndexYa.getTranslate(key);

      return {
        key,
        value: 0,
        ...translate
      };
    });

    this.state = {
      data,
      selectedPart: partList.length ? partList[0] : null,
      isLoading: false
    };
  }

  componentDidMount() {
    eventBus.addListener(
      EVENT_BUS.YAMAHA_ADVANCE_DATA_CHANGE,
      this._onDataChange
    );
  }

  componentWillUnmount() {
    if (!this.state.backPress) {
      // Back do swipe gesture
      ble.esyDataManager.yamaha.toIdleMode();
    }
    eventBus.removeListener(this._onDataChange);
  }

  _onDataChange = (params: { key: number; convertedValue: number }) => {
    const { key, convertedValue } = params;
    const { data } = this.state;
    data.forEach(d => {
      if (d.key === key) {
        d.value = convertedValue;
      }
    });
    this.setState({ data: [...data] });
  };

  handleBackPress = () => {
    console.log("AdvanceDataYa", "handleBackPress");
    // works best when the goBack is async
    this.onBack();
    return true;
  };

  onBack = async () => {
    console.log("AdvanceDataYa", "onBack");
    /**
     * Nếu đang hiển thị popup -> Tắt popup
     */
    // Đang chờ Kline
    const isWaitingKline = OverLayWaitingKline_tools.isShowing();
    if (isWaitingKline) {
      // Không làm gì cả
      return;
    }

    this.setState({ backPress: true }, () => {
      ble.esyDataManager.yamaha.toIdleMode();
      navigationService.goBack();
    });
  };

  renderBody = () => {
    if (this.state.data.length === 0) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <ImageBackground
            resizeMode="contain"
            style={{ height: 135, width: 135 }}
            source={require("../../../../assets/Images/Share/data-storage.png")}
          />
          <Text
            style={{
              marginTop: 20,
              fontSize: 20
            }}
          >
            KHÔNG CÓ THÔNG TIN
          </Text>
        </View>
      );
    }
    return this.renderPartList();
  };

  renderPartList = () => {
    const { data, selectedPart } = this.state;
    return (
      <View style={{ flex: 1, padding: 10 }}>
        <FlatList
          bounces={false}
          ItemSeparatorComponent={() => (
            <View style={{ width: "100%", height: 10 }} />
          )}
          ListHeaderComponent={() => <View style={{ marginTop: 23 }} />}
          style={{ paddingTop: 5, flex: 1 }}
          data={data}
          keyExtractor={item => item.key.toString()}
          renderItem={({ item }) => {
            return (
              <PartItem
                isSelected={selectedPart === item.key}
                keyPart={item.key}
                value={item.value}
                unit={item.unit}
                name={item.name}
                explain={item.explain}
                checkMethods={item.checkMethods}
                standard={item.standard}
                onPressItem={this.onPressItem}
                onPressActivate={this.onPressActive}
                testable={item.testable}
              />
            );
          }}
        />
      </View>
    );
  };

  onPressItem = async (key: number) => {
    this.setState({ selectedPart: key, isLoading: true });
    try {
      let res = await ble.sendData({
        data: ble.generateCommand.advanceSelectPart(key),
        char: ble.CHAR_DATA_ADVANCE_CONTROL,
        service: ble.SERVICE_ADVANCE_DATA,
        waitForResponse: true
      });
      while (res[3] !== key) {
        res = await ble.sendData({
          data: [],
          waitForResponse: true
        });
      }
    } catch (e) {}
    this.setState({ isLoading: false });
  };

  onPressActive = async () => {
    this.setState({ isLoading: true });
    await ble.sendData({
      data: ble.generateCommand.advanceTrigger(),
      char: ble.CHAR_DATA_ADVANCE_CONTROL,
      service: ble.SERVICE_ADVANCE_DATA
    });
    this.setState({ isLoading: false });
  };

  render() {
    const { isLoading } = this.state;
    return (
      <BaseScreen
        statusbarStyle="light-content"
        bodyBackgroundColor="#F5F5F5"
        handleBackPress={this.handleBackPress}
      >
        {/* NavBar */}
        <NavBar onLeftPress={this.handleBackPress} title="Chẩn đoán nâng cao" />
        {this.renderBody()}
        {isLoading && <Loading />}
      </BaseScreen>
    );
  }
}

export default AdvanceDataYa;
