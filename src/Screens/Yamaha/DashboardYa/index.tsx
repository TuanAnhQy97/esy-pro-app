// @flow
import React from "react";
import { View, FlatList } from "react-native";

import BaseScreen from "../../../Components/BaseScreen";
import NavBar from "../../../Components/NavBar";
import eventBus from "../../../services/eventBus";
import ble from "../../../services/ble";
import ThreeColumnInRow from "../../../Components/Table/ThreeColumnInRow";
import navigationService from "../../../services/navigationService";
// import OverlayExplainDashboard, {
//   tools as OverlayExplainDashboard_tools
// } from "./OverlayExplainDashboardYa";
import DashboardExplainIndexYa from "./DashboardExplainIndexYa";
import { EVENT_BUS } from "../../../constants";

const UpdateFrequency = 10;

type Props = {};

type States = {
  backPress?: boolean;
  data: { key: number; value: number | string }[];
  selectedKey: number;
};

class DashboardYa extends React.PureComponent<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = {
      data: [],
      selectedKey: 0
    };
    this.data = [];
    this.klineSampleCount = 0;
  }

  // eslint-disable-next-line react/sort-comp
  data: {
    key: number;
    value: number | string;
    name: string;
    pos: number;
    unit: string;
  }[];

  klineSampleCount: number;

  componentDidMount() {
    eventBus.addListener(
      EVENT_BUS.YAMAHA_DATA_LIST_CHANGE,
      this._onValueChange
    );
  }

  componentWillUnmount() {
    if (!this.state.backPress) {
      // Back do swipe gesture
      ble.esyDataManager.yamaha.toIdleMode();
    }
    eventBus.removeListener(this._onValueChange);
  }

  _onValueChange = (params: {
    data: { key: number; convertedValue: number }[];
    type: "kline" | "obd";
  }) => {
    const { data, type } = params;
    let needSort = false;
    data.forEach(p => {
      const { key, convertedValue } = p;

      let exists = false;
      this.data.some(value => {
        if (value.key === key) {
          exists = true;
          needSort = true;
          value.value = convertedValue;
          return true;
        }
      });
      if (!exists) {
        const explain =
          type === "kline"
            ? DashboardExplainIndexYa.getNameAndUnit(key)
            : DashboardExplainIndexYa.getNameAndUnitObd(key);
        const { pos, name, unit } = explain;
        this.data.push({
          key,
          value: convertedValue,
          pos,
          name,
          unit
        });
      }
    });
    if (needSort) {
      this.data.sort((a, b) => {
        return a.pos - b.pos;
      });
    }

    if (type === "kline") {
      this.klineSampleCount += 1;
    }

    if (type === "kline" && this.klineSampleCount > UpdateFrequency) {
      this.setState({ data: [...this.data] });
      this.klineSampleCount = 0;
    } else {
      this.setState({ data: [...this.data] });
    }
  };

  renderBody = () => {
    return this.renderEngineIndex();
  };

  renderEngineIndex = () => {
    return (
      <View style={{ flex: 1 }}>
        {this.renderDataListHeader()}
        {this.renderDataListBody()}
      </View>
    );
  };

  renderDataListHeader = () => {
    return (
      <View
        style={{
          paddingTop: 5,
          backgroundColor: "green"
        }}
      >
        <ThreeColumnInRow
          value1="Tên"
          value2="Giá trị"
          value3="Đơn vị"
          disabled
        />
      </View>
    );
  };

  renderDataListBody = () => {
    const { data } = this.state;

    return (
      <FlatList
        style={{
          paddingTop: 5,
          height: "100%"
        }}
        data={data}
        keyExtractor={item => item.key.toString()}
        renderItem={this.renderItem}
      />
    );
  };

  renderItem = ({ item, index }) => {
    const { value, name, unit } = item;
    let backgroundColor;
    if (index % 2) {
      backgroundColor = "#C8E6C9";
    }
    return (
      <ThreeColumnInRow
        value1={name}
        value2={typeof value === "number" ? value.toFixed(1) : value}
        value3={unit}
        backgroundColor={backgroundColor}
        onPress={() => {
          // this.onPressDetail(item.key);
        }}
      />
    );
  };

  onPressDetail = (key: number) => {
    this.setState({ selectedKey: key }, () => {
      // OverlayExplainDashboard_tools.showPopup(true);
    });
  };

  handleBackPress = () => {
    console.log("DashboardYa", "handleBackPress");
    // works best when the goBack is async
    this.onBack();
    return true;
  };

  onBack = () => {
    /**
     * Nếu đang hiển thị popup -> Tắt popup
     */
    // Đang chờ Kline
    // const isWaitingKline = OverLayWaitingKline_tools.isShowing();
    // if (isWaitingKline) {
    //   // Không làm gì cả
    //   return;
    // }

    this.setState({ backPress: true }, () => {
      ble.esyDataManager.yamaha.toIdleMode();
      navigationService.goBack();
    });
  };

  render() {
    return (
      <BaseScreen
        statusbarStyle="light-content"
        handleBackPress={this.handleBackPress}
      >
        {/* NavBar */}
        <NavBar onLeftPress={this.handleBackPress} title="Thông số" />
        {this.renderBody()}
        {/* <OverlayExplainDashboard
          indexOrder={this.state.selectedPos}
          ref={_ref => {
            OverlayExplainDashboard_tools.setRef(_ref);
          }}
        /> */}
      </BaseScreen>
    );
  }
}

/**
 * Sắp xếp lại thứ tự các thông số động cơ
 * @param {*} data DashboardYa data
 */
function arrangePosition(
  data1: number[],
  data2: number[],
  /**
   * Extra
   */
  extraData1: number[],
  extraData2: number[],
  extraData3: number[],
  extraData4: number[]
): number[] {
  let finalArr;
  // Đã có dữ liệu
  if (data1.length !== 0) {
    // Lấy các số chính trong combo data 1
    finalArr = data1.slice(0, 9); // SUM = 9
    // // Chèn giá trị điện áp
    // // Temp Volt
    finalArr.splice(3, 0, extraData1[1]);
    // // TPS Volt
    finalArr.splice(8, 0, extraData1[0]);
    // // MAP Volt
    finalArr.splice(10, 0, extraData1[3]);
    // // IAT Volt
    finalArr.splice(12, 0, extraData1[2]); // SUM = 9 + 4 = 13

    // Đẩy nốt thông số còn lại
    //  Data 2 - Oxy
    finalArr.push(data2[0]); // SUM = 14
    //  ex Data 1 - Pull
    finalArr.push(extraData1[4]);
    finalArr.push(extraData1[5]); // SUM = 16
    //  ex Data 2 - Tỉ lệ xăng/gió, Nung Oxy
    finalArr.push(extraData2[0]);
    finalArr.push(extraData2[1]); // SUM = 18
    // ex Data 3 - Bộ nhớ mã lỗi (Mill), BA, MAP trung bình
    finalArr.push(extraData3[0]);
    finalArr.push(extraData3[1]); // SUM =
    finalArr.push(extraData3[2]);
    // ex Data 4 - Bộ bơm xăng, Chân chống cạnh, Nút Đề, Quạt gió...
    finalArr.push(...extraData4.slice(0, 11)); // SUM  =
  }
  // Chưa có dữ liệu
  else {
    finalArr = [];
  }

  return finalArr;
}

export default DashboardYa;
