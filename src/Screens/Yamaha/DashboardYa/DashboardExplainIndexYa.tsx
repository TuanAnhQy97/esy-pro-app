/**
 * @flow
 */

const NameUnitArr: ExplainStructure[] = [];
let pos = 0;
NameUnitArr[0x001] = {
  name: 'Vòng tua máy',
  unit: 'rpm',
  pos: pos += 1,
};
NameUnitArr[0x101] = {
  name: 'Nhiệt độ động cơ',
  unit: '°C',
  pos: pos += 1,
};

NameUnitArr[0x05] = {name: 'Nhiệt độ khí nạp', unit: '°C', pos: pos += 1};
NameUnitArr[0x08] = {name: 'BARO', unit: '', pos: pos += 1};
NameUnitArr[0x80] = {name: 'Vận tốc', unit: 'km/h', pos: pos += 1};
NameUnitArr[0x81] = {name: 'Điện áp bình', unit: 'Volt', pos: pos += 1};
NameUnitArr[0x82] = {name: 'TPS Volt', unit: 'Volt', pos: pos += 1};
NameUnitArr[0x84] = {name: 'Cảm biến O2', unit: 'Volt', pos: pos += 1};
NameUnitArr[0x86] = {name: 'MAP', unit: 'kPa', pos: pos += 1};
NameUnitArr[0x88] = {name: 'Góc bướm ga', unit: '°', pos: pos += 1};
NameUnitArr[0x96] = {name: 'Number of fault deleti', unit: '', pos: pos += 1};
NameUnitArr[0x98] = {name: 'Thời gian phun', unit: 'ms', pos: pos += 1};
NameUnitArr[0x99] = {name: 'Thời gian đánh lửa', unit: 'ms', pos: pos += 1};
NameUnitArr[0x9a] = {name: 'Van ISC', unit: '', pos: pos += 1};
NameUnitArr[0x9b] = {name: 'O2 sensor fuel trim', unit: '', pos: pos += 1};
NameUnitArr[0x0c] = {name: 'APS', unit: 'Volt', pos: pos += 1};
NameUnitArr[0x83] = {name: 'Lean angle sensor or O', unit: '', pos: pos += 1};
NameUnitArr[0x85] = {name: 'O2 sensor 2', unit: 'Volt', pos: pos += 1};
NameUnitArr[0x87] = {name: 'Áp suất khí nạp', unit: 'kPa', pos: pos += 1};
NameUnitArr[0x89] = {name: 'APS', unit: '°', pos: pos += 1};
NameUnitArr[0x90] = {name: 'Mode', unit: '', pos: pos += 1};
NameUnitArr[0x91] = {name: 'Gear position', unit: '', pos: pos += 1};

NameUnitArr[0x192] = {name: 'Throttle switch', unit: '', pos: pos += 1};
NameUnitArr[0x292] = {name: 'Parking switch', unit: '', pos: pos += 1};
NameUnitArr[0x392] = {name: 'Override switch', unit: '', pos: pos += 1};
NameUnitArr[0x492] = {name: 'Brake switch', unit: '', pos: pos += 1};

NameUnitArr[0x0a2] = {name: 'I-Shift switch', unit: '', pos: pos += 1};
NameUnitArr[0x1a2] = {name: 'Ycc-AT mode switch', unit: '', pos: pos += 1};

NameUnitArr[0xa1] = {
  name: 'Primary sheave position',
  unit: '',
  pos: pos += 1,
};

NameUnitArr[0xa3] = {name: 'I-shift position', unit: '', pos: pos += 1};
NameUnitArr[0xa0] = {name: 'YCC-AT motor duty con', unit: '', pos: pos += 1};

const NameUnitArrOBD: ExplainStructure[] = [];
let posObd = 0;
NameUnitArrOBD[5] = {
  name: 'Vòng tua máy',
  unit: 'rpm',
  pos: posObd += 1,
};
NameUnitArrOBD[7] = {
  name: 'Vận tốc',
  unit: 'km/h',
  pos: posObd += 1,
};
NameUnitArrOBD[8] = {
  name: 'Thời gian phun',
  unit: 'ms',
  pos: posObd += 1,
};
NameUnitArrOBD[9] = {
  name: 'Góc đánh lửa ',
  unit: '°',
  pos: posObd += 1,
};
NameUnitArrOBD[10] = {
  name: 'Giá trị % van ISC',
  unit: '',
  pos: posObd += 1,
};
NameUnitArrOBD[11] = {
  name: 'Điện áp bình',
  unit: 'Volt',
  pos: posObd += 1,
};
NameUnitArrOBD[12] = {
  name: 'TPS (Volt)',
  unit: 'Volt',
  pos: posObd += 1,
};
NameUnitArrOBD[13] = {
  name: 'TPS',
  unit: '°',
  pos: posObd += 1,
};
NameUnitArrOBD[14] = {
  name: 'Điện áp cảm biến góc nghiêng',
  unit: 'Volt',
  pos: posObd += 1,
};
NameUnitArrOBD[15] = {
  name: 'Góc nghiêng độ',
  unit: '°',
  pos: posObd += 1,
};
NameUnitArrOBD[16] = {
  name: 'Điện áp oxygen 1',
  unit: 'Volt',
  pos: posObd += 1,
};
NameUnitArrOBD[17] = {
  name: 'Điện áp oxygen 2',
  unit: 'Volt',
  pos: posObd += 1,
};
NameUnitArrOBD[18] = {
  name: 'Điện áp cảm biến áp suất dầu',
  unit: 'Volt',
  pos: posObd += 1,
};
NameUnitArrOBD[19] = {
  name: 'Nhiệt độ động cơ ',
  unit: '°C',
  pos: posObd += 1,
};
NameUnitArrOBD[20] = {
  name: 'Nhiệt độ cảm biến khí nạp IAT',
  unit: '°C',
  pos: posObd += 1,
};
NameUnitArrOBD[21] = {
  name: 'Áp suất cảm biến khí nạp MAP',
  unit: 'kPa',
  pos: posObd += 1,
};
NameUnitArrOBD[22] = {
  name: 'Áp suất cảm biến khí nạp MAP 2',
  unit: 'kPa',
  pos: posObd += 1,
};
NameUnitArrOBD[23] = {
  name: 'Áp suất cảm biến không khí',
  unit: 'kPa',
  pos: posObd += 1,
};
NameUnitArrOBD[24] = {
  name: 'Số',
  unit: '',
  pos: posObd += 1,
};
NameUnitArrOBD[25] = {
  name: 'Chế độ hoạt động',
  unit: '',
  pos: posObd += 1,
};
NameUnitArrOBD[26] = {
  name: 'Chưa xác định',
  unit: '',
  pos: posObd += 1,
};
NameUnitArrOBD[27] = {
  name: 'Lưu lượng Oxy bù',
  unit: '%',
  pos: posObd += 1,
};
NameUnitArrOBD[28] = {
  name: 'Số lỗi',
  unit: '',
  pos: posObd += 1,
};
NameUnitArrOBD[29] = {
  name: 'Lỗi hiện tại',
  unit: '',
  pos: posObd += 1,
};
NameUnitArrOBD[31] = {
  name: 'Tạng thái lỗi hệ thống',
  unit: '',
  pos: posObd += 1,
};
NameUnitArrOBD[32] = {
  name: 'Hiệu suất động cơ',
  unit: '%',
  pos: posObd += 1,
};
NameUnitArrOBD[33] = {
  name: 'Van SACV',
  unit: '',
  pos: posObd += 1,
};
NameUnitArrOBD[34] = {
  name: 'Cắt nhiên liệu ngắn hạn',
  unit: '',
  pos: posObd += 1,
};
NameUnitArrOBD[35] = {
  name: 'Cắt nhiên liệu dài hạn',
  unit: '',
  pos: posObd += 1,
};
NameUnitArrOBD[36] = {
  name: 'Cắt nhiên liệu ngắn hạn 2',
  unit: '%',
  pos: posObd += 1,
};
NameUnitArrOBD[37] = {
  name: 'Cắt nhiên liệu dài hạn 2',
  unit: '%',
  pos: posObd += 1,
};

type ExplainStructure = {
  name: string;
  unit: string;
  pos: number;
};

class DashboardExplainIndexYa {
  static instance: DashboardExplainIndexYa;

  static getInstance(): DashboardExplainIndexYa {
    if (!DashboardExplainIndexYa.instance) {
      DashboardExplainIndexYa.instance = new DashboardExplainIndexYa();
    }
    return DashboardExplainIndexYa.instance;
  }

  getNameAndUnit = (key: number): ExplainStructure => {
    let explain = NameUnitArr[key];
    if (!explain) {
      explain = {name: 'Unknown', unit: 'Unknown', pos: 100000};
    }

    return explain;
  };

  getNameAndUnitObd = (key: number): ExplainStructure => {
    let explain = NameUnitArrOBD[key];
    if (!explain) {
      explain = {name: 'Unknown', unit: 'Unknown', pos: 100000};
    }

    return explain;
  };
}
export default DashboardExplainIndexYa.getInstance();
