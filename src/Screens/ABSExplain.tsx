/**
 * @flow
 */
import React from "react";
import {
  StyleSheet,
  Dimensions,
  FlatList,
  TouchableOpacity,
  Text,
  View
} from "react-native";
import HTML from "react-native-render-html";
import BaseScreen from "../Components/BaseScreen";
import NavBar from "../Components/NavBar";

import { horizontalScale } from "../ScaleUtility";
import { colors } from "../styles/colors";
import ASHelper from "../ASHelper";
import navigationService from "../services/navigationService";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

type Props = {};
type States = {
  error: "no_internet" | "unknown" | "";
  isLoading: boolean;

  phone: string;
  deviceId: number[];
  storeName: string;
  addr: string;
  expiredDate: number;
};

const data = [{ title: "", content: "" }];

class ABSExplain extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  componentWillUnmount() {}

  onBack = async () => {
    console.log("ABSExplain", "onBack");
    navigationService.goBack();
  };

  handleBackPress = () => {
    console.log("ABSExplain", "handleBackPress");
    // works best when the goBack is async
    this.onBack();
    return true;
  };

  render() {
    const data = this.props.navigation.getParam("data", {});

    return (
      <BaseScreen
        statusbarStyle="light-content"
        handleBackPress={this.handleBackPress}
      >
        {/* NavBar */}
        <NavBar onLeftPress={this.handleBackPress} title="Mã lỗi ABS Honda" />

        <View style={styles.body}>
          <Text style={[styles.title, { textTransform: "uppercase" }]}>
            {data.title}
          </Text>
          <Text style={styles.content}>{data.content}</Text>
        </View>
      </BaseScreen>
    );
  }
}

export default ABSExplain;

const styles = StyleSheet.create({
  body: { paddingHorizontal: 15 },
  title: {
    fontSize: 18,
    marginTop: 15,
    fontWeight: "bold",
    paddingVertical: 5
  },
  content: { marginTop: 10, fontSize: 20, lineHeight: 30 }
});
