/**
 * @flow
 */
import React from "react";
import { View, TouchableOpacity } from "react-native";
import Text from "../../../Components/Text";
import { MaterialIndicator } from "react-native-indicators";
import { colors } from "../../../styles/colors";
import { horizontalScale } from "../../../ScaleUtility";
type States = {};

class Processing extends React.PureComponent<{}, States> {
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          paddingHorizontal: 19
        }}
      >
        {/*  */}
        <MaterialIndicator color={colors.secondary_dark} size={100} />
        <Text style={{ fontSize: horizontalScale(20), marginVertical: 15 }}>
          Đang phân tích
        </Text>
        {/* Điều kiện + ý nghĩa của phép phân tích */}
        <Text style={{ textAlign: "center", color: colors.third_dark }}>
          Đánh giá sự cố, hoạt động sai lệch của hệ thống điện khi xe không nổ
        </Text>
      </View>
    );
  }
}

export default Processing;
