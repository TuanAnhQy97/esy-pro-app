/**
 * @flow
 */
import React from "react";
import { View, TouchableOpacity } from "react-native";
import Text from "../../../../Components/Text";
import { tools as OverlayExplainFIError_tools } from "../../../../Components/Popup/OverlayExplainFIError";

import { horizontalScale, verticalScale } from "../../../../ScaleUtility";
import ExplainError from "../../../../Library/ExplainError";
import SharedStyles from "./SharedStyles";
import { colors } from "../../../../styles/colors";

type States = {};

class DisplayErrorList extends React.PureComponent<
  { errorList: number[][], setStateAutoCheckOne: Function },
  States
> {
  render() {
    return (
      <View style={{}}>
        {/* Lỗi FI */}
        <Text style={SharedStyles.header}>Lỗi FI từng xảy ra</Text>
        {this.renderErrorList()}
      </View>
    );
  }
  renderErrorList() {
    // trường hợp không có lỗi
    if (this.props.errorList.length === 0) {
      return (
        <View style={[SharedStyles.contentWrapper]}>
          <View style={{ flexDirection: "row" }}>
            <View style={SharedStyles.cirlceNotError} />
            <Text style={SharedStyles.contentTextNoError}>
              Không có bản ghi nào
            </Text>
          </View>
        </View>
      );
    } else {
      // Có lỗi
      return this.props.errorList.map((error, index) => {
        let explain = ExplainError.getExplain(error[0], error[1]);
        let errorName = explain.name;
        let effect = explain.effect;
        return (
          <TouchableOpacity
            onPress={() => this.onPressItem(error)}
            key={index.toString()}
            style={[SharedStyles.contentWrapper]}
          >
            <View style={{ flexDirection: "row" }}>
              <View style={SharedStyles.circleError} />
              <View style={{ paddingRight: 17, paddingLeft: 14 }}>
                <Text
                  style={{
                    fontSize: horizontalScale(18),
                    marginTop: 9,
                    paddingBottom: 6.5
                  }}
                >
                  {error[0]}-{error[1]} {errorName}
                </Text>
                <View
                  style={{
                    height: 1,
                    backgroundColor: "#70707055"
                  }}
                />
                <Text
                  style={{
                    fontSize: horizontalScale(18),
                    marginTop: 6.5,
                    marginBottom: 8
                  }}
                >
                  {effect}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        );
      });
    }
  }
  onPressItem(error: number[]) {
    // Update state cho AutoCheckOne
    this.props.setStateAutoCheckOne({
      errorCode: error[0],
      errorState: error[1]
    });
    // show Popup
    OverlayExplainFIError_tools.showPopup(true);
  }
}

export default DisplayErrorList;
