/**
 * @flow
 */
import { StyleSheet, Platform } from "react-native";
import { colors } from "../../../../styles/colors";
import { horizontalScale } from "../../../../ScaleUtility";

const supportElevation =
  Platform.OS === "android" ? Platform.Version >= 21 : true;

const styles = StyleSheet.create({
  contentWrapper: {
    backgroundColor: "#fffffc",
    borderRadius: 8,
    marginBottom: 18,
    marginHorizontal: 13,
    /**
     * Android
     */
    elevation: 2,
    borderWidth: supportElevation ? 0 : 1,
    borderColor: supportElevation ? undefined : colors.third_dark,
    /**
     * iOS
     */
    shadowOpacity: 0.2,
    shadowRadius: 3,
    shadowOffset: { height: 2, width: 2 }
  },
  contentTextNoError: {
    // color: colors.primary_light
    fontSize: horizontalScale(18),
    paddingVertical: 8,
    paddingHorizontal: 17
  },
  header: {
    paddingHorizontal: 19,
    fontSize: horizontalScale(20),
    fontWeight: "bold",
    paddingTop: 21,
    paddingBottom: 11,
    color: colors.third_dark
  },
  cirlceNotError: {
    height: 10,
    width: 10,
    borderRadius: 10,
    backgroundColor: colors.secondary_dark,
    marginTop: 14,
    marginLeft: 5
  },
  circleError: {
    height: 10,
    width: 10,
    borderRadius: 10,
    backgroundColor: "#FFB300",
    marginTop: 14,
    marginLeft: 5
  }
});

export default styles;
