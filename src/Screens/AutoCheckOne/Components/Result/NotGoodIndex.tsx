/**
 * @flow
 */
import React from "react";
import { View, TouchableOpacity, ActivityIndicator } from "react-native";
import Text from "../../../../Components/Text";

import { horizontalScale } from "../../../../ScaleUtility";
import SharedStyles from "./SharedStyles";

import DiagnosticHelper, {
  DATA1234
} from "../../../../DiagnosticHelper/DiagnosticHelper";
import ExplainError from "../../../../Library/ExplainError";
import { tools as OverlayExplainFIError_tools } from "../../../../Components/Popup/OverlayExplainFIError";

type States = { isLoading: boolean; notGoodthings: number[] };

class NotGoodIndex extends React.PureComponent<
  {
    dataList: DATA1234;
    setStateAutoCheckOne: Function;
  },
  States
> {
  constructor() {
    super();
    this.state = { isLoading: true, notGoodthings: [] };
  }

  componentDidMount() {
    const { dataList } = this.props;
    // Analyze
    const notGoodthings = DiagnosticHelper.analyzeAutoCheckOneData(dataList);
    this.setState({ isLoading: false, notGoodthings });
  }

  componentWillReceiveProps(newProps: any) {
    const { dataList } = newProps;
    // Analyze
    const notGoodthings = DiagnosticHelper.analyzeAutoCheckOneData(dataList);
    this.setState({ isLoading: false, notGoodthings });
  }

  onPressItem(error: number[]) {
    // Update state cho AutoCheckOne
    this.props.setStateAutoCheckOne({
      errorCode: error[0],
      errorState: error[1]
    });
    // show Popup
    OverlayExplainFIError_tools.showPopup(true);
  }

  renderNotGoodIndex = () => {
    // Đang phân tích dữ liệu
    if (this.state.isLoading) {
      return <ActivityIndicator size="large" />;
    }
    // Đã phân tích xong
    if (this.state.notGoodthings.length === 0) {
      // Không có sai lệch gì cả
      return (
        <View style={[SharedStyles.contentWrapper]}>
          <View style={{ flexDirection: "row" }}>
            <View style={SharedStyles.cirlceNotError} />
            <Text style={SharedStyles.contentTextNoError}>
              Không phát hiện dấu hiệu bất thường khác
            </Text>
          </View>
        </View>
      );
    }
    // Có sai lệch
    return this.state.notGoodthings.map((value, index) => {
      const explain = ExplainError.getExplain(value, 0);
      return (
        <TouchableOpacity
          onPress={() => this.onPressItem([value, 0])}
          key={index.toString()}
          style={[SharedStyles.contentWrapper]}
        >
          <View style={{ flexDirection: "row" }}>
            <View style={SharedStyles.circleError} />
            <View style={{ paddingRight: 17, paddingLeft: 14 }}>
              <Text
                style={{
                  fontSize: horizontalScale(18),
                  marginVertical: 9
                }}
              >
                {explain.name}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      );
    });
  };

  render() {
    // TODO: Tính toán để hiện thị kết quả
    return (
      <View style={{}}>
        <Text style={SharedStyles.header}>Sai lệch cảm biến khác</Text>
        {this.renderNotGoodIndex()}
      </View>
    );
  }
}

export default NotGoodIndex;
