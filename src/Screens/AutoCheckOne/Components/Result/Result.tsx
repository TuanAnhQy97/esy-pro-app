/**
 * @flow
 */
import React from "react";
import { ScrollView } from "react-native";
import {  AUTO_CHECK_ONE_RESULT } from "../../../../Store/AutoCheckOneReducer";
import DisplayErrorList from "./DisplayErrorList";
import NotGoodIndex from "./NotGoodIndex";
import BottomSpaceForScrollView from "../../../../Components/BottomSpaceForScrollView";

type States = {};

class Result extends React.PureComponent<
  { result: AUTO_CHECK_ONE_RESULT, setStateAutoCheckOne: Function },
  States
> {
  render() {
    // TODO: Tính toán để hiện thị kết quả
    return (
      <ScrollView style={{ flex: 1 }}>
        {/* Lỗi FI */}
        <DisplayErrorList
          errorList={this.props.result.errorList}
          setStateAutoCheckOne={this.props.setStateAutoCheckOne}
        />
        {/* Các chỉ số sai lệch */}
        <NotGoodIndex
          dataList={this.props.result.dataList}
          setStateAutoCheckOne={this.props.setStateAutoCheckOne}
        />
        <BottomSpaceForScrollView />
      </ScrollView>
    );
  }
}

export default Result;
