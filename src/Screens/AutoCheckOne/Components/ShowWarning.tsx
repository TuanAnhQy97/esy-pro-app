/**
 * @flow
 */
import React from "react";
import { View } from "react-native";
import Text from "../../../Components/Text";
import { horizontalScale } from "../../../ScaleUtility";

type States = {};

class ShowWarning extends React.PureComponent<{}, States> {
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          paddingHorizontal: 19
        }}
      >
        {/* Hiển thị hướng dẫn + điều kiện */}
        <Text style={{ fontSize: horizontalScale(20), marginBottom: 5 }}>
          Để đánh giá từ trạng cảm biến
        </Text>
        <Text style={{ fontWeight: "bold", fontSize: horizontalScale(20) }}>
          Vui lòng không nổ máy
        </Text>
      </View>
    );
  }
}

export default ShowWarning;
