/**
 * @flow
 */
import React from "react";

import BaseScreen from "../../Components/BaseScreen";
import NavBar from "../../Components/NavBar";
import ShowWarning from "./Components/ShowWarning";
import Result from "./Components/Result/Result";
import Processing from "./Components/Processing";
import OverlayExplainFIError, {
  tools as OverlayExplainFIError_tools
} from "../../Components/Popup/OverlayExplainFIError";
import { tools as OverLayWaitingKline_tools } from "../../Components/Popup/OverLayWaitingKline";

import MyActivityIndicator from "../../Components/MyActivityIndicator";

import ble from "../../services/ble";
import navigationService from "../../services/navigationService";
import { AUTO_CHECK_ONE_PROGRESS, EVENT_BUS } from "../../constants";
import { AUTO_CHECK_ONE_RESULT } from "../../Store/AutoCheckOneReducer";
import eventBus from "../../services/eventBus";

type Props = {};
type States = {
  errorCode: number;
  errorState: number;
  backPress?: boolean;
  progress: string;
  result: AUTO_CHECK_ONE_RESULT;
};

class AutoCheckOne extends React.PureComponent<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = {
      errorCode: 0,
      errorState: 0,
      progress: AUTO_CHECK_ONE_PROGRESS.INIT,
      result: {
        errorList: [],
        dataList: { data1: [], data2: [], data3: [], data4: [] }
      }
    };
  }

  componentDidMount() {
    eventBus.addListener(
      EVENT_BUS.BIKE_AUTO_CHECK_ONE_PROGRESS_CHANGE,
      this._onProgressChange
    );

    eventBus.addListener(
      EVENT_BUS.BIKE_AUTO_CHECK_ONE_RESULT_CHANGE,
      this._onResultChange
    );
  }

  componentWillUnmount() {
    if (!this.state.backPress) {
      // Back do swipe gesture
      ble.esyDataManager.honda.toIdleMode();
    }
    eventBus.removeListener(this._onProgressChange);
    eventBus.removeListener(this._onResultChange);
  }

  _onProgressChange = (progress: string) => this.setState({ progress });

  _onResultChange = (result: AUTO_CHECK_ONE_RESULT) =>
    this.setState({ result });

  handleBackPress = () => {
    console.log("AutoCheckOne", "handleBackPress");
    // works best when the goBack is async
    this.onBack();
    return true;
  };

  onBack = async () => {
    /**
     * Nếu đang hiển thị popup -> Tắt popup
     */
    // Đang chờ Kline
    const isWaitingKline = OverLayWaitingKline_tools.isShowing();
    if (isWaitingKline) {
      // Không làm gì cả
      return;
    }
    // Popup Giải thích
    const isShowFIError = OverlayExplainFIError_tools.isShowing();
    if (isShowFIError) {
      OverlayExplainFIError_tools.showPopup(false);
      return;
    }

    this.setState({ backPress: true }, () => {
      ble.esyDataManager.honda.toIdleMode();
      navigationService.goBack();
    });
  };

  /**
   * Set state of this Component
   * @param {*} params
   */
  setStateAutoCheckOne = (params: any) => {
    this.setState({ ...params });
  };

  renderBody = () => {
    const { result, progress } = this.state;
    switch (progress) {
      case AUTO_CHECK_ONE_PROGRESS.INIT:
        return <MyActivityIndicator />;
      case AUTO_CHECK_ONE_PROGRESS.SHOW_WARNING:
        return <ShowWarning />;
      case AUTO_CHECK_ONE_PROGRESS.PROCESSING:
        return <Processing />;
      case AUTO_CHECK_ONE_PROGRESS.SHOW_RESULT:
        return (
          <Result
            result={result}
            setStateAutoCheckOne={this.setStateAutoCheckOne}
          />
        );
      default:
        return null;
    }
  };

  render() {
    return (
      <BaseScreen>
        <NavBar title="Đánh giá cảm biến" onLeftPress={this.handleBackPress} />
        {this.renderBody()}
        <OverlayExplainFIError
          ref={_ref => {
            OverlayExplainFIError_tools.setRef(_ref);
          }}
          errorCode={this.state.errorCode}
          errorState={this.state.errorState}
        />
      </BaseScreen>
    );
  }
}
export default AutoCheckOne;
