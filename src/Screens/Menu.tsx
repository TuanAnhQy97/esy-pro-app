/**
 * @flow
 */
import React from "react";
import {
  View,
  TouchableOpacity,
  ImageBackground,
  Alert,
  StyleSheet,
  Text,
  Platform
} from "react-native";
import BaseScreen from "../Components/BaseScreen";
import NavBar from "../Components/NavBar";
import { colors } from "../styles/colors";
import navigationService from "../services/navigationService";
import ASHelper from "../ASHelper";

type Props = {};
type States = {};

class Menu extends React.PureComponent<Props, States> {
  render() {
    return (
      <BaseScreen>
        <NavBar
          title="Chức năng"
          leftIconSource={null}
          onLeftPress={() => {}}
        />
        <TouchableOpacity
          style={styles.btn}
          onPress={async () => {
            const device = await ASHelper.loadDeviceInfoFromAS();
            if (Platform.OS === "android") {
              const CheckGpsState = require("../../NativeModules/CheckGpsState");
              const isGpsOn = await CheckGpsState.isGpsOn();
              if (isGpsOn) {
                navigationService.navigate("ScanningScreen", { device });
              } else {
                Alert.alert(
                  "GPS không được bật",
                  "Nếu bạn ứng dụng không thể tìm thấy thiết bị. Vui lòng bật GPS và thử lại",
                  [
                    { text: "Bỏ qua" },
                    { text: "Bật GPS", onPress: CheckGpsState.openGpsSetting }
                  ]
                );
              }
            } else {
              navigationService.navigate("ScanningScreen", { device });
            }
          }}
        >
          <Text style={styles.btnText}>Kết nối với thiết bị</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => {
            navigationService.navigate("TrialList");
          }}
        >
          <Text style={styles.btnText}>Tra cứu mã lỗi</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => {
            navigationService.navigate("UserInfo");
          }}
        >
          <Text style={styles.btnText}>Thông tin tài khoản</Text>
        </TouchableOpacity>
      </BaseScreen>
    );
  }
}

export default Menu;

const styles = StyleSheet.create({
  btn: {
    marginTop: 20,
    height: 130,
    marginHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.secondary_dark
  },
  btnText: {
    color: "white",
    fontSize: 20
  }
});
