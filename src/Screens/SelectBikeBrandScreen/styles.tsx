import { StyleSheet } from "react-native";

export default StyleSheet.create({
  scene: {
    flex: 1
  },
  scrollView: {
    flex: 1
  }
});
