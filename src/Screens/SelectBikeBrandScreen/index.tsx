import React from "react";
import { View, ScrollView, Alert } from "react-native";

import { cpus } from "os";
import withSafeArea from "../../Components/withSafeViewArea";
import styles from "./styles";
import BikeManufactureItem, {
  BIKE_MANUFACTURE_DATA
} from "../../Components/BikeManufactureItem";
import NavBar from "../../Components/NavBar";
import Loading from "../../Components/Loading";
import PopupConnectionError, {
  setRef,
  responsePopup
} from "../../Components/PopupConnectionError";
import ble from "../../services/ble";
import {
  compareArrays,
  translateBleError,
  compareVersions,
  translateBleErrorTitle
} from "../../SharedFunctions";
import {
  ESY_INIT_ECU_SUCCEED,
  BLE_ERROR,
  BLE_RESPONSE_DATA_TYPE,
  DEVICE_VERSION_2,
  BIKE_BRAND_LIST,
  EVENT_BUS
} from "../../constants";
import navigationService from "../../services/navigationService";
import bikeLibHelper from "../../services/bikeLibHelper";
import eventBus from "../../services/eventBus";
import OverlaySwitchKey from "../../Components/Popup/OverlaySwitchKey";
import {
  ARRAY_SWITCH_KEY_REQUEST,
  ARRAY_SWITCH_KEY_OKAY,
  ARRAY_SWITCH_KEY_FAILED
} from "../../services/ble/esyDataManager/yamaha/yamaha";

const DATA: BIKE_MANUFACTURE_DATA[] = [
  {
    name: "Dòng xe Honda",
    key: BIKE_BRAND_LIST.HONDA,
    licenseTime: 1,
    imageSource: require("../../../assets/Images/Share/Honda.png")
  },
  {
    name: "Dòng xe Yamaha",
    key: BIKE_BRAND_LIST.YAMAHA,
    licenseTime: 0,
    imageSource: require("../../../assets/Images/Share/Yamaha.png")
  }
];

type Props = {};
type States = {
  isSettingUp: boolean;
  deviceVersion: number[];
  userInfo: any;
  ecuId: number[];
};

class SelectBikeBrandScreen extends React.PureComponent<Props, States> {
  constructor(props: Props) {
    super(props);
    const deviceVersion = props.navigation.getParam("deviceVersion", []);
    const userInfo = props.navigation.getParam("userInfo", {});

    this.state = { isSettingUp: false, deviceVersion, userInfo };
  }

  componentDidMount() {
    eventBus.addListener(EVENT_BUS.BLE_CHANGE_ERROR, this.onBleError);
  }

  componentWillUnmount() {
    eventBus.removeListener(this.onBleError);
  }

  onBleError = (error: string | Error) => {
    if (error === BLE_ERROR.KLINE_ERROR) {
      ble.disconnect();
      return responsePopup({
        title: "Mất tín hiệu K-line",
        subtitle: translateBleError(error),
        touchOutsideToDismiss: false,
        retryCb: () => navigationService.navigate("ScanningScreen"),
        retryTitle: "Đồng ý",
        icon: require("../../../assets/Images/Share/klineError.png"),
        iconSize: { width: 250, height: 100 }
      });
    }
    return responsePopup({
      title: "Kết nối không thành công",
      subtitle: translateBleError(error),
      touchOutsideToDismiss: false,
      retryCb: () => navigationService.navigate("ScanningScreen")
    });
  };

  onPressWhenValid = async (brand: string) => {
    // Alert turn on power for ECU for Yamaha
    if (brand === "yamaha") {
      const confirmed = await new Promise((resolve, reject) => {
        Alert.alert(
          "Vui lòng tắt và bật khóa điện của xe để thiết lập kết nối",
          "Đã thực hiện?",
          [
            { text: "Tiếp tục", onPress: () => resolve(true) },
            {
              text: "Bỏ qua",
              onPress: () => resolve(false),
              style: "destructive"
            }
          ],
          { cancelable: false }
        );
      });
      if (!confirmed) {
        return;
      }
    }

    this.setState({ isSettingUp: true });
    bikeLibHelper.setCurrentBikeBrand(brand);
    let ecuId;
    let initEsyOkay = true;
    try {
      const { deviceVersion } = this.state;
      // ESY PRO ver 2
      if (compareVersions(deviceVersion, DEVICE_VERSION_2) >= 0) {
        console.log("DEVICE VER 2");
        // init esy
        const initResponse = await ble.sendData({
          data: ble.generateCommand.chooseBikeBrand(brand),
          waitForResponse: true
        });
        console.log("initResponse", initResponse);

        if (brand === "honda") {
          // Init succeed
          if (compareArrays(initResponse, [250, 250, 3, 0, 1])) {
            // GET ECU ID
            await ble.sendData({
              data: ble.generateCommand.goToIdle()
            });
            const ecuIdResponse = await ble.sendData({
              data: ble.generateCommand.getEcuId(),
              waitForResponse: true
            });
            ecuId =
              (ecuIdResponse &&
                ble.extractDataFromResponse(
                  ecuIdResponse,
                  BLE_RESPONSE_DATA_TYPE.ECU_ID
                )) ||
              [];
            ble.setECUId(ecuId);
          } else {
            // Init Failure
            initEsyOkay = false;
            console.log("init false", initResponse);
          }
        } else if (brand === "yamaha") {
          // yamaha
          // Need to switch key
          if (compareArrays(initResponse, [0xfa, 0xfa, 0xfa, 0xfa])) {
            console.log("yamaha wait for switch key response");
            let switchKeyRes = [];
            while (true) {
              // Init succeed
              if (compareArrays(switchKeyRes, [0xfa, 0xfa, 0x03, 0x01, 0x01])) {
                break;
              } else if (
                compareArrays(switchKeyRes, [0xfa, 0xfa, 0x03, 0x01, 0x00]) || // Init fail
                compareArrays(switchKeyRes, [0xfa, 0xfa, 0xfa, 0x00]) // Switch key fail
              ) {
                // Init fail
                initEsyOkay = false;
                break;
              } else {
                // Wait for init
                console.log("Still wait for init Ya");
                switchKeyRes = await ble.sendData({
                  data: [],
                  waitForResponse: true
                });
              }
            }
          }
          // Kline error
          else if (
            // compareArrays(initResponse, [0xff, 0xff, 0x03, 0x00]) ||
            compareArrays(initResponse, [0xff, 0xff, 0xff])
          ) {
            ble.disconnect();
            responsePopup({
              title: "Mất tín hiệu K-line",
              subtitle:
                "Vui lòng tắt bật khoá điện, rút và cấp lại nguồn cho thiết bị ESY Pro",
              touchOutsideToDismiss: false,
              retryCb: () => navigationService.navigate("ScanningScreen"),
              retryTitle: "Đồng ý",
              icon: require("../../../assets/Images/Share/klineError.png"),
              iconSize: { width: 250, height: 100 }
            });
            return;
          }
          // Init succeed
          else {
          }
        }
      } else {
        // ESY PRO ver 1
        console.log("DEVICE VER 1");
        // GET ECU ID
        ecuId = this.props.navigation.getParam("ecuId", []);
      }

      if (!initEsyOkay) {
        responsePopup({
          title: "Kết nối không thành công",
          subtitle: translateBleError(BLE_ERROR.INIT_ECU_FAILURE),
          touchOutsideToDismiss: false,
          retryCb: () => navigationService.navigate("ScanningScreen")
        });
        return;
      }

      // Get Lib
      switch (brand) {
        case "honda":
          await bikeLibHelper.honda.fetchHondaLib(ecuId.slice(0, 4));
          break;

        case "yamaha":
          // eslint-disable-next-line no-case-declarations
          const partListRes = await ble.sendData({
            data: ble.generateCommand.getPartList(),
            waitForResponse: true,
            char: ble.CHAR_DATA_ADVANCE_CONTROL,
            service: ble.SERVICE_ADVANCE_DATA
          });
          const total = partListRes[3];
          // eslint-disable-next-line no-case-declarations
          const partList = partListRes.slice(4);
          while (partList.length < total) {
            const res = await ble.sendData({
              data: [],
              waitForResponse: true
            });
            res.forEach(value => partList.push(value));
          }
          bikeLibHelper.yamaha.setPartList(partList);

          // get ECU Id
          let idRes = await ble.sendData({
            data: ble.generateCommand.getEcuIdYa(),
            waitForResponse: true
          });
          while (
            compareArrays(idRes, ARRAY_SWITCH_KEY_REQUEST) ||
            compareArrays(idRes, ARRAY_SWITCH_KEY_OKAY) ||
            compareArrays(idRes, ARRAY_SWITCH_KEY_FAILED)
          ) {
            idRes = await ble.sendData({
              data: [],
              waitForResponse: true
            });
          }

          const id = idRes.slice(2);

          bikeLibHelper.yamaha.setEcuId(id);
          break;

        default:
          break;
      }
      // Send bike config to ESY
      switch (brand) {
        case "honda":
          await bikeLibHelper.honda.sendHondaBikeConfigToESY();
          return navigationService.navigate("MainScreen", { ecuId, brand });

        case "yamaha":
          await bikeLibHelper.yamaha.sendYamahaBikeConfigToESY();
          return navigationService.navigate("MainScreenYa", {});

        default:
          break;
      }
    } catch (e) {
      this.onBleError(e);
    }
  };

  render() {
    const { isSettingUp, userInfo } = this.state;
    return (
      <View style={styles.scene}>
        <NavBar
          title="Chọn hãng xe"
          leftIconSource={null}
          onLeftPress={() => null}
          rightIconSource={require("../../../assets/Images/Share/information.png")}
          onRightPress={() => {
            navigationService.navigate("UserInfo", {
              deviceId: ble.targetDeviceId || []
            });
          }}
        />
        <ScrollView contentContainerStyle={styles.scrollView}>
          {DATA.map(v => {
            const res = { ...v };
            if (res.key === BIKE_BRAND_LIST.YAMAHA) {
              res.licenseTime = userInfo.expiredDateYa;
            } else if (res.key === BIKE_BRAND_LIST.HONDA) {
              res.licenseTime = userInfo.expiredDate;
            }
            return res;
          }).map((value, index) => (
            <BikeManufactureItem
              key={index.toString()}
              data={value}
              onPressWhenValid={this.onPressWhenValid}
            />
          ))}
        </ScrollView>
        {isSettingUp && <Loading />}
        <OverlaySwitchKey />
        <PopupConnectionError ref={_ref => setRef(_ref)} />
      </View>
    );
  }
}

export default withSafeArea({ defaultBackHandler: false })(
  SelectBikeBrandScreen
);
