/**
 * @flow
 */
import React from "react";
import { StyleSheet, ScrollView, View } from "react-native";
import BaseScreen from "../Components/BaseScreen";
import NavBar from "../Components/NavBar";
import Text from "../Components/Text";
import { horizontalScale } from "../ScaleUtility";
import { colors } from "../styles/colors";

type Props = {};
type States = { basicInfo: Object, ecuId: number[] };

class BikeInfo extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
    const basicInfo = props.navigation.getParam("basicInfo");
    const ecuId = props.navigation.getParam("ecuId");
    this.state = { basicInfo, ecuId };
  }

  render() {
    const { basicInfo, ecuId } = this.state;
    return (
      <BaseScreen>
        <NavBar title="Thông tin xe" />
        <ScrollView
          style={{
            padding: 20
          }}
        >
          {/* Tên xe */}
          <Text style={styles.header}>Tên xe</Text>
          <View style={styles.contentContainer}>
            <Text style={styles.content}>{basicInfo.name}</Text>
          </View>
          {/* Mã xe */}
          {/* <Text style={styles.header}>Mã xe</Text>
          <View style={styles.contentContainer}>
            <Text style={styles.content}>Chưa có</Text>
          </View> */}
          {/* ID */}
          <Text style={styles.header}>ID</Text>
          <View style={styles.contentContainer}>
            <Text style={styles.content}>{versionToString(ecuId)}</Text>
          </View>
          {/* Năm */}
          <Text style={styles.header}>Năm</Text>
          <View style={styles.contentContainer}>
            <Text style={styles.content}>{basicInfo.gen}</Text>
          </View>
          {/* Dung tích xy lanh */}
          <Text style={styles.header}>Dung tích xy lanh</Text>
          <View style={styles.contentContainer}>
            <Text style={styles.content}>{basicInfo.cylinderCap}cc</Text>
          </View>
        </ScrollView>
      </BaseScreen>
    );
  }
}

export default BikeInfo;

const styles = StyleSheet.create({
  header: {
    fontSize: horizontalScale(18),
    marginTop: 15,
    fontWeight: "bold",
    color: colors.third_dark
  },
  content: {
    fontSize: horizontalScale(18),
    fontWeight: "bold",
    color: colors.secondary_dark
  },
  contentContainer: {
    marginTop: 10,
    padding: 10,
    width: "100%",
    backgroundColor: "#fffffe",
    borderRadius: 4,
    elevation: 2,
    shadowColor: colors.third_dark,
    shadowOffset: { height: 2, width: 2 },
    shadowOpacity: 0.35,
    shadowRadius: 3
  }
});
function versionToString(ver: number[]) {
  let hexStr = "";
  ver.forEach((value, index) => {
    let hex = value.toString(16);
    if (value < 16) {
      hex = "0" + hex;
    }
    if (index !== 0) {
      hex = "-" + hex;
    }
    hexStr += hex.toUpperCase();
  });
  return hexStr;
}
