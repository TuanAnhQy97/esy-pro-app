/**
 * @flow
 */
import { combineReducers } from "redux";

import DashboardReducer from "./DashboardReducer";
import InternetReducer from "./InternetReducer";
import BleStateReducer from "./BleStateReducer";
import ErrorFIReducer from "./ErrorFIReducer";
import NetInfo from "./NetInfo";
import AutoCheckOne from "./AutoCheckOneReducer";
import BleForActive from "./BleForActiveNew";
import EcuResetAndErase from "./EcuResetAndErase";
import Orientation from "./Orientation";

export default combineReducers({
  BleStateReducer,
  BleForActive,
  DashboardReducer,
  InternetReducer,
  ErrorFIReducer,
  NetInfo,
  AutoCheckOne,
  EcuResetAndErase,
  Orientation
});
