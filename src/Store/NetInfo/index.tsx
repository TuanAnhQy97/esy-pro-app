/**
 * @flow
 */

/** DEFINE ACTION TYPE */
export type TYPE_STORE_ACTION_NET_INFO =
  | "UPDATE_NETINFO_BLE"
  | "UPDATE_NETINFO_INTERNET";

export class TYPE_STORE_DATA_NETINFO extends Object {
  isBleOn = false;
  isInternetOn = false;
}

const Reducer = (
  state: TYPE_STORE_DATA_NETINFO = new TYPE_STORE_DATA_NETINFO(),
  action: Object
) => {
  let type: TYPE_STORE_ACTION_NET_INFO = action.type;
  let values = action.values;
  switch (type) {
    case "UPDATE_NETINFO_BLE":
      return {
        ...state,
        isBleOn: values
      };
    case "UPDATE_NETINFO_INTERNET":
      return {
        ...state,
        isInternetOn: values
      };
    default:
      return state;
  }
};

export default Reducer;
