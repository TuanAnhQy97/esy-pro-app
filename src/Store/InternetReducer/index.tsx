/**
 * @flow
 */

/** DEFINE ACTION TYPE */
export type TYPE_STORE_ACTION_INTERNET = "UPDATE_INTERNET_PROGRESS";

export class TYPE_STORE_DATA_INTERNET extends Object {
  progress: number;
  constructor() {
    super();
    this.progress = 0;
  }
}

const Reducer = (
  state: TYPE_STORE_DATA_INTERNET = new TYPE_STORE_DATA_INTERNET(),
  action: Object
) => {
  let type: TYPE_STORE_ACTION_INTERNET = action.type;
  let values = action.values;
  switch (type) {
    case "UPDATE_INTERNET_PROGRESS":
      return {
        ...state,
        progress: values
      };
    default:
      return state;
  }
};

export default Reducer;
