/**
 * @flow
 */
import Ble from "../../Ble/BleForNew";

/** DEFINE ACTION TYPE */
export type TYPE_STORE_ACTION_BLE_FOR_ACTIVE_NEW =
  | "BLE_FOR_ACTIVE_ERROR"
  | "BLE_FOR_ACTIVE_PROGRESS";

export class TYPE_STORE_DATA_BLE_FOR_ACTIVE_NEW extends Object {
  error: number = 0;
  progress: number = Ble.BLE_PROGRESS.PROGRESS.INIT;
}

const Reducer = (
  state: TYPE_STORE_DATA_BLE_FOR_ACTIVE_NEW = new TYPE_STORE_DATA_BLE_FOR_ACTIVE_NEW(),
  action: Object
) => {
  let type: TYPE_STORE_ACTION_BLE_FOR_ACTIVE_NEW = action.type;
  let values = action.values;
  switch (type) {
    case "BLE_FOR_ACTIVE_ERROR":
      return {
        progress: Ble.BLE_PROGRESS.PROGRESS.INIT,
        error: values
      };
    case "BLE_FOR_ACTIVE_PROGRESS":
      return {
        error: 0,
        progress: values
      };
    default:
      return state;
  }
};

export default Reducer;
