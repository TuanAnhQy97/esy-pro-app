/**
 * @flow
 */
import Ble from "../../Ble/BleForPaired";

/** DEFINE ACTION TYPE */
export type TYPE_STORE_ACTION_BLE_STATE =
  | "UPDATE_BLE_ERROR"
  | "UPDATE_BLE_PROGRESS"
  | "UPDATE_BLE_CHANGING_MODE";

export class TYPE_STORE_DATA_BLE_STATE extends Object {
  error: number = 0;
  progress: number = Ble.BLE_PROGRESS.PROGRESS.INIT;

  changingMode: boolean = false;
}

const Reducer = (
  state: TYPE_STORE_DATA_BLE_STATE = new TYPE_STORE_DATA_BLE_STATE(),
  action: Object
) => {
  let type: TYPE_STORE_ACTION_BLE_STATE = action.type;
  let values = action.values;
  switch (type) {
    case "UPDATE_BLE_ERROR":
      return {
        progress: Ble.BLE_PROGRESS.PROGRESS.INIT,
        error: values
      };
    case "UPDATE_BLE_PROGRESS":
      return {
        error: 0,
        progress: values
      };

    case "UPDATE_BLE_CHANGING_MODE":
      return {
        ...state,
        changingMode: values
      };
    default:
      return state;
  }
};

export default Reducer;
