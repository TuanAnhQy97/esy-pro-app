/**
 * @flow
 */

/** DEFINE ACTION TYPE */
export type TYPE_STORE_ECU_RESET_AND_ERASE =
  | "ECU_RESET_AND_ERASE_STATUS"
  | "ECU_RESET_AND_ERASE_JOB";

export class TYPE_STORE_DATA_ECU_RESET_AND_ERASE extends Object {
  status: "doing" | "error" | "done" = "doing";
  errorCode: number = 0;
  job: "reset" | "erase" = "reset";
}

const Reducer = (
  state: TYPE_STORE_DATA_ECU_RESET_AND_ERASE = new TYPE_STORE_DATA_ECU_RESET_AND_ERASE(),
  action: Object
) => {
  let type: TYPE_STORE_ECU_RESET_AND_ERASE = action.type;
  let values = action.values;
  switch (type) {
    case "ECU_RESET_AND_ERASE_STATUS":
      return {
        ...state,
        status: values,
        errorCode: 0
      };
    case "ECU_RESET_AND_ERASE_JOB":
      return {
        ...state,
        job: values
      };
    default:
      return state;
  }
};

export default Reducer;
