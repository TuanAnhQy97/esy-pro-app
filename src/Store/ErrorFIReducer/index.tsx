/**
 * @flow
 */

/** DEFINE ACTION TYPE */
export type TYPE_STORE_ACTION_ERROR_FI =
  | "UPDATE_ERROR_FI_DATA"
  | "UPDATE_ERROR_FI_LOADING";

export class TYPE_STORE_DATA_ERROR_FI extends Object {
  errorList: number[][];
  isLoading: boolean;
  constructor() {
    super();
    this.errorList = [];
    this.isLoading = true;
  }
}

const Reducer = (
  state: TYPE_STORE_DATA_ERROR_FI = new TYPE_STORE_DATA_ERROR_FI(),
  action: Object
) => {
  let type: TYPE_STORE_ACTION_ERROR_FI = action.type;
  let values = action.values;
  switch (type) {
    case "UPDATE_ERROR_FI_DATA":
      return {
        ...state,
        errorList: [...values],
        isLoading: false
      };
    case "UPDATE_ERROR_FI_LOADING":
      return {
        ...state,
        isLoading: true
      };
    default:
      return state;
  }
};

export default Reducer;
