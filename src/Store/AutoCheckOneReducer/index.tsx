/**
 * @flow
 */
import {  DATA1234 } from "../../DiagnosticHelper/DiagnosticHelper";
/** DEFINE ACTION TYPE */
export type TYPE_STORE_ACTION_AUTO_CHECK_ONE =
  | "UPDATE_AUTO_CHECK_ONE_PROGRESS"
  | "UPDATE_AUTO_CHECK_ONE_RESULT";
export type AUTO_CHECK_ONE_PROGRESS =
  | "init"
  | "showWarning"
  | "processing"
  | "showResult";
export type AUTO_CHECK_ONE_RESULT = {
  errorList: number[][],
  dataList: DATA1234
};
export class TYPE_STORE_DATA_AUTO_CHECK_ONE extends Object {
  result: AUTO_CHECK_ONE_RESULT;
  progress: AUTO_CHECK_ONE_PROGRESS;
  constructor() {
    super();
    this.result = {
      errorList: [],
      dataList: { data1: [], data2: [], data3: [], data4: [] }
    };
    this.progress = "init";
  }
}

const Reducer = (
  state: TYPE_STORE_DATA_AUTO_CHECK_ONE = new TYPE_STORE_DATA_AUTO_CHECK_ONE(),
  action: Object
) => {
  let type: TYPE_STORE_ACTION_AUTO_CHECK_ONE = action.type;
  let values = action.values;
  switch (type) {
    case "UPDATE_AUTO_CHECK_ONE_PROGRESS":
      return {
        ...state,
        progress: values
      };
    case "UPDATE_AUTO_CHECK_ONE_RESULT":
      return {
        progress: "showResult",
        result: values
      };
    default:
      return state;
  }
};

export default Reducer;
