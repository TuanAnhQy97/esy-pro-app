/**
 * @flow
 */
import Orientation from "react-native-orientation-locker";
const initialOrientation = Orientation.getInitialOrientation();
/** DEFINE ACTION TYPE */
export type TYPE_STORE_ACTION_ORIENTATION = "UPDATE_ORIENTATION";

export class TYPE_STORE_DATA_ORIENTATION extends Object {
  orientation: Orientation.Orientation = initialOrientation;
}

const Reducer = (
  state: TYPE_STORE_DATA_ORIENTATION = new TYPE_STORE_DATA_ORIENTATION(),
  action: Object
) => {
  let type: TYPE_STORE_ACTION_ORIENTATION = action.type;
  let values = action.values;
  switch (type) {
    case "UPDATE_ORIENTATION":
      return { ...state, orientation: values };

    default:
      return state;
  }
};

export default Reducer;
