/**
 * @flow
 */

/** DEFINE ACTION TYPE */
export type TYPE_STORE_ACTION_DASHBOARD = "UPDATE_DASHBOARD_DATA";

export class TYPE_STORE_DATA_DASHBOARD extends Object {
  data1: number[];
  data2: number[];
  // data3: number[];
  // data4: number[];
  /**
   * Extra
   */
  extraData1: number[];
  extraData2: number[];
  extraData3: number[];
  extraData4: number[];
  constructor() {
    super();
    this.data1 = [];
    this.data2 = [];
    // this.data3 = [];
    // this.data4 = [];
    /**
     *
     */
    this.extraData1 = [];
    this.extraData2 = [];
    this.extraData3 = [];
    this.extraData4 = [];
  }
}

const Reducer = (
  state: TYPE_STORE_DATA_DASHBOARD = new TYPE_STORE_DATA_DASHBOARD(),
  action: Object
) => {
  let type: TYPE_STORE_ACTION_DASHBOARD = action.type;
  let values = action.values;
  switch (type) {
    case "UPDATE_DASHBOARD_DATA":
      return {
        ...state,
        data1: values.data1,
        data2: values.data2,
        // data3: values.data3,
        // data4: values.data4,
        /**
         *
         */
        extraData1: values.extraData1,
        extraData2: values.extraData2,
        extraData3: values.extraData3,
        extraData4: values.extraData4
      };
    default:
      return state;
  }
};

export default Reducer;
