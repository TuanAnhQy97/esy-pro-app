/**
 * @flow
 */

import { createStore, applyMiddleware } from "redux";

import RootReducer from "./RootReducer";
import {
  TYPE_STORE_ACTION_DASHBOARD,
  TYPE_STORE_DATA_DASHBOARD
} from "./DashboardReducer";
import {
  TYPE_STORE_DATA_INTERNET,
  TYPE_STORE_ACTION_INTERNET
} from "./InternetReducer";
import {
  TYPE_STORE_DATA_BLE_STATE,
  TYPE_STORE_ACTION_BLE_STATE
} from "./BleStateReducer";
import {
  TYPE_STORE_DATA_ERROR_FI,
  TYPE_STORE_ACTION_ERROR_FI
} from "./ErrorFIReducer";
import { TYPE_STORE_ACTION_NET_INFO, TYPE_STORE_DATA_NETINFO } from "./NetInfo";
import {
  TYPE_STORE_ACTION_AUTO_CHECK_ONE,
  TYPE_STORE_DATA_AUTO_CHECK_ONE
} from "./AutoCheckOneReducer";
import {
  TYPE_STORE_ACTION_BLE_FOR_ACTIVE_NEW,
  TYPE_STORE_DATA_BLE_FOR_ACTIVE_NEW
} from "./BleForActiveNew";
import {
  TYPE_STORE_DATA_ECU_RESET_AND_ERASE,
  TYPE_STORE_ECU_RESET_AND_ERASE
} from "./EcuResetAndErase";
import {
  TYPE_STORE_ACTION_ORIENTATION,
  TYPE_STORE_DATA_ORIENTATION
} from "./Orientation";
/**
 *          TUTORIAL
 *  Mỗi khi có thêm Reducer mới. Yêu cầu:
 * - TYPE_STORE_ACTION_XXX của reducer đó -> Add vào TYPE_STORE_ACTION
 * - TYPE_STORE_DATA_XXX của reducer đó -> Add vào TYPE_STORE_DATA
 */

/**
 * Các Action dùng được với Store
 */
type TYPE_STORE_ACTION =
  | TYPE_STORE_ACTION_DASHBOARD
  | TYPE_STORE_ACTION_INTERNET
  | TYPE_STORE_ACTION_BLE_STATE
  | TYPE_STORE_ACTION_ERROR_FI
  | TYPE_STORE_ACTION_NET_INFO
  | TYPE_STORE_ACTION_AUTO_CHECK_ONE
  | TYPE_STORE_ACTION_BLE_FOR_ACTIVE_NEW
  | TYPE_STORE_ECU_RESET_AND_ERASE
  | TYPE_STORE_ACTION_ORIENTATION;
/**
 * Cấu trúc dữ liệu trong Store.
 * Giúp gợi ý cho hàm mapStateToProps
 */
export type TYPE_STORE_DATA = {
  DashboardReducer: TYPE_STORE_DATA_DASHBOARD;
  InternetReducer: TYPE_STORE_DATA_INTERNET;
  BleStateReducer: TYPE_STORE_DATA_BLE_STATE;
  ErrorFIReducer: TYPE_STORE_DATA_ERROR_FI;
  NetInfo: TYPE_STORE_DATA_NETINFO;
  AutoCheckOne: TYPE_STORE_DATA_AUTO_CHECK_ONE;
  BleForActive: TYPE_STORE_DATA_BLE_FOR_ACTIVE_NEW;
  EcuResetAndErase: TYPE_STORE_DATA_ECU_RESET_AND_ERASE;
  Orientation: TYPE_STORE_DATA_ORIENTATION;
};
/**
 * Cấu trúc tổng quan của Store
 */
type TYPE_STORE = {
  getState(): TYPE_STORE_DATA;
  dispatch(
    value: { type: TYPE_STORE_ACTION; values?: any } | Function
  ): Function;
};

class Store {
  static instance: TYPE_STORE;

  static getInstance() {
    if (!Store.instance) {
      Store.instance = createStore(
        RootReducer,
        applyMiddleware()
        // thunkMiddleware // lets us dispatch() functions
        // loggerMiddleware // neat middleware that logs actions
      );
    }
    return Store.instance;
  }
}
export default Store.getInstance();
