/**
 * @flow
 */
import { NavigationActions, StackActions } from "react-navigation";
import { Alert } from "react-native";
import { SCREEN_KEY_NAME } from "./Routers";

let _navigator;
let basePopupRef;

export function setTopLevelNavigator(navigatorRef: any) {
  _navigator = navigatorRef;
}

export function navigate(routeName: SCREEN_KEY_NAME, params?: Object) {
  _navigator.dispatch(
    NavigationActions.navigate({
      type: NavigationActions.NAVIGATE,
      routeName,
      params
    })
  );
}
export function navigateGoBack() {
  _navigator.dispatch(StackActions.pop());
}
export function navigatePopToTop() {
  _navigator.dispatch(StackActions.popToTop());
}

export function showAlertNoInternet() {
  Alert.alert("Chưa có kết nối Internet", "Vui lòng kết nối mạng đề sử dụng", [
    { text: "Đồng ý" }
  ]);
}
export function showAlertNoBle() {
  Alert.alert(
    "Chưa có kết nối Bluetooth",
    "Vui lòng bật Bluetooth đề sử dụng",
    [{ text: "Đồng ý" }]
  );
}
