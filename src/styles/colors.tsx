/**
 * @flow
 */

/**
 *  #184423
    #3a9244
    #4ab743
    #8c8077
    #ebebeb
    #fffffe

    #fd7808
    #fb1106
    #ce0004
 */
export const colors = Object.freeze({
  primary_dark: "#184423",
  secondary_dark: "#3a9244",
  third_dark: "#8c8077",
  primary_light: "#fffffe",
  secondary_light: "#ebebeb",
  warning: "#fd7808",
  error: "#fb1106"
});
