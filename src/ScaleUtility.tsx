/**
 * @flow
 */

import DeviceInfo from "react-native-device-info";
import { Dimensions, StatusBar } from "react-native";
const { width, height } = Dimensions.get("window");

// Lấy chiều cao, chiều rộng, so sánh với trường hợp màn xoay ngang
const screenWidth = height > width ? width : height;
const screenHeight = height > width ? height : width;

export { screenHeight, screenWidth };

let safeAreaHeight = 0,
  statusBarHeight = 0,
  bottomBarHeight = 0;
let isIpad = false;
let defaultFactor = 0;
/**
 * Return status bar height based on device model,name, brand...
 */
export function getStatusBarHeight(): number {
  let brand = DeviceInfo.getBrand();
  let deviceModel = DeviceInfo.getModel();
  let phoneId = DeviceInfo.getDeviceId();
  if (brand === "Apple") {
    // Apple devices
    if (phoneId === "iPhone10,3" || phoneId === "iPhone10,6") {
      // Iphone X
      return 44;
    } else return 22;
  } else {
    // Other brands
    return StatusBar.currentHeight;
  }
}
function getBottomBarHeight(): number {
  let brand = DeviceInfo.getBrand();
  let deviceModel = DeviceInfo.getModel();
  let phoneId = DeviceInfo.getDeviceId();
  if (brand === "Apple") {
    // Apple devices
    if (phoneId.includes("iPad")) {
      isIpad = true;
      defaultFactor = -0.3;
    }
    if (phoneId === "iPhone10,3" || phoneId === "iPhone10,6") {
      // Iphone X
      return 34;
    } else return 0;
  } else {
    // Other brands
  }
  return 0;
}

/**
 * Setup Displayable Height Area. Call when app open as soon as possible
 */
function setUpSafeAreaHeight() {
  safeAreaHeight = screenHeight - getStatusBarHeight() - getBottomBarHeight();
}
setUpSafeAreaHeight();
//Guideline sizes are based iphone 6
const guidelineBaseWidth = 375;
const guidelineBaseHeight = 667 - 22; // status bar;

function horizontalScale(size: number, factor: number = defaultFactor) {
  let scaleSize = (screenWidth / guidelineBaseWidth) * size;
  return scaleSize + (scaleSize - size) * factor;
}
function verticalScale(size: number, factor: number = defaultFactor) {
  let scaleSize = (safeAreaHeight / guidelineBaseHeight) * size;
  return scaleSize + (scaleSize - size) * factor;
}
export { horizontalScale, verticalScale };
