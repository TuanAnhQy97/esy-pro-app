/**
 * @flow
 */
import CryptoJS from "crypto-js";
import { AsyncStorage, Alert } from "react-native";

import { to } from "../SharedFunctions";
import { DeviceInfo } from "../Model/DeviceInfo";
import { Credentials } from "../Model/Credentials";
import { USER_INFO } from "../Model/UserInfo";
import navigationService from "../services/navigationService";

/**
 * ALWAYS USE this one when write to AS
 * @param {*} path
 * @param {*} data
 */
export async function writeToAS(path: string, data: string) {
  const maxTry = 3;
  let res;
  let error;
  for (let count = 1; count <= maxTry; count++) {
    [error, res] = await to(AsyncStorage.setItem(path, data));
    if (error) {
      // Nếu lỗi
      //   logEventFailure({ where: "storage", why: error.message });
      console.log("writeToAS ERROR: ", error, path, data);
    } else {
      // Nếu không có lỗi -> Ghi thành công -> OKAY
      return;
    }
  }
  dealWithFatalErrorInAS();
}
/**
 * ALWAYS USE this one when read from AS
 * @param {*} path
 */
export async function readFromAS(path: string) {
  const maxTry = 3;
  let res;
  let error;
  for (let count = 1; count <= maxTry; count++) {
    [error, res] = await to(AsyncStorage.getItem(path));
    if (error) {
      //   logEventFailure({ where: "storage", why: error.message });
      console.log("readFromAS ERROR: ", error, path);
    } else {
      return res;
    }
  }
  dealWithFatalErrorInAS();
  return null;
}
/**
 * Transform object to a form of encrypted string
 * @param {Object} someObject object needed to be encrypted
 * @returns {string} encrypted string
 */
export function encryptObject(someObject: Object | Array<any>): string {
  const encrypted = CryptoJS.AES.encrypt(
    JSON.stringify(someObject),
    "S@vyHihiPizza!/!yY"
  );
  return encrypted.toString();
}
/**
 * decrypt an encrypted string and transform it to Object form
 * @param {string} encrypted encrypted string
 * @returns {Object} decrypted from given string
 */
export function decryptObject(encrypted: string): any {
  const bytes = CryptoJS.AES.decrypt(encrypted, "S@vyHihiPizza!/!yY");
  const plaintext = bytes.toString(CryptoJS.enc.Utf8);
  const credentials = JSON.parse(plaintext);
  return credentials;
}
/**
 * Clear all storage of App
 */
export function clearAS(): void {
  console.log("clearAsyncStorage");
  AsyncStorage.clear();
}
/**
 * Use when App has fatal error in AS that cannot work anymore.
 * @DO:
 * - Clear AS
 * - Show Alert
 * - Logout
 */
export function dealWithFatalErrorInAS() {
  clearAS();
  Alert.alert("Lỗi bộ nhớ", "Vui lòng đăng nhập lại", [{ text: "Đồng ý" }]);
  navigationService.navigate("Auth");
}

/**
 *  Save credentials from sever to AsyncStorage
 */
export async function updateCredentialsOnAS(credentials: Credentials) {
  console.log("updateCredentialsOnAsyncStorage");
  const encrypted = encryptObject(credentials); // encryt
  await writeToAS("cs", encrypted); // store on phone
}
/**
 * Load credentials from AsyncStorage
 */
export async function loadCredentialsFromAS(): Promise<Credentials | null> {
  console.log("loadCredentialsFromAsyncStorage");
  const credentialsStr = await readFromAS("cs"); // get from AsyncStorage
  if (credentialsStr === null) return null;
  const credentials = decryptObject(credentialsStr); // decrypt and convert to object
  return credentials;
}
/**
 * load device list from AsyncStorage
 * @return [] null when not found
 * else
 * @return DeviceInfo[]
 */
export async function loadDeviceInfoFromAS(): Promise<DeviceInfo | null> {
  const deviceListStr = await readFromAS("di");
  if (deviceListStr === null) {
    // in case not found
    return null;
  }
  let deviceList: DeviceInfo;
  deviceList = decryptObject(deviceListStr);
  return deviceList;
}

/**
 * Encrypt and save deviceInfo to AsyncStorage.
 * Overide any previous deviceInfo on AsyncStorage
 * @param {*} newDeviceInfo new device info
 */
export async function replaceDeviceInfoOnAS(newDeviceInfo: DeviceInfo) {
  const encrypt = encryptObject(newDeviceInfo); // encrypt new info
  await writeToAS("di", encrypt); // store on AsyncStorage
}

/**
 * load device list from AsyncStorage
 * @return null when not found
 * else
 * @return UserInfo
 */
export async function loadUserInfoFromAS(): Promise<USER_INFO | null> {
  const deviceListStr = await readFromAS("ui");
  if (deviceListStr === null) {
    // in case not found
    return null;
  }
  let deviceList: DeviceInfo;
  deviceList = decryptObject(deviceListStr);
  return deviceList;
}

/**
 * Encrypt and save user info to AsyncStorage.
 * Overide any previous user info on AsyncStorage
 * @param {*} newUserInfo new user info
 */
export async function replaceUserInfoOnAS(newUserInfo: USER_INFO) {
  const encrypt = encryptObject(newUserInfo); // encrypt new info
  await writeToAS("ui", encrypt); // store on AsyncStorage
}
