/**
 * @flow
 */
import BikeHelper from "./BikeHelper";
type ExplainStructure = {
  name: string,
  def: string,
  ifOutRange: string,
  warningIfHigher: string,
  warningIfLower: string
};
const unitArr = ["vòng/phút", "°C", "ms", "V", "°", "°", "kPa", "°C", "mV"];

class PerformanceExplainIndex {
  static instance: PerformanceExplainIndex;
  static getInstance(): PerformanceExplainIndex {
    if (!PerformanceExplainIndex.instance) {
      PerformanceExplainIndex.instance = new PerformanceExplainIndex();
    }
    return PerformanceExplainIndex.instance;
  }
  getExplain(indexOrder: number) {
    let explain: ExplainStructure = {
      name: "",
      def: "",
      ifOutRange: "",
      warningIfHigher: "",
      warningIfLower: ""
    };
    let range = BikeHelper.lib.standardQD[indexOrder];
    let min = range[0],
      max = range[1],
      bestValueLow = range[2],
      bestValueHigh = range[3],
      unit = unitArr[indexOrder];
    switch (indexOrder) {
      /**
       * RPM
       */
      case 0:
        explain.name = "Tốc độ vòng tua";
        explain.def =
          "Giá trị này nên trong khoảng từ " +
          min +
          " đến " +
          max +
          " " +
          unit +
          ".";
        explain.ifOutRange =
          "Khi thấp hơn xe động cơ yếu và hay chết máy.\nKhi cao hơn, động cơ tốn xăng hơn, hoặc có thể bị rồ ga.";
        explain.warningIfHigher =
          "Cần kiểm tra sửa chữa hoặc thay thế Van điều khiển tốc độ cầm chừng, IACV hoặc ECM";
        explain.warningIfLower =
          "Cần kiểm tra sửa chữa hoặc thay thế Van điều khiển tốc độ cầm chừng, IACV hoặc ECM";
        break;
      /**
       * ECT
       */
      case 1:
        explain.name = "Nhiệt độ dung dịch làm mát";
        explain.def =
          "Giá trị này nên trong khoảng từ " +
          min +
          " đến " +
          max +
          " " +
          unit +
          ".";
        explain.ifOutRange =
          "Nếu cao hơn, chứng tỏ hệ thống làm mát không tốt.";
        explain.warningIfHigher =
          "Nhiệt độ dung dịch làm mát vượt ngưỡng, nên kiểm tra hệ thống làm mát và dầu máy.";
        explain.warningIfLower = "";
        break;
      /**
       * IT
       */
      case 2:
        explain.name = "Thời gian phun nhiên liệu";
        explain.def =
          "Thể hiện lượng nhiên liệu được dùng trong mỗi chu kỳ nổ.\nGiá trị này nên trong khoảng từ " +
          min +
          " đến " +
          max +
          " " +
          unit +
          ".";
        explain.ifOutRange =
          "Nếu cao hơn, có thể dẫn đến hao xăng, động cơ không ổn định";
        explain.warningIfHigher =
          "Thời gian phun cao, kiểm tra ECU và hệ thống bơm xăng - kim phun";
        explain.warningIfLower =
          "Thời gian phun thấp, kiểm tra ECU và hệ thống bơm xăng - kim phun";
        break;
      /**
       * Battery
       */
      case 3:
        explain.name = "Điện áp ắc quy";
        explain.def = "Giá trị này nên trong khoảng từ 12 đến 15 Volt.";
        explain.ifOutRange =
          "Nếu thấp hơn ECU hoạt động không ổn định\nNếu cao hơn, hư hỏng bình Ắc quy, ECU";
        explain.warningIfHigher =
          "Điện áp đang vượt ngưỡng, cần kiểm tra hệ thống nạp sạc";
        explain.warningIfLower =
          "Điện áp đang thấp, cần kiểm tra dây điện và hệ thống nạp sạc";
        break;
      /**
       * GDL
       */
      case 4:
        explain.name = "Góc đánh lửa";
        // Best value
        if (bestValueHigh !== undefined) {
          explain.def = `Giá trị tốt nhất trong khoảng ${bestValueLow}° đến ${bestValueHigh}°`;
        } else {
          explain.def = "Giá trị tốt nhất là " + bestValueLow + "°";
        }
        explain.ifOutRange =
          "Góc đánh lửa không chính xác khiến động cơ vận hành không ổn định";
        explain.warningIfHigher =
          "Kiểm tra hệ thống đánh lửa, bugi, mobin, cảm biến trục khuỷu (CKP)";
        explain.warningIfLower =
          "Kiểm tra hệ thống đánh lửa, bugi, mobin, cảm biến trục khuỷu (CKP)";
        break;
      /**
       *   TPS
       */
      case 5:
        explain.name = "Góc bướm ga";
        explain.def = "Giá trị tốt nhất là 0°";
        explain.ifOutRange = "Nếu cao hơn khiến xe rồ ga, hao xăng";
        explain.warningIfHigher =
          "Kiểm tra dây ga, dây dẫn, cảm biến TPS hoặc ECU";
        explain.warningIfLower =
          "Kiểm tra dây ga, dây dẫn, cảm biến TPS hoặc ECU";
        break;
      /**
       * MAP
       */
      case 6:
        explain.name = "Áp suất khí nạp";
        explain.def =
          "Áp suất không khí nạp vào buồng đốt\n" +
          "Giá trị này nên trong khoảng từ " +
          min +
          " đến " +
          max +
          " " +
          unit +
          ".";
        explain.ifOutRange = "Khi áp suất quá cao, rất hao xăng";
        explain.warningIfHigher =
          "Cần kiểm tra ECU và cảm biến áp suất khi nạp";
        explain.warningIfLower = "Cần kiểm tra ECU và cảm biến áp suất khi nạp";
        break;
      /**
       *  IAT
       */
      case 7:
        explain.name = "Nhiệt độ khí nạp";
        explain.def =
          "Áp suất không khí nạp vào buồng đốt.\n" +
          "Giá trị này nên trong khoảng từ -20 đến 120°C.";
        explain.ifOutRange =
          "Thấp hơn, xe hụt ga và hao xăng\nCao hơn, xe hao xăng và mất idling stop";
        explain.warningIfHigher =
          "Cần kiểm tra ECU và cảm biến nhiệt độ khí nạp";
        explain.warningIfLower =
          "Cần kiểm tra ECU và cảm biến nhiệt độ khí nạp";
        break;
      /**
       *  OXY
       */
      case 8:
        explain.name = "Chất lượng khí thải";
        explain.def =
          "Phản ánh độ hiệu quả của việc sử dụng nhiên liệu.\n" +
          "Giá trị này nên trong khoảng từ " +
          min +
          " đến " +
          max +
          " " +
          unit +
          ".";
        explain.ifOutRange = "Thông số nếu ngoài khoảng gây ra hao xăng";
        explain.warningIfHigher = "Cần kiểm tra ECU và cảm biến Oxy";
        explain.warningIfLower = "Cần kiểm tra ECU và cảm biến Oxy";
        break;
      default:
        break;
    }

    return explain;
  }
}
export default PerformanceExplainIndex.getInstance();
