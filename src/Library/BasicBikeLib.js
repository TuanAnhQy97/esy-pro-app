/**
 * @flow
 */

/**
 * Array has type and name
 */
export const BIKE_TYPE_ARRAY = Object.freeze([
  ["SH nhập", 100], //0
  ["SH Mode", 101],
  ["SH Việt", 102],
  ["AirBlade 110cm³ (2009)", 110],
  ["AirBlade 110cm³ (2010)", 111], // 4
  ["AirBlade 125cm³", 112],
  ["Lead 110cm³", 120],
  ["Lead 125cm³", 121], // 7
  ["Vision", 130],
  ["PCX", 140] // 9
]);

const ECU_TYPE = Object.freeze({
  TYPE_A: 0,
  TYPE_B: 1,
  TYPE_C: 2
});
const NOZZLE_TYPE = Object.freeze({
  NOZZLE_1: 0.001333333333333, // 80 ml/m -> ml/ms, Lead Scr Ab2010
  NOZZLE_2: 0.001778333333333, // 106.7 ml/m -> ml/ms, SHmode, Lead 125, Vision 125, AB 125, AB 12 13, SHVN
  NOZZLE_3: 0.002333333333333, // 140 ml/m -> ml/ms, SH nhập
  NOZZLE_4: 0.0024, // 144 ml/m CBR 150 8 lỗ
  NOZZLE_5: 0.013733333333333, // 12 lỗ, 4 máy CB 1100cm3, x4 so với kim thường vì 4 máy 0.0024/ 2 * 3 * 4
  NOZZLE_6: 0.003433333333333, // 12 lỗ
  NOZZLE_7: 0.000833333333333 // 3 lỗ, xe 50cc
});
const QD_TEMP_CONDITION = Object.freeze({
  EOT: 100,
  ECT: 80
});
const QD_VARIATION = Object.freeze({
  _400: [0x01, 0x90],
  _600: [0x02, 0x58],
  _700: [0x02, 0xbc],
  _800: [0x03, 0x20]
});

//#region define default const
const defaultPos1B = Object.freeze([
  [4, 5], // RPM 0
  [13], // ECT 1
  [15, 16], // IT 2
  [14], // Vbat 3
  [19], // speed 4
  [17], // GDL 5
  [7], // TPS, 6
  [], // MAP 7
  [11] // IAT 8
]);
const defaultPos1A = [
  [4, 5], // RPM 0
  [9], // ECT 1
  [18, 19], // IT 2
  [16], // Vbat 3
  [], // speed 4
  [20], // GDL 5
  [7], // TPS, 6
  [13], // MAP 7
  [11] // IAT 8
];
const defaultPos2 = [[4]];

class BasicBikeLib {
  ecuId: number[];
  basicInfo: {
    name: string,
    gen: number,
    cylinderCap: number,
    bikeType: number
  };
  feature: {
    idlingStop?: boolean,
    abs?: boolean,
    hasVS?: boolean
  };
  averKmPerLit: number;
  /**
   *
   */
  standardQD: number[][]; // Performance standard range
  qdTempCondition: number; // Performance conditions to capture
  qdSampleToProcess: number; // Performance conditions to capture
  qdVariation: number[]; // Performance conditions to capture
  ecuType: number;
  nozzle: number;
  pos1: number[][];
  pos2: number[][];

  // Overide Request data
  overideReq1: void | number[];
  overideReq2: void | number[];
  overideReq3: void | number[];
  overideReq4: void | number[];

  constructor(
    ecuId: number[],
    basicInfo: Object,
    standardQD: number[][],
    ecuType: number,
    nozzle: number,
    feature: Object | null, // if null, default will be set
    overidePos1: number[][] | null, // if null, default will be set
    overidePos2: number[][] | null, // if null, default will be set,
    qdTempCondition: number, // điều kiện nhiệt độ cho QD
    qdSampleToProcess: number, // số mẫu phần cứng phân tích để quyết định thông số đã ổn định hay chưa
    qdVariation: number[], // độ giao động của RPM
    averKmPerLit: number, // average distance per a litter of fuel
    /**
     * Overide Request data
     */
    _overideReq1?: number[],
    _overideReq2?: number[],
    _overideReq3?: number[],
    _overideReq4?: number[]
  ) {
    this.ecuId = ecuId;
    this.basicInfo = basicInfo;
    this.standardQD = standardQD;
    this.ecuType = ecuType;
    this.nozzle = nozzle;
    this.feature = feature === null ? {} : feature;
    this.averKmPerLit = averKmPerLit;
    if (ecuType === ECU_TYPE.TYPE_A) {
      this.pos1 = overidePos1 ? overidePos1 : defaultPos1A;
    } else {
      this.pos1 = overidePos1 ? overidePos1 : defaultPos1B;
    }
    this.pos2 = overidePos2 ? overidePos2 : defaultPos2;
    this.qdTempCondition = qdTempCondition;
    this.qdSampleToProcess = qdSampleToProcess;
    this.qdVariation = qdVariation;
    /**
     * Overide Request Data
     */
    this.overideReq1 = _overideReq1;
    this.overideReq2 = _overideReq2;
    this.overideReq3 = _overideReq3;
    this.overideReq4 = _overideReq4;
  }
}
export default BasicBikeLib;
export { ECU_TYPE, NOZZLE_TYPE, QD_TEMP_CONDITION, QD_VARIATION };
