/**
 * @flow
 */

import BasicBikeLib from "./BasicBikeLib";
import ExplainError, { ESY_ERROR_CODE } from "./ExplainError";

class BikeHelper {
  static bikeHelper: BikeHelper;
  static getInstance(): BikeHelper {
    if (!BikeHelper.bikeHelper) {
      BikeHelper.bikeHelper = new BikeHelper();
      return BikeHelper.bikeHelper;
    } else {
      return BikeHelper.bikeHelper;
    }
  }
  lib: BasicBikeLib;
  constructor() {
    //#region Binding methods
    // main methods
    this.convertData1 = this.convertData1.bind(this);
    this.convertData2 = this.convertData2.bind(this);
    this.prepareLib = this.prepareLib.bind(this);
    //#endregion
  }
  /**
   * Use this function to init BikeHelper when for a bike
   * @param {*} ecuId ecuId of that bike
   */
  prepareLib: Function = function(_lib: BasicBikeLib) {
    this.lib = _lib;
  };
  /**
   * Get bike name = name + gen + cylinderSize
   */
  getBikeName(): string {
    return (
      this.lib.basicInfo.name +
      " " +
      this.lib.basicInfo.cylinderCap +
      "cm³" +
      " " +
      this.lib.basicInfo.gen
    );
  }

  convertData1 = function(data: number[]) {
    return [
      convertSpeed.bind(this)(data), // Speed 0
      convertRPM.bind(this)(data), // RPM 1
      convertEngineTemp.bind(this)(data), // Temp 2
      convertInjectionTime.bind(this)(data), // IT 3
      convertVBat.bind(this)(data), // Vbat 4
      convertGDL.bind(this)(data), // GDL 5
      convertTPS.bind(this)(data), // TPS 6
      convertMAP.bind(this)(data), // MAP 7
      convertIAT.bind(this)(data) // IAT 8,
    ];
  };

  convertData2 = function(data) {
    return [convertOxy.bind(this)(data)]; // Oxy 0
  };
  convertData4 = function(data) {
    return [
      convertSidekick.bind(this)(data), // Side kick
      convertBreakAndStartSignal.bind(this)(data), // Break and start Button
      convertFuelPump.bind(this)(data) // Bơm xăng
    ];
  };
}

//#region Sub-convert Functions
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
function convertRPM(data: number[]) {
  let pos = this.lib.pos1[0];
  if (pos === null) return null;
  let value = data[pos[0]] * 256 + data[pos[1]];
  return value;
}
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
function convertEngineTemp(data: number[]) {
  let pos = this.lib.pos1[1];
  if (pos.length === 0) return null;
  let value = data[pos[0]] - 40;
  return value;
}
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
function convertInjectionTime(data: number[]) {
  let pos = this.lib.pos1[2];
  if (pos.length === 0) return null;
  let value = (data[pos[0]] * 256 + data[pos[1]]) / 250;
  return value;
}
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
function convertVBat(data: number[]) {
  let pos = this.lib.pos1[3];
  if (pos.length === 0) return null;
  let value = data[pos[0]] / 10;
  return value;
}
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
function convertSpeed(data: number[]) {
  let pos = this.lib.pos1[4];
  if (pos.length === 0) return null;
  let value = data[pos[0]];
  return value;
}
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
function convertGDL(data: number[]) {
  let pos = this.lib.pos1[5];
  if (pos.length === 0) return null;
  let value = data[pos[0]] / 2 - 64;
  return value;
}
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
function convertTPS(data: number[]) {
  let pos = this.lib.pos1[6];
  if (pos.length === 0) return null;
  let value = data[pos[0]] / 2;
  return value;
}
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
function convertMAP(data: number[]) {
  let pos = this.lib.pos1[7];
  if (pos.length === 0) return null;
  let value = data[pos[0]];
  return value;
}
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
function convertIAT(data: number[]) {
  let pos = this.lib.pos1[8];
  if (pos.length === 0) return null;
  let value = data[pos[0]] - 40;
  if (value < 0) return 0;
  return value;
}
/**
 *
 * @param {*} data array data from ECU
 * return DEC value if that bike support.
 * else return null
 */
function convertOxy(data: number[]) {
  let pos = this.lib.pos2[0];
  if (pos.length === 0) return null;
  let value = (data[pos[0]] * 625) / 32; // * 5 * 1000 / 256
  return value;
}
/**
 *
 * @param {*} data
 */
function convertSidekick(data: number[]) {
  let pos = [4];
  if (pos.length === 0) return null;
  let value = data[pos[0]] % 16;
  return value;
}
/**
 *
 * @param {*} data
 */
function convertBreakAndStartSignal(data: number[]) {
  let pos = [4];
  if (pos.length === 0) return null;
  let value = parseInt(data[pos[0]] / 16);
  return value;
}
/**
 *
 * @param {*} data
 */
function convertFuelPump(data: number[]) {
  let pos = [8];
  if (pos.length === 0) return null;
  let value = data[pos[0]];
  return value;
}
//#endregion

export default BikeHelper.getInstance();
