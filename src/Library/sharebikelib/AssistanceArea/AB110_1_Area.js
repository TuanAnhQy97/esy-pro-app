/**
 * @flow
 */
import AssistanceHelper, {
  type TITLE_OF_ASSISTANCE,
  type AssistanceStructure
} from "./AssistanceHelper";
/**
 * Return title list for AB110 đèn pha to và AB 125
 */
/**
  {
    title: "",
    symptons: "",
    reasons: "",
    howToFix: ""
  }
*/
const ASSISTANCE_DATA = Object.freeze([
  {
    title: "Không thể nổ máy",
    symptons: "",
    reasons:
      "Hư hỏng bộ phận đánh lửa (bugi, ecu), mô tơ bơm xăng không hoạt động, có sự cố về kim phun",
    howToFix: "Kiểm tra bộ phận đánh lửa, mô tơ bơm xăng, kim phun"
  },
  {
    title: "Khó nổ máy",
    symptons: "",
    reasons:
      "Lỗi điện: Lỗi cảm biến và ECU\n\n" + "Lỗi cơ khí: Mất áp suất buồng đốt",
    howToFix:
      "Kiểm tra hệ thống cảm biến và ECU\n\nKiểm tra áp suất buồng đốt và thay thế"
  },
  {
    title: "Hệ thống làm mát không tốt",
    symptons: "Đồng hồ nhiệt độ vượt mức tiêu chuẩn, động cơ bị ỳ",
    reasons: "Hết nước mát, hệ thống làm mát bị rò rỉ hoặc thiếu dầu bôi trơn",
    howToFix: "Kiểm tra hệ thống làm mát, kiểm tra dầu máy"
  },
  {
    title: "Hay chết máy",
    symptons: "",
    reasons:
      "Lỗi hệ thống cảm biến hoặc ECU\nTốc độ vòng tua trạng thái cầm chừng quá thấp",
    howToFix: "Kiểm tra hệ thống cảm biến và ECU\nĐiều chỉnh garanti"
  },
  {
    title: "Tiêu hao nhiều xăng hơn bình thường",
    symptons: "Xe có khói đen hoặc có mùi xăng sống lúc khởi động",
    reasons:
      "Lỗi điện: Cài đặt thông số khí quyển không phù hợp\nLỗi cơ khí: Giảm áp suất buồng đốt",
    howToFix:
      "Cài đặt lại thông số khí quyển\nKiểm tra và thay thế chi tiết làm kín buồng đốt (pít tông, xéc măng, xupap)"
  },
  {
    title: "Rồ ga",
    symptons:
      "Không kiểm soát được động cơ khi dừng đèn xanh đèn đỏ, xe tự tăng tốc khi không kéo tay ga",
    reasons:
      "Hư hỏng cám biến tay ga, bộ phận truyền động (bi côn, côn văng bị bám dính)",
    howToFix: "Kiểm tra bộ phận truyền động và cảm biến tay ga"
  },
  {
    title: "Tăng tốc chậm",
    symptons: "Động cơ bị ỳ khi kéo ga",
    reasons:
      "Chế độ phun nhiên liệu không phù hợp, bộ truyền động (bi côn, côn văng) bị mài mòn",
    howToFix: "Kiểm tra bộ truyền động và thông số hoạt động của cảm biến"
  }
]);

export function getAssistanceListForAB110_1(): TITLE_OF_ASSISTANCE[] {
  let numberOfLetter = 80;
  return ASSISTANCE_DATA.map(
    (value, index): TITLE_OF_ASSISTANCE => {
      let getBriefFrom = "";
      let brief = AssistanceHelper.extractBriefFromContent(value);

      return { title: value.title, id: index, brief };
    }
  );
}

export function getDetailAssistanceForAB110_1(
  titleId: number
): AssistanceStructure {
  let detail: AssistanceStructure = {};
  if (ASSISTANCE_DATA[titleId] === undefined) {
    detail.title = "Unknown (" + titleId + ")";
    return detail;
  }
  return ASSISTANCE_DATA[titleId];
}
