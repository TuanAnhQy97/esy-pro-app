/**
 * @flow
 */
import {
  type TITLE_OF_ASSISTANCE,
  type AssistanceStructure
} from "./AssistanceHelper";
/**
 * Return title list for Lead 110
 */

const ASSISTANCE_DATA = Object.freeze([
  {
    title: "Không thể nổ máy",
    symptons: "",
    reasons:
      "Lỗi điện: Hư hỏng bộ phận đánh lửa (bugi, ecu), mô tơ bơm xăng không hoạt động, có sự cố về kim phun\nLỗi cơ khí: Mất áp suất nén buồng đốt",
    howToFix:
      "Kiểm tra bộ phận đánh lửa, mô tơ bơm xăng, kim phun\nKiểm tra áp suất buồng đốt"
  },
  {
    title: "Khó nổ máy",
    symptons: "",
    reasons: "Lỗi điện: Lỗi cảm biến và ECU\nLỗi cơ khí: Mất áp suất buồng đốt",
    howToFix: "Kiểm tra hệ thống cảm biến và ECU\nKiểm tra áp suất buồng đốt"
  },
  {
    title: "Hệ thống làm mát không tốt",
    symptons: "Đồng hồ nhiệt độ vượt mức tiêu chuẩn, động cơ bị ỳ",
    reasons: "Hết nước mát, hệ thống làm mát bị rò rỉ hoặc thiếu dầu bôi trơn",
    howToFix: "Kiểm tra hệ thống làm mát, kiểm tra dầu máy"
  },
  {
    title: "Hay chết máy",
    symptons: "",
    reasons:
      "Lỗi điện: Lỗi hệ thống cảm biến hoặc ECU\nLỗi cơ khí: Mất áp suất buồng đốt",
    howToFix: "Kiểm tra hệ thống cảm biến và ECU\nKiểm tra áp suất buồng đốt"
  },
  {
    title: "Tiêu hao nhiều xăng hơn bình thường",
    symptons: "Xe có khói đen hoặc có mùi xăng sống lúc khởi động",
    reasons:
      "Lỗi điện: Lỗi hệ thống cảm biến hoặc ECU hoặc chi tiết động cơ (xupap, xi lanh, xéc-măng) bị bào mòn hoặc rò rỉ hệ thống nhiên liệu\nLỗi cơ khí: Giảm áp suất buồng đốt Thiêu oxy do lọc gió bẩn Chi tiết động cơ (xupap, xi lanh, xéc-măng) bị bào mòn hoặc rò rỉ hệ thống nhiên liệu",
    howToFix:
      "Kiểm tra và điều chỉnh hoặc thay thế những chi tiết hư hỏng\nKiểm tra và thay thế chi tiết làm kín buồng đốt (pít - tông, xéc - măng, xupap)"
  },
  {
    title: "Rồ ga",
    symptons:
      "Không kiểm soát được động cơ khi dừng đèn xanh đèn đỏ, xe tự tăng tốc khi không kéo tay ga",
    reasons:
      "Hư hỏng cám biến tay ga, bộ phận truyền động (bi côn, côn văng bị bám dính)",
    howToFix: "Kiểm tra bộ phận truyền động và cảm biến tay ga"
  },
  {
    title: "Tăng tốc chậm",
    symptons: "Động cơ bị ỳ khi kéo ga",
    reasons:
      "Chế độ phun nhiên liệu không phù hợp, bộ truyền động (bi côn, côn văng) bị mài mòn",
    howToFix: "Kiểm tra cảm biến nhiệt độ động cơ, bộ truyền động"
  }
]);

export function getAssistanceListForLead110(): TITLE_OF_ASSISTANCE[] {
  let numberOfLetter = 80;
  return ASSISTANCE_DATA.map(
    (value, index): TITLE_OF_ASSISTANCE => {
      let getBriefFrom = "";
      let brief;

      if (value.symptons) {
        getBriefFrom = value.symptons;
      } else if (value.reasons) {
        getBriefFrom = value.reasons;
      } else getBriefFrom = value.howToFix;

      if (getBriefFrom.length < numberOfLetter) brief = getBriefFrom + "...";
      else {
        brief = getBriefFrom.slice(0, numberOfLetter);
        let lastWordPos = brief.lastIndexOf(" ");
        brief = brief.slice(0, lastWordPos);
        brief += " ...";
      }

      return { title: value.title, id: index, brief };
    }
  );
}

export function getDetailAssistanceForLead110(
  titleId: number
): AssistanceStructure {
  let detail: AssistanceStructure = {};
  if (ASSISTANCE_DATA[titleId] === undefined) {
    detail.title = "Unknown (" + titleId + ")";
    return detail;
  }
  return ASSISTANCE_DATA[titleId];
}
