/**
 * @flow
 */
import BikeHelper from "../BikeHelper";
import { getAssistanceListForSH, getDetailAssistanceForSH } from "./SH_Area";
import { BIKE_TYPE_ARRAY } from "../BasicBikeLib";
import {
  getAssistanceListForAB110_1,
  getDetailAssistanceForAB110_1
} from "./AB110_1_Area";
import {
  getAssistanceListForVision,
  getDetailAssistanceForVision
} from "./Vision_Area";
import {
  getAssistanceListForAB110_2,
  getDetailAssistanceForAB110_2
} from "./AB110_2_Area";
import {
  getDetailAssistanceForLead110,
  getAssistanceListForLead110
} from "./Lead110_Area";
import {
  getDetailAssistanceForLead125,
  getAssistanceListForLead125
} from "./Lead125_Area";

const numberOfLetter = 80;

/**
 * SH [100, xxx]
 * AB [110, xxx]
 * LE [120, xxx]
 * VI [130, xxx]
 * PC [140, xxx]
 *
 */
export type TITLE_OF_ASSISTANCE = {
  title: string,
  brief: string,
  id: number
};

export type AssistanceStructure = {
  title?: string,
  symptons?: string,
  reasons?: string,
  howToFix?: string
};

class AssistanceHelper {
  static instance: AssistanceHelper;
  static getInstance(): AssistanceHelper {
    if (!AssistanceHelper.instance) {
      AssistanceHelper.instance = new AssistanceHelper();
    }
    return AssistanceHelper.instance;
  }
  /**
   * Return detail of an assistance based on its ID
   * @param {*} assisId
   */
  getAssistanceDetail(assisId: number[]): AssistanceStructure {
    switch (assisId[0]) {
      case 100: // SH
        return getDetailAssistanceForSH(assisId[1]);
      case 110:
        return getDetailAssistanceForAB110_1(assisId[1]);
      case 111: // AB
      case 112:
        return getDetailAssistanceForAB110_2(assisId[1]);
      case 120: // LE
        return getDetailAssistanceForLead110(assisId[1]);
      case 121: // LE
        return getDetailAssistanceForLead125(assisId[1]);
      case 130: // VI
        return getDetailAssistanceForVision(assisId[1]);
    }
    return {
      title: "Unknown (" + assisId[0] + ", " + assisId[1] + ")",
      symptons: "",
      reasons: "",
      howToFix: ""
    };
  }
  getAssistanceListForBike(
    bikeType?: number | null = null
  ): TITLE_OF_ASSISTANCE[] {
    if (!bikeType) bikeType = BikeHelper.lib.basicInfo.bikeType;
    switch (bikeType) {
      case 100: // SH
        return getAssistanceListForSH();
      case 110:
        return getAssistanceListForAB110_2();
      case 111: // AB
      case 112:
        return getAssistanceListForAB110_1();
      case 120: // LE
        return getAssistanceListForLead110();
      case 121: // LE
        return getAssistanceListForLead125();
      case 130: // VI
        return getAssistanceListForVision();
      case 140: // PC
        break;
    }
    return [];
  }
  getOtherBikeList() {
    let currentBikeType = BikeHelper.lib.basicInfo.bikeType;
    let otherBikeList = [];
    for (let i = 0; i < BIKE_TYPE_ARRAY.length; i++)
      if (BIKE_TYPE_ARRAY[i][1] !== currentBikeType)
        otherBikeList.push(BIKE_TYPE_ARRAY[i]);

    return otherBikeList;
  }
  extractBriefFromContent(content: AssistanceStructure): string {
    let getBriefFrom = "";
    let brief = "";
    if (content.symptons) {
      getBriefFrom = content.symptons;
    } else if (content.reasons) {
      getBriefFrom = content.reasons;
    } else if (content.howToFix) {
      getBriefFrom = content.howToFix;
    }

    if (getBriefFrom.length < numberOfLetter) brief = getBriefFrom + "...";
    else {
      brief = getBriefFrom.slice(0, numberOfLetter);
      let lastWordPos = brief.lastIndexOf(" ");
      brief = brief.slice(0, lastWordPos);
      brief += " ...";
    }
    brief = brief.replace("\n\n", "\n");
    console.log("extractBriefFromContent", brief);
    return brief;
  }
}
export default AssistanceHelper.getInstance();
