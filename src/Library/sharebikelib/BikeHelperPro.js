/**
 * @flow
 */

import BasicBikeLibPro from "./BasicBikeLibPro";

class BikeHelperPro {
  static bikeHelperPro: BikeHelperPro;
  static getInstance(): BikeHelperPro {
    if (!BikeHelperPro.bikeHelperPro) {
      BikeHelperPro.bikeHelperPro = new BikeHelperPro();
      return BikeHelperPro.bikeHelperPro;
    } else {
      return BikeHelperPro.bikeHelperPro;
    }
  }
  constructor() {
    //#region Binding methods
    // main methods
    //#endregion
  }
  /**
   *      PUBLIC METHOD
   */
  lib: BasicBikeLibPro;
  prepareLib(_lib: BasicBikeLibPro) {
    this.lib = _lib;
  }
  /**
   *
   * @param {*} data
   */
  convertData1(data: number[]) {
    return [
      this.convertTPSVolt(data), // TPS
      this.convertTempVolt(data), // Temp
      this.convertIATVolt(data), // IAT
      this.convertMAPVolt(data), // MAP
      this.convertPullCNT(data), // Step
      this.convertIACCommand(data) // Lệnh IAC
    ];
  }
  convertData2(data) {
    return [
      this.convertAF(data), // AF
      this.convertHeatOxy(data) // Nung Oxy
    ];
  }
  convertData3(data) {
    return [
      this.convertMill(data), // Mill Status
      this.convertBaValue(data), // Góc nghiêng
      this.convertMAPAver(data) // MAP trung bình
    ];
  }
  convertData4(data) {
    return [
      this.convertFuelPump(data), // Bơm xăng
      this.convertSideStand(data), // Side kick
      this.convertStartSwitchInputSignal(data), // Break and start Button
      this.convertFan(data), // Quạt
      this.convertSCS(data), // SCS
      this.convertStarterSwitchCommand(data), // Lệnh công tắc đề
      this.convertStartSolenoidValve(data), // Van từ nhanh
      this.convertEVAPstatus(data), // Trạng thái hoạt động EVAP
      this.convertBaStatus(data), // Trạng thái ngắt của cảm biến nghiêng
      this.convertPCV(data), // Van PCV
      this.convertEngineStartInhibiterStatus(data)
    ];
  }

  //#region       INTERNAL METHOD
  /**
   *
   * @param {*} data array data from ECU
   * return DEC value if that bike support.
   * else return null
   */
  convertTPSVolt(data: number[]) {
    if (!this.lib.pos1.tps) {
      return null;
    }
    let value = (data[6] * 5) / 256;
    return value;
  }
  /**
   *
   * @param {*} data array data from ECU
   * return DEC value if that bike support.
   * else return null
   */
  convertTempVolt(data: number[]) {
    // Không có cảm biến này
    if (!this.lib.pos1.tempA && !this.lib.pos1.tempB) {
      return null;
    }
    let value;
    // Nhiệt độ theo Chip A
    if (this.lib.pos1.tempA) {
      value = (data[8] * 5) / 256;
    }
    // Nhiệt độ theo Chip B
    else if (this.lib.pos1.tempB) {
      value = (data[12] * 5) / 256;
    }
    return value;
  }
  /**
   *
   * @param {*} data array data from ECU
   * return DEC value if that bike support.
   * else return null
   */
  convertIATVolt(data: number[]) {
    if (!this.lib.pos1.iat) {
      return null;
    }
    let value = (data[10] * 5) / 256;
    return value;
  }
  /**
   *
   * @param {*} data array data from ECU
   * return DEC value if that bike support.
   * else return null
   */
  convertMAPVolt(data: number[]) {
    if (!this.lib.pos1.map) {
      return null;
    }
    let value = (data[12] * 5) / 256;
    return value;
  }
  /**
   *
   * @param {*} data
   */
  convertPullCNT(data: number[]) {
    if (!this.lib.pos1.pulcnt) {
      return null;
    }
    let value = data[21];
    return value;
  }
  /**
   *
   * @param {*} data
   */
  convertIACCommand(data: number[]) {
    if (!this.lib.pos1.iacCommand) {
      return null;
    }
    let value = data[22] * 1.74431 + data[23] * 0.0068;
    return value;
  }
  ///////////////////////////////// DATA 2
  /**
   *
   * @param {*} data array data from ECU
   * return DEC value if that bike support.
   * else return null
   */
  convertAF(data: number[]) {
    if (!this.lib.pos2.af) {
      return null;
    }
    let value = data[5] * 0.0078;
    return value;
  }
  /**
   *
   * @param {*} data array data from ECU
   * return DEC value if that bike support.
   * else return null
   */
  convertHeatOxy(data: number[]) {
    if (!this.lib.pos2.heatOxy) {
      return null;
    }
    let value = data[6];
    return value;
  }
  //////////////////////////////////// DATA 3
  /**
   * @param {*} data
   */
  convertMill(data: number[]) {
    if (!this.lib.pos3.mill) {
      return null;
    }
    return data[4];
  }
  /**
   * @param {*} data
   */
  convertBaValue(data: number[]) {
    if (!this.lib.pos3.baValue) {
      return null;
    }
    let value = (data[17] * 5) / 256;
    return value;
  }
  /**
   * @param {*} data
   */
  convertMAPAver(data: number[]) {
    if (!this.lib.pos3.mapAverage) {
      return null;
    }
    let value = data[24];
    return value;
  }
  //////////////////////////////////// DATA 4
  /**
   *
   * @param {*} data
   */
  convertEngineStartInhibiterStatus(data: number[]) {
    if (!this.lib.pos4.engineStartInhibiterStatus) {
      return null;
    }
    return byteTobitArr(data[4])[0];
  }
  /**
   *
   * @param {*} data
   */
  convertSideStand(data: number[]) {
    if (!this.lib.pos4.sideStand) {
      return null;
    }
    return byteTobitArr(data[4])[1];
  }
  /**
   * @param {*} data
   */
  convertSCS(data: number[]) {
    if (!this.lib.pos4.scs) {
      return null;
    }
    return byteTobitArr(data[4])[2];
  }

  /**
   * @param {*} data
   */
  convertBaStatus(data: number[]) {
    if (!this.lib.pos4.ba) {
      return null;
    }
    return byteTobitArr(data[4])[4];
  }

  /**
   *
   * @param {*} data
   */
  convertStartSwitchInputSignal(data: number[]) {
    if (!this.lib.pos4.starterSwitchInputSignal) {
      return null;
    }
    return byteTobitArr(data[4])[5];
  }
  /**
   *
   * @param {*} data
   */
  convertFuelPump(data: number[]) {
    if (!this.lib.pos4.pump) {
      return null;
    }
    return byteTobitArr(data[8])[0];
  }
  /**
   *
   * @param {*} data
   */
  convertPCV(data: number[]) {
    if (!this.lib.pos4.pcv) {
      return null;
    }
    return byteTobitArr(data[8])[2];
  }
  /**
   * @param {*} data
   */
  convertEVAPstatus(data: number[]) {
    if (!this.lib.pos4.evap) {
      return null;
    }
    return byteTobitArr(data[8])[3];
  }
  /**
   * @param {*} data
   */
  convertStarterSwitchCommand(data: number[]) {
    if (!this.lib.pos4.startSwitchCommand) {
      return null;
    }
    return byteTobitArr(data[8])[4];
  }
  /**
   * @param {*} data
   */
  convertStartSolenoidValve(data: number[]) {
    if (!this.lib.pos4.startSolenoidValve) {
      return null;
    }
    return byteTobitArr(data[8])[6];
  }
  /**
   * @param {*} data
   */
  convertFan(data: number[]) {
    if (!this.lib.pos4.fan) {
      return null;
    }
    return byteTobitArr(data[9])[0];
  }
  //#endregion
}

export default BikeHelperPro.getInstance();
/**
 * Đổi byte thành mảng bit
 * @param {*} byte  0 <= số <= 255
 */
function byteTobitArr(byte) {
  // let bitArr = [
  //   (byte & 0x80) >> 7,
  //   (byte & 0x40) >> 6,
  //   (byte & 0x20) >> 5,
  //   (byte & 0x10) >> 4,
  //   (byte & 0x08) >> 3,
  //   (byte & 0x04) >> 2,
  //   (byte & 0x02) >> 1,
  //   byte & 0x01
  // ];
  let bitArr = [
    byte & 0x01,
    (byte & 0x02) >> 1,
    (byte & 0x04) >> 2,
    (byte & 0x08) >> 3,
    (byte & 0x10) >> 4,
    (byte & 0x20) >> 5,
    (byte & 0x40) >> 6,
    (byte & 0x80) >> 7
  ];
  return bitArr;
}
