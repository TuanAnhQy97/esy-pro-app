/**
 * @flow
 */
type ExplainStructure = {
  name: string,
  cause: string,
  effect: string,
  howToFix: string,
  icon: any,
};

const ESY_ERROR_CODE_BASE = 1000;
const ERROR_STATE_LOW = 0,
  ERROR_STATE_HIGH = 1,
  ERROR_STATE_VERY_LOW = 3,
  ERROR_STATE_VERY_HIGH = 2;
const ESY_ERROR_CODE = Object.freeze({
  /**
   * Các mã lỗi riêng do ESY phát hiện ra
   * Format: ESY_ERROR_CODE_BASE + errorCode + errorState
   * Ex: ESY_ERROR_CODE_BASE + 100 + 01 = 1101;
   */
  /** BATTERY */
  BATTERY_LOW: 100 + ERROR_STATE_LOW + ESY_ERROR_CODE_BASE,
  BATTERY_VERY_LOW: 100 + ERROR_STATE_VERY_LOW + ESY_ERROR_CODE_BASE,
  /** TEMPERATURE */
  TEMPERATURE_VERY_LOW: 110 + ERROR_STATE_LOW + ESY_ERROR_CODE_BASE,
  TEMPERATURE_VERY_HIGH: 110 + ERROR_STATE_VERY_HIGH + ESY_ERROR_CODE_BASE,
  TEMPERATURE_HIGH: 110 + ERROR_STATE_HIGH + ESY_ERROR_CODE_BASE,
  /** TPS */
  TPS_VERY_HIGH: 120 + ERROR_STATE_VERY_HIGH + ESY_ERROR_CODE_BASE,
  TPS_HIGH: 120 + ERROR_STATE_HIGH + ESY_ERROR_CODE_BASE,
  TPS_SLIGHTLY_HIGH: 120 + ERROR_STATE_LOW + ESY_ERROR_CODE_BASE,
  /** MAP */
  MAP_LOW: 130 + ERROR_STATE_LOW + ESY_ERROR_CODE_BASE,
  /** SIDE KICK */
  SIDEKICK_IS_ON: 140 + ERROR_STATE_LOW + ESY_ERROR_CODE_BASE,
  /** RPM */
  RPM_LOW: 150 + ERROR_STATE_LOW + ESY_ERROR_CODE_BASE,
  RPM_VERY_LOW: 150 + ERROR_STATE_VERY_LOW + ESY_ERROR_CODE_BASE,
  RPM_HIGH: 150 + ERROR_STATE_HIGH + ESY_ERROR_CODE_BASE,
  RPM_VERY_HIGH: 150 + ERROR_STATE_VERY_HIGH + ESY_ERROR_CODE_BASE,
  /** Break and Start signal */
  BREAK_AND_START_SIGNAL: 160 + ERROR_STATE_LOW + ESY_ERROR_CODE_BASE,
  /** CHARGER */
  CHARGER_SYSTEM_UNDER: 170 + ERROR_STATE_LOW + ESY_ERROR_CODE_BASE,
  CHARGER_SYSTEM_OVER: 170 + ERROR_STATE_HIGH + ESY_ERROR_CODE_BASE,
  /** INJECTION TIME */
  INJECTIONTIME_NOT_IN_RANGE: 180 + 4 + ESY_ERROR_CODE_BASE,
  /** GDL */
  GDL_NOT_IN_RANGE: 190 + 4 + ESY_ERROR_CODE_BASE,
  /** OXY */
  OXY_0_mV: 200 + 4 + ESY_ERROR_CODE_BASE,
  /** Oil */
  OIL_GOING_TO_REPLACE: 210 + ERROR_STATE_LOW + ESY_ERROR_CODE_BASE,
  OIL_REPLACE_NOW: 210 + ERROR_STATE_VERY_LOW + ESY_ERROR_CODE_BASE,
});
export {ESY_ERROR_CODE};

class ExplainError {
  static instance: ExplainError;
  static getInstance(): ExplainError {
    if (!ExplainError.instance) {
      ExplainError.instance = new ExplainError();
    }
    return ExplainError.instance;
  }
  /**
   * Lấy giải thích mã lỗi
   * @param {*} errorCode Mã lỗi
   * @param {*} errorState 1 or 2 (low or high)
   */
  getExplain(errorCode: number, errorState: number) {
    let explain: ExplainStructure = {
      name: '',
      cause: '',
      effect: '',
      howToFix: '',
      icon: null,
    };
    if (errorCode < ESY_ERROR_CODE_BASE) {
      switch (errorCode) {
        case 1:
          explain.name = 'Cảm biến Áp suất khí nạp (MAP)';
          explain.effect = 'Ga lên không được hoặc hao xăng';
          explain.icon = require('../../assets/Images/BikeComponents/pressure.png');
          switch (errorState) {
            case 1:
              explain.cause = 'Điện áp cảm biến MAP thấp';
              explain.howToFix = '- Hở mạch hoặc chập mạch ở dây cảm biến MAP\n- Cảm biến MAP bị lỗi\n- ECU bị lỗi';
              break;
            case 2:
              explain.cause = 'Điện áp cảm biến MAP cao';
              explain.howToFix =
                '- Tiếp xúc lỏng hoặc kém ở đầu nối cảm biến MAP\n- Mạch hở ở dây cảm biến MAP\n- Cảm biến MAP bị lỗi\n- ECU bị lỗi';
              break;
            default:
              break;
          }
          break;
        case 7:
          explain.name = 'Cảm biến nhiệt độ động cơ';
          explain.effect = 'Báo sai nhiệt độ động cơ hoặc hao xăng';
          explain.icon = require('../../assets/Images/BikeComponents/fire.png');
          switch (errorState) {
            case 1:
              explain.cause = 'Điện áp cảm biến thấp';
              explain.howToFix = '- Chập mạch ở dây cảm biến\n- Cảm biến bị lỗi\n- ECU bị lỗi';
              break;
            case 2:
              explain.cause = 'Điện áp cảm biến cao';
              explain.howToFix =
                '- Tiếp xúc ở đầu nối cảm biến lỏng hoặc kém\n- Hở mạch hoặc chập mạch ở dây cảm biến\n- Cảm biến bị lỗi\n- ECU bị lỗi';
              break;
            default:
              break;
          }
          break;
        case 8:
          explain.name = 'Cảm biến vị trí tay ga (TP)';
          explain.effect = 'Hụt ga, òa ga, hao xăng hay chết máy';
          explain.icon = require('../../assets/Images/BikeComponents/throttle.png');
          switch (errorState) {
            case 1:
              explain.cause = 'Điện áp cảm biến TP thấp';
              explain.howToFix =
                '- Tiếp xúc ở đầu nối cảm biến TP lỏng hoặc kém\n- Hở mạch hoặc chập mạch ở dây cảm biến TP\n- Cảm biến TP bị lỗi\n- ECU bị lỗi';
              break;
            case 2:
              explain.cause = 'Điện áp cảm biến TP cao';
              explain.howToFix = '- Chập mạch ở dây cảm biến TP\n- Cảm biến TP bị lỗi\n- ECU bị lỗi';
              break;
            default:
              break;
          }
          break;
        case 9:
          explain.name = 'Cảm biến nhiệt độ dòng khí nạp (IAT)';
          explain.effect = 'Hao xăng hoặc thiếu xăng';
          explain.icon = require('../../assets/Images/BikeComponents/airTemp.png');
          switch (errorState) {
            case 1:
              explain.cause = 'Điện áp cảm biến IAT thấp';
              explain.howToFix = '- Chập mạch ở dây cảm biến IAT\n- Cảm biến IAT bị lỗi\n- ECU bị lỗi';
              break;
            case 2:
              explain.cause = 'Điện áp cảm biến IAT cao';
              explain.howToFix =
                '- Tiếp xúc ở đầu nối cảm biến IAT lỏng hoặc kém\n- Hở mạch hoặc chập mạch ở dây cảm biến IAT\n- Cảm biến IAT bị lỗi\n- ECU bị lỗi';
              break;
            default:
              break;
          }
          break;
        case 11:
          explain.name = 'Cảm biến vận tốc (VSS)';
          explain.cause = 'VSS không tạo xung (không có tín hiệu cảm biến tốc độ xe (trục trặc mạch điện))';
          explain.effect = 'Đồng hồ công tơ mét và chế độ Idling Stop không hoạt động';
          explain.howToFix =
            '- Tiếp xúc của đầu nối với cảm biến tốc độ xe bị lỏng hoặc kém.\n- Hở mạch hoặc chập mạnh ở đầu nối cảm biến tốc độ xe.\n- Cảm biến tốc độ xe bị lỗi\n- ECU bị lỗi';
          explain.icon = require('../../assets/Images/Dashboard/speedometer.png');
          break;
        case 12:
          explain.name = 'Kim phun';
          explain.cause = '';
          explain.effect = 'Động hay chết máy hoặc không nổ được máy';
          explain.howToFix =
            '- Tiếp xúc của đầu nối kim phun số bị lỏng hoặc kém\n- Hở mạch hoặc chập mạch ở dây đầu nối kim phun\n- Kim phun bị lỗi.\n- ECU bị lỗi';
          explain.icon = require('../../assets/Images/BikeComponents/injector.png');
          break;
        case 13:
          explain.name = 'Kim phun (2)';
          explain.cause = '';
          explain.effect = 'Động hay chết máy hoặc không nổ được máy';
          explain.howToFix =
            '- Tiếp xúc của đầu nối kim phun số bị lỏng hoặc kém\n- Hở mạch hoặc chập mạch ở dây đầu nối kim phun\n- Kim phun bị lỗi.\n- ECU bị lỗi';
          explain.icon = require('../../assets/Images/BikeComponents/injector.png');
          break;
        case 14:
          explain.name = 'Kim phun (3)';
          explain.cause = '';
          explain.effect = 'Động hay chết máy hoặc không nổ được máy';
          explain.howToFix =
            '- Tiếp xúc của đầu nối kim phun số bị lỏng hoặc kém\n- Hở mạch hoặc chập mạch ở dây đầu nối kim phun\n- Kim phun bị lỗi.\n- ECU bị lỗi';
          explain.icon = require('../../assets/Images/BikeComponents/injector.png');
          break;
        case 15:
          explain.name = 'Kim phun (4)';
          explain.cause = '';
          explain.effect = 'Động hay chết máy hoặc không nổ được máy';
          explain.howToFix =
            '- Tiếp xúc của đầu nối kim phun số bị lỏng hoặc kém\n- Hở mạch hoặc chập mạch ở dây đầu nối kim phun\n- Kim phun bị lỗi.\n- ECU bị lỗi';
          explain.icon = require('../../assets/Images/BikeComponents/injector.png');
          break;
        case 21:
          explain.name = 'Tín hiệu khí thải';
          explain.cause = '';
          explain.effect = 'Nhiên liệu tiêu hao không ổn định';
          explain.howToFix =
            '- Tiếp xúc ở đầu nối cảm biến Oxy lỏng hoặc kém.\n- Chập mạch ở cảm biến Oxy.\n- Cảm biến Oxy bị lỗi.\n- ECU bị lỗi';
          explain.icon = require('../../assets/Images/BikeComponents/eco.png');
          break;
        case 23:
          explain.name = 'Bộ nung Oxy';
          explain.cause = '';
          explain.effect = 'Nhiên liệu tiêu hao không ổn định';
          explain.howToFix =
            '- Tiếp xúc ở đầu nối cảm biến Oxy lỏng hoặc kém\n- Hở mạch hoặc chập mạch ở máy làm nóng cảm biến Oxy\n- Máy làm nóng cảm biến Oxy bị lỗi\n- ECU bị lỗi';
          break;
        case 29:
          explain.name = 'Van điều khiển không tải (IACV)';
          explain.cause = '';
          explain.effect = 'Thường xuyên chết máy, khó nổ hoặc hụt ga';
          explain.howToFix =
            '- Tiếp xúc ở đầu nối IACV bị lỏng hoặc kém\n- Hở mạch hoặc chập mạch ở IACV\n- IACV bị lỗi\n- ECU bị lỗi';
          break;
        case 33:
          explain.name = 'EEPROM';
          explain.cause = 'Lỗi bộ nhớ ECU';
          explain.effect = 'Xe mất garanti và chết máy đột ngột';
          explain.howToFix = 'Kiểm tra và sửa chữa ECU';
          break;
        case 48:
          explain.name = 'Kim phun (7)';
          explain.cause = '';
          explain.effect = 'Động hay chết máy hoặc không nổ được máy';
          explain.howToFix =
            '- Tiếp xúc của đầu nối kim phun số bị lỏng hoặc kém\n- Hở mạch hoặc chập mạch ở dây đầu nối kim phun\n- Kim phun bị lỗi.\n- ECU bị lỗi';
          explain.icon = require('../../assets/Images/BikeComponents/injector.png');
          break;
        case 49:
          explain.name = 'Kim phun (8)';
          explain.cause = '';
          explain.effect = 'Động hay chết máy hoặc không nổ được máy';
          explain.howToFix =
            '- Tiếp xúc của đầu nối kim phun số bị lỏng hoặc kém\n- Hở mạch hoặc chập mạch ở dây đầu nối kim phun\n- Kim phun bị lỗi.\n- ECU bị lỗi';
          explain.icon = require('../../assets/Images/BikeComponents/injector.png');
          break;
        case 52:
          explain.name = 'Trục trặc cảm biến CKP';
          explain.cause = 'Trục trặc cảm biến vị trí trục khuỷu';
          explain.effect = 'Không nổ được máy khi đèn báo lỗi sáng';
          explain.howToFix =
            '- Tiếp xúc trên đầu nối của máy phát điện bị lỏng hoặc kém.\n- Hở hoặc chập mạch trong dây dẫn cảm biến CKP\n- Cảm biến CKP bị lỗi\n- ECU bị lỗi';
          break;
        case 54:
          explain.name = 'Cảm biến góc nghiêng (BA)';
          explain.cause = '';
          explain.effect = 'Không nổ được máy khi đèn báo lỗi sáng';
          explain.howToFix =
            '- Tiếp xúc trên đầu nối cảm biến BA bị lỏng hoặc kém\n- Hở hoặc chập mạch trong dây dẫn cảm biến BA\n- Cảm biến BA bị lỗi\n- ECU bị lỗi';
          break;
        case 57:
          explain.name = 'Quá nhiệt';
          explain.cause = 'Hệ thống làm mát không tốt';
          explain.effect = 'Xe bị ì hoặc động cơ bị bó';
          explain.howToFix = 'Kiểm tra hệ thống làm mát và dầu máy';
          explain.icon = require('../../assets/Images/BikeComponents/fire.png');
          break;
        case 79:
          explain.name = 'Hệ thống ga điện';
          explain.cause = 'Lỗi motor điều khiển hoặc gián đoạn tín hiệu từ ECU';
          explain.effect = 'Xe không thể tăng tốc';
          explain.howToFix = '';
          break;
        // case 81:
        //   explain.name = "Mô bin đánh lửa";
        //   explain.cause = "Chập tín hiệu từ dây xuống gnd or hỏng ecu";
        //   explain.effect = "Không nổ được máy khi đèn báo lỗi sáng";
        //   explain.howToFix = "Kiểm dây điện hoặc thay ECU hoặc thay thế mobin";
        //   break;
        case 82:
          explain.name = 'Van cầm chừng nhanh';
          explain.cause = '';
          explain.effect = 'Khó khởi động';
          explain.howToFix = 'Kiểm tra van từ nhanh hoặc ECU';
          break;
        case 88:
          explain.name = 'EVAP van ngưng tụ khí gas';
          explain.cause = '';
          explain.effect = 'Chết máy hoặc khó khởi động, tốc độ vòng tua cầm chừng không ổn định';
          explain.howToFix = '';
          break;
        case 91:
          explain.name = 'Cuộn đánh lửa';
          explain.cause = 'Chập cuộn sơ cấp hoặc lỗi ECU';
          explain.effect = '';
          explain.howToFix = '';
          break;
        default:
          explain.name = 'Unknown (' + errorCode + ')';
          explain.cause = 'Unknown';
          explain.effect = 'Unknown';
          explain.howToFix = 'Unknown';
          explain.icon = require('../../assets/Images/Share/question.png');
          break;
      }
      if (explain.icon === null) {
        explain.icon = require('../../assets/Images/Share/errorTriangle.png');
      }
    } else {
      switch (errorCode) {
        /**
         * Battery
         */
        case ESY_ERROR_CODE.BATTERY_VERY_LOW:
          explain.name = 'Nguồn điện quá yếu';
          explain.cause = 'Điện áp bình ắc quy quá thấp do hết điện hoặc bình đã hỏng hoặc dây điện chập chờn';
          explain.effect = 'Khó nổ máy hoặc không thể nổ máy';
          explain.howToFix = 'Sạc điện bình ắc quy hoặc thay thế';
          explain.icon = require('../../assets/Images/BikeComponents/battery.png');
          break;
        case ESY_ERROR_CODE.BATTERY_LOW:
          explain.name = 'Nguồn điện yếu';
          explain.cause = 'Điện áp bình ắc quy thấp hoặc dây điện chập chờn';
          explain.effect = 'Khó nổ máy hoặc không thể nổ máy';
          explain.howToFix = 'Cần bảo trì bình ắc quy';
          explain.icon = require('../../assets/Images/BikeComponents/battery.png');
          break;
        /**
         * TEMPERATURE
         */
        case ESY_ERROR_CODE.TEMPERATURE_VERY_LOW:
          explain.name = 'Nhiệt độ quá thấp';
          explain.cause = 'Nhiệt độ động cơ quá thấp';
          explain.effect = 'Khó nổ máy hoặc không thể nổ máy';
          explain.howToFix = '';
          explain.icon = require('../../assets/Images/BikeComponents/fire.png');
          break;
        case ESY_ERROR_CODE.TEMPERATURE_HIGH:
          explain.name = 'Quá nhiệt';
          explain.cause = 'Hệ thống làm mát không tốt';
          explain.effect = '';
          explain.howToFix = 'Kiểm tra hệ thống làm mát và dầu máy';
          explain.icon = require('../../assets/Images/BikeComponents/fire.png');
          break;
        case ESY_ERROR_CODE.TEMPERATURE_VERY_HIGH:
          explain.name = 'Quá nhiệt';
          explain.cause = 'Hệ thống làm mát không tốt';
          explain.effect = 'Khó nổ máy hoặc không thể nổ máy';
          explain.howToFix = 'Kiểm tra hệ thống làm mát và dầu máy';
          explain.icon = require('../../assets/Images/BikeComponents/fire.png');
          break;
        /**
         * TPS
         */
        case ESY_ERROR_CODE.TPS_VERY_HIGH:
          explain.name = 'Cảm biến góc tay ga hoạt động sai lệch';
          explain.cause = 'Cảm biến góc tay ga hoạt động sai lệch\nKiểm tra tình trạng dây ga và chất lượng cảm biến';
          explain.effect = 'Không thể nổ máy';
          explain.howToFix = '';
          explain.icon = require('../../assets/Images/BikeComponents/throttle.png');
          break;
        case ESY_ERROR_CODE.TPS_HIGH:
          explain.name = 'Cảm biến góc tay ga hoạt động sai lệch';
          explain.cause = 'Cảm biến góc tay ga hoạt động sai lệch\nKiểm tra tình trạng dây ga và chất lượng cảm biến';
          explain.effect = 'Rồ ga hoặc khó nổ máy';
          explain.howToFix = '';
          explain.icon = require('../../assets/Images/BikeComponents/throttle.png');
          break;
        case ESY_ERROR_CODE.TPS_SLIGHTLY_HIGH:
          explain.name = 'Cảm biến góc tay ga hoạt động sai lệch';
          explain.cause = 'Cảm biến góc tay ga hoạt động sai lệch\nKiểm tra tình trạng dây ga và chất lượng cảm biến';
          explain.effect = 'Rồ ga hoặc khó nổ máy';
          explain.howToFix = '';
          explain.icon = require('../../assets/Images/BikeComponents/throttle.png');
          break;
        /**
         * MAP
         */
        case ESY_ERROR_CODE.MAP_LOW:
          explain.name = 'Áp suất khí nạp';
          explain.cause = 'Áp suất khí nạp thấp';
          explain.effect = 'Cảm giác lái bị hụt ga';
          explain.howToFix = '';
          explain.icon = require('../../assets/Images/BikeComponents/airTemp.png');
          break;
        case ESY_ERROR_CODE.SIDEKICK_IS_ON:
          explain.name = 'Cảm biến chân chống cạnh trục trặc';
          explain.cause = 'Chân chống cạnh được gạt xuống hoặc cảm biến chân chống cạnh không hoạt động';
          explain.effect = 'Xe không nổ được máy';
          explain.howToFix = 'Kiểm tra chân chống cạnh';
          explain.icon = require('../../assets/Images/BikeComponents/sideStand.png');
          break;
        case ESY_ERROR_CODE.BREAK_AND_START_SIGNAL:
          explain.name = 'Nút đề và phanh trục trặc';
          explain.cause = 'Nút đề hoặc cảm biến tay phanh bị trập trờn hoặc hỏng';
          explain.icon = require('../../assets/Images/BikeComponents/startButton.png');
          break;
        /**
         * RPM
         */
        case ESY_ERROR_CODE.RPM_LOW:
          explain.name = 'Vòng tua máy chế độ không tải thấp';
          break;
        case ESY_ERROR_CODE.RPM_VERY_LOW:
          explain.name = 'Vòng tua máy chế độ không tải rất thấp';
          break;
        case ESY_ERROR_CODE.RPM_HIGH:
          explain.name = 'Vòng tua máy chế độ không tải cao';
          break;
        case ESY_ERROR_CODE.RPM_VERY_HIGH:
          explain.name = 'Vòng tua máy chế độ không tải rất cao';
          break;
        /**
         * Injection time
         */
        case ESY_ERROR_CODE.INJECTIONTIME_NOT_IN_RANGE:
          explain.name = 'Thời gian phun nhiên liệu không phù hợp';
          break;
        /**
         * Charger
         */
        case ESY_ERROR_CODE.CHARGER_SYSTEM_UNDER:
          explain.name = 'Điện áp hệ thống nạp sạc thấp';
          break;
        case ESY_ERROR_CODE.CHARGER_SYSTEM_OVER:
          explain.name = 'Điện áp hệ thống nạp sạc vượt ngưỡng';
          break;
        /**
         * GDL
         */
        case ESY_ERROR_CODE.GDL_NOT_IN_RANGE:
          explain.name = 'Góc đánh lửa không chính xác';
          break;
        /**
         * Oxy
         */
        case ESY_ERROR_CODE.OXY_0_mV:
          explain.name = 'Cảm biến Oxy hoạt động sai lệch';
          explain.cause = 'Cảm biến Oxy hoạt động sai lệch';
          explain.effect = 'Xe tiêu hao nhiều nhiên liệu';
          explain.howToFix = 'Kiểm tra cảm biến Oxy';
          explain.icon = require('../../assets/Images/BikeComponents/eco.png');
          break;
        /**
         * Oil
         */
        case ESY_ERROR_CODE.OIL_GOING_TO_REPLACE:
          explain.name = 'Thay dầu đúng hạn để xế luôn vận hành êm ái';
          explain.cause = '';
          explain.effect = '';
          explain.howToFix = '';
          explain.icon = require('../../assets/Images/BikeComponents/oil.png');
          break;
        case ESY_ERROR_CODE.OIL_REPLACE_NOW:
          explain.name = 'Thay dầu không đúng hạn giảm tuổi thọ động cơ';
          explain.cause = '';
          explain.effect = '';
          explain.howToFix = '';
          explain.icon = require('../../assets/Images/BikeComponents/oil.png');
          break;

        default:
          explain.name = 'Unknown (' + errorCode + ')';
          explain.cause = 'Unknown';
          explain.effect = 'Unknown';
          explain.howToFix = 'Unknown';
          explain.icon = require('../../assets/Images/Share/question.png');
          break;
      }
    }
    return explain;
  }
}
export default ExplainError.getInstance();
