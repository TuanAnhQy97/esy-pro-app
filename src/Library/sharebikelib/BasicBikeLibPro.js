/**
 * @flow
 */

export type POS1 = {
  tps?: boolean, //TPS                         POS 6
  tempA?: boolean, // Temp                       POS 8
  tempB?: boolean, // Temp                       POS 12
  iat?: boolean, // IAT                       POS 10
  map?: boolean, // MAP                       POS 12
  pulcnt?: boolean, // Pull CNT                  POS 21
  iacCommand?: boolean // Lệnh IAC               POS 22 23
};

export type POS2 = {
  af: boolean, // AF
  heatOxy?: boolean // Nung oxy                    POS 6
};

export type POS3 = {
  mill: boolean, // Mill                       POS 4
  baValue?: boolean, // Nghiêng                     POS 17
  mapAverage?: boolean // MAP trung bình               POS 24
};

export type POS4 = {
  /**
   * Byte 4
   */
  engineStartInhibiterStatus?: boolean, // Engine start inhibiter status
  sideStand?: boolean, // side Stand
  scs: boolean,
  ba?: boolean, // CB nghiêng khi đổ xe
  starterSwitchInputSignal?: boolean, // Starter switch input signal
  /**
   * Byte 8
   */
  pump: boolean, // Bơm
  pcv?: boolean, //  Van Pair Control Value
  evap?: boolean, // EVAP
  startSwitchCommand?: boolean, // Starter switch command
  startSolenoidValve?: boolean, // Start solenoid valve
  /**
   * Byte 9
   */
  fan?: boolean
};

class BasicBikeLibPro {
  pos1: POS1;
  pos2: POS2;
  pos3: POS3;
  pos4: POS4;
}
export default BasicBikeLibPro;
