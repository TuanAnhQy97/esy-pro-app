/**
 * @flow
 */

export type TipStructure = {
  title: string,
  content: string
};

const TIP_DATA: TipStructure[] = Object.freeze([
  {
    title: "4 cách đi xe tiết kiệm xăng hiệu quả",
    content:
      "Không tăng giảm ga đột ngột\nKhông nên chở quá nặng\nThường xuyên kiểm tra lốp xe\nNên tắt máy khi dừng xe từ 25s"
  },
  {
    title: "7 lưu ý để xe tay ga luôn bền khỏe",
    content:
      "Đợi đèn báo FI tắt mới khởi động xe\nDùng đồng thời cả 2 phanh\nKhông vừa ga vừa phanh\nKhông đi xe ở tốc độ quá chậm\nGiữ tay ga ổn định\nKhông quên thay dầu láp\nKhông đi xe khi xăng gần cạn kiệt"
  },
  {
    title: "Khóa phanh xe đơn giản và hiệu quả",
    content:
      "Bước 1: Bóp hết tay phanh bên trái (phanh sau), gạt nhẹ lẫy lên cho đến khi nghe tiếng cạch. \nBước 2: Nhả tay ra, cần phanh sẽ giữ nguyên vị trí và phanh sau đã được siết vào. \nNgoài ra có thể đặt tay gẩy nhẹ lẫy khoá lên trong quá trình bóp phanh sẽ dễ hơn.\nMở khóa phanh: Bóp hết lực phanh sau đó nhả tay ra "
  },
  {
    title: "Điểm mặt ưu và nhược điểm của hệ thống phun xăng điện tử (PGM FI)",
    content:
      "1. Ưu điểm:\nTiết kiệm nhiên liệu hơn xe dùng chế hoà khí từ 15-20%.\nĐộng cơ làm việc nhạy cảm hơn ở các chế độ, nhất là khả năng khởi động xe trong thời tiết lạnh.\nKhí thải ít độc hại hơn, đặc biệt ở các xe máy sử dụng hệ thống FI có cảm biến Lambda.\n2. Nhược điểm:\nChi phí cao: Do cấu tạo phức tạp và nhạy cảm nên chi phí của hệ thống phun xăng điện tử cao hơn bộ chế hòa khí . Từ đó đẩy giá thành xe cao hơn so với những phiên bản trước đây.\nYêu cầu khắt khe về nhiên liệu: Trong quá trình phun xăng, nếu chất lượng nhiên liệu không tốt, bộ lọc không hiệu quả sẽ dẫn tới việc kim phun bị tắc, đóng cặn. Khi đó, lượng xăng cung cấp không đủ theo nhu cầu thực tế nên xe yếu và thường xuyên chết máy.\nChi phí sửa chữa, thay thế cao: Cấu tạo phức tạp nên việc sửa chữa, bảo dưỡng khó khăn đòi hỏi trình độ chuyên môn cao, thiết bị chuyên dung kiểm tra. Ngoài ra, thay thế phụ tùng linh kiện của hệ thống này cũng cao hơn bình thường, thậm chí phải thay cả cụm.\nHệ thống bơm nhiên liệu dễ bị hỏng: Nguyên nhân là do thói quen nhất nút khởi động ngay sau khi bật khóa điện hoặc nhấn nút khởi động liên tục của người dung. Điều này khiến hệ thống không đủ thời gian nạp nhiên liệu vào vòi bơm, làm giảm tuổi thọ của hệ thống bơm nhiên liệu."
  },
  {
    title: "5 cách phân biệt nhớt giả và nhớt thật",
    content:
      "- Bề mặt nắp chai kém sắc nét, niêm thủ công, vết hàn thô sơ, màu sắc khác lạ\n- Không có logo in giữa nắp và thân\n- Thân chai bị lõm 2 bên, không đầy\n- Có lỗi chính tả trên nhãn\n- Giá rẻ hơn giá thị trường"
  },
  {
    title: "Chọn loại dầu xe máy theo thời tiết",
    content:
      '- Mùa hè, nên sử dụng các loại dầu có chỉ số đặc để bảo vệ máy cũng như bền bỉ hơn khi chạy đường dài.\n- Mùa đông, cần chọn các loại dầu có chỉ số loãng để xe dễ nổ máy hơn, đặc biệt là sáng sớm hoặc xe ít sử dụng.\nHiện nay trên thị trường đang thông dụng loại dầu nhớt đa cấp với ký hiệu "W" trên bình dầu. "W" có nghĩa là Weather (thời tiết), nhằm chỉ khả năng phù hợp với nhiều loại thời tiết. Để nhận biết chỉ số loãng hay đặc trên các lọ dầu, bạn cần nhìn vào thông số trên thân lọ như 20W50, 15W30, 10W40... số càng lớn nhớt càng đặc. Nhớt đặc giúp xe chạy êm hơn nhưng máy lỳ hơn. Ngược lại, dầu loãng thông số nhỏ và giúp xe chạy bốc hơn, nhưng tiếng máy lại kêu hơn.\n* Chú ý:\nĐối với xe cũ hoặc vận hành trong điều kiện đường xấu, nhiều dốc, ẩm ướt hoặc bụi bặm thì cần cần thay dầu sớm hơn.\n❌ Xe chạy trong thành phố với chế độ chạy/dừng liên tục cần thay dầu thường xuyên hơn so với xe chạy đường trường tốc độ ổn định và nên dùng dầu loãng (chữ số sau W nhỏ: 15W20, 15W30) là tốt nhất để xe dễ khởi động sau khi dừng đèn giao thông. \n❌ Khi đi phượt nên chọn nhớt đặc hơn, khi máy nóng nhớt sẽ loãng ra là vừa, tránh hiện tượng “rồ máy”.'
  }
]);

class TipHelper {
  static instance: TipHelper;
  static getInstance(): TipHelper {
    if (!TipHelper.instance) {
      TipHelper.instance = new TipHelper();
    }
    return TipHelper.instance;
  }
  /**
   * Return tip list
   */
  getTipList() {
    let numberOfLetter = 80;
    return TIP_DATA.map((value, index) => {
      let getBriefFrom = value.content;
      let brief;
      if (getBriefFrom.length < numberOfLetter) brief = getBriefFrom + "...";
      else {
        brief = getBriefFrom.slice(0, numberOfLetter);
        let lastWordPos = brief.lastIndexOf(" ");
        brief = brief.slice(0, lastWordPos);
        brief += " ...";
      }
      return { id: index, title: value.title, brief };
    });
  }
  /**
   * Return detail of an tip based on its ID
   * @param {*} tipId
   */
  getTipDetail(tipId: number): TipStructure {
    return TIP_DATA[tipId];
  }
}
export default TipHelper.getInstance();
