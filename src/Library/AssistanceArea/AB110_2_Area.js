/**
 * @flow
 */
import AssistanceHelper, {
  type TITLE_OF_ASSISTANCE,
  type AssistanceStructure
} from "./AssistanceHelper";
/**
 * Return title list for AB 110 đèn pha nhỏ
 */
/**
  {
    title: "",
    symptons: "",
    reasons: "",
    howToFix: ""
  }
*/
const ASSISTANCE_DATA = Object.freeze([
  {
    title: "Không thể nổ máy",
    symptons: "",
    reasons:
      "Lỗi điện: Hư hỏng bộ phận đánh lửa (bugi, ecu), mô tơ bơm xăng không hoạt động, có sự cố về kim phun\nLỗi cơ khí: Mất áp suất nén buồng đốt",
    howToFix:
      "Kiểm tra bộ phận đánh lửa, mô tơ bơm xăng, kim phun\nKiểm tra áp suất buồng đốt"
  },
  {
    title: "Khó nổ máy",
    symptons: "",
    reasons:
      "Lỗi điện: Hư hỏng bộ phận đánh lửa (bugi, ecu), mô tơ bơm xăng không hoạt động, có sự cố về kim phun\nLỗi cơ khí: Mất áp suất nén buồng đốt",
    howToFix:
      "Kiểm tra bộ phận đánh lửa, mô tơ bơm xăng, kim phun\nKiểm tra áp suất buồng đốt"
  },
  {
    title: "Hệ thống làm mát không tốt",
    symptons: "Đồng hồ nhiệt độ vượt mức tiêu chuẩn, động cơ bị ỳ",
    reasons: "Hết nước mát, hệ thống làm mát bị rò rỉ hoặc thiếu dầu bôi trơn",
    howToFix: "Kiểm tra hệ thống làm mát, kiểm tra dầu máy"
  },
  {
    title: "Hay chết máy",
    symptons: "",
    reasons:
      "Lỗi điện: Hư hỏng bộ phận đánh lửa (bugi, ecu), mô tơ bơm xăng không hoạt động, có sự cố về kim phun\nLỗi cơ khí: Mất áp suất nén buồng đốt",
    howToFix:
      "Kiểm tra bộ phận đánh lửa, mô tơ bơm xăng, kim phun\nKiểm tra áp suất buồng đốt "
  },
  {
    title: "Tiêu hao nhiều xăng hơn bình thường",
    symptons: "Xe có khói đen hoặc có mùi xăng sống lúc khởi động",
    reasons:
      "Lỗi điện: Cài đặt thông số khí quyển không phù hợp\nLỗi cơ khí: Giảm áp suất buồng đốt",
    howToFix:
      "Cài đặt lại thông số khí quyển\nKiểm tra và thay thế chi tiết làm kín buồng đốt (pít tông, xéc măng, xupap)"
  },
  {
    title: "Rồ ga",
    symptons:
      "Không kiểm soát được động cơ khi dừng đèn xanh đèn đỏ, xe tự tăng tốc khi không kéo tay ga",
    reasons:
      "Hư hỏng cám biến tay ga, bộ phận truyền động (bi côn, côn văng bị bám dính)",
    howToFix: "Kiểm tra bộ phận truyền động và cảm biến tay ga"
  },
  {
    title: "Tăng tốc chậm",
    symptons: "Động cơ bị ỳ khi kéo ga",
    reasons:
      "Lỗi điện: Chế độ phun nhiên liệu không phù hợp, cài đặt thông số khí quyển không phù hợp\nLỗi cơ khí: Bi côn bị mài mòn",
    howToFix:
      "Cài đặt lại thông số khí quyển\nKiểm tra bộ truyền động và thông số hoạt động của cảm biến"
  }
]);

export function getAssistanceListForAB110_2(): TITLE_OF_ASSISTANCE[] {
  let numberOfLetter = 80;
  return ASSISTANCE_DATA.map(
    (value, index): TITLE_OF_ASSISTANCE => {
      let getBriefFrom = "";
      let brief = AssistanceHelper.extractBriefFromContent(value);

      return { title: value.title, id: index, brief };
    }
  );
}

export function getDetailAssistanceForAB110_2(
  titleId: number
): AssistanceStructure {
  let detail: AssistanceStructure = {};
  if (ASSISTANCE_DATA[titleId] === undefined) {
    detail.title = "Unknown (" + titleId + ")";
    return detail;
  }
  return ASSISTANCE_DATA[titleId];
}
