/**
 * @flow
 */
type ExplainStructure = {
  name: string,
  effect?: string,
  reasons?: string,
  howToFix?: string,
};

class ExplainErrorYamaha {
  static instance: ExplainErrorYamaha;

  static getInstance(): ExplainErrorYamaha {
    if (!ExplainErrorYamaha.instance) {
      ExplainErrorYamaha.instance = new ExplainErrorYamaha();
    }
    return ExplainErrorYamaha.instance;
  }

  /**
   * Lấy giải thích mã lỗi
   * @param {*} errorCode Mã lỗi
   * @param {*} errorMem Vị trí bộ nhớ lỗi
   * @param {*} type "kline" | "obd"
   */
  getExplain = (params: {errorCode: number, errorMem?: number, type: 'kline' | 'obd'}) => {
    const {errorCode, type, errorMem} = params;
    const explain: ExplainStructure = {
      name: 'Chưa xác định',
    };
    // if (type === 'kline') {
    switch (errorCode) {
      case 0x01:
        explain.name = 'Lỗi cảm biến tốc độ bánh xe (Dành cho Sure park)';
        return explain;

      case 0x03:
        explain.name = 'Rơle SURE PARK';
        return explain;

      case 0x08:
        explain.name = 'Lỗi cảm biến vị trí bướm ga';
        return explain;

      case 0x11:
        explain.name = 'Lỗi cảm biến vị trí xy lanh';
        return explain;

      case 0x12:
        explain.name = 'Lỗi cảm biến vị trí góc quay trục khuỷu';
        explain.reasons = `hỏng jắc nối giữa cảm biến và ECU
          hở ngắn mạch ở hệ thống dây giữa cảm biến với ECU
          cảm biến góc nghiêng lắp không đúng
          hư hỏng vô lăng điện 
          cảm biến trục khuỷu bị hỏng
          ECU hỏng`;
        return explain;

      case 0x13:
        explain.name = 'Lỗi cảm biến áp suất khí nạp 1';
        explain.reasons = `hỏng jắc nối giữa cảm biến và ECU
          hở ngắn mạch ở hệ thống dây giữa cảm biến với ECU
          cảm biến áp suất khí nạp
          ECU hỏng`;
        return explain;

      case 0x14:
        explain.name = 'Lỗi cảm biến áp suất khí nạp 1';
        explain.reasons = `Mô đun cảm biến bị tháo rời
          bơm ga lắp không đúng
          lỗ cảm biến bị nghẹt`;
        return explain;

      case 0x15:
        explain.name = 'Lỗi cảm biến vị trí bươm ga';
        explain.reasons = `hỏng jắc nối giữa cảm biến và ECU
          hở ngắn mạch ở hệ thống dây giữa cảm biến với ECU
          cảm biến bướm ga hỏng
          ECU hỏng`;
        return explain;

      case 0x16:
        explain.name = 'Lỗi cảm biến vị trí bươm ga';
        explain.reasons = `Mô đun lắp không đúng
          cảm biến vị trí bướm ga bị hỏng
          ECU hỏng`;
        return explain;

      case 0x17:
        explain.name = 'Lỗi mạch mô tơ điều khiển của xả';
        return explain;

      case 0x18:
        explain.name = 'Lỗi mô tơ điều khiển của xả';
        return explain;

      case 0x19:
        explain.name = '[Dành cho MC] Công tắc chân chống cạnh hoặc dây xanh/vàng của ECU';
        return explain;

      case 0x20:
        explain.name = 'Cảm biến áp suất khí nạp 1 hoặc 2 hoặc cảm biến áp suất môi trường';
        return explain;

      case 0x21:
        explain.name = 'Lỗi cảm biến nhiệt độ dung dịch làm mát';
        explain.reasons = `hỏng jắc nối giữa cảm biến và ECU
          hở ngắn mạch ở hệ thống dây giữa cảm biến với ECU
          cảm biến nhiệt độ dung dịch làm mát bị hỏng
          cảm biến lắp không đúng
          ECU hỏng`;
        return explain;

      case 0x22:
        explain.name = 'Lỗi cảm biến nhiệt độ khí nạp';
        return explain;

      case 0x23:
        explain.name = 'Cảm biến áp suất môi trường';
        return explain;

      case 0x24:
        explain.name = 'Cảm biến oxy 1';
        explain.reasons = `hỏng jắc nối giữa cảm biến và ECU
          hở ngắn mạch ở hệ thống dây giữa cảm biến với ECU
          cảm biến bị hỏng
          cảm biến lắp không đúng
          ECU hỏng`;
        return explain;

      case 0x25:
        explain.name = 'Cảm biến áp suất khí nạp 2';
        return explain;

      case 0x26:
        explain.name = 'Cảm biến áp suất khí nạp 2';
        return explain;

      case 0x27:
        explain.name = 'Cảm biến nhiệt độ dầu động cơ';
        return explain;

      case 0x28:
        explain.name = 'Cảm biến nhiệt độ dầu động cơ';
        return explain;

      case 0x29:
        explain.name = 'Cuộn dây bộ giảm áp';
        return explain;

      case 0x30:
        explain.name = 'Cảm biến nghiêng xe hoặc áp suất dầu động cơ';
        explain.reasons = `xe bị bật 
          cảm biến góc nghiêng lắp không đúng
          cảm biến góc nghiêng bị hỏng 
          ECU bị hỏng`;
        return explain;

      case 0x31:
        explain.name = 'Cảm biến oxy 1';
        return explain;

      case 0x32:
        explain.name = 'Cảm biến oxy 1';
        return explain;

      case 0x33:
        explain.name = 'Mô bin sườn số 1';
        return explain;

      case 0x34:
        explain.name = 'Mô bin sườn số 2';
        return explain;

      case 0x35:
        explain.name = 'Mô bin sườn số 3';
        return explain;

      case 0x36:
        explain.name = 'Mô bin sườn số 4';
        return explain;

      case 0x37:
        explain.name = 'Van ISC hoặc van FID';
        return explain;

      case 0x38:
        explain.name = 'Cuộn dây bộ giảm áp';
        return explain;

      case 0x39:
        explain.name = 'Vòi phun';
        explain.reasons = `hỏng jắc nối giữa kim phun và ECU
          hở ngắn mạch ở hệ thống dây giữa kim phun với ECU
          kim phun bị hỏng
          kim phun lắp không đúng
          ECU hỏng`;
        return explain;

      case 0x40:
        explain.name = 'Vòi phun thứ cấp';
        return explain;

      case 0x41:
        explain.name = 'Cảm biến nghiêng xe';
        explain.reasons = `hỏng jắc nối giữa cảm biến và ECU
          hở ngắn mạch ở hệ thống dây giữa cảm biến với ECU
          cảm biến bị hỏng
          cảm biến lắp không đúng
          ECU hỏng`;
        return explain;

      case 0x42:
        explain.name = 'Cảm biến tốc độ hoặc công tắc số không hoặc công tắc ly hợp';
        explain.reasons = `hỏng jắc nối giữa cảm biến và ECU
            hở ngắn mạch ở hệ thống dây giữa cảm biến với ECU
            cảm biến bị hỏng
            cảm biến lắp không đúng
            ECU hỏng`;
        return explain;

      case 0x43:
        explain.name = 'Điện áp hệ thống nhiên liệu';
        return explain;

      case 0x44:
        explain.name = 'Lỗi đọc ghi lên EEPROM';
        explain.reasons = `Lỗi bên trong ECU`;
        return explain;

      case 0x45:
        explain.name = 'ECU bị lỗi';
        return explain;

      case 0x46:
        explain.name = 'Hệ thống nguồn xe điện';
        explain.reasons = `ắc quy quá nạp
          ắc quy phóng điện quá mức`;
        return explain;

      case 0x47:
        explain.name = 'Cảm biến vị trí bươm ga phụ';
        return explain;

      case 0x48:
        explain.name = 'Mô tơ bướm ga phụ';
        return explain;

      case 0x50:
        explain.name = 'Lỗi bên trong ECU';
        explain.reasons = `ECU hỏng`;
        return explain;

      case 0x59:
        explain.name = 'Cảm biến vị trí bộ gia tốc';
        return explain;

      case 0x60:
        explain.name = 'Mô tơ bươm ga';
        return explain;

      case 0x61:
        explain.name = 'Van ISC';
        return explain;

      case 0x62:
        explain.name = 'Cảm biến nhiệt dộ quạt hệ thống làm mát';
        return explain;

      case 0x63:
        explain.name = 'Cảm biến nhiệt dộ quạt hệ thống làm mát';
        return explain;

      // case 0x64:
      //   explain.name = "";
      //   return explain;

      case 0x65:
        explain.name = 'Cảm biến gõ máy';
        return explain;

      case 0x66:
        explain.name = 'Cuộn dây giảm chấn tay lái';
        return explain;

      case 0x67:
        explain.name = 'Cảm biến áp suất dầu động cơ';
        return explain;

      case 0x68:
        explain.name = 'Cảm biến oxy 2';
        return explain;

      case 0x69:
        explain.name = '[Dành cho MC] \nCảm biến bánh xe trước \n[Dành cho SMB]';
        return explain;

      case 0x70:
        explain.name = 'Tắt chế độ galanty động cơ';
        return explain;

      case 0x71:
        explain.name = 'Sợi đốt cảm biến oxy';
        return explain;

      // case 0x72:
      //   explain.name = "";
      //   return explain;

      case 0x73:
        explain.name = 'Cảm biến vị trí puly sơ cấp';
        return explain;

      case 0x74:
        explain.name = 'Cảm biến vị trí puly thứ cấp';
        return explain;

      case 0x75:
        explain.name = 'Puly sơ cấp';
        return explain;

      case 0x76:
        explain.name = 'Vị trí Puly sơ cấp';
        return explain;

      case 0x77:
        explain.name = 'Nguồn cấp cho ECVT';
        return explain;

      case 0x78:
        explain.name = 'Nhiệt độ bên trong ECU';
        return explain;

      case 0x79:
        explain.name = 'Lượng khí nạp không bình thường';
        return explain;

      case 0x80:
        explain.name = 'Công tắc sưởi ấm tay nắm';
        return explain;

      case 0x81:
        explain.name = 'Sưởi ấm tay nắm';
        return explain;

      // case 0x82:
      //   explain.name = "";
      //   return explain;

      case 0x83:
        explain.name = 'Sưởi ấm tay nắm';
        return explain;

      case 0x84:
        explain.name = 'Van biến thiên VVA';
        explain.effect = 'Không điều khiển góc mở suppap hút để tăng tốc động cơ';
        return explain;

      case 0x85:
        explain.name = 'Công tắc áp suất dầu động cơ';
        return explain;

      case 0x88:
        explain.name = '[Dành cho SMB]\nCông tắc bánh răng đảo  chiều điện\n[Dành cho SURE-PARK]';
        return explain;

      case 0x89:
        explain.name = 'CAN truyền tải';
        return explain;

      case 0x90:
        explain.name = 'Công tắc huỷ điều khiển hành trình';
        return explain;

      case 0x91:
        explain.name = 'Công tắc thiết lập/ tiếp tục hành trình';
        return explain;

      case 0x92:
        explain.name = 'Cuộn dây từ cụm van';
        return explain;

      case 0x96:
        explain.name = 'Hệ thống nguồn điện xe 2';
        explain.reasons = `ắc quy hỏng
          hệ thống sạc hỏng`;
        return explain;

      case 0x98:
        explain.name = 'IMU';
        return explain;

      case 0x99:
        explain.name = 'Lỗi truyền CAN (IMU)';
        return explain;

      // default:
      //   explain.name = 'Chưa xác định';
      //   break;
    }
    // } else if (type === 'obd') {
    // Biến cố
    // if (errorMem === 0x1e) {
    switch (errorCode) {
      case 0xc0:
        explain.name = 'Phát hiện được sự bất thường về giá trị của cảm biến khí nạp 1';
        return explain;
      case 0xcb:
        explain.name = 'Phát hiện được sự bất thường về giá trị của cảm biến góc nghiêng';
        return explain;
      case 0xc1:
        explain.name = 'Phát hiện được sự cố bất thường về giá trị của cảm biến bướm ga';
        return explain;
      case 0xc4:
        explain.name = 'Phát hiện được sự cố bất thường về giá trị của cảm biến nhiệt độ dung dịch làm mát';
        return explain;
      case 0xc5:
        explain.name = 'Phát hiện được sự cố bất thường về giá trị của cảm biến nhiệt độ khí nạp';
        return explain;
      case 0xda:
        explain.name = 'Phát hiện được sự cố bất thường về giá trị của cảm biến vị trí trục khuỷu';
        return explain;
      case 0xf0:
        explain.name = 'Phát hiện được sự cố bất thường về giá trị của cảm biến oxygen (cao hơn tiêu chuẩn)';
        return explain;
      case 0xf1:
        explain.name = 'Phát hiện được sự cố bất thường về giá trị của cảm biến oxygen (thấp hơn tiêu chuẩn)';
        return explain;
      case 0xf4:
        explain.name =
          'Phát hiện đươc tình trạng khó khởi động động cơ (kiểm tra hệ thống nhiên liệu, bộ phận đánh lửa, accu, ECU)';
        return explain;
      case 0xf5:
        explain.name =
          'Phát hiện đươc tình trạng  động cơ hay chết máy (kiểm tra hệ thống nhiên liệu, bộ phận đánh lửa, accu, ECU)';
        return explain;
      case 0xf7:
        explain.name =
          'Phát hiện đươc tình trạng  động cơ hay chết máy (kiểm tra hệ thống nhiên liệu, bộ phận đánh lửa, accu, ECU)';
        return explain;

      // default:
      //   break;;
    }
    // } else {
    let errorStr = [errorMem, errorCode].map(v => {
      let hex = v.toString(16);
      if (hex.length === 1) {
        hex = `0${hex}`;
      }
      return hex;
    });
    errorStr = `P${errorStr.join('')}`.toUpperCase();
    switch (errorStr) {
      case 'P0106':
        explain.name = 'Cảm biến áp suất khí nạp';
        explain.effect = 'Xe vận hành không ổn định';
        explain.reasons = 'Cảm biến không hoạt động';
        explain.howToFix = 'Kiểm tra cảm biến, dây dẫn hoặc ECU';
        return explain;
      case 'P0107':
        explain.name = 'Cảm biến áp suất khí nạp';
        explain.effect = 'Xe vận hành không ổn định';
        explain.reasons = 'Cảm biến áp suất khí nạp hở ngoặc ngắn mạch tiếp mát';
        explain.howToFix = 'Kiểm tra cảm biến, dây dẫn hoặc ECU ';
        return explain;
      case 'P0108':
        explain.name = 'Cảm biến áp suất khí nạp';
        explain.effect = 'Xe vận hành không ổn định';
        explain.reasons = 'Cảm biến áp suất khí nạp bị ngắn mạch ';
        explain.howToFix = 'Kiểm tra cảm biến, dây dẫn hoặc ECU ';
        return explain;
      case 'P0112':
        explain.name = 'Cảm biến nhiệt độ khí nạp';
        explain.effect = 'Xe vận hành không ổn định';
        explain.reasons = 'Cảm biến nhiệt độ khí nạp ngắn mạch hoặc tiếp mát';
        explain.howToFix = 'Kiểm tra cảm biến, dây dẫn hoặc ECU';
        return explain;
      case 'P0113':
        explain.name = 'Cảm biến nhiệt độ khí nạp';
        explain.effect = 'Xe vận hành không ổn định';
        explain.reasons = 'Cảm biến nhiệt độ khí nạp bị hở hoặc ngắn mạch';
        explain.howToFix = 'Kiểm tra cảm biến, dây dẫn hoặc ECU';
        return explain;
      case 'P0117':
        explain.name = 'Cảm biến nhiệt độ dung dịch làm mát';
        explain.effect = 'Hệ thống cảnh báo nhiệt hoạt động sai';
        explain.reasons = 'Cảm biến ngắn mạch hoặc tiếp mát';
        explain.howToFix = 'Kiểm tra cảm biến, dây dẫn hoặc ECU ';
        return explain;
      case 'P0118':
        explain.name = 'Cảm biến nhiệt độ dung dịch làm mát';
        explain.effect = 'Hệ thống cảnh báo nhiệt hoạt động sai';
        explain.reasons = 'Cảm biến hở hoặc ngắn mạch';
        explain.howToFix = 'Kiểm tra cảm biến, dây dẫn hoặc ECU ';
        return explain;
      case 'P0122':
        explain.name = 'Cảm biến vị trí bướm ga';
        explain.effect = 'Xe vận hành không ổn định';
        explain.reasons = 'Cảm biến hở hoặc ngắn mạch ';
        explain.howToFix = 'Kiểm tra cảm biến, dây dẫn hoặc ECU ';
        return explain;
      case 'P0123':
        explain.name = 'Cảm biến vị trí bướm ga';
        explain.effect = 'Xe vận hành không ổn định';
        explain.reasons = 'Cảm biến hở hoặc ngắn mạch ';
        explain.howToFix = 'Kiểm tra cảm biến, dây dẫn hoặc ECU ';
        return explain;
      case 'P0132':
        explain.name = 'Cảm biến Oxygen';
        explain.effect = 'Xe hao xăng hơn mức tiêu chuẩn';
        explain.reasons = 'Cảm biến hư hỏng hoặc ngắn mạch';
        explain.howToFix = 'Kiểm tra cảm biến, dây dẫn hoặc ECU ';
        return explain;
      case 'P0201':
        explain.name = 'Lỗi Kim phun';
        explain.effect = 'Không thể nổ máy hoặc động cơ hoạt động không ổn định';
        explain.reasons = 'Kim phun hư hỏng hoặc ngắn mạch ';
        explain.howToFix = 'Kiểm tra kim phun, dây dẫn hoặc ECU ';
        return explain;
      case 'P0335':
        explain.name = 'Cảm biến vị trí trục khuỷu số 1';
        explain.effect = 'Không thể nổ máy hoặc động cơ hoạt động không ổn định';
        explain.reasons = 'ECU không nhận được tín hiệu bình thường từ cảm biến';
        explain.howToFix = 'Kiểm tra cảm biến, dây dẫn hoặc ECU ';
        return explain;
      case 'P0351':
        explain.name = 'Hệ thống đánh lửa';
        explain.effect = 'Không thể nổ máy hoặc động cơ hoạt động không ổn định';
        explain.reasons = 'Chập cuộn đánh lửa hoặc dây dẫn từ ECU tới cuộn đánh lửa';
        explain.howToFix = 'Kiểm tra mobin đánh lửa, dây dẫn hoặc ECU ';
        return explain;
      case 'P0480':
        explain.name = 'Hệ thống quạt gió tản nhiệt';
        explain.effect = 'Hệ thống làm mát động cơ trục trặc';
        explain.reasons = 'Tín hiệu điều khiển từ ECU, rơ le dây dẫn không bình thường';
        explain.howToFix = 'Kiểm tra rơ le ,dây dẫn và ECU';
        return explain;
      case 'P5000':
        explain.name = 'Cảm biến vận tốc';
        explain.effect = 'Đồng hồ không hiển thị vận tốc';
        explain.reasons = 'Không nhận được tín hiệu bình thường từ cảm biến';
        explain.howToFix = 'Kiểm tra cảm biến ,dây dẫn và ECU';
        return explain;
      case 'P0560':
        explain.name = 'Hệ thống nguồn sạc';
        explain.effect = 'Điện áp sạc bất thường';
        explain.reasons = 'Hệ thống sạc bất thường';
        explain.howToFix = 'Kiểm tra máy phát, IC sạc và accu';
        return explain;
      case 'P0601':
        explain.name = 'Bộ nhớ ECU bị lỗi';
        explain.effect = 'Không thể nổ máy hoặc động cơ hoạt động không ổn định';
        explain.reasons = 'Lỗi dữ liệu ECU';
        explain.howToFix = 'Kiểm tra ECU';
        return explain;
      case 'P062F':
        explain.name = 'Số mã lỗi ECU';
        explain.effect = 'Xe vận hành không ổn định';
        explain.reasons = 'Lỗi ghi đọc dữ liệu trong ECU';
        explain.howToFix = 'Kiểm tra ECU';
        return explain;
      case 'P0657':
        explain.name = 'Hệ thống cấp nhiên liệu';
        explain.effect = 'Xe vận hành không ổn định';
        explain.reasons = 'Hệ thống bơm xăng và kim phun hoạt động không tốt';
        explain.howToFix = 'Kiểm tra hệ thống bơm xăng và kim phun';
        return explain;
      case 'P1602':
        explain.name = 'Hệ thống phản hồi PID không làm việc';
        explain.effect = 'Xe hao xăng hơn mức tiêu chuẩn';
        explain.reasons = 'ECU không nhận được tín hiệu bình thường từ cảm biến';
        explain.howToFix = 'Kiểm tra chất lượng cảm biến oxy và ECU';
        return explain;
      case 'P1604':
        explain.name = 'Cảm biến góc nghiêng';
        explain.effect = 'Không thể nổ máy hoặc động cơ hoạt động không ổn định';
        explain.reasons = 'ECU không nhận được tín hiệu bình thường từ cảm biến';
        explain.howToFix = 'Kiểm tra vị trí lắp cảm biến ,chất lượng cảm biến,dây dẫn hoặc ECU';
        return explain;
      case 'P1605':
        explain.name = 'Cảm biến góc nghiêng';
        explain.effect = 'Không thể nổ máy hoặc động cơ hoạt động không ổn định';
        explain.reasons = 'ECU không nhận được tín hiệu bình thường từ cảm biến';
        explain.howToFix = 'Kiểm tra vị trí lắp cảm biến ,chất lượng cảm biến,dây dẫn hoặc ECU';
        return explain;
      case 'P1E30':
        explain.name = 'Cảm biến góc nghiêng hoạt động không ổn định';
        explain.effect = 'Không thể nổ máy hoặc động cơ hoạt động không ổn định';
        explain.reasons = 'ECU không nhận được tín hiệu bình thường từ cảm biến';
        explain.howToFix = 'Kiểm tra vị trí lắp cảm biến ,chất lượng cảm biến,dây dẫn hoặc ECU';
        return explain;
      case 'P1E96':
        explain.name = 'Hệ thống nguồn sạc';
        explain.effect = 'Điện áp sạc bất thường';
        explain.reasons = 'Hệ thống sạc bất thường';
        explain.howToFix = 'Kiểm tra máy phát, IC sạc và accu';
        return explain;
      case 'P2195':
        explain.name = 'Cảm biến Oxygen';
        explain.effect = 'Xe hao xăng hơn mức tiêu chuẩn';
        explain.reasons = 'ECU không nhận được tín hiệu bình thường từ cảm biến Oxygen';
        explain.howToFix = 'Kiểm tra cảm biến, dây dẫn,hoặc ECU';
        return explain;

      // default:
      //   break;
    }
    // }
    // }
    return explain;
  };
}
export default ExplainErrorYamaha.getInstance();
