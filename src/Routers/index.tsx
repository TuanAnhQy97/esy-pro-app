/**
 *@flow
 */
import React from "react";
import { createStackNavigator, createSwitchNavigator } from "react-navigation";

import { View } from "react-native";
import AppLoadingScreen from "../Screens/AppLoadingScreen";

import MainScreen from "../Screens/MainScreen";
import Dashboard from "../Screens/Dashboard/DashboardScreen";
import ErrorFI from "../Screens/ErrorFI";
import AutoCheckOne from "../Screens/AutoCheckOne/AutoCheckOne";
import AutoCheckTwo from "../Screens/AutoCheckTwo/AutoCheckTwo";
import AuthScreens, { AUTH_SCREEN_KEY } from "../Screens/AuthScreens";
import UpdateEcu, {
  SCREEN_KEY as UPDATE_ECU_SCREEN_KEY
} from "../Screens/UpdateEcu/Router/Router";

import Menu from "../Screens/Menu";

import UserInfo from "../Screens/UserInfo";
import BikeInfo from "../Screens/BikeInfo";
import Setting from "../Screens/Setting";

import MainScreenYa from "../Screens/Yamaha/MainScreenYa";
import DashboardYa from "../Screens/Yamaha/DashboardYa";
import ErrorYa from "../Screens/Yamaha/ErrorYa";

import TrialList from "../Screens/TrialList";
import TrialSearchABS from "../Screens/TrialSearchABS";
import ABSExplain from "../Screens/ABSExplain";
import TrialSearchSmartKey from "../Screens/TrialSearchSmartKey";

import ReportScreen, {
  SCREEN_NAME as REPORT_SCREEN_KEY
} from "../Screens/ReportScreen/Router";
import OverlaySwitchKey from "../Components/Popup/OverlaySwitchKey";
import PopupConnectionError, {
  setRef
} from "../Components/PopupConnectionError";
import AdvanceDataYa from "../Screens/Yamaha/AdvanceDataYa";
import withSafeArea from "../Components/withSafeViewArea";

import SetupDevice from "./SetupDevice";

export type SCREEN_KEY_NAME =
  /**
   * Auth Stack
   */
  | AUTH_SCREEN_KEY
  /**
   * AppStack
   */
  | "Main"
  | "Dashboard"
  | "ErrorFI"
  | "AutoCheckOne"
  | "AutoCheckTwo"
  | "UserInfo"
  | "Setting"
  | "BikeInfo"
  /**
   * Update ECU stack
   */
  | UPDATE_ECU_SCREEN_KEY
  /**
   * REPORT
   */
  | REPORT_SCREEN_KEY
  /**
   * RootRouter
   */
  | "AppLoading"
  | "Auth"
  | "App"
  | "SetupDevice";

const AppHonda = createStackNavigator(
  {
    MainScreen: { screen: MainScreen },
    Dashboard: { screen: Dashboard },
    ErrorFI: { screen: ErrorFI },
    AutoCheckOne: {
      screen: AutoCheckOne
    },
    AutoCheckTwo: {
      screen: AutoCheckTwo
    },
    /**
     * Update ECU
     */
    UpdateEcu: {
      screen: UpdateEcu,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    //
    BikeInfo: {
      screen: BikeInfo
    },
    // Report
    Report: ReportScreen
  },
  {
    mode: "card",
    headerMode: "none"
  }
);
// const getStateForActionScreensStack = App.router.getStateForAction;

const AppYamaha = createStackNavigator(
  {
    MainScreenYa: { screen: MainScreenYa },
    DashboardYa: { screen: DashboardYa },
    ErrorYa: { screen: ErrorYa },
    AdvanceDataYa: { screen: AdvanceDataYa }
  },
  {
    mode: "card",
    headerMode: "none"
  }
);

class AppYamahaWrapper extends React.Component {
  static router = AppYamaha.router;

  render() {
    const { navigation } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <AppYamaha navigation={navigation} />
        <OverlaySwitchKey />
        <PopupConnectionError ref={_ref => setRef(_ref)} />
      </View>
    );
  }
}

// App.router = {
//   ...App.router,
//   getStateForAction(action, state) {
//     if (action.type == "Navigation/BACK") {
//       console.log("We are going back...");
//     }
//     return getStateForActionScreensStack(action, state);
//   }
// };

const AppSwitch = createSwitchNavigator(
  {
    AppLoading: AppLoadingScreen,
    Auth: AuthScreens,
    Menu,
    SetupDevice,
    AppHonda,
    AppYamaha: AppYamahaWrapper
  },
  {
    initialRouteName: "AppLoading"
  }
);

const RootStack = createStackNavigator(
  {
    AppSwitch,
    UserInfo: {
      screen: withSafeArea()(UserInfo)
    },
    Setting: {
      screen: withSafeArea()(Setting)
    },
    TrialList,
    TrialSearchABS,
    TrialSearchSmartKey,
    ABSExplain
  },
  {
    mode: "card",
    headerMode: "none"
  }
);

export default RootStack;
