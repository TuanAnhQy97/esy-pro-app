import { createSwitchNavigator } from "react-navigation";

import DfuUpdateScreen from "../Screens/Dfu";
import ScanningScreen from "../Screens/ScanningScreen";
import SelectBikeBrandScreen from "../Screens/SelectBikeBrandScreen";

const SetupDevice = createSwitchNavigator({
  ScanningScreen: {
    screen: ScanningScreen
  },
  SelectBikeBrandScreen: {
    screen: SelectBikeBrandScreen
  },
  Dfu: {
    screen: DfuUpdateScreen
  }
});

export default SetupDevice;
