/**
 * @flow
 */

import {Platform} from 'react-native';

import ASHelper from './ASHelper';
import {BLE_ERROR} from './constants';
import navigationService from './services/navigationService';

/**
 * Logout from App
 */
export async function logout() {
  // Tắt Popup
  await ASHelper.clearAS();
  navigationService.navigate('AppLoading'); // Về Login
}
/**
 * Compare 2 two date, if they are in the same Day
 * @param {Date} date1
 * @param {Date} date2
 */
export function isTheSameDay(date1: Date, date2: Date): boolean {
  if (date1.getFullYear() !== date2.getFullYear()) return false;
  if (date1.getMonth() !== date2.getMonth()) return false;
  if (date1.getDay() !== date2.getDay()) return false;
  return true;
}
/**
 * Compare 2 array number
 * @param {*} arr1 array 1
 * @param {*} arr2 array 2
 * @returns true if they are the same, else return false
 */
export function compareArrays(arr1: number[] | readonly number[], arr2: number[] | readonly number[]): boolean {
  if (arr1.length != arr2.length) return false;
  for (let i = 0; i < arr1.length; i++) {
    if (arr1[i] != arr2[i]) return false;
  }
  return true;
}
/**
 * Delay ms
 * @param {*} ms
 */
export async function wait(ms: number = 0) {
  return new Promise(r => setTimeout(r, ms));
}

export function deviceIdToString(deviceId: number[]) {
  let stringId = '';
  deviceId.forEach(ele => {
    stringId += ele.toString();
  });
  return stringId;
}
/**
 * Translate jobProgress to title
 * @param {*} code
 */
export function getTitleForConnectProgress(code: number): string {
  let str;
  const errorHex = numberToString(code);
  switch (
    code // STATUS
  ) {
    // case Ble.BLE_PROGRESS.PROGRESS.INIT:
    //   str = "Kiểm tra kết nối";
    //   break;
    // case Ble.BLE_PROGRESS.PROGRESS.SCANNING:
    //   str = "Đang tìm thiết bị";
    //   break;
    // case Ble.BLE_PROGRESS.PROGRESS.CONNECTING:
    //   str = "Đang kết nối";
    //   break;
    // case Ble.BLE_PROGRESS.PROGRESS.DOING_JOB:
    //   str = "Đang kiểm tra";
    //   break;
    // // ERROR
    // case Ble.BLE_PROGRESS.ERROR.NOT_FOUND:
    //   str = "Không tìm thấy thiết bị" + ` (0x${errorHex})`;
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.FAILURE:
    //   str = "Mã dịch vụ" + ` 0x${errorHex}`;
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.ESY_NEED_RESET:
    //   str = "Mã dịch vụ" + ` 0x${errorHex}`;
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.KLINE_ERROR:
    //   str = "Tín hiệu ECU không ổn định" + ` (0x${errorHex})`;
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.BLE_129:
    //   str = "Mã dịch vụ" + ` 0x${errorHex}`;
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.BLE_TIMEOUT_CONNECT:
    //   str = "Mã dịch vụ" + ` 0x${errorHex}`;
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.BLE_RETRIEVE_SERVICE:
    //   str = "Mã dịch vụ" + ` 0x${errorHex}`;
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.DATA_TIMEOUT:
    //   str = "Mã dịch vụ" + ` 0x${errorHex}`;
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.BLE_NOTIFY_CHAR:
    //   str = "Mã dịch vụ" + ` 0x${errorHex}`;
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.BLE_ERROR_AFTER_LOG_OUT:
    //   str = "Mã dịch vụ" + ` 0x${errorHex}`;
    //   break;
    default:
      str = 'Unknown' + ` 0x${errorHex}`;
      break;
  }
  return str;
  function numberToString(code: number) {
    if (code > 9) {
      return code;
    }
    return `0${code}`;
  }
}
/**
 * Translate jobProgress to subtitle
 * @param {*} code
 */
export function getSubtitle(code: number): string {
  let str;
  switch (code) {
    // // STATUS
    // case Ble.BLE_PROGRESS.PROGRESS.CONNECTING:
    // case Ble.BLE_PROGRESS.PROGRESS.SCANNING:
    //   str = "Vui lòng bật khoá điện và giữ\nkhoảng cách gần với xe của bạn";
    //   break;
    // case Ble.BLE_PROGRESS.PROGRESS.DOING_JOB:
    //   str = "";
    //   break;
    // // ERROR
    // case Ble.BLE_PROGRESS.ERROR.NOT_FOUND:
    //   str = "Bật tắt khoá điện, kiểm tra giắc cắm và thử lại";
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.FAILURE:
    //   if (Platform.OS === "android") {
    //     str =
    //       "Vui lòng thử lại với những cách sau:\n" +
    //       "1. Thử lại sau vài giây\n" +
    //       "2. Tắt bật khoá điện, tắt bật lại Bluetooth\n" +
    //       `3. Bỏ ghép đôi "ESY" trong "Cài đặt Bluetooth"`;
    //   } else {
    //     str = "Vui lòng thử lại sau vài giây";
    //   }
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.ESY_NEED_RESET:
    //   if (Platform.OS === "android") {
    //     str = "Vui lòng tắt khoá điện, sau 15 giây bật khoá và thử lại";
    //   } else {
    //     str = "Vui lòng tắt khoá điện, sau vài giây bật khoá và thử lại";
    //   }
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.KLINE_ERROR:
    //   str =
    //     "Đường truyền giữa ESY và ECU bị gián đoạn, vui lòng kiểm tra nguồn điện của xe";
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.BLE_129:
    //   str = "Vui lòng tắt Bluetooth, sau 15 giây bật và thử lại";
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.BLE_TIMEOUT_CONNECT:
    //   str = "Vui lòng tắt bật khoá điện, Bluetooth và thử lại sau vài giây";
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.BLE_RETRIEVE_SERVICE:
    //   str = "Vui lòng tắt bật khoá điện, Bluetooth và thử lại sau vài giây";
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.DATA_TIMEOUT:
    //   if (Platform.OS === "android") {
    //     str =
    //       "Vui lòng thử lại với những cách sau:\n" +
    //       "1. Thử lại sau vài giây\n" +
    //       "2. Tắt bật khoá điện, tắt bật lại Bluetooth\n";
    //   } else {
    //     str = "Vui lòng thử lại sau vài giây";
    //   }
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.BLE_NOTIFY_CHAR:
    //   str = "Vui lòng tắt bật Bluetooth và thử lại sau vài giây";
    //   break;
    // case Ble.BLE_PROGRESS.ERROR.BLE_ERROR_AFTER_LOG_OUT:
    //   str =
    //     "Vui lòng thử lại với những cách sau:\n" +
    //     "1. Thử lại sau vài giây\n" +
    //     "2. Khởi động lại ứng dụng\n";
    //   break;
    default:
      str = '';
      break;
  }
  return str;
}

/**
 * Xoá dấu tiếng Việt của một chuỗi
 * @param {*} str
 * @example Hà Nội -> Ha Noi
 */
export function string_xoa_dau(str: string) {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  str = str.replace(/đ/g, 'd');
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
  str = str.replace(/Đ/g, 'D');
  return str;
}

/* 
 // help catch error when await a promise http://blog.grossman.io/how-to-write-async-await-without-try-catch-blocks-in-javascript/
  EXAMPLE
     [err, savedTask] = await to(TaskModel({userId: user.id, name: 'Demo Task'}));
     if(err) return cb('Error occurred while saving task');
  */
export function to(promise: any) {
  return promise
    .then(data => {
      return [null, data];
    })
    .catch(err => [err]);
}

export const translateBleError = (error: string | Error) => {
  const message = typeof error === 'string' ? error : error.message;

  switch (message) {
    case BLE_ERROR.BLE_OFF:
      return 'Vui lòng bật Bluetooth';

    case BLE_ERROR.DISCONNECTED:
      return 'Kết nối gián đoạn';

    case BLE_ERROR.NOT_FOUND:
      return 'Không tìm thấy thiết bị';

    case BLE_ERROR.NO_PERMISSION:
      return 'Ứng dụng không có quyền để sử dụng Bluetooth';

    case BLE_ERROR.BLE_SECURITY_FAIL:
      return 'Thiết bị không tương thích';

    case BLE_ERROR.INIT_ECU_FAILURE:
      return 'Kết nối tới ECU không thành công';

    case BLE_ERROR.KLINE_ERROR:
      return 'Vui lòng tắt bật khoá điện và thử lại sau vài giây';

    default:
      return message;
  }
};

export const translateBleErrorTitle = (error: string | Error) => {
  const message = typeof error === 'string' ? error : error.message;

  switch (message) {
    case BLE_ERROR.KLINE_ERROR:
      return 'Kết nối ECU gián đoạn';

    default:
      return 'Kết nối gián đoạn';
  }
};

/**
 * @return
 * - 1 if ver1 > ver2
 * - 0 if ver1 = ver2
 * - -1 if ver1 < ver2
 * @param ver1
 * @param ver2
 */
export const compareVersions = (ver1: number[] | readonly number[], ver2: number[] | readonly number[]) => {
  for (let i = 0; i < 3; i += 1) {
    if (ver1[i] === ver2[i]) {
      continue;
    } else if (ver1[i] > ver2[i]) {
      return 1;
    } else {
      return -1;
    }
  }
  return 0;
};

export const splitLineFromString = (str?: string) => {
  if (str) {
    return str
      .split('\n')
      .map(v => {
        if (v) {
          const _v = v.trim();
          return _v.charAt(0).toUpperCase() + _v.slice(1);
        }
        return v;
      })
      .join('\n*');
  }

  return str;
};
