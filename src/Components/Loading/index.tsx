import React, { PureComponent } from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";

type Props = {
  backgroundColor?: string;
  backgroundOpacity?: number;
};
class Loading extends PureComponent<Props> {
  static defaultProps = {
    backgroundColor: "gray",
    backgroundOpacity: 0.3
  };

  render() {
    const { backgroundColor, backgroundOpacity } = this.props;
    return (
      <View
        style={[
          StyleSheet.absoluteFill,
          { justifyContent: "center", alignItems: "center" }
        ]}
      >
        <View
          style={[
            StyleSheet.absoluteFill,
            { backgroundColor, opacity: backgroundOpacity }
          ]}
        />
        <ActivityIndicator size="large" />
      </View>
    );
  }
}

export default Loading;
