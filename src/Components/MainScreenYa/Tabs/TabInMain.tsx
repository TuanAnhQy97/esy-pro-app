/**
 * @flow
 */
import { createBottomTabNavigator, TabBarBottom } from "react-navigation";
import DiagnosticTab from "./DiagnosticTab";
import ToolTab from "./ToolTab";
import TabBarComponent from "./TabBarComponent";
import { colors } from "../../../styles/colors";
import { horizontalScale } from "../../../ScaleUtility";
import { REGULAR_FONT } from "../../Text";

const TabInMain = createBottomTabNavigator(
  {
    Diagnostic: {
      screen: DiagnosticTab
    }
    // ToolTab: {
    //   screen: ToolTab
    // }
  },
  {
    tabBarOptions: {
      activeTintColor: colors.primary_light,
      inactiveTintColor: colors.third_dark,
      allowFontScaling: false
    },
    tabBarComponent: TabBarComponent
  }
);

export default TabInMain;
