/**
 * @flow
 */
import React from "react";
import { ScrollView, View, ImageBackground } from "react-native";
import { connect } from "react-redux";
import { Orientation } from "react-native-orientation-locker";

import ControllerUnit from "../ControllerUnit";
import UserInfoButton from "../UserInfoButton";
import BottomSpaceForScrollView from "../../../Components/BottomSpaceForScrollView";
import { TYPE_STORE_DATA } from "../../../Store";

import { tools as OverlayConfirmErase_tools } from "../../Popup/OverlayConfirmErase";

import { colors } from "../../../styles/colors";
import navigationService from "../../../services/navigationService";
import ble from "../../../services/ble";

const lineWidth = 1;
const tabBarIconSize = 24;

type Props = { orientation: Orientation };
type States = { mill: boolean };

class ToolTab extends React.PureComponent<Props, States> {
  static navigationOptions = () => {
    return {
      title: "Công cụ sửa chữa",
      tabBarIcon: ({ focused, tintColor }) => (
        <ImageBackground
          style={{ width: tabBarIconSize, height: tabBarIconSize }}
          resizeMode="contain"
          imageStyle={{ tintColor: focused ? tintColor : null }}
          source={require("../../../../assets/Images/Main/tools.png")}
        />
      )
    };
  };

  constructor(props: Props) {
    super(props);
    this.state = { mill: true };
  }

  componentDidMount = () => {};

  componentWillUnmount = () => {};

  render() {
    const disabled = false;
    let lineColor;
    if (disabled) {
      lineColor = "gray";
    } else {
      lineColor = colors.secondary_dark;
    }
    const { orientation } = this.props;
    const { mill } = this.state;

    return (
      <ScrollView style={{ flex: 1 }}>
        <View style={{ flexDirection: "row" }}>
          <ControllerUnit
            orientation={orientation}
            disabled={disabled || !mill}
            title="XOÁ BỘ NHỚ LỖI"
            onPress={() => {
              OverlayConfirmErase_tools.showPopup(true);
            }}
            icon={require("../../../../assets/Images/Main/eraser.png")}
          />
        </View>
        {/* User Ìnfo */}
        <UserInfoButton />
        <BottomSpaceForScrollView />
      </ScrollView>
    );
  }
}
function mapStateToProps(state: TYPE_STORE_DATA) {
  return {
    orientation: state.Orientation.orientation
  };
}
export default connect(mapStateToProps)(ToolTab);
