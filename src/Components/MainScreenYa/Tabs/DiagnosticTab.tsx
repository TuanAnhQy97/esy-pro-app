/**
 * @flow
 */
import React from 'react';
import {ScrollView, View, ImageBackground, Alert} from 'react-native';
import {connect} from 'react-redux';
import {Orientation} from 'react-native-orientation-locker';
import {colors} from '../../../styles/colors';

import Text from '../../Text';
import {horizontalScale} from '../../../ScaleUtility';
import ControllerUnit from '../ControllerUnit';
import ble from '../../../services/ble';
import navigationService from '../../../services/navigationService';
import BottomSpaceForScrollView from '../../../Components/BottomSpaceForScrollView';
import {TYPE_STORE_DATA} from '../../../Store';
import UserInfoButton from '../UserInfoButton';
import bikeLibHelper from '../../../services/bikeLibHelper';

const lineWidth = 1;
const tabBarIconSize = 24;

type Props = {orientation: Orientation};
type States = {mill: boolean};

class DiagnosticTab extends React.PureComponent<Props, States> {
  static navigationOptions = () => {
    return {
      title: 'Chẩn đoán',
      tabBarIcon: ({focused, tintColor}) => (
        <ImageBackground
          style={{width: tabBarIconSize, height: tabBarIconSize}}
          resizeMode="contain"
          imageStyle={{tintColor: focused ? tintColor : null}}
          source={require('../../../../assets/Images/Main/diagnostic.png')}
        />
      ),
    };
  };

  constructor(props: Props) {
    super(props);
    this.state = {mill: false};
  }

  componentDidMount = () => {};

  componentWillUnmount = () => {};

  render() {
    const {mill} = this.state;
    // Xem bật chức năng hay không dựa vào tình trạng kết nối
    const disabled = false;

    let lineColor;
    if (disabled) {
      lineColor = 'gray';
    } else {
      lineColor = colors.secondary_dark;
    }
    const {orientation} = this.props;
    const working = bikeLibHelper.yamaha.convert.workingFunction(bikeLibHelper.yamaha.getEcuId());
    return (
      <ScrollView style={{flex: 1}}>
        {/* ADVANCE */}
        {/* <Text
          style={{
            fontSize: horizontalScale(20),
            paddingLeft: 10,
            paddingTop: 10
          }}
        >
          Chẩn đoán nâng cao
        </Text> */}
        <View style={{flexDirection: 'row'}}>
          <ControllerUnit
            orientation={orientation}
            disabled={disabled || !working.error}
            title="BỘ NHỚ LỖI"
            onPress={() => {
              if (working.error) {
                ble.esyDataManager.yamaha.toReadErrorMode();
                navigationService.navigate('ErrorYa');
              } else {
                Alert.alert('Chức năng chưa hỗ trợ cho dòng xe này');
              }
            }}
            icon={require('../../../../assets/Images/Main/engine.png')}
            highLight={mill}
          />
          <View
            style={{
              width: lineWidth,
              borderRadius: 2,
              backgroundColor: lineColor,
              marginVertical: 10,
            }}
          />
          <ControllerUnit
            orientation={orientation}
            disabled={disabled || !working.dashboard}
            title="THÔNG SỐ"
            onPress={() => {
              if (working.dashboard) {
                ble.esyDataManager.yamaha.toDashboardMode();
                navigationService.navigate('DashboardYa');
              } else {
                Alert.alert('Chức năng chưa hỗ trợ cho dòng xe này');
              }
            }}
            icon={require('../../../../assets/Images/Main/dashboard.png')}
          />
        </View>
        <ControllerUnit
          orientation={orientation}
          disabled={disabled || !working.advance}
          title="CHUẨN ĐOÁN NÂNG CAO"
          onPress={() => {
            if (working.advance) {
              ble.esyDataManager.yamaha.toAdvanceData();
              navigationService.navigate('AdvanceDataYa');
            } else {
              Alert.alert('Chức năng chưa hỗ trợ cho dòng xe này');
            }
          }}
          icon={require('../../../../assets/Images/Main/autoCheckOne.png')}
        />
        {/* User Ìnfo */}
        <UserInfoButton />
        <BottomSpaceForScrollView />
      </ScrollView>
    );
  }
}

const mapStateToProps = (state: TYPE_STORE_DATA) => {
  return {
    orientation: state.Orientation.orientation,
  };
};
export default connect(mapStateToProps)(DiagnosticTab);
