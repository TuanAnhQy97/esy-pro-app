/**
 * @flow
 */
import React from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import Text from "../../../Components/Text";

import { TabBarBottomProps, NavigationRoute } from "react-navigation";
import { colors } from "../../../styles/colors";

// just for ide hints
interface TabBarProps extends TabBarBottomProps {}

interface TabBarState {}

class Component extends React.PureComponent<TabBarProps, TabBarState> {
  constructor(props: TabBarProps) {
    super(props);
  }

  navigationStateIndex = null;

  // call when each time user click different tab
  navigationAvaliableFuncs: {
    [key: string]: () => boolean
  } = {
    //Account: this._needSignIn.bind(this),
    //Progress: this._needSignIn.bind(this),
  };

  // call when clicking tab got refused
  navigationRefusedFuncs: {
    [key: string]: () => void
  } = {
    // Account: this._refusedByNeedSignIn.bind(this),
    // Progress: this._refusedByNeedSignIn.bind(this)
  };

  render() {
    const { navigation, style } = this.props;
    const tabBarButtons = navigation.state.routes.map(
      this.renderTabBarButton.bind(this)
    );
    return <View style={[style, styles.tabBar]}>{tabBarButtons}</View>;
  }

  renderTabBarButton(route: NavigationRoute, idx: any) {
    const {
      activeTintColor,
      inactiveTintColor,
      navigation,
      getLabelText,
      renderIcon
    } = this.props;
    const currentIndex = navigation.state.index;
    const isForced = currentIndex === idx;
    const color = isForced ? activeTintColor : inactiveTintColor;

    const label = getLabelText({
      route,
      focused: isForced,
      index: idx
    });
    let backgroundColor;
    if (isForced) {
      backgroundColor = colors.secondary_dark;
    } else {
      backgroundColor = colors.primary_light;
    }
    return (
      <TouchableOpacity
        onPress={() => {
          if (currentIndex != idx) {
            if (this.isNavigateAvaliable(label)) {
              navigation.navigate(route.routeName);
            } else {
              this._onNavigationRefused(label);
            }
          }
        }}
        style={[
          styles.tabBarUnit,
          {
            backgroundColor,
            borderTopColor: colors.third_dark,
            borderTopWidth: isForced ? 0.5 : 1
          }
        ]}
        key={route.routeName}
      >
        {renderIcon({
          route,
          tintColor: color,
          focused: currentIndex === idx,
          index: idx
        })}
        <Text style={{ ...styles.label, color }}>{label}</Text>
      </TouchableOpacity>
    );
  }

  isNavigateAvaliable(label: string) {
    const func: any = this.navigationAvaliableFuncs[label];
    if (func) {
      return func();
    }
    return true;
  }
  _onNavigationRefused(label: string) {
    const func = this.navigationRefusedFuncs[label];
    if (func) {
      func();
    }
  }
}

export default Component;

const styles = StyleSheet.create({
  tabBar: {
    flexDirection: "row"
  },
  tabBarUnit: {
    flex: 1,
    height: 50,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  label: {
    marginLeft: 5,
    fontSize: 16
  }
});
