/**
 * @flow
 */
import React, { Component } from "react";
import { View, ImageBackground, Animated, Easing } from "react-native";
import { getTitleForConnectProgress, getSubtitle } from "../../SharedFunctions";
import { horizontalScale, verticalScale } from "../../ScaleUtility";
import RetryButton from "./RetryButton";
import { colors } from "../../styles/colors";
import Text from "../Text";

class FailureAminate extends React.PureComponent<
  { errorCode: number, callback: Function },
  {}
> {
  opacity = new Animated.Value(0);
  constructor() {
    super();
  }
  componentDidMount() {
    this.showup.bind(this)();
  }
  render() {
    let title = getTitleForConnectProgress(this.props.errorCode);
    let subtitle = getSubtitle(this.props.errorCode);
    return (
      <Animated.View style={{ alignItems: "center", opacity: this.opacity }}>
        <View
          style={{
            width: horizontalScale(272),
            alignItems: "center",
            flexDirection: "row"
          }}
        >
          <ImageBackground
            style={{
              width: horizontalScale(40),
              height: horizontalScale(40)
            }}
            imageStyle={{
              resizeMode: "contain"
            }}
            source={require("../../../assets/Images/Share/errorSmall.png")}
          />
          <View
            style={{
              marginLeft: horizontalScale(20)
            }}
          >
            <Text
              style={{
                width: horizontalScale(200),
                fontSize: horizontalScale(16)
              }}
            >
              {title}
            </Text>
            {subtitle ? (
              <Text
                style={{
                  width: horizontalScale(200),
                  fontSize: horizontalScale(14),
                  marginTop: 5,
                  color: colors.third_dark
                }}
              >
                {subtitle}
              </Text>
            ) : null}
          </View>
        </View>
        <RetryButton title="Thử lại" callback={this.props.callback} />
      </Animated.View>
    );
  }
  showup() {
    this.opacity.setValue(0);
    Animated.timing(this.opacity, {
      toValue: 1,
      duration: 400,
      easing: Easing.linear
    }).start();
  }
}
export default FailureAminate;
