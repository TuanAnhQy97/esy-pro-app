/**
 * @flow
 */
import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { verticalScale, horizontalScale } from "../../ScaleUtility";
import { colors } from "../../styles/colors";

const hitSlop = { top: 20, bottom: 20, left: 20, right: 20 };

class RetryButton extends React.PureComponent<
  { title: string, callback: Function },
  {}
> {
  constructor() {
    super();
  }
  componentDidMount() {}
  /** RENDER */
  render() {
    return (
      <TouchableOpacity
        hitSlop={hitSlop}
        style={{
          width: "100%",
          marginVertical: verticalScale(10),
          alignItems: "center",
          justifyContent: "center"
        }}
        onPress={this.props.callback}
      >
        <Text
          style={{
            fontSize: horizontalScale(18),
            color: colors.secondary_dark
          }}
        >
          {this.props.title}
        </Text>
      </TouchableOpacity>
    );
  }
}

export default RetryButton;
