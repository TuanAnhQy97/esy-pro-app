/**
 * @flow
 */
import React from "react";
import { View, ImageBackground } from "react-native";
import { verticalScale, horizontalScale } from "../../ScaleUtility";
import { wait } from "../../SharedFunctions";

import { colors as COLORS } from "../../styles/colors";

class DfuAnimate extends React.Component<
  {},
  { colors: string[]; activeIndex: number }
> {
  isUnmounting = false;

  constructor() {
    super();
    this.state = {
      colors: ["gray", "gray", "gray"],
      activeIndex: 0
    };
  }

  componentDidMount() {
    this.startAnimate.bind(this)();
  }

  componentWillUnmount() {
    this.isUnmounting = true;
  }

  startAnimate() {
    if (this.isUnmounting) {
      return;
    }
    if (this.state.activeIndex === 3) {
      this.state.activeIndex = 0;
    }
    const colors = ["gray", "gray", "gray"];
    colors[this.state.activeIndex] = COLORS.secondary_dark;
    this.setState(
      {
        colors,
        activeIndex: this.state.activeIndex + 1
      },
      async () => {
        await wait(400);
        this.startAnimate.bind(this)();
      }
    );
  }

  /** RENDER */
  render() {
    const circleRadius = horizontalScale(10);
    const cirleMargin = verticalScale(5);
    return (
      <View style={{ alignItems: "center", marginTop: verticalScale(68) }}>
        <ImageBackground
          style={{ height: verticalScale(66), width: horizontalScale(135) }}
          imageStyle={{ resizeMode: "contain" }}
          source={require("../../../assets/Images/DFU/cloud.png")}
        />
        {/* Aminate View */}
        <View
          style={{
            height: circleRadius,
            width: circleRadius,
            marginVertical: cirleMargin,
            borderRadius: circleRadius,
            backgroundColor: this.state.colors[0]
          }}
        />
        <View
          style={{
            height: circleRadius,
            width: circleRadius,
            marginVertical: cirleMargin,
            borderRadius: circleRadius,
            backgroundColor: this.state.colors[1]
          }}
        />
        <View
          style={{
            height: circleRadius,
            width: circleRadius,
            marginVertical: cirleMargin,
            borderRadius: circleRadius,
            backgroundColor: this.state.colors[2]
          }}
        />
        <ImageBackground
          style={{ height: verticalScale(67), width: horizontalScale(113) }}
          imageStyle={{ resizeMode: "contain" }}
          source={require("../../../assets/Images/DFU/ESY.png")}
        />
      </View>
    );
  }
}

export default DfuAnimate;
