/**
 * @flow
 */
import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  Animated,
  Easing,
  TouchableOpacity
} from "react-native";
import { getTitleForConnectProgress, getSubtitle } from "../../SharedFunctions";
import { horizontalScale, verticalScale } from "../../ScaleUtility";
import { colors } from "../../styles/colors";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

class FailureAminateDashboard extends React.PureComponent<
  { errorCode: number, callback: Function },
  {}
> {
  opacity = new Animated.Value(0);
  constructor() {
    super();
  }
  componentDidMount() {
    this.showup.bind(this)();
  }
  render() {
    let title = getTitleForConnectProgress(this.props.errorCode);
    let subtitle = getSubtitle(this.props.errorCode);
    let iconSize = horizontalScale(165, -0.5);
    return (
      <Animated.View
        style={{
          opacity: this.opacity,
          flex: 1
        }}
      >
        {/* Icon */}
        <ImageBackground
          style={{
            width: iconSize,
            height: iconSize,
            marginTop: verticalScale(126),
            alignSelf: "center"
          }}
          imageStyle={{
            resizeMode: "contain"
            // tintColor: colors.primary_dark
          }}
          source={require("../../../assets/Images/Share/errorLarge.png")}
        />
        <View
          style={{
            paddingLeft: horizontalScale(30),
            paddingRight: horizontalScale(47)
          }}
        >
          {/* Title */}
          <Text
            style={{
              fontSize: horizontalScale(16, -0.5),
              marginTop: verticalScale(39),
              color: colors.third_dark
            }}
          >
            {title}
          </Text>
          {/* Subtitle */}
          {subtitle ? (
            <Text
              style={{
                fontSize: horizontalScale(16, -0.5),
                marginTop: 6,
                textAlign: "justify"
              }}
            >
              {subtitle}
            </Text>
          ) : null}
          <TouchableOpacity
            hitSlop={hitSlop}
            style={{ paddingTop: verticalScale(11), alignSelf: "flex-end" }}
            onPress={this.props.callback}
          >
            <Text
              style={{
                fontSize: horizontalScale(16, -0.5),
                color: colors.secondary_dark
              }}
            >
              Thử lại
            </Text>
          </TouchableOpacity>
        </View>
      </Animated.View>
    );
  }
  showup() {
    this.opacity.setValue(0);
    Animated.timing(this.opacity, {
      toValue: 1,
      duration: 400,
      easing: Easing.linear
    }).start();
  }
}
export default FailureAminateDashboard;
