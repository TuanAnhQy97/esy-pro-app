/**
 * @flow
 */
import React from "react";
import { View } from "react-native";
import Lottie from "lottie-react-native";

type Props = {};
type States = {};

class Component extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Lottie
          speed={1.5}
          source={require("./bike_loader.json")}
          style={{ height: 200, width: 200 }}
          autoPlay={true}
          loop={false}
        />
      </View>
    );
  }
}

export default Component;
