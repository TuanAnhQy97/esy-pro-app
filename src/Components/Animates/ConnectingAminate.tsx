/**
 * @flow
 */
import React, { Component } from "react";
import { View, Animated, Easing } from "react-native";

import { BallIndicator } from "react-native-indicators";
import { colors } from "../../styles/colors";
import { horizontalScale } from "../../ScaleUtility";
import Text from "../Text";

class ConnectingAminate extends React.PureComponent<
  { title: string, titleColor?: string },
  {}
> {
  opacity = new Animated.Value(0);
  static defaultProps = {
    titleColor: "#000000"
  };
  componentDidMount() {
    this.showup.bind(this)();
  }
  render() {
    return (
      <Animated.View
        style={{
          flexDirection: "row",
          opacity: this.opacity,
          alignItems: "center"
        }}
      >
        <BallIndicator color={colors.secondary_dark} size={50} />
        <Text
          style={{
            fontSize: horizontalScale(18),
            color: this.props.titleColor,
            marginLeft: horizontalScale(22)
          }}
        >
          {this.props.title}
        </Text>
      </Animated.View>
    );
  }
  showup() {
    this.opacity.setValue(0);
    Animated.timing(this.opacity, {
      toValue: 1,
      duration: 400,
      easing: Easing.linear
    }).start();
  }
}

export default ConnectingAminate;
