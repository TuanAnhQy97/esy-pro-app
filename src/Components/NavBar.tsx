/**
 * @flow
 */
import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ImageBackground,
  TouchableOpacity
} from "react-native";
import Text from "../Components/Text";

import { screenWidth, horizontalScale } from "../ScaleUtility";
import { colors } from "../styles/colors";
import navigationService from "../services/navigationService";

const navBarHeight = 44; // iOS

type NavBarProps = {
  onLeftPress?: Function;
  title: string;
  leftIconSource?: any;
  rightIconSource?: any;
  onRightPress?: Function;
  backgroundColor?: string;
};

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

class NavBar extends React.PureComponent<NavBarProps, {}> {
  static defaultProps = {
    leftIconSource: require("../../assets/Images/Share/back.png"),
    rightIconSource: null,
    onRightPress: () => {},
    onLeftPress: navigationService.goBack,
    backgroundColor: colors.primary_dark
  };

  render() {
    return (
      <View
        style={[
          styles.navBar,
          {
            backgroundColor: this.props.backgroundColor
          }
        ]}
      >
        {/* Left Button */}
        <TouchableOpacity
          style={[
            styles.navbarIconTouchArea,
            { marginLeft: horizontalScale(18) }
          ]}
          onPress={this.props.onLeftPress}
          hitSlop={hitSlop}
        >
          <ImageBackground
            style={styles.navBarIcon}
            imageStyle={{ resizeMode: "contain" }}
            source={this.props.leftIconSource}
          />
        </TouchableOpacity>
        {/* Title */}
        <Text
          style={{
            flex: 1,
            fontSize: horizontalScale(20, -0.5),
            color: colors.primary_light,
            textAlign: "center",
            textAlignVertical: "center"
          }}
        >
          {this.props.title}
        </Text>
        {/* Right Button */}
        <TouchableOpacity
          style={[
            styles.navbarIconTouchArea,
            { marginRight: horizontalScale(18), alignItems: "flex-end" }
          ]}
          onPress={this.props.onRightPress}
          hitSlop={hitSlop}
        >
          <ImageBackground
            style={styles.navBarIcon}
            imageStyle={{
              resizeMode: "contain",
              tintColor: colors.primary_light
            }}
            source={this.props.rightIconSource}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navBar: {
    width: "100%",
    height: navBarHeight,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
    // borderWidth: 1,
    // borderColor: "gray"
  },
  navBarTitle: {
    flex: 1,
    fontSize: horizontalScale(20),
    color: colors.primary_light,
    textAlign: "center",
    textAlignVertical: "center"
  },
  navBarIcon: {
    width: 25,
    height: 25
  },
  navbarIconTouchArea: {
    width: 40,
    height: 40,
    justifyContent: "center"
  }
});

export default NavBar;
