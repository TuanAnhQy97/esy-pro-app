/**
 * @flow
 */
import React, { Component } from "react";
import {
  View,
  StatusBar,
  BackHandler,
  Platform,
  SafeAreaView
} from "react-native";
import { colors } from "../styles/colors";
import navigationService from "../services/navigationService";

class BaseScreen extends React.PureComponent<
  {
    children: any;
    bodyBackgroundColor?: string;
    backgroundColor?: string;
    statusbarStyle?: "light-content" | "default" | "dark-content";
    handleBackPress?: Function;
  },
  { handleBackPress: Function }
> {
  static defaultProps = {
    bodyBackgroundColor: colors.primary_light,
    backgroundColor: colors.primary_dark,
    statusbarStyle: "light-content",
    handleBackPress: () => {
      /**
       * The event subscriptions are called in reverse order (i.e. last registered subscription first),
       * and if one subscription returns true then subscriptions registered earlier will not be called.
       */
      console.log("handleBackPress DEFAULT");
      navigationService.goBack();
      return true;
    }
  };

  constructor(props: any) {
    super(props);
  }

  componentDidMount() {
    // Tránh trường hợp hàm truyền vào bị thay đổi
    // -> Dẫn đến không remove được
    if (Platform.OS === "android") {
      this.setState(
        {
          handleBackPress: () => {
            const result = this.props.handleBackPress();
            if (result === undefined) {
              return true;
            }
            return result;
          }
        },
        () => {
          BackHandler.addEventListener(
            "hardwareBackPress",
            this.state.handleBackPress
          );
        }
      );
    }
  }

  componentWillUnmount() {
    if (Platform.OS === "android") {
      BackHandler.removeEventListener(
        "hardwareBackPress",
        this.state.handleBackPress
      );
    }
  }

  render() {
    return (
      <SafeAreaView
        style={{ flex: 1, backgroundColor: this.props.backgroundColor }}
      >
        <StatusBar
          barStyle={this.props.statusbarStyle}
          backgroundColor={this.props.backgroundColor}
        />
        <View
          style={{
            flex: 1,
            backgroundColor: this.props.bodyBackgroundColor
          }}
        >
          {this.props.children}
        </View>
      </SafeAreaView>
    );
  }
}
export default BaseScreen;
