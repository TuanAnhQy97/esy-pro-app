import React from "react";
import { View, StatusBar, StatusBarStyle } from "react-native";
import SafeAreaView from "react-native-safe-area-view";
import { colors } from "../../styles/colors";
import BackHandlerHelper from "../../services/BackHandlerHelper";

type inset = "always" | "never";

type Props = {
  defaultBackHandler?: boolean;
  forceInset?: { top?: inset; right?: inset; left?: inset; bottom?: inset };
  bottomColor?: string;
  topColor?: string;
  bodyColor?: string;
  barStyle?: StatusBarStyle;
};

const withSafeArea = (
  {
    defaultBackHandler = true,
    forceInset = {},
    bottomColor,
    topColor,
    barStyle,
    bodyColor = colors.primary_light
  } = {} as Props
) => (Component: any) =>
  class extends React.PureComponent {
    render() {
      // Android Only
      let paddingTop;
      if (forceInset.top !== "never") {
        paddingTop = StatusBar.currentHeight ? StatusBar.currentHeight : 0;
      } else {
        paddingTop = 0;
      }
      const { props } = this;
      StatusBar.setBarStyle(barStyle || "light-content");
      return (
        <View
          style={{ flex: 1, backgroundColor: topColor || colors.primary_dark }}
        >
          <StatusBar translucent backgroundColor="rgba(0,0,0,0)" />
          <SafeAreaView
            forceInset={forceInset || {}}
            style={[
              {
                flex: 1,
                backgroundColor: topColor || colors.primary_dark,
                paddingTop
              }
            ]}
          >
            {/* Override bottom color */}
            {bottomColor ? (
              <View
                style={{
                  position: "absolute",
                  bottom: 0,
                  left: 0,
                  height: 50,
                  width: "100%",
                  backgroundColor: bottomColor
                }}
              />
            ) : null}
            <View style={{ flex: 1, backgroundColor: bodyColor }}>
              <Component {...props} />
            </View>
          </SafeAreaView>

          {defaultBackHandler && <BackHandlerHelper />}
        </View>
      );
    }
  };

export default withSafeArea;
