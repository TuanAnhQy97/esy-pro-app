import { StyleSheet } from "react-native";
import { horizontalScale } from "../../ScaleUtility";

export default StyleSheet.create({
  rootContainer: { backgroundColor: "gray" },
  imageBackgroundStyle: {
    height: 200,
    width: "100%",
    justifyContent: "center"
  },
  contentInImage: {},
  name: { fontWeight: "bold", fontSize: horizontalScale(20), color: "white" }
});
