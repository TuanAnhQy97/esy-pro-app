import React from "react";
import {
  Text,
  TouchableOpacity,
  ImageBackground,
  ImageSourcePropType,
  Alert
} from "react-native";

import styles from "./styles";

export type BIKE_MANUFACTURE_DATA = {
  key: string;
  name: string;
  imageSource: ImageSourcePropType;
  licenseTime?: number;
};

type Props = {
  data: BIKE_MANUFACTURE_DATA;
  onPressWhenValid: (key: string) => void;
};
type States = {};

class BikeManufactureItem extends React.PureComponent<Props, States> {
  static defaultProps = {
    imageSource: undefined
  };

  constructor(props: Props) {
    super(props);
  }

  _onPress = () => {
    const { data, onPressWhenValid } = this.props;
    const { licenseTime, key } = data;
    if (licenseTime) {
      // Has license for this bike
      onPressWhenValid(key);
    } else {
      // No license
      Alert.alert(
        "Để sử dụng tính năng này an toàn, bạn cần cáp kết nối cáp chính hãng",
        "Vui lòng liên hệ Fangia Savy để biết thêm chi tiết"
      );
    }
  };

  render() {
    const { data } = this.props;
    const { licenseTime, name, imageSource } = data;
    return (
      <TouchableOpacity style={[styles.rootContainer]} onPress={this._onPress}>
        <ImageBackground
          blurRadius={0.3}
          source={imageSource}
          imageStyle={styles.contentInImage}
          style={[
            styles.imageBackgroundStyle,
            { opacity: licenseTime ? 1 : 0.5 }
          ]}
          resizeMode="cover"
        >
          <Text style={styles.name}>{name}</Text>
        </ImageBackground>
      </TouchableOpacity>
    );
  }
}

export default BikeManufactureItem;
