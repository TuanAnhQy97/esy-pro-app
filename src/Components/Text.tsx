/**
 * @flow
 */

import React, { Component } from "react";
import { Text, TextStyle } from "react-native";

import { horizontalScale } from "../ScaleUtility";

export const REGULAR_FONT = "Cabin";
export const BOLD_FONT = "Cabin-Bold";

const defaultFontSize = horizontalScale(14);

class MyText extends React.PureComponent<
  { children: any, style?: TextStyle },
  {}
> {
  static defaultProps = {
    style: {}
  };
  render() {
    return (
      <Text
        style={[
          {
            fontSize: defaultFontSize,
            color: "#000000",
            fontFamily: REGULAR_FONT
          },
          { ...this.props.style }
        ]}
      >
        {this.props.children}
      </Text>
    );
  }
}

export default MyText;
