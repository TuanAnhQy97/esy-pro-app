/**
 * @flow
 */
import React from "react";
import { ScrollView, View, ImageBackground } from "react-native";
import { connect } from "react-redux";
import { Orientation } from "react-native-orientation-locker";

import ControllerUnit from "../ControllerUnit";
import UserInfoButton from "../UserInfoButton";
import SearchErrorButton from "../SearchErrorButton";
import BottomSpaceForScrollView from "../../../Components/BottomSpaceForScrollView";
import { TYPE_STORE_DATA } from "../../../Store";
import { tools as OverlayConfirmReset_tools } from "../../Popup/OverlayConfirmReset";
import { tools as OverlayConfirmErase_tools } from "../../Popup/OverlayConfirmErase";
import eventBus from "../../../services/eventBus";
import { EVENT_BUS } from "../../../constants";
import { colors } from "../../../styles/colors";
import navigationService from "../../../services/navigationService";
import ble from "../../../services/ble";
import bikeLibHelper from "../../../services/bikeLibHelper";

const lineWidth = 1;
const tabBarIconSize = 24;

type Props = { orientation: Orientation };
type States = { mill: boolean };

class ToolTab extends React.PureComponent<Props, States> {
  static navigationOptions = () => {
    return {
      title: "Công cụ sửa chữa",
      tabBarIcon: ({ focused, tintColor }) => (
        <ImageBackground
          style={{ width: tabBarIconSize, height: tabBarIconSize }}
          resizeMode="contain"
          imageStyle={{ tintColor: focused ? tintColor : null }}
          source={require("../../../../assets/Images/Main/tools.png")}
        />
      )
    };
  };

  constructor(props: Props) {
    super(props);
    this.state = { mill: false };
  }

  componentDidMount = () => {
    eventBus.addListener(
      EVENT_BUS.BIKE_IDLE_MODE_DATA_CHANGE,
      this._onMillChange
    );
  };

  componentWillUnmount = () => {
    eventBus.removeListener(this._onMillChange);
  };

  _onMillChange = ({ mill }) => this.setState({ mill });

  render() {
    const disabled = false;
    let lineColor;
    if (disabled) {
      lineColor = "gray";
    } else {
      lineColor = colors.secondary_dark;
    }
    const { orientation } = this.props;
    const { mill } = this.state;

    return (
      <ScrollView style={{ flex: 1 }}>
        <View style={{ flexDirection: "row" }}>
          <ControllerUnit
            orientation={orientation}
            disabled={disabled}
            title="CẬP NHẬT ECU"
            onPress={() => {
              const params = {
                ecuId: [...ble.ecuId]
              };
              navigationService.navigate("UpdateEcu_CheckVer", params);
            }}
            icon={require("../../../../assets/Images/Main/updateEcu.png")}
          />
          <View
            style={{
              width: lineWidth,
              borderRadius: 2,
              backgroundColor: lineColor,
              marginVertical: 10
            }}
          />
          <ControllerUnit
            orientation={orientation}
            disabled={disabled || !mill}
            title="XOÁ BỘ NHỚ LỖI"
            onPress={() => {
              OverlayConfirmErase_tools.showPopup(true);
            }}
            icon={require("../../../../assets/Images/Main/eraser.png")}
          />
        </View>
        <View style={{ flexDirection: "row" }}>
          <View
            style={{
              marginHorizontal: 10,
              height: lineWidth,
              borderRadius: 2,
              flex: 1,
              backgroundColor: lineColor
            }}
          />

          <View
            style={{
              marginHorizontal: 10,
              height: lineWidth,
              borderRadius: 2,
              flex: 1,
              backgroundColor: lineColor
            }}
          />
        </View>
        <View style={{ flexDirection: "row" }}>
          <ControllerUnit
            orientation={orientation}
            disabled={disabled}
            title="KHÔI PHỤC ECU"
            onPress={() => {
              OverlayConfirmReset_tools.showPopup(true);
            }}
            icon={require("../../../../assets/Images/Main/reset.png")}
          />
          <View
            style={{
              width: lineWidth,
              borderRadius: 2,
              backgroundColor: lineColor,
              marginVertical: 10
            }}
          />
          <ControllerUnit
            orientation={orientation}
            disabled={disabled}
            title="THÔNG TIN XE"
            onPress={() => {
              const params = {
                basicInfo: bikeLibHelper.honda._hondaLib.basicInfo,
                ecuId: [...ble.ecuId]
              };
              navigationService.navigate("BikeInfo", params);
            }}
            icon={require("../../../../assets/Images/Main/info.png")}
          />
        </View>
        <View
          style={{
            marginHorizontal: 10,
            height: lineWidth,
            borderRadius: 2,
            flex: 1,
            backgroundColor: lineColor
          }}
        />
        {/* User Ìnfo */}
        <UserInfoButton />
        <SearchErrorButton />
        <BottomSpaceForScrollView />
      </ScrollView>
    );
  }
}
function mapStateToProps(state: TYPE_STORE_DATA) {
  return {
    orientation: state.Orientation.orientation
  };
}
export default connect(mapStateToProps)(ToolTab);
