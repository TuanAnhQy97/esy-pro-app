/**
 * @flow
 */
import React from "react";
import {
  ScrollView,
  View,
  ImageBackground,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import { Orientation } from "react-native-orientation-locker";
import ControllerUnit from "../ControllerUnit";
import BottomSpaceForScrollView from "../../../Components/BottomSpaceForScrollView";
import { TYPE_STORE_DATA } from "../../../Store";

import Text from "../../Text";
import { horizontalScale } from "../../../ScaleUtility";
import { colors } from "../../../styles/colors";
import TrackUser from "../../../TrackUser";
import eventBus from "../../../services/eventBus";
import { EVENT_BUS } from "../../../constants";
import ble from "../../../services/ble";
import navigationService from "../../../services/navigationService";
import bikeLibHelper from "../../../services/bikeLibHelper";

const lineWidth = 1;
const tabBarIconSize = 24;

type Props = { orientation: Orientation };
type States = { mill: boolean };

class DiagnosticTab extends React.PureComponent<Props, States> {
  static navigationOptions = () => {
    return {
      title: "Chẩn đoán",
      tabBarIcon: ({ focused, tintColor }) => (
        <ImageBackground
          style={{ width: tabBarIconSize, height: tabBarIconSize }}
          resizeMode="contain"
          imageStyle={{ tintColor: focused ? tintColor : null }}
          source={require("../../../../assets/Images/Main/diagnostic.png")}
        />
      )
    };
  };

  constructor(props: Props) {
    super(props);
    this.state = { mill: false };
  }

  componentDidMount = () => {
    eventBus.addListener(
      EVENT_BUS.BIKE_IDLE_MODE_DATA_CHANGE,
      this._onMillChange
    );
  };

  componentWillUnmount = () => {
    eventBus.removeListener(this._onMillChange);
  };

  _onMillChange = ({ mill }) => this.setState({ mill });

  render() {
    const { mill } = this.state;
    // Xem bật chức năng hay không dựa vào tình trạng kết nối
    const disabled = false;

    let lineColor;
    if (disabled) {
      lineColor = "gray";
    } else {
      lineColor = colors.secondary_dark;
    }
    const { orientation } = this.props;
    return (
      <ScrollView style={{ flex: 1 }}>
        {/* AUTO */}
        <Text
          style={{
            fontSize: horizontalScale(20),
            paddingLeft: 10,
            paddingTop: 10
          }}
        >
          Chẩn đoán tự động
        </Text>
        <View style={{ flexDirection: "row" }}>
          <ControllerUnit
            orientation={orientation}
            disabled={disabled}
            title="ĐÁNH GIÁ CẢM BIẾN"
            onPress={() => {
              ble.esyDataManager.honda.toAutoCheckOne();
              navigationService.navigate("AutoCheckOne");
              TrackUser.logEvent("open_auto_check_one");
            }}
            icon={require("../../../../assets/Images/Main/autoCheckOne.png")}
          />
          <View
            style={{
              width: lineWidth,
              borderRadius: 2,
              backgroundColor: lineColor,
              marginVertical: 10
            }}
          />
          <ControllerUnit
            orientation={orientation}
            disabled={disabled}
            title="HIỆU SUẤT ĐỘNG CƠ"
            onPress={() => {
              ble.esyDataManager.honda.toAutoCheckTwo();
              navigationService.navigate("AutoCheckTwo");
              TrackUser.logEvent("open_auto_check_two");
            }}
            icon={require("../../../../assets/Images/Main/autoCheckTwo.png")}
          />
        </View>
        {/* ADVANCE */}
        <Text
          style={{
            fontSize: horizontalScale(20),
            paddingLeft: 10,
            paddingTop: 10
          }}
        >
          Chẩn đoán nâng cao
        </Text>
        <View style={{ flexDirection: "row" }}>
          <ControllerUnit
            orientation={orientation}
            disabled={disabled}
            title="BỘ NHỚ LỖI"
            onPress={() => {
              ble.esyDataManager.honda.toReadErrorMode();
              navigationService.navigate("ErrorFI");
              TrackUser.logEvent("open_read_fi_error");
            }}
            icon={require("../../../../assets/Images/Main/engine.png")}
            highLight={mill}
          />
          <View
            style={{
              width: lineWidth,
              borderRadius: 2,
              backgroundColor: lineColor,
              marginVertical: 10
            }}
          />
          <ControllerUnit
            orientation={orientation}
            disabled={disabled}
            title="THÔNG SỐ"
            onPress={() => {
              ble.esyDataManager.honda.toDashboardMode();
              navigationService.navigate("Dashboard");
              TrackUser.logEvent("open_dashboard");
            }}
            icon={require("../../../../assets/Images/Main/dashboard.png")}
          />
        </View>
        <TouchableOpacity
          disabled={disabled}
          style={{
            marginVertical: 10,
            marginHorizontal: 10,
            padding: 10,
            borderRadius: 5,
            backgroundColor: disabled ? "gray" : colors.secondary_dark,
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "row"
          }}
          onPress={() => {
            const params = { bikeName: bikeLibHelper.honda.getBikeName() };
            navigationService.navigate("Report_fillInfo", params);
          }}
        >
          <ImageBackground
            style={{
              width: tabBarIconSize,
              height: tabBarIconSize,
              marginRight: 10
            }}
            resizeMode="contain"
            imageStyle={{ tintColor: "white" }}
            source={require("../../../../assets/Images/Main/printer.png")}
          />
          <Text
            style={{
              color: colors.primary_light,
              fontSize: horizontalScale(16)
            }}
          >
            Xuất báo cáo khách hàng
          </Text>
        </TouchableOpacity>
        <BottomSpaceForScrollView />
      </ScrollView>
    );
  }
}

const mapStateToProps = (state: TYPE_STORE_DATA) => {
  return {
    orientation: state.Orientation.orientation
  };
};
export default connect(mapStateToProps)(DiagnosticTab);
