/**
 * @flow
 */

import React, { Component } from "react";
import {
  TouchableOpacity,
  Animated,
  ImageBackground,
  Easing
} from "react-native";

import Text from "../../Text";
import { verticalScale, horizontalScale } from "../../../ScaleUtility";

import { colors } from "../../../styles/colors";
import { Orientation } from "react-native-orientation-locker";

class ControllerUnit extends React.PureComponent<
  {
    disabled: boolean,
    icon?: any,
    title: string,
    onPress: Function,
    icon?: any,
    highLight?: boolean,
    orientation: Orientation
  },
  {}
> {
  blinkValue = new Animated.Value(0);
  constructor() {
    super();
    this.state = {};
  }
  componentDidMount() {
    this.blink();
  }

  render() {
    const { orientation } = this.props;
    const iconSize =
      orientation === "PORTRAIT" ? verticalScale(70) : verticalScale(60);

    const height =
      orientation === "PORTRAIT"
        ? verticalScale(150, 0)
        : verticalScale(100, 0);
    const flexDirection = orientation === "PORTRAIT" ? "column" : "row";

    const justifyContent = orientation === "PORTRAIT" ? "center" : null;
    const alignItems = orientation === "PORTRAIT" ? null : "center";

    let animatedOpacity;
    if (this.props.highLight && !this.props.disabled) {
      animatedOpacity = this.blinkValue.interpolate({
        inputRange: [0, 1, 2, 3, 4],
        outputRange: [1, 0.75, 0, 0.75, 1]
      });
    } else {
      animatedOpacity: 1;
    }

    let backgroundColor = colors.secondary_dark;
    let opacity = 1;
    if (this.props.disabled) {
      backgroundColor = "gray";
      opacity = 0.8;
    }
    let tintColor;
    if (this.props.disabled) {
      tintColor = null;
    } else if (this.props.highLight) {
      tintColor = colors.warning;
    } else {
      // tintColor = colors.primary_light;
    }
    return (
      <TouchableOpacity
        disabled={this.props.disabled}
        style={{
          marginVertical: 10,
          marginHorizontal: 10,
          borderRadius: 5,
          flex: 1,
          height: height,
          borderColor: "gray",
          backgroundColor,
          opacity,
          flexDirection,
          justifyContent,
          alignItems
        }}
        onPress={this.props.onPress}
      >
        <Animated.View style={{ opacity: animatedOpacity }}>
          <ImageBackground
            source={this.props.icon}
            style={{
              margin: 10,
              width: iconSize,
              height: iconSize,
              alignSelf: "center"
            }}
            imageStyle={{
              tintColor: tintColor
            }}
            resizeMode="contain"
          />
        </Animated.View>
        <Text
          style={{
            alignSelf: "center",
            color: "white",
            textAlign: "center",
            fontSize: horizontalScale(16),
            paddingHorizontal: 10
          }}
        >
          {this.props.title}
        </Text>
      </TouchableOpacity>
    );
  }
  blink() {
    this.blinkValue.setValue(0);
    Animated.timing(this.blinkValue, {
      toValue: 4,
      duration: 1000,
      easing: Easing.sin
    }).start(() => {
      this.blink();
    });
  }
}

export default ControllerUnit;
