/**
 * @flow
 */
import React from "react";
import { TouchableOpacity } from "react-native";
import Text from "../../Components/Text";
import { colors } from "../../styles/colors";
import { horizontalScale } from "../../ScaleUtility";
import navigationService from "../../services/navigationService";

type Props = {};
type States = {};

class Component extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity
        onPress={() => {
          navigationService.navigate("TrialList");
        }}
        style={{
          marginVertical: 10,
          marginHorizontal: 10,
          padding: 10,
          borderRadius: 5,
          backgroundColor: colors.secondary_dark,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Text
          style={{ color: colors.primary_light, fontSize: horizontalScale(16) }}
        >
          TRA CỨU MÃ LỖI
        </Text>
      </TouchableOpacity>
    );
  }
}

export default Component;
