/**
 * @flow
 */
import React from "react";
import { View, StyleSheet } from "react-native";
import { MaterialIndicator } from "react-native-indicators";
import Text from "../../Components/Text";

type States = {};
type Props = { show: boolean };

class OverlayWaitingChangeMode extends React.PureComponent<Props, States> {
  render() {
    if (this.props.show) {
      return (
        <View
          style={{
            ...StyleSheet.absoluteFillObject,
            backgroundColor: "gray",
            opacity: 0.7,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <MaterialIndicator />
          <Text
            style={{
              // color: colors.primary_light,
              alignSelf: "center",
              fontSize: 20,
              marginTop: 20
            }}
          >
            Đang chuyển dữ liệu
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }
}
export default OverlayWaitingChangeMode;
