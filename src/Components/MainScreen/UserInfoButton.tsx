/**
 * @flow
 */
import React from "react";
import { TouchableOpacity } from "react-native";
import Text from "../../Components/Text";
import { colors } from "../../styles/colors";
import { horizontalScale } from "../../ScaleUtility";
import navigationService from "../../services/navigationService";
import ble from "../../services/ble";

type Props = {};
type States = {};

class Component extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity
        onPress={() => {
          navigationService.navigate("UserInfo", {
            deviceId: ble.targetDeviceId || []
          });
        }}
        style={{
          marginVertical: 10,
          marginHorizontal: 10,
          padding: 10,
          borderRadius: 5,
          backgroundColor: colors.secondary_dark,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Text
          style={{ color: colors.primary_light, fontSize: horizontalScale(16) }}
        >
          THÔNG TIN TÀI KHOẢN
        </Text>
      </TouchableOpacity>
    );
  }
}

export default Component;
