/**
 * @flow
 */

import React from "react";
import { View, ImageBackground } from "react-native";
import { connect } from "react-redux";
import { Orientation } from "react-native-orientation-locker";

import Text from "../../Text";
import { TYPE_STORE_DATA } from "../../../Store";
import { verticalScale, horizontalScale } from "../../../ScaleUtility";
import { colors } from "../../../styles/colors";
import bikeLibHelper from "../../../services/bikeLibHelper";

class Header extends React.PureComponent<
  {
    orientation: Orientation;
  },
  {}
> {
  render() {
    const { orientation } = this.props;
    switch (orientation) {
      case "PORTRAIT":
        return this.renderPortraitLayout();

      default:
        return this.renderLandscapeLayout();
    }
  }

  renderLandscapeLayout() {
    return (
      <View
        style={{
          paddingHorizontal: 5,
          paddingVertical: 10,
          backgroundColor: colors.primary_dark,
          flexDirection: "row",
          justifyContent: "space-between"
        }}
      >
        {/* Icon thể hiện kết nối Xe === ESY === App */}
        {this.renderConnectIcon()}
        {/* Tên xe (Nếu đã kết nối Okay) */}
        {this.renderBikeName()}
      </View>
    );
  }

  renderPortraitLayout() {
    return (
      <View
        style={{
          paddingLeft: 5,
          paddingVertical: 10,
          backgroundColor: colors.primary_dark,
          flexDirection: "row",
          justifyContent: "space-between"
        }}
      >
        <View>
          {/* Icon thể hiện kết nối Xe === ESY === App */}
          {this.renderConnectIcon()}
          {/* Tên xe (Nếu đã kết nối Okay) */}
          {this.renderBikeName()}
        </View>
      </View>
    );
  }

  renderConnectIcon = () => {
    const bike_Esy = true;
    const Esy_app = true;

    const iconWidth = horizontalScale(30);
    const iconHeight = verticalScale(18);
    const marginHorizontal = horizontalScale(11);
    return (
      <View
        style={{
          flexDirection: "row",
          width: horizontalScale(30 * 5 + 11 * 2 * 2) + 10 * 2,
          padding: 10,
          borderColor: bike_Esy || Esy_app ? undefined : colors.warning,
          borderWidth: 1,
          borderRadius: 8
        }}
      >
        {/* Bike */}
        <ImageBackground
          style={{ height: iconHeight, width: iconWidth }}
          resizeMode="contain"
          source={require("../../../../assets/Images/Main/bike.png")}
        />
        <ImageBackground
          style={{
            height: iconHeight,
            width: iconWidth,
            marginHorizontal
          }}
          resizeMode="contain"
          source={
            bike_Esy
              ? require("../../../../assets/Images/Main/transfer.png")
              : require("../../../../assets/Images/Main/transferError.png")
          }
          imageStyle={{ tintColor: bike_Esy ? "white" : colors.warning }}
        />
        {/* ESY */}
        <ImageBackground
          style={{ height: iconHeight, width: iconWidth }}
          resizeMode="contain"
          source={require("../../../../assets/Images/Main/ESY.png")}
        />
        <ImageBackground
          style={{
            height: iconHeight,
            width: iconWidth,
            marginHorizontal
          }}
          resizeMode="contain"
          source={
            Esy_app
              ? require("../../../../assets/Images/Main/transfer.png")
              : require("../../../../assets/Images/Main/transferError.png")
          }
          imageStyle={{ tintColor: Esy_app ? "white" : colors.warning }}
        />
        {/* app */}
        <ImageBackground
          style={{ height: iconHeight, width: iconWidth }}
          resizeMode="contain"
          source={require("../../../../assets/Images/Main/app.png")}
        />
      </View>
    );
  };

  renderBikeName() {
    let bikeName;
    try {
      bikeName = bikeLibHelper.honda.getBikeName();
    } catch (e) {}
    if (bikeName) {
      return (
        <Text
          style={{
            paddingTop: 10,
            width: "100%",
            textAlign: "center",
            color: colors.primary_light,
            fontSize: horizontalScale(20)
          }}
        >
          {bikeName}
        </Text>
      );
    }
    return null;
  }
}
function mapPropToStore(store: TYPE_STORE_DATA) {
  return {
    orientation: store.Orientation.orientation
  };
}
export default connect(mapPropToStore)(Header);
