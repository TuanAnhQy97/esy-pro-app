/**
 * @flow
 */
import React from "react";
import { Platform, View } from "react-native";

type Props = {};
type States = {};

class Component extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
  }

  render() {
    if (Platform.OS === "android") {
      // if (Platform.Version <= 21) {
      return <View style={{ height: 22 }} />;
      // } else {
      // return null;
      // }
    } else {
      return null;
    }
  }
}

export default Component;
