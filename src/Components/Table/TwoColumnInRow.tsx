/**
 * @flow
 */

import React, { Component } from "react";
import { View } from "react-native";

import Text from "../Text";
import { horizontalScale } from "../../ScaleUtility";

class TwoColumnInRow extends React.PureComponent<{ value1: any, value2: any }, {}> {
  static defaultProps = {
    style: {}
  };
  render() {
    return (
      <View style={{ width: "100%", flexDirection: "row" }}>
        <Text
          style={{
            width: horizontalScale(100),
            paddingVertical: 10,
            textAlign: "center",
            fontSize: horizontalScale(16)
          }}
        >
          {this.props.value1}
        </Text>
        <View style={{ width: 1, height: "100%", backgroundColor: "gray" }} />
        <Text
          style={{
            flex: 1,
            paddingVertical: 10,
            paddingLeft: horizontalScale(10),
            textAlign: "center",
            fontSize: horizontalScale(16)
          }}
        >
          {this.props.value2}
        </Text>
      </View>
    );
  }
}

export default TwoColumnInRow;
