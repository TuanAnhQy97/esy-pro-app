/**
 * @flow
 */

import React, { Component } from "react";
import { View, TouchableOpacity } from "react-native";

import Text from "../Text";
import { horizontalScale } from "../../ScaleUtility";

class ThreeColumnInRow extends React.PureComponent<
  {
    value1: any,
    value2: any,
    value3: any,
    backgroundColor?: string,
    disabled?: boolean,
    onPress?: Function
  },
  {}
> {
  static defaultProps = {
    style: {}
  };
  render() {
    return (
      <TouchableOpacity
        disabled={this.props.disabled}
        onPress={this.props.onPress}
        style={{
          width: "100%",
          paddingHorizontal: horizontalScale(17),
          backgroundColor: this.props.backgroundColor
        }}
      >
        <View
          style={{ width: "100%", flexDirection: "row", alignItems: "center" }}
        >
          <Text
            style={{
              flex: 1.5,
              paddingVertical: 12,
              fontSize: horizontalScale(16)
            }}
          >
            {this.props.value1}
          </Text>
          <View style={{ width: 1, height: "100%", backgroundColor: "gray" }} />
          <Text
            style={{
              fontSize: horizontalScale(18),
              flex: 1.5,
              textAlign: "center",
              paddingVertical: 15
            }}
          >
            {this.props.value2}
          </Text>
          <View style={{ width: 1, height: "100%", backgroundColor: "gray" }} />
          <Text
            style={{
              flex: 1,
              textAlign: "right",
              paddingVertical: 15,
              fontSize: horizontalScale(16)
            }}
          >
            {this.props.value3}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default ThreeColumnInRow;
