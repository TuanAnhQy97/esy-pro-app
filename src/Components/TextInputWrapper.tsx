/**
 * @flow
 */
import React, { Component } from "react";
import { Animated, Easing, ViewStyle } from "react-native";

class TextInputWrapper extends React.PureComponent<
  { style?: ViewStyle, children: any },
  { paddingHorizontal: number }
> {
  shakeAnimationValue: any;
  shake: any;
  constructor(props: any) {
    super(props);
    this.shake = this.shake.bind(this);
    this.state = { paddingHorizontal: 0 };
  }

  componentWillMount() {
    this.shakeAnimationValue = new Animated.Value(0);
  }

  shake() {
    const { shakeAnimationValue } = this;
    shakeAnimationValue.setValue(0);
    this.setState({ paddingHorizontal: 15 }, () => {
      Animated.timing(shakeAnimationValue, {
        duration: 375,
        toValue: 3,
        ease: Easing.bounce
      }).start(() => {
        this.setState({ paddingHorizontal: 0 });
      });
    });
  }

  render() {
    const { style } = this.props;
    const translateX = this.shakeAnimationValue.interpolate({
      inputRange: [0, 0.5, 1, 1.5, 2, 2.5, 3],
      outputRange: [0, -15, 0, 15, 0, -15, 0]
    });
    return (
      <Animated.View
        style={[
          style,
          {
            paddingHorizontal: this.state.paddingHorizontal,
            transform: [{ translateX }]
          }
        ]}
      >
        {this.props.children}
      </Animated.View>
    );
  }
}

export default TextInputWrapper;
