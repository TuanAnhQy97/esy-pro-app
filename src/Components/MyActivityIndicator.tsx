/**
 * @flow
 */
import React, { Component } from "react";
import { View, ActivityIndicator } from "react-native";

import { colors } from "../styles/colors";

class MyActivityIndicator extends React.PureComponent<{}, {}> {
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: colors.primary_light
        }}
      >
        <ActivityIndicator size="large" />
      </View>
    );
  }
}

export default MyActivityIndicator;
