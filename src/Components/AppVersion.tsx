/**
 * @flow
 */
import React, { Component } from "react";
import Text from "./Text";

import DeviceInfo from "react-native-device-info";
import { horizontalScale } from "../ScaleUtility";
import { colors } from "../styles/colors";
import codepush from "react-native-code-push";
import ble from "../services/ble";

class AppVersion extends React.PureComponent<
  {},
  { isLoading: boolean, version: string, build: string, jsVer: string }
> {
  constructor() {
    super();
    this.state = { isLoading: true, version: "", build: "", jsVer: "" };
  }
  async componentDidMount() {
    // Get version and build
    let version = DeviceInfo.getVersion();
    let build = DeviceInfo.getBuildNumber();
    let jsVer = await codepush.getUpdateMetadata();
    if (jsVer) {
      jsVer = jsVer.label.replace("v", "c");
    } else {
      jsVer = "c0";
    }
    this.setState({
      isLoading: false,
      version,
      build,
      jsVer
    });
  }
  render() {
    const { isLoading, version, build, jsVer } = this.state;
    if (isLoading) {
      return null;
    } else {
      const deviceVersion = (ble.getCurrentVersion() || []).join(".");
      return (
        <Text
          style={{
            fontSize: horizontalScale(12),
            width: "100%",
            textAlign: "center",
            color: colors.third_dark
          }}
        >
          Version {version} ({build}-{jsVer}) [{deviceVersion}]
        </Text>
      );
    }
  }
}
export default AppVersion;
