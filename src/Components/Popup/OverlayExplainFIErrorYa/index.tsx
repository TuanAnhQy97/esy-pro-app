/**
 * @flow
 */
import React from "react";
import {
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
  View,
  ScrollView,
  StyleSheet
} from "react-native";
import { colors } from "../../../styles/colors";

import Text from "../../Text";
import { horizontalScale, verticalScale } from "../../../ScaleUtility";

import ExplainErrorYamaha from "../../../Library/ExplainErrorYamaha";
import { splitLineFromString } from "../../../SharedFunctions";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

class OverlayExplainFIErrorYa extends React.PureComponent<
  { errorCode: number; errorMem: number; errorType: "kline" | "obd" },
  { isShowing: boolean }
> {
  constructor(props: any) {
    super(props);
    this.state = {
      isShowing: false
    };
  }

  componentDidMount() {
    showed = false;
  }

  componentWillUnmount() {
    showed = false;
  }

  renderContent = () => {
    const { errorCode, errorMem, errorType } = this.props;
    let hexStr = errorCode.toString(16);
    if (hexStr.length === 1) {
      hexStr = `0${hexStr}`;
    }
    hexStr = hexStr.toUpperCase();

    const explain = ExplainErrorYamaha.getExplain({
      errorCode,
      errorMem,
      type: errorType
    });
    // Tên lỗi
    const errorName = `${explain.name} (${hexStr})`;
    // Nội dung hướng giải quyết
    const { reasons, effect, howToFix } = explain;
    return (
      <SafeAreaView
        style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
      >
        <ScrollView style={[{}]}>
          <View
            style={{
              backgroundColor: colors.primary_light,
              width: horizontalScale(330),
              borderBottomLeftRadius: 8,
              borderBottomRightRadius: 8,
              alignSelf: "center",
              paddingTop: verticalScale(20),
              paddingBottom: verticalScale(30),
              paddingHorizontal: horizontalScale(10)
            }}
          >
            {/* Tên lỗi */}
            <Text
              style={{
                fontSize: horizontalScale(20),
                color: colors.error
              }}
            >
              {errorName}
            </Text>
            <View
              style={{
                marginTop: verticalScale(5),
                height: 1,
                width: "100%",
                alignSelf: "center",
                backgroundColor: "gray"
              }}
            />

            {this.renderTitleAndContent("Nguyên nhân", reasons || "")}
            {effect
              ? this.renderTitleAndContent("Ảnh hưởng", effect || "")
              : null}
            {howToFix
              ? this.renderTitleAndContent("Gợi ý khắc phục", howToFix || "")
              : null}
          </View>

          {/* Close Button */}
          <TouchableOpacity
            hitSlop={hitSlop}
            onPress={() => {
              showPopup(false);
            }}
            style={{
              alignSelf: "center",
              justifyContent: "center",
              alignItems: "center",
              marginTop: 17,
              height: 44,
              width: 44,
              borderRadius: 44,
              backgroundColor: colors.primary_light
            }}
          >
            <ImageBackground
              style={{
                width: 14,
                height: 14
              }}
              imageStyle={{
                resizeMode: "contain",
                transform: [{ rotateZ: "-90deg" }]
              }}
              source={require("../../../../assets/Images/Share/next.png")}
            />
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    );
  };

  renderTitleAndContent = (title: string, content: string) => {
    if (!content) return null;

    return (
      <View>
        <Text
          style={{
            fontSize: horizontalScale(20),
            color: "gray",
            marginTop: verticalScale(10)
          }}
        >
          {title}
        </Text>
        <Text
          style={{
            fontSize: horizontalScale(16),
            marginBottom: verticalScale(10)
          }}
        >
          {content ? splitLineFromString(content) : ""}
        </Text>
      </View>
    );
  };

  show() {
    this.setState({ isShowing: true });
  }

  dismiss() {
    this.setState({ isShowing: false });
  }

  render() {
    if (this.state.isShowing) {
      return (
        <View
          style={[
            StyleSheet.absoluteFillObject,
            {
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }
          ]}
        >
          {/* Background color */}
          <View
            style={[
              StyleSheet.absoluteFillObject,
              { backgroundColor: colors.third_dark, opacity: 0.87 }
            ]}
          />
          {/* Content */}
          {this.renderContent()}
        </View>
      );
    }
    return null;
  }
}
export default OverlayExplainFIErrorYa;
/**
 * ================================================================================

 * ================================================================================
 */
let popupRef;
let showed = false;

/**
 * Set ref cho popup
 * @param {*} ref
 */
function setRef(ref: any) {
  popupRef = ref;
}
/**
 * Hiển thị popup
 * @param {*} on
 */
function showPopup(on?: boolean = false) {
  if (showed !== on) {
    showed = on;
    if (on) {
      popupRef.show();
    } else {
      popupRef.dismiss();
    }
  }
}
/**
 * Có đang hiển thị không?
 */
export function isShowing() {
  return showed;
}
export const tools = { setRef, showPopup, isShowing };
