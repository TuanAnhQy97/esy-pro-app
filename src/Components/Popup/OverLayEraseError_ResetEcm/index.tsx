/**
 * @flow
 */
import React from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import Lottie from "lottie-react-native";

import { colors } from "../../../styles/colors";
import DoneAnimate from "./DoneAnimate";

import Text from "../../Text";
import eventBus from "../../../services/eventBus";
import { EVENT_BUS } from "../../../constants";

type Props = {};
type States = {
  isShowing: boolean;
  job: "erase" | "reset";
  progress: "doing" | "error" | "done";
};

class OverLayResetEraseEcu extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
    this.state = {
      isShowing: false,
      job: "erase",
      progress: "doing"
    };
  }

  componentDidMount() {
    showed = false;
    eventBus.addListener(
      EVENT_BUS.BIKE_ERASE_RESET_PROGRESS_CHANGE,
      this._onProgressChange
    );
  }

  componentWillUnmount() {
    showed = false;
    eventBus.removeListener(this._onProgressChange);
  }

  _onProgressChange = (progress: any) => this.setState({ progress });

  render() {
    if (this.state.isShowing) {
      return (
        <View
          style={[
            StyleSheet.absoluteFillObject,
            {
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }
          ]}
        >
          {/* Background color */}
          <View
            style={[
              StyleSheet.absoluteFillObject,
              { backgroundColor: colors.primary_light, opacity: 1 }
            ]}
          />
          {/* Content */}
          {this.renderContent()}
        </View>
      );
    }
    return null;
  }

  renderContent() {
    const { job, progress } = this.state;
    if (progress === "doing") {
      let animateSource;
      let title;
      switch (job) {
        case "reset":
          animateSource = require("./gears.json");
          title = "Đang khôi phục dữ liệu\nVui lòng chờ";
          break;
        case "erase":
          animateSource = require("./clear.json");
          title = "Đang giải phóng bộ nhớ lỗi\nVui lòng chờ";
          break;
        default:
          animateSource = require("./gears.json");
          title = "";
          break;
      }
      return (
        <View style={{ alignItems: "center" }}>
          <Lottie
            source={animateSource}
            style={{ height: 200, width: 200 }}
            autoPlay
          />
          <Text style={{ textAlign: "center", fontSize: 16, marginTop: 20 }}>
            {title}
          </Text>
        </View>
      );
    }
    if (progress === "error") {
      return (
        <View style={{ alignItems: "center" }}>
          <Lottie
            source={require("./error_cross.json")}
            style={{ height: 200, width: 200 }}
            autoPlay
            loop={false}
          />
          <Text style={{ textAlign: "center", fontSize: 20, marginTop: 20 }}>
            KHÔNG THÀNH CÔNG
          </Text>
          <Text style={{ textAlign: "center", fontSize: 16, marginTop: 5 }}>
            Vui lòng thử lại sau
          </Text>
          <TouchableOpacity
            style={{
              marginTop: 30,
              backgroundColor: colors.secondary_dark,
              borderRadius: 8,
              padding: 15
            }}
            onPress={() => {
              showPopup(false);
            }}
          >
            <Text
              style={{
                textAlign: "center",
                fontSize: 16,
                color: colors.primary_light
              }}
            >
              Quay về
            </Text>
          </TouchableOpacity>
        </View>
      );
    }
    if (progress === "done") {
      return <DoneAnimate />;
    }
  }

  show(job: "erase" | "reset") {
    this.setState({ isShowing: true, job });
  }

  dismiss() {
    this.setState({ isShowing: false });
  }
}

export default OverLayResetEraseEcu;

/**
 * ================================================================================

 * ================================================================================
 */
let popupRef;
let showed = false;

/**
 * Set ref cho popup
 * @param {*} ref
 */
function setRef(ref: any) {
  if (ref) {
    popupRef = ref;
  }
}
/**
 * Hiển thị popup
 * @param {*} on
 * @param {*} _callbackConfirm
 * @param {*} _callbackCancel
 */
function showPopup(on?: boolean = false, job?: "erase" | "reset") {
  if (showed !== on) {
    showed = on;
    if (on) {
      popupRef.show(true, job);
    } else {
      popupRef.dismiss();
    }
  }
}
/**
 * Có đang hiển thị không?
 */
function isShowing() {
  return showed;
}

export const tools = { setRef, showPopup, isShowing };
