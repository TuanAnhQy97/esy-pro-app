/**
 * @flow
 */
import React from "react";
import { View, TouchableOpacity } from "react-native";
import Lottie from "lottie-react-native";

import Text from "../../Text";
import { tools as OverLayEraseError_ResetEcm_tools } from ".";
import { colors } from "../../../styles/colors";

type Props = {};
type States = {};

class Component extends React.PureComponent<Props, States> {
  animation: any;

  constructor(props: any) {
    super(props);
  }

  componentDidMount() {
    this.animation.play(45, 120);
  }

  render() {
    return (
      <View style={{ alignItems: "center" }}>
        <Lottie
          ref={animation => {
            this.animation = animation;
          }}
          source={require("./loading_and_done!.json")}
          style={{ height: 200, width: 200 }}
          // autoPlay={true}
          loop={false}
        />
        <Text style={{ textAlign: "center", fontSize: 20, marginTop: 20 }}>
          THÀNH CÔNG
        </Text>
        <Text style={{ textAlign: "center", fontSize: 16, marginTop: 5 }}>
          Vui lòng tắt khoá điện và bật lại sau 10 giây
        </Text>
        <TouchableOpacity
          style={{
            marginTop: 30,
            backgroundColor: colors.secondary_dark,
            borderRadius: 8,
            padding: 15
          }}
          onPress={() => {
            OverLayEraseError_ResetEcm_tools.showPopup();
          }}
        >
          <Text
            style={{
              textAlign: "center",
              fontSize: 16,
              color: colors.primary_light
            }}
          >
            Quay về
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Component;
