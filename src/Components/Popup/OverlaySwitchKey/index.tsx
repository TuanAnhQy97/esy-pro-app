/**
 * @flow
 */
import React from "react";
import { View, StyleSheet } from "react-native";
import { colors } from "../../../styles/colors";

import Text from "../../Text";
import { horizontalScale, verticalScale } from "../../../ScaleUtility";
import eventBus from "../../../services/eventBus";
import { EVENT_BUS } from "../../../constants";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

class OverlaySwitchKey extends React.PureComponent<{}, { isShowing: boolean }> {
  constructor(props: any) {
    super(props);
    this.state = { isShowing: false };
  }

  componentDidMount() {
    eventBus.addListener(EVENT_BUS.SWITCH_KEY_REQUEST, this.onSwitchKeyRequest);
    eventBus.addListener(EVENT_BUS.SWITCH_KEY_OKAY, this.onSwitchKeyOkay);
    eventBus.addListener(EVENT_BUS.SWITCH_KEY_FAILED, this.onSwitchKeyFailed);
  }

  componentWillUnmount() {
    eventBus.removeListener(this.onSwitchKeyRequest);
    eventBus.removeListener(this.onSwitchKeyOkay);
    eventBus.removeListener(this.onSwitchKeyFailed);
  }

  onSwitchKeyRequest = () => {
    this.setState({ isShowing: true });
  };

  onSwitchKeyOkay = () => {
    this.setState({ isShowing: false });
  };

  onSwitchKeyFailed = () => {
    this.setState({ isShowing: false });
  };

  renderContent = () => {
    return (
      <View style={styles.rootContainer}>
        <Text style={styles.title}>Vui lòng làm theo các bước sau:</Text>
        <Text style={styles.message}>1. Tắt khoá điện</Text>
        <Text style={styles.message}>2. Bật khoá điện</Text>
        <Text style={styles.message}>Lưu ý không nổ máy</Text>
      </View>
    );
  };

  show() {
    this.setState({ isShowing: true });
  }

  dismiss() {
    this.setState({ isShowing: false });
  }

  render() {
    if (this.state.isShowing) {
      return (
        <View
          style={[
            StyleSheet.absoluteFillObject,
            {
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }
          ]}
        >
          {/* Background color */}
          <View
            style={[
              StyleSheet.absoluteFillObject,
              { backgroundColor: colors.third_dark, opacity: 0.87 }
            ]}
          />
          {/* Content */}
          {this.renderContent()}
        </View>
      );
    }
    return null;
  }
}
export default OverlaySwitchKey;

const styles = StyleSheet.create({
  rootContainer: {
    width: "100%",
    backgroundColor: colors.primary_light,
    paddingVertical: 20,
    paddingHorizontal: 10,
    borderRadius: 8
  },
  title: {
    fontSize: horizontalScale(20),
    fontWeight: "bold",
    marginBottom: 10,
    paddingTop: 10
  },
  message: {
    fontSize: horizontalScale(16)
  },
  btnContainer: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "center"
  },
  btn: { padding: 10, flex: 1 },
  confirmText: {
    fontSize: horizontalScale(16),
    fontWeight: "bold",
    color: colors.secondary_dark,
    alignSelf: "flex-end"
  },
  cancelText: {
    fontSize: horizontalScale(16),
    color: colors.third_dark,
    alignSelf: "flex-end"
  }
});
