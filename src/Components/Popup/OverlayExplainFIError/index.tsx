/**
 * @flow
 */
import React, { Component } from "react";
import {
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
  View,
  ScrollView,
  StyleSheet
} from "react-native";
import { colors } from "../../../styles/colors";

import Text from "../../Text";
import {
  horizontalScale,
  verticalScale,
  screenHeight
} from "../../../ScaleUtility";

import ExplainError from "../../../Library/ExplainError";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

class Index extends React.PureComponent<
  { errorCode: number; errorState: number },
  { isShowing: boolean }
> {
  constructor(props: any) {
    super(props);
    this.state = {
      isShowing: false
    };
  }

  componentDidMount() {
    showed = false;
  }

  componentWillUnmount() {
    showed = false;
  }

  render() {
    if (this.state.isShowing) {
      return (
        <View
          style={[
            StyleSheet.absoluteFillObject,
            {
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }
          ]}
        >
          {/* Background color */}
          <View
            style={[
              StyleSheet.absoluteFillObject,
              { backgroundColor: colors.third_dark, opacity: 0.87 }
            ]}
          />
          {/* Content */}
          {this.renderContent()}
        </View>
      );
    }
    return null;
  }

  renderContent() {
    const { errorCode } = this.props;
    const { errorState } = this.props;

    const explain = ExplainError.getExplain(errorCode, errorState);
    // Tên lỗi
    const errorName = `${explain.name} (${errorCode}-${errorState})`;
    // Nội dung hướng giải quyết
    const baseGuide = explain.howToFix;
    return (
      <SafeAreaView
        style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
      >
        <ScrollView style={[{}]}>
          <View
            style={{
              backgroundColor: colors.primary_light,
              width: horizontalScale(330),
              borderBottomLeftRadius: 8,
              borderBottomRightRadius: 8,
              alignSelf: "center",
              paddingTop: verticalScale(20),
              paddingBottom: verticalScale(30),
              paddingHorizontal: horizontalScale(10)
            }}
          >
            {/* Tên lỗi */}
            <Text
              style={{
                fontSize: horizontalScale(20),
                color: colors.error
              }}
            >
              {errorName}
            </Text>
            <View
              style={{
                marginTop: verticalScale(5),
                height: 1,
                width: "100%",
                alignSelf: "center",
                backgroundColor: "gray",
                marginBottom: verticalScale(10)
              }}
            />
            {/* Giải thích + Cách xử lý */}

            {this.renderTitleAndContent("Nguyên nhân", explain.cause)}
            {this.renderTitleAndContent("Tác hại", explain.effect)}
            {this.renderTitleAndContent("Gợi ý kiểm tra", explain.howToFix)}
            {/* <Text
              style={{
                marginTop: verticalScale(10),
                fontSize: horizontalScale(18)
              }}
            >
              {baseGuide}
            </Text> */}
          </View>

          {/* Close Button */}
          <TouchableOpacity
            hitSlop={hitSlop}
            onPress={() => {
              showPopup(false);
            }}
            style={{
              alignSelf: "center",
              justifyContent: "center",
              alignItems: "center",
              marginTop: 17,
              height: 44,
              width: 44,
              borderRadius: 44,
              backgroundColor: colors.primary_light
            }}
          >
            <ImageBackground
              style={{
                width: 14,
                height: 14
              }}
              imageStyle={{
                resizeMode: "contain",
                transform: [{ rotateZ: "-90deg" }]
              }}
              source={require("../../../../assets/Images/Share/next.png")}
            />
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    );
  }

  renderTitleAndContent(title: string, content: string) {
    if (content === "") return null;

    return (
      <View>
        <Text
          style={{
            fontSize: horizontalScale(20),
            color: "gray"
          }}
        >
          {title}
        </Text>
        <Text
          style={{
            fontSize: horizontalScale(16),
            marginBottom: verticalScale(10)
          }}
        >
          {content}
        </Text>
      </View>
    );
  }

  show() {
    this.setState({ isShowing: true });
  }

  dismiss() {
    this.setState({ isShowing: false });
  }
}
export default Index;
/**
 * ================================================================================

 * ================================================================================
 */
let popupRef;
let showed = false;

/**
 * Set ref cho popup
 * @param {*} ref
 */
function setRef(ref: any) {
  popupRef = ref;
}
/**
 * Hiển thị popup
 * @param {*} on
 */
function showPopup(on?: boolean = false) {
  if (showed !== on) {
    showed = on;
    if (on) {
      popupRef.show();
    } else {
      popupRef.dismiss();
    }
  }
}
/**
 * Có đang hiển thị không?
 */
export function isShowing() {
  return showed;
}
export const tools = { setRef, showPopup, isShowing };
