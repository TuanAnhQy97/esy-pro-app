/**
 * @flow
 */
import React, { Component } from "react";
import {
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
  View,
  Animated,
  StyleSheet
} from "react-native";
import { connect } from "react-redux";
import { colors } from "../../../styles/colors";

import Text from "../../Text";
import {
  horizontalScale,
  verticalScale,
  screenHeight
} from "../../../ScaleUtility";
import { TYPE_STORE_DATA } from "../../../Store";

const hitSlop = { top: 15, bottom: 15, left: 15, right: 15 };

class Index extends React.PureComponent<
  { errorCode: number },
  { animation: any; isShowing: boolean }
> {
  constructor(props: any) {
    super(props);
    this.state = {
      animation: new Animated.Value(0),
      isShowing: false
    };
  }

  componentDidMount() {
    showed = false;
  }

  componentWillUnmount() {
    showed = false;
  }

  render() {
    if (this.state.isShowing) {
      return (
        <View
          style={[
            StyleSheet.absoluteFillObject,
            {
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }
          ]}
        >
          {/* Background color */}
          <View
            style={[
              StyleSheet.absoluteFillObject,
              { backgroundColor: colors.third_dark, opacity: 0.87 }
            ]}
          />
          {/* Content */}
          {this.renderContent()}
        </View>
      );
    }
    return null;
  }

  renderContent() {
    const { errorCode } = this.props;
    // Nội dung tên lỗi
    const errorName = getErrorName(errorCode);
    // Nội dung hướng giải quyết
    const baseGuide = getErrorGuide(errorCode);

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Animated.View
          style={[
            {
              transform: [
                {
                  translateY: this.state.animation.interpolate({
                    inputRange: [0, 1],
                    outputRange: [-screenHeight / 4, 0]
                  })
                }
              ]
            }
          ]}
        >
          <View
            style={{
              backgroundColor: colors.primary_light,
              width: horizontalScale(330),
              borderBottomLeftRadius: 8,
              borderBottomRightRadius: 8,
              alignSelf: "center",
              paddingTop: verticalScale(20),
              paddingBottom: verticalScale(30),
              paddingHorizontal: horizontalScale(10)
            }}
          >
            {/* Tên lỗi */}
            <Text
              style={{
                fontSize: horizontalScale(18),
                color: colors.error
              }}
            >
              {errorName}
            </Text>
            <View
              style={{
                marginTop: verticalScale(5),
                height: 1,
                width: "100%",
                alignSelf: "center",
                backgroundColor: "gray"
              }}
            />
            {/* Giải thích + Cách xử lý */}
            <Text
              style={{
                marginTop: verticalScale(10),
                fontSize: horizontalScale(18)
              }}
            >
              {baseGuide}
            </Text>
          </View>

          {/* Close Button */}
          <TouchableOpacity
            hitSlop={hitSlop}
            onPress={() => {
              showPopup(false);
            }}
            style={{
              alignSelf: "center",
              justifyContent: "center",
              alignItems: "center",
              marginTop: 17,
              height: 44,
              width: 44,
              borderRadius: 44,
              backgroundColor: colors.primary_light
            }}
          >
            <ImageBackground
              style={{
                width: 14,
                height: 14
              }}
              imageStyle={{
                resizeMode: "contain",
                transform: [{ rotateZ: "-90deg" }]
              }}
              source={require("../../../../assets/Images/Share/next.png")}
            />
          </TouchableOpacity>
        </Animated.View>
      </SafeAreaView>
    );
  }

  show() {
    this.setState({ isShowing: true }, () => {
      Animated.timing(this.state.animation, {
        duration: 250,
        toValue: 1
      }).start();
    });
  }

  dismiss() {
    Animated.timing(this.state.animation, {
      duration: 250,
      toValue: 0
    }).start(() => {
      this.setState({ isShowing: false });
    });
  }
}
function mapStateToProps(state: TYPE_STORE_DATA, ownProps) {
  return { errorCode: state.BleStateReducer.error };
}
export default connect(
  mapStateToProps,
  null,
  null,
  { withRef: true }
)(Index);

/**
 * ================================================================================

 * ================================================================================
 */
let popupRef;
let showed = false;

/**
 * Set ref cho popup
 * @param {*} ref
 */
function setRef(ref: any) {
  if (ref) {
    popupRef = ref;
  }
}
/**
 * Hiển thị popup
 * @param {*} on
 * @param {*} _callbackConfirm
 * @param {*} _callbackCancel
 */
function showPopup(on?: boolean = false) {
  if (showed !== on) {
    showed = on;
    if (on) {
      popupRef.show();
    } else {
      popupRef.dismiss();
    }
  }
}
/**
 * Có đang hiển thị không?
 */
function isShowing() {
  return showed;
}

export const tools = { setRef, showPopup, isShowing };

/**
 * ================================================================================

 * ================================================================================
 */
function getErrorName(errorCode: number) {
  let errorName = "";
  switch (errorCode) {
    // case ERROR.KLINE_ERROR:
    //   errorName = "Mất tín hiệu KLINE";
    //   break;
    // case ERROR.NOT_FOUND:
    //   errorName = "Không tìm thấy thiết bị";
    //   break;
    // case ERROR.BIKE_NOT_SUPPORT:
    //   errorName = "Dòng xe chưa hỗ trợ";
    //   break;
    // case ERROR.NO_BLE:
    //   errorName = "Không có kết nối Bluetooth";
    //   break;
    // case ERROR.NO_INTERNET:
    //   errorName = "Không có kết nối mạng";
    //   break;
    // case ERROR.NETWORK_ERROR:
    //   errorName = "Lỗi kết nối mạng";
    //   break;
    // case ERROR.ECU_NEED_RESTART:
    //   errorName = "Cần khởi động lại ECU";
    //   break;
    default:
      errorName = "Kết nối không thành công";
      break;
  }
  if (errorCode) {
    let hexForm; // fake
    if (errorCode >= 10) {
      hexForm = errorCode;
    } else {
      hexForm = `0${errorCode}`;
    }
    errorName += ` (0x${hexForm})`;
  }
  return errorName;
}

function getErrorGuide(errorCode: number) {
  let baseGuide = "Hướng dẫn xử lý:\n";
  switch (errorCode) {
    default:
      baseGuide = "Hệ thống sẽ tự động kết nối lại";
      // baseGuide =
      //   "Lỗi không xác định. Hãy thử lại với hướng dẫn sau:" +
      //   "\n" +
      //   " 1. Tắt khoá điện và bật lại\n" +
      //   " 2. Tắt Bluetooth và bật lại\n" +
      //   " 3. Khởi động lại ứng dụng\n";
      // "\n";
      break;
  }
  return baseGuide;
}
