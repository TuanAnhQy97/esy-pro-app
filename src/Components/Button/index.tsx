import React, { PureComponent } from "react";
import { TouchableOpacity, Text, View } from "react-native";

// Styles
import styles from "./styles";

class Button extends PureComponent {
  static defaultProps = {
    onPress: () => false,
    style: {},
    title: "Button",
    full: false,
    border: false,
    textStyle: null,
    background: "red",
    size: "md",
    icon: null,
    disabled: false,
    loading: false,
    disabledText: false,
    innerStyle: null
  };

  render() {
    const {
      onPress,
      style,
      title,
      full,
      border,
      background,
      size,
      icon,
      textStyle,
      disabled,
      loading,
      disabledText,
      innerStyle
    } = this.props;
    const width = full ? "100%" : "auto";
    const borderWidth = border ? 1 : 0;
    const backgroundColor = border ? "transparent" : background;
    const opacity = disabled ? 0.5 : 1;
    let height = 46;
    let fontSize = 16;
    switch (size) {
      case "sm":
        height = 40;
        fontSize = 14;
        break;
      case "lg":
        height = 52;
        fontSize = 18;
        break;
      case "xl":
        height = 62;
        fontSize = 18;
        break;
      default:
        break;
    }
    return (
      <TouchableOpacity
        onPress={onPress}
        style={[
          styles.container,
          {
            width,
            borderWidth,
            backgroundColor,
            height,
            opacity
          },
          style
        ]}
        disabled={disabled}
      >
        <View style={[styles.inner, innerStyle]}>
          {icon}
          {!disabledText && (
            <Text
              style={[styles.text, { fontSize }, textStyle]}
              numberOfLines={1}
              ellipsizeMode="tail"
            >
              {title}
            </Text>
          )}
        </View>
      </TouchableOpacity>
    );
  }
}

export default Button;
