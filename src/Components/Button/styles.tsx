import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    backgroundColor: "tomato",
    height: 50,
    paddingHorizontal: 20,
    borderRadius: 2,
    alignItems: "center",
    borderColor: "white",
    flexDirection: "row"
  },
  inner: {
    alignItems: "center",
    flexDirection: "row"
  },
  text: {
    color: "white",
    textAlign: "center",
    fontSize: 18,
    fontWeight: "bold",
    flex: 1
  }
});
