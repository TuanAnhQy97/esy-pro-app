import { StyleSheet } from "react-native";

export default StyleSheet.create({
  wrapPopup: {
    borderRadius: 6
  },
  wrapPopupContent: {
    paddingHorizontal: 17,
    paddingVertical: 25,
    paddingBottom: 15
  },
  popupTitle: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
    marginBottom: 15
  },
  popupSubtitle: {
    fontSize: 13,
    color: "gray",
    textAlign: "center",
    marginBottom: 15
  },
  popupBtn: {
    marginBottom: 10
  }
});
