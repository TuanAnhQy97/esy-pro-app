module.exports = {
  root: true,
  extends: ["airbnb", "plugin:prettier/recommended", "@react-native-community"],
  plugins: ["prettier"],
  parser: "babel-eslint",
  env: {
    jest: true
  },
  rules: {
    "linebreak-style": 0,
    "no-use-before-define": "off",
    "react/jsx-wrap-multilines": "off",
    "react/jsx-filename-extension": "off",
    "react/jsx-one-expression-per-line": "off",
    "comma-dangle": "off",
    "react/prefer-stateless-function": "off",
    "import/no-extraneous-dependencies": "off",
    "react/forbid-prop-types": "off",
    "global-require": "off",
    "import/no-self-import": "off",
    "no-empty": "off",
    "no-empty-function": "off",
    "no-underscore-dangle": "off",
    "react/destructuring-assignment": "off",
    "max-len": "off"
  },
  globals: {
    fetch: false,
    __DEV__: false
  }
};
